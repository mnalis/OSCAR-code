<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja_JP" sourcelanguage="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="35"/>
        <source>&amp;About</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;Aこのソフトウェアについて</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="49"/>
        <location filename="../oscar/aboutdialog.cpp" line="129"/>
        <source>Release Notes</source>
        <translation>リリースノート</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="63"/>
        <source>Credits</source>
        <translation>謝辞</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="77"/>
        <source>GPL License</source>
        <translation>GPLライセンス</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="238"/>
        <source>Close</source>
        <translation>閉じる</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="36"/>
        <source>Show data folder</source>
        <translation>データフォルダーを表示する</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="40"/>
        <source>About OSCAR %1</source>
        <translation>OSCAR について %1</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="88"/>
        <source>Sorry, could not locate About file.</source>
        <translation>「このソフトウェアについて」のファイルが見つかりません。</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="103"/>
        <source>Sorry, could not locate Credits file.</source>
        <translation>「謝辞」のファイルが見つかりません。</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="118"/>
        <source>Sorry, could not locate Release Notes.</source>
        <translation>「リリースノート」が見つかりません。</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="133"/>
        <source>Important:</source>
        <translation>重要：</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="134"/>
        <source>As this is a pre-release version, it is recommended that you &lt;b&gt;back up your data folder manually&lt;/b&gt; before proceeding, because attempting to roll back later may break things.</source>
        <translation>このバージョンはリリース前のバージョンであり、後ほど元のバージョンに戻す際にに何かが壊れる可能性があります。作業を進める前に&lt;b&gt;手動でご自分のデータフォルダのバックアップを取ることを&lt;/b&gt;お勧めします。</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="147"/>
        <source>To see if the license text is available in your language, see %1.</source>
        <translation>ライセンスに関する情報を確認するには、%1をご覧下さい。</translation>
    </message>
</context>
<context>
    <name>CMS50F37Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="890"/>
        <source>Could not find the oximeter file:</source>
        <translation>オキシメーターファイルが見つかりません:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="896"/>
        <source>Could not open the oximeter file:</source>
        <translation>オキシメーターファイルが開けません:</translation>
    </message>
</context>
<context>
    <name>CMS50Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="492"/>
        <source>Could not get data transmission from oximeter.</source>
        <translation>オキシメーターからデータを受信することができません。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="492"/>
        <source>Please ensure you select &apos;upload&apos; from the oximeter devices menu.</source>
        <translation>オキシメーターのメニューから「アップロード」を選んでください。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="560"/>
        <source>Could not find the oximeter file:</source>
        <translation>オキシメーターファイルが見つかりません:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="566"/>
        <source>Could not open the oximeter file:</source>
        <translation>オキシメーターファイルが開けません:</translation>
    </message>
</context>
<context>
    <name>CheckUpdates</name>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="250"/>
        <source>Checking for newer OSCAR versions</source>
        <translation>OSCAR の更新版を確認しています</translation>
    </message>
</context>
<context>
    <name>Daily</name>
    <message>
        <location filename="../oscar/daily.ui" line="506"/>
        <source>Go to the previous day</source>
        <translation>前の日に戻る</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="551"/>
        <source>Show or hide the calender</source>
        <translation>カレンダーの表示／非表示</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="614"/>
        <source>Go to the next day</source>
        <translation>次の日に移動</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="674"/>
        <source>Go to the most recent day with data records</source>
        <translation>データが存在する最も最近の日に移動</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="861"/>
        <source>Events</source>
        <translation>イベント</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="923"/>
        <source>View Size</source>
        <translation>表示サイズ</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="968"/>
        <location filename="../oscar/daily.ui" line="1391"/>
        <source>Notes</source>
        <translation>ノート</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1022"/>
        <source>Journal</source>
        <translation>日誌</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1050"/>
        <source> i </source>
        <translation> i </translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1062"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1074"/>
        <source>u</source>
        <translation>u</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1084"/>
        <source>Color</source>
        <translation>色</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1106"/>
        <location filename="../oscar/daily.ui" line="1116"/>
        <source>Small</source>
        <translation>小</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1121"/>
        <source>Medium</source>
        <translation>中</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1126"/>
        <source>Big</source>
        <translation>大</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1184"/>
        <source>Zombie</source>
        <translation>ゾンビ</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1196"/>
        <source>I&apos;m feeling ...</source>
        <translatorcomment>Because Japanese sentences end with verbs, the order of word won&apos;t be the same.  This may be problematic on UI.</translatorcomment>
        <translation>…と感じる</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1212"/>
        <source>Weight</source>
        <translation>体重</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1219"/>
        <source>If height is greater than zero in Preferences Dialog, setting weight here will show Body Mass Index (BMI) value</source>
        <translation>設定ダイアログで身長が 0 より大きいなら、ここに体重を設定することで、BMI (Body Mass Index) 値が表示されます</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1288"/>
        <source>Awesome</source>
        <translation>素晴らしい</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1326"/>
        <source>B.M.I.</source>
        <translatorcomment>It is usual in Japanese to write BMI without periods.</translatorcomment>
        <translation>BMI</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1342"/>
        <source>Bookmarks</source>
        <translation>ブックマーク</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1363"/>
        <source>Add Bookmark</source>
        <translation>ブックマークを追加</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1386"/>
        <source>Starts</source>
        <translation>開始</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1399"/>
        <source>Remove Bookmark</source>
        <translation>ブックマークを削除</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1407"/>
        <source>Search</source>
        <translation type="unfinished">検索</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1495"/>
        <source>Layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1502"/>
        <source>Save and Restore Graph Layout Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1515"/>
        <source>Show/hide available graphs.</source>
        <translation>グラフを表示する／隠す</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="298"/>
        <source>Breakdown</source>
        <translatorcomment>I could not think of a good word for graph breakdown, so I used &quot;detail&quot; instead</translatorcomment>
        <translation>詳細</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="298"/>
        <source>events</source>
        <translation>イベント</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="310"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="311"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="370"/>
        <source>Time at Pressure</source>
        <translation>加圧時間</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="673"/>
        <source>No %1 events are recorded this day</source>
        <translation>この日には %1 のイベントは記録されていません</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="791"/>
        <source>%1 event</source>
        <translation>%1 イベント</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="792"/>
        <source>%1 events</source>
        <translatorcomment>Nouns do not conjugate even if they are plural in Japanese.</translatorcomment>
        <translation>%1 イベント</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="835"/>
        <source>Session Start Times</source>
        <translation>セッション開始時間</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="836"/>
        <source>Session End Times</source>
        <translation>セッション終了時間</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1032"/>
        <source>Session Information</source>
        <translation>セッション情報</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1055"/>
        <source>Oximetry Sessions</source>
        <translation>オキシメトリーセッション</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1075"/>
        <source>Duration</source>
        <translation>長さ</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1143"/>
        <source>Device Settings</source>
        <translation>機器設定</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1312"/>
        <source>(Mode and Pressure settings missing; yesterday&apos;s shown.)</source>
        <translation>(モードと加圧設定がありません。昨日のものを表示しています）</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1727"/>
        <source>This CPAP device does NOT record detailed data</source>
        <translation>この CPAP 機は詳細なデータを記録しません</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1807"/>
        <source>no data :(</source>
        <translation>データがありません :(</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1808"/>
        <source>Sorry, this device only provides compliance data.</source>
        <translation>残念ながらこのデバイスはコンプライアンスのデータのみを提供しています。</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2423"/>
        <source>This bookmark is in a currently disabled area..</source>
        <translation>このブックマークは現在無効のエリアにあります。</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1052"/>
        <source>CPAP Sessions</source>
        <translation>CPAP セッション</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="190"/>
        <source>Details</source>
        <translation>詳細</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1058"/>
        <source>Sleep Stage Sessions</source>
        <translation>睡眠のステージセッション</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1061"/>
        <source>Position Sensor Sessions</source>
        <translation>位置センサーセッション</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1066"/>
        <source>Unknown Session</source>
        <translation>不明なセッション</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1303"/>
        <source>Model %1 - %2</source>
        <translation>モデル %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1308"/>
        <source>PAP Mode: %1</source>
        <translation>PAP モデル %1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1429"/>
        <source>This day just contains summary data, only limited information is available.</source>
        <translation>この日はサマリーのデータのみ含まれるため、限られた情報のみが存在します。</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1455"/>
        <source>Total ramp time</source>
        <translation>合計ランプ時間</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1459"/>
        <source>Time outside of ramp</source>
        <translation>ランプ外時間</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1500"/>
        <source>Start</source>
        <translation>開始</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1500"/>
        <source>End</source>
        <translation>終了</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1537"/>
        <source>Unable to display Pie Chart on this system</source>
        <translation>このシステムでは円グラフを表示できません</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1827"/>
        <source>&quot;Nothing&apos;s here!&quot;</source>
        <translation>「何もありません！」</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1830"/>
        <source>No data is available for this day.</source>
        <translation>この日のデータはありません。</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1276"/>
        <source>Oximeter Information</source>
        <translation>オキシメーターの情報</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1095"/>
        <source>Click to %1 this session.</source>
        <translatorcomment>%1 needs to be translated to form a complete Japanese sentence,</translatorcomment>
        <translation>このセッションを %1 するにはクリックしてください。</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="584"/>
        <source>Disable Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="585"/>
        <source>Disabling a session will remove this session data 
from all  graphs, reports and statistics.

The Search tab can find disabled sessions

Continue ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1095"/>
        <source>disable</source>
        <translation>無効にする</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1095"/>
        <source>enable</source>
        <translation>有効にする</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1111"/>
        <source>%1 Session #%2</source>
        <translation>%1 セッション #%2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1112"/>
        <source>%1h %2m %3s</source>
        <translation>%1 時 %2 分 %3 秒</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1146"/>
        <source>&lt;b&gt;Please Note:&lt;/b&gt; All settings shown below are based on assumptions that nothing has changed since previous days.</source>
        <translation>&lt;b&gt;ご注意: &lt;/b&gt;以下に表示されているすべての設定は前日から何も変更されていないという仮定に基づいています。</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1281"/>
        <source>SpO2 Desaturations</source>
        <translatorcomment>&quot;desaturate&quot; has different translation, but since it is about lowered of saturation, I used the word &quot;lower.&quot;</translatorcomment>
        <translation>SpO2 低下</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1282"/>
        <source>Pulse Change events</source>
        <translatorcomment>Fonts are in Chinese chracter, not Japanese fonts.  This is annoying in other tranlsations too.</translatorcomment>
        <translation>脈拍変化イベント</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1283"/>
        <source>SpO2 Baseline Used</source>
        <translation>SpO2 使用されている基準</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1349"/>
        <source>Statistics</source>
        <translation>統計</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1439"/>
        <source>Total time in apnea</source>
        <translation>無呼吸合計時間</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1449"/>
        <source>Time over leak redline</source>
        <translation>基準を超えたリーク時間</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1524"/>
        <source>Event Breakdown</source>
        <translation>イベント詳細</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1798"/>
        <source>Sessions all off!</source>
        <translation>セッションはすべてオフです！</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1800"/>
        <source>Sessions exist for this day but are switched off.</source>
        <translation>この日のセッションはありますが電源が切れていました。</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1803"/>
        <source>Impossibly short session</source>
        <translation>あり得ないくらい短いセッション</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1804"/>
        <source>Zero hours??</source>
        <translation>0時間??</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1809"/>
        <source>Complain to your Equipment Provider!</source>
        <translation>機器提供者に苦情を言いましょう！</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2129"/>
        <source>Pick a Colour</source>
        <translation>色を選んでください</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2466"/>
        <source>Bookmark at %1</source>
        <translation>%1 をブックマークします</translation>
    </message>
    <message>
        <location filename="../oscar/daily.h" line="148"/>
        <source>Hide All Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.h" line="149"/>
        <source>Show All Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.h" line="150"/>
        <source>Hide All Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.h" line="151"/>
        <source>Show All Graphs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DailySearchTab</name>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="180"/>
        <source>Match:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="192"/>
        <location filename="../oscar/dailySearchTab.cpp" line="985"/>
        <source>Select Match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="227"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="228"/>
        <location filename="../oscar/dailySearchTab.cpp" line="977"/>
        <location filename="../oscar/dailySearchTab.cpp" line="1087"/>
        <source>Start Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="250"/>
        <source>DATE
Jumps to Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="262"/>
        <source>Notes</source>
        <translation type="unfinished">ノート</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="263"/>
        <source>Notes containing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="264"/>
        <source>Bookmarks</source>
        <translation type="unfinished">ブックマーク</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="265"/>
        <source>Bookmarks containing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="266"/>
        <source>AHI </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="267"/>
        <source>Daily Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="268"/>
        <source>Session Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="269"/>
        <source>Days Skipped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="270"/>
        <source>Disabled Sessions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="271"/>
        <source>Number of Sessions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="318"/>
        <source>Click HERE to close Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="322"/>
        <source>Help</source>
        <translation type="unfinished">ヘルプ</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="526"/>
        <source>No Data
Jumps to Date&apos;s Details </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="531"/>
        <source>Number Disabled Session
Jumps to Date&apos;s Details </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="537"/>
        <location filename="../oscar/dailySearchTab.cpp" line="554"/>
        <source>Note
Jumps to Date&apos;s Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="542"/>
        <location filename="../oscar/dailySearchTab.cpp" line="547"/>
        <source>Jumps to Date&apos;s Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="561"/>
        <source>AHI
Jumps to Date&apos;s Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="567"/>
        <source>Session Duration
Jumps to Date&apos;s Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="574"/>
        <source>Number of Sessions
Jumps to Date&apos;s Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="581"/>
        <source>Daily Duration
Jumps to Date&apos;s Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="589"/>
        <source>Number of events
Jumps to Date&apos;s Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="599"/>
        <source>Automatic start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="819"/>
        <source>More to Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="822"/>
        <source>Continue Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="825"/>
        <source>End of Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="830"/>
        <source>No Matches</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1035"/>
        <source> Skip:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1036"/>
        <source>%1/%2%3 days.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1039"/>
        <source>Found %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1127"/>
        <source>Finds days that match specified criteria.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1129"/>
        <source>  Searches from last day to first day.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1132"/>
        <source>First click on Match Button then select topic.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1134"/>
        <source>  Then click on the operation to modify it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1136"/>
        <source>  or update the value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1138"/>
        <source>Topics without operations will automatically start.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1141"/>
        <source>Compare Operations: numberic or character. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1143"/>
        <source>  Numberic  Operations: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1146"/>
        <source>  Character Operations: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1150"/>
        <source>Summary Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1152"/>
        <source>  Left:Summary - Number of Day searched</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1154"/>
        <source>  Center:Number of Items Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1156"/>
        <source>  Right:Minimum/Maximum for item searched</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1158"/>
        <source>Result Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1160"/>
        <source>  Column One: Date of match. Click selects date.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1162"/>
        <source>  Column two: Information. Click selects date.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1164"/>
        <source>    Then Jumps the appropiate tab.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1167"/>
        <source>Wildcard Pattern Matching:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1170"/>
        <source>  Wildcards use 3 characters:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1172"/>
        <source>  Asterisk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1175"/>
        <source> Question Mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1178"/>
        <source> Backslash.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1181"/>
        <source>  Asterisk matches any number of characters.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1183"/>
        <source>  Question Mark matches a single character.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1185"/>
        <source>  Backslash matches next character.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DateErrorDisplay</name>
    <message>
        <location filename="../oscar/overview.cpp" line="814"/>
        <source>ERROR
The start date MUST be before the end date</source>
        <translation>エラー
開始日は終了日の前でなくてはなりません</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="817"/>
        <source>The entered start date %1 is after the end date %2</source>
        <translation>入力された開始日付 %1 は終了日付 %2 よりも後です</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="818"/>
        <source>
Hint: Change the end date first</source>
        <translation>ヒント: 終了日を先に変更してください</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="820"/>
        <source>The entered end date %1 </source>
        <translation>入力された終了日付は %1 です </translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="821"/>
        <source>is before the start date %1</source>
        <translation>は開始日 %1 より前です</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="822"/>
        <source>
Hint: Change the start date first</source>
        <translation>ヒント: 開始日を先に変更してください</translation>
    </message>
</context>
<context>
    <name>ExportCSV</name>
    <message>
        <location filename="../oscar/exportcsv.ui" line="14"/>
        <source>Export as CSV</source>
        <translation>CSV形式でエクスポートする</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="24"/>
        <source>Dates:</source>
        <translation>日付：</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="37"/>
        <source>Resolution:</source>
        <translation>解決策：</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="46"/>
        <source>Details</source>
        <translation>詳細</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="53"/>
        <source>Sessions</source>
        <translation>セッション</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="60"/>
        <source>Daily</source>
        <translation>日次</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="85"/>
        <source>Filename:</source>
        <translation>ファイル名：</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="107"/>
        <source>Cancel</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="114"/>
        <source>Export</source>
        <translation>エクスポート</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="131"/>
        <source>Start:</source>
        <translation>開始：</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="154"/>
        <source>End:</source>
        <translation>終了：</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="192"/>
        <source>Quick Range:</source>
        <translatorcomment>Not sure where this is used in the software.  Without a context, this is hard to translate.</translatorcomment>
        <translation>クイックレンジ</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="200"/>
        <location filename="../oscar/exportcsv.cpp" line="61"/>
        <location filename="../oscar/exportcsv.cpp" line="123"/>
        <source>Most Recent Day</source>
        <translation>最も最近の日</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="205"/>
        <location filename="../oscar/exportcsv.cpp" line="126"/>
        <source>Last Week</source>
        <translation>先週</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="210"/>
        <location filename="../oscar/exportcsv.cpp" line="129"/>
        <source>Last Fortnight</source>
        <translatorcomment>We don&apos;t have word &quot;fortnight&quot; so I needed to translate it as &quot;last two weeks.&quot;</translatorcomment>
        <translation>過去2週間</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="215"/>
        <location filename="../oscar/exportcsv.cpp" line="132"/>
        <source>Last Month</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="220"/>
        <location filename="../oscar/exportcsv.cpp" line="135"/>
        <source>Last 6 Months</source>
        <translation>過去6ヶ月</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="225"/>
        <location filename="../oscar/exportcsv.cpp" line="138"/>
        <source>Last Year</source>
        <translation>昨年</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="230"/>
        <location filename="../oscar/exportcsv.cpp" line="120"/>
        <source>Everything</source>
        <translation>すべて</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="235"/>
        <location filename="../oscar/exportcsv.cpp" line="109"/>
        <source>Custom</source>
        <translation>カスタム</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="77"/>
        <source>Details_</source>
        <translatorcomment>If this is used as a part of file name, you would like to make sure your code is UTF-8 free.</translatorcomment>
        <translation>詳細_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="79"/>
        <source>Sessions_</source>
        <translatorcomment>If this is used as a part of file name, you would like to make sure your code is UTF-8 free.</translatorcomment>
        <translation>セッション_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="81"/>
        <source>Summary_</source>
        <translatorcomment>If this is used as a part of file name, you would like to make sure your code is UTF-8 free.</translatorcomment>
        <translation>サマリー_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="88"/>
        <source>Select file to export to</source>
        <translatorcomment>The dialog allows the user to either choose file to expoert to or write the name of the file, therefore, the original text may not be valid.  This is a cosmetic issue since it is a title for a CSV export dialog, but if you are concerned, I can change the translation.</translatorcomment>
        <translation>エクスポート先のファイルを選んでください</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="89"/>
        <source>CSV Files (*.csv)</source>
        <translation>CSV ファイル (*.csv)</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>DateTime</source>
        <translation>日時</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Session</source>
        <translation>セッション</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Event</source>
        <translation>イベント</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Data/Duration</source>
        <translatorcomment>I may have to look at the context for &quot;duration&quot; to translate it precisely.</translatorcomment>
        <translation>データ／長さ</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Date</source>
        <translation>日付</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <source>Session Count</source>
        <translation>セッション数</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Start</source>
        <translation>開始</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>End</source>
        <translation>終了</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>Total Time</source>
        <translation>合計時間</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>AHI</source>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="214"/>
        <source> Count</source>
        <translation> 回数</translation>
    </message>
</context>
<context>
    <name>FPIconLoader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="242"/>
        <source>Import Error</source>
        <translation>インポートエラー</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="243"/>
        <source>This device Record cannot be imported in this profile.</source>
        <translation>このデバイスのレコードはこのプロフィールにインポートできません。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="243"/>
        <source>The Day records overlap with already existing content.</source>
        <translation>日次レコードが既に存在するデータと重なります。</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../oscar/help.ui" line="91"/>
        <source>Hide this message</source>
        <translation>このメッセージを隠す</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="196"/>
        <source>Search Topic:</source>
        <translation>トピックで検索する:</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="59"/>
        <source>Help Files are not yet available for %1 and will display in %2.</source>
        <translation>%1 のヘルプファイルがないので、%2 を表示します。</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="69"/>
        <source>Help files do not appear to be present.</source>
        <translation>ヘルプファイルが見当たりません。</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="85"/>
        <source>HelpEngine did not set up correctly</source>
        <translation>ヘルプエンジンが正しく設定されていません</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="100"/>
        <source>HelpEngine could not register documentation correctly.</source>
        <translation>ヘルプエンジンはドキュメントを正しく登録できません。</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="111"/>
        <source>Contents</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="112"/>
        <source>Index</source>
        <translation>インデックス</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="116"/>
        <source>Search</source>
        <translation>検索</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="126"/>
        <source>No documentation available</source>
        <translation>ドキュメントがありません</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="214"/>
        <source>Please wait a bit.. Indexing still in progress</source>
        <translation>しばらくお待ちください。インデックス作成中です</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="240"/>
        <source>No</source>
        <translation>いいえ</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="241"/>
        <source>%1 result(s) for &quot;%2&quot;</source>
        <translation>「%2」について %1 件あります</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="242"/>
        <source>clear</source>
        <translation>クリア</translation>
    </message>
</context>
<context>
    <name>MD300W1Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="166"/>
        <source>Could not find the oximeter file:</source>
        <translation>オキシメーターのファイルが見つかりません：</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="172"/>
        <source>Could not open the oximeter file:</source>
        <translation>オキシメーターのファイルが開けられません：</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../oscar/mainwindow.ui" line="507"/>
        <source>&amp;Statistics</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;S統計</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="561"/>
        <source>Report Mode</source>
        <translation>レポートモード</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="568"/>
        <source>Standard</source>
        <translation>標準</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="578"/>
        <source>Monthly</source>
        <translation>月次</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="585"/>
        <source>Date Range</source>
        <translation>日付の範囲</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="956"/>
        <source>Statistics</source>
        <translation>統計</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1006"/>
        <source>Daily</source>
        <translation>日次</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1050"/>
        <source>Overview</source>
        <translation>概要</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1094"/>
        <source>Oximetry</source>
        <translation>オキシメーター</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1144"/>
        <source>Import</source>
        <translation>インポート</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1194"/>
        <source>Help</source>
        <translation>ヘルプ</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2345"/>
        <source>&amp;File</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;Fファイル</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2370"/>
        <source>&amp;View</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;V表示</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2374"/>
        <source>&amp;Reset Graphs</source>
        <translation>&amp;Rグラフをリセット</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2399"/>
        <source>&amp;Help</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;Hヘルプ</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2403"/>
        <source>Troubleshooting</source>
        <translation>トラブルシューティング</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2429"/>
        <source>&amp;Data</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;Dデータ</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2433"/>
        <source>&amp;Advanced</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;A高度な操作</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2465"/>
        <source>Rebuild CPAP Data</source>
        <translation>CPAPのデータを再構築する</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2488"/>
        <source>&amp;Import CPAP Card Data</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;ICPAP のカードからインポートする</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2514"/>
        <source>Show Daily view</source>
        <translation>日次情報を表示</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2525"/>
        <source>Show Overview view</source>
        <translation>概要を表示</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2571"/>
        <source>&amp;Maximize Toggle</source>
        <translation>&amp;M最大化画面の切り替え</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2574"/>
        <source>Maximize window</source>
        <translation>ウインドウの最大化</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2593"/>
        <source>Reset Graph &amp;Heights</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>グラフの&amp;H高さを元に戻す</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2596"/>
        <source>Reset sizes of graphs</source>
        <translation>グラフのサイズを元に戻す</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2656"/>
        <source>Show Right Sidebar</source>
        <translation>右のスライドバーを表示</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2670"/>
        <source>Show Statistics view</source>
        <translation>統計情報を表示</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2683"/>
        <source>Import &amp;Dreem Data</source>
        <translation>&amp;Dreemのデータをインポート</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2726"/>
        <source>Show &amp;Line Cursor</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;Lラインカーソルを表示</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2748"/>
        <source>Show Daily Left Sidebar</source>
        <translation>日次の左のスライドバーを表示</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2765"/>
        <source>Show Daily Calendar</source>
        <translation>日毎のカレンダーを表示</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2786"/>
        <source>Create zip of CPAP data card</source>
        <translation>CPAP データカードの ZIP ファイルを作る</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2791"/>
        <source>Create zip of OSCAR diagnostic logs</source>
        <translation>OSCAR 分析ログの ZIP ファイルを作る</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2796"/>
        <source>Create zip of all OSCAR data</source>
        <translation>OSCAR のすべてのデータの ZIP ファイルを作る</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2811"/>
        <source>Report an Issue</source>
        <translation>問題を報告する</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2816"/>
        <source>System Information</source>
        <translation>システム情報</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2827"/>
        <source>Show &amp;Pie Chart</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;P円グラフを表示する</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2830"/>
        <source>Show Pie Chart on Daily page</source>
        <translation>日次のページに円グラフを表示する</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2838"/>
        <source>Standard - CPAP, APAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2841"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Standard graph order, good for CPAP, APAP,  Basic BPAP&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2846"/>
        <source>Advanced - BPAP, ASV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2849"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Advanced graph order, good for BPAP w/BU, ASV, AVAPS, IVAPS&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2860"/>
        <source>Show Personal Data</source>
        <translation>個人情報を表示</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2868"/>
        <source>Check For &amp;Updates</source>
        <translation>&amp;Uアップデートがあるか確認する</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2876"/>
        <source>Purge Current Selected Day</source>
        <translatorcomment>I am unsure what &quot;current selected day&quot; means.  Is it &quot;currently selected day?&quot;</translatorcomment>
        <translation>選んだ日の現在の情報をパージする</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2881"/>
        <source>&amp;CPAP</source>
        <translation>&amp;CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2886"/>
        <source>&amp;Oximetry</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;Oオキシメーター</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2891"/>
        <source>&amp;Sleep Stage</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;S睡眠の段階</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2896"/>
        <source>&amp;Position</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;P位置</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2901"/>
        <source>&amp;All except Notes</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;Aノートを除くすべて</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2906"/>
        <source>All including &amp;Notes</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;Nノートを含むすべて</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2496"/>
        <source>&amp;Preferences</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;Pプリファレンス</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2501"/>
        <source>&amp;Profiles</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;Pプロフィール</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2563"/>
        <source>&amp;About OSCAR</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;A OSCARについて</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2781"/>
        <source>Show Performance Information</source>
        <translation>性能の情報を表示</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2801"/>
        <source>CSV Export Wizard</source>
        <translation>CSVエクスポートウィザード</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2806"/>
        <source>Export for Review</source>
        <translation>レビュー用にエクスポート</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="119"/>
        <source>E&amp;xit</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;x終了</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2506"/>
        <source>Exit</source>
        <translation>終了</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2511"/>
        <source>View &amp;Daily</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;D日次を表示</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2522"/>
        <source>View &amp;Overview</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;O概要を表示</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2533"/>
        <source>View &amp;Welcome</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;Wようこそを表示する</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2558"/>
        <source>Use &amp;AntiAliasing</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;Aアンチエイリアシングを使う</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2585"/>
        <source>Show Debug Pane</source>
        <translation>デバッグ情報を表示する</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2601"/>
        <source>Take &amp;Screenshot</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;Sスクリーンショットをとる</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2609"/>
        <source>O&amp;ximetry Wizard</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;xオキシメーターウィザード</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2617"/>
        <source>Print &amp;Report</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;Rレポートを印刷</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2622"/>
        <source>&amp;Edit Profile</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;Eプロフィールを編集</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2713"/>
        <source>Import &amp;Viatom/Wellue Data</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;Viatom/Wellue のデータをインポート</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2762"/>
        <source>Daily Calendar</source>
        <translation>日次カレンダー</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2773"/>
        <source>Backup &amp;Journal</source>
        <translation>&amp;ジャーナルをバックアップする</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2627"/>
        <source>Online Users &amp;Guide</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>オンラインユーザー&amp;ガイド</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2632"/>
        <source>&amp;Frequently Asked Questions</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;Fよくある質問</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2637"/>
        <source>&amp;Automatic Oximetry Cleanup</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>オキシメーターを&amp;A自動でクリーンアップする</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2642"/>
        <source>Change &amp;User</source>
        <translation>&amp;ユーザーを変更する</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2448"/>
        <source>Purge &amp;Current Selected Day</source>
        <translatorcomment>I am not sure what is the context of &quot;current selected day.&quot;  It this &quot;currenlty selected day?&quot;  Also, if &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;C現在選択されている日をパージする</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2653"/>
        <source>Right &amp;Sidebar</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>右&amp;Sサイドバー</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2745"/>
        <source>Daily Sidebar</source>
        <translation>日次サイドバー</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2664"/>
        <source>View S&amp;tatistics</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;t統計を表示</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="885"/>
        <source>Navigation</source>
        <translation>ナビゲーション</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1316"/>
        <source>Bookmarks</source>
        <translation>ブックマーク</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2285"/>
        <source>Records</source>
        <translation>レコード</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2349"/>
        <source>Exp&amp;ort Data</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;データをエクスポート</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="912"/>
        <source>Profiles</source>
        <translation>プロフィール</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2437"/>
        <source>Purge Oximetry Data</source>
        <translation>オキシメーターのデータをパージ</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2443"/>
        <source>Purge ALL Device Data</source>
        <translation>全デバイスのデータをパージ</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2667"/>
        <source>View Statistics</source>
        <translation>統計情報を表示</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2678"/>
        <source>Import &amp;ZEO Data</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;ZEO データをインポート</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2688"/>
        <source>Import RemStar &amp;MSeries Data</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>RemStar &amp;M シリーズのデータをインポート</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2693"/>
        <source>Sleep Disorder Terms &amp;Glossary</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>睡眠障害の語句&amp;一覧</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2698"/>
        <source>Change &amp;Language</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;言語を変更する</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2703"/>
        <source>Change &amp;Data Folder</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;データフォルダを変更する</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2708"/>
        <source>Import &amp;Somnopose Data</source>
        <translatorcomment>If &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;Somnopose のデータをインポートする</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2718"/>
        <source>Current Days</source>
        <translation>最近</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="560"/>
        <location filename="../oscar/mainwindow.cpp" line="2227"/>
        <source>Welcome</source>
        <translation>ようこそ</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="116"/>
        <source>&amp;About</source>
        <translatorcomment>Since &quot;About&quot; does not make sense without a noun in Japanese, I translate it as &quot;About this software.&quot;  Also, if &amp;&lt;alphabet&gt; means a shortcut for the menu, I may have to revisit and assign them because Japanese characters are not on the keyboard.</translatorcomment>
        <translation>&amp;このソフトウェアについて</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="830"/>
        <location filename="../oscar/mainwindow.cpp" line="1903"/>
        <source>Please wait, importing from backup folder(s)...</source>
        <translation>しばらくお待ちください。バックアップフォルダからインポートしてます…</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="766"/>
        <source>Import Problem</source>
        <translation>インポートの問題</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="766"/>
        <source>Couldn&apos;t find any valid Device Data at

%1</source>
        <translation>%1 に有効なデータがありませんでした。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="931"/>
        <source>Please insert your CPAP data card...</source>
        <translation>CPAP のデータカードを挿入してください。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1021"/>
        <source>Access to Import has been blocked while recalculations are in progress.</source>
        <translation>再計算中のため、インポートがストップされました。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1073"/>
        <source>CPAP Data Located</source>
        <translation>CPAP データが見つかりました</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1108"/>
        <source>Import Reminder</source>
        <translation>インポートのリマインダー</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1123"/>
        <source>Find your CPAP data card</source>
        <translation>CPAP のデータカードを見つけてください。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1189"/>
        <source>Importing Data</source>
        <translation>データをインポート中です</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1491"/>
        <source>Choose where to save screenshot</source>
        <translation>スクリーンショットを保管する場所を選んでください</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1491"/>
        <source>Image files (*.png)</source>
        <translation>イメージファイル (*.png)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1565"/>
        <source>The User&apos;s Guide will open in your default browser</source>
        <translation>デフォルトのブラウザでユーザーガイドが開きます</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1573"/>
        <source>The FAQ is not yet implemented</source>
        <translation>FAQはまだ実装されていません。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1715"/>
        <location filename="../oscar/mainwindow.cpp" line="1742"/>
        <source>If you can read this, the restart command didn&apos;t work. You will have to do it yourself manually.</source>
        <translation>このメッセージを読んでいるようであれば、再起動コマンドはうまくいきませんでした。手動で再起動してください。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2073"/>
        <source>No help is available.</source>
        <translation>ヘルプがありません。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2589"/>
        <source>You must select and open the profile you wish to modify</source>
        <translation>変更したいプロフィールを選んで開いてください</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2602"/>
        <source>%1&apos;s Journal</source>
        <translation>%1のジャーナル</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2604"/>
        <source>Choose where to save journal</source>
        <translation>ジャーナルの保存先を選んでください</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2604"/>
        <source>XML Files (*.xml)</source>
        <translation>XMLファイル (*.xml)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2624"/>
        <source>Export review is not yet implemented</source>
        <translation>レビューのエクスポートはまだ実装されていません</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2634"/>
        <source>Would you like to zip this card?</source>
        <translation>このカードをZIP圧縮しますか？</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2656"/>
        <location filename="../oscar/mainwindow.cpp" line="2727"/>
        <location filename="../oscar/mainwindow.cpp" line="2778"/>
        <source>Choose where to save zip</source>
        <translation>ZIPファイルの保存先を選んでください</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2656"/>
        <location filename="../oscar/mainwindow.cpp" line="2727"/>
        <location filename="../oscar/mainwindow.cpp" line="2778"/>
        <source>ZIP files (*.zip)</source>
        <translation>ZIPファイル(*.zip)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2703"/>
        <location filename="../oscar/mainwindow.cpp" line="2741"/>
        <location filename="../oscar/mainwindow.cpp" line="2812"/>
        <source>Creating zip...</source>
        <translation>ZIPファイルを作成しています…</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2688"/>
        <location filename="../oscar/mainwindow.cpp" line="2796"/>
        <source>Calculating size...</source>
        <translation>サイズを計算しています…</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2834"/>
        <source>Reporting issues is not yet implemented</source>
        <translation>問題の報告はまだ実装されていません</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1168"/>
        <location filename="../oscar/mainwindow.cpp" line="2843"/>
        <source>OSCAR Information</source>
        <translation>OSCAR情報</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="288"/>
        <source>Help Browser</source>
        <translation>ヘルプブラウザ</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="581"/>
        <source>%1 (Profile: %2)</source>
        <translation>%1 (プロフィール %2)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1107"/>
        <source>Please remember to select the root folder or drive letter of your data card, and not a folder inside it.</source>
        <translation>データカードの下位のフォルダではなく、最上位のフォルダあるいはドライブレターを選択してください。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1168"/>
        <source>No supported data was found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1380"/>
        <source>Please open a profile first.</source>
        <translation>プロフィールを先に開いてください。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1442"/>
        <source>Check for updates not implemented</source>
        <translation>更新の確認は実装されていません</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1878"/>
        <source>Are you sure you want to rebuild all CPAP data for the following device:

</source>
        <translatorcomment>There is no white space before or adter translation, but Linguist detects them.</translatorcomment>
        <translation>本当に以下のデバイス上のCPAPのデータをすべて再構築しますか:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1888"/>
        <source>For some reason, OSCAR does not have any backups for the following device:</source>
        <translation>原因不明ですが、OSCARには以下のデバイのバックアップが存在しません:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1890"/>
        <source>Provided you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your CPAP data&lt;/i&gt;, you can still complete this operation, but you will have to restore from your backups manually.</source>
        <translation>&lt;i&gt;ご自分&lt;b&gt;自身の&lt;/b&gt;CPAPデータのすべてのバックアップ&lt;/&gt;を取ってあるのであれば、この操作を完了できますが、バックアップから手作業でリストアする必要があります。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1891"/>
        <source>Are you really sure you want to do this?</source>
        <translation>本当にこの操作を実施しても良いですか？</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1906"/>
        <source>Because there are no internal backups to rebuild from, you will have to restore from your own.</source>
        <translation>再構築のための内部バックアップがないため、ご自分のバックアップからリストアしていただく必要があります。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1956"/>
        <source>Note as a precaution, the backup folder will be left in place.</source>
        <translation>注意事項ですが、バックアップフォルダは現在の場所に残ります。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1958"/>
        <source>OSCAR does not have any backups for this device!</source>
        <translation>OSCARはこのデバイスにはバックアップがありません！</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1959"/>
        <source>Unless you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your data for this device&lt;/i&gt;, &lt;font size=+2&gt;you will lose this device&apos;s data &lt;b&gt;permanently&lt;/b&gt;!&lt;/font&gt;</source>
        <translation>&lt;i&gt;ご自分&lt;b&gt;自身の&lt;/b&gt;CPAPデータのすべてのバックアップ&lt;/&gt;を取ったのでない限り、&lt;font size=+2&gt;このデバイス上のデータを&lt;b&gt;永遠に&lt;/b&gt;失うことになります！&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1965"/>
        <source>You are about to &lt;font size=+2&gt;obliterate&lt;/font&gt; OSCAR&apos;s device database for the following device:&lt;/p&gt;</source>
        <translation>OSCARのデバイスデータベースから、以下のデバイスを&lt;font size=+2&gt;消し去ろう&lt;/font&gt; としています&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1968"/>
        <source>Are you &lt;b&gt;absolutely sure&lt;/b&gt; you want to proceed?</source>
        <translation>&lt;b&gt;本当に&lt;/b&gt;この操作を実施ししても良いですか？</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2023"/>
        <source>A file permission error caused the purge process to fail; you will have to delete the following folder manually:</source>
        <translation>ファイルへのアクセス権の問題で、パージの操作が失敗したため、以下のフォルダーを手作業で削除していただく必要があります:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2286"/>
        <source>The Glossary will open in your default browser</source>
        <translation>でフォルのとブラウザで用語集が開きます</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2374"/>
        <location filename="../oscar/mainwindow.cpp" line="2378"/>
        <source>There was a problem opening %1 Data File: %2</source>
        <translation>データファイル %2 で %1 を開く際に問題が発生しました</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2377"/>
        <source>%1 Data Import of %2 file(s) complete</source>
        <translation>%1 データインポート %2 個のファイルのインポート完了</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2379"/>
        <source>%1 Import Partial Success</source>
        <translation>%1 インポートが部分的に成功</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2381"/>
        <source>%1 Data Import complete</source>
        <translation>%1 データインポート完了</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2510"/>
        <source>Are you sure you want to delete oximetry data for %1</source>
        <translation>%1のオキシメーターのデータを消去しますか</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2512"/>
        <source>&lt;b&gt;Please be aware you can not undo this operation!&lt;/b&gt;</source>
        <translation>&lt;b&gt;この操作を取り消すことはできません!&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2542"/>
        <source>Select the day with valid oximetry data in daily view first.</source>
        <translation>日次ビューから正しいオキシメーターのデータを選択してください。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="542"/>
        <source>Loading profile &quot;%1&quot;</source>
        <translation>プロフィール %1 を読み込んでいます</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="762"/>
        <source>Imported %1 CPAP session(s) from

%2</source>
        <translation>%2 から %1 の CPAP セッションをインポートしました</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="762"/>
        <source>Import Success</source>
        <translation>インポート成功</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="764"/>
        <source>Already up to date with CPAP data at

%1</source>
        <translation>CPAPデータは %1 の最新のものです</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="764"/>
        <source>Up to date</source>
        <translation>最新</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="938"/>
        <source>Choose a folder</source>
        <translation>フォルダーを選んでください</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1017"/>
        <source>No profile has been selected for Import.</source>
        <translation>インポートをするためのプロフィールが選ばれていません。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1025"/>
        <source>Import is already running in the background.</source>
        <translation>バックグラウンドでインポートを既に実行しています。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1066"/>
        <source>A %1 file structure for a %2 was located at:</source>
        <translation>%2 用の %1 ファイルが以下にあります:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1068"/>
        <source>A %1 file structure was located at:</source>
        <translation>%1 ファイルが以下にあります:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1033"/>
        <source>Would you like to import from this location?</source>
        <translation>この場所からインポートしますか？</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1077"/>
        <source>Specify</source>
        <translation>指定してください</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1385"/>
        <source>Access to Preferences has been blocked until recalculation completes.</source>
        <translation>再計算が完了するまで、設定を開くことはできません。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1501"/>
        <source>There was an error saving screenshot to file &quot;%1&quot;</source>
        <translation>スクリーンショットを%1 に保存する際エラーが発生しました</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1503"/>
        <source>Screenshot saved to file &quot;%1&quot;</source>
        <translation>スクリーンショットはファイル %1 に保存されました</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1881"/>
        <source>Please note, that this could result in loss of data if OSCAR&apos;s backups have been disabled.</source>
        <translation>OSCARのバックアップが有効になっていない場合、この操作はデータを破壊する可能性があります。</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1907"/>
        <source>Would you like to import from your own backups now? (you will have no data visible for this device until you do)</source>
        <translation>ご自分のバックアップからインポートしますか？（インポートするまでこのデバイスのデータは見ることができません。）</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2271"/>
        <source>There was a problem opening MSeries block File: </source>
        <translation>MSeriesブロックファイルを開く際に問題が発生しました: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2275"/>
        <source>MSeries Import complete</source>
        <translation>MSeries インポート完了</translation>
    </message>
</context>
<context>
    <name>MinMaxWidget</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2186"/>
        <source>Auto-Fit</source>
        <translation>自動で合わせる</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2187"/>
        <source>Defaults</source>
        <translation>デフォルト</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2188"/>
        <source>Override</source>
        <translation>書き換える</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2189"/>
        <source>The Y-Axis scaling mode, &apos;Auto-Fit&apos; for automatic scaling, &apos;Defaults&apos; for settings according to manufacturer, and &apos;Override&apos; to choose your own.</source>
        <translation>Y軸のスケーリングモードでは &#x3000;「自動で合わせる」を選ぶと自動的に拡大縮小されます。「デフォルト」は機器製造元の設定に合わせる場合、「書き換える」は自身の設定をする場合に選びます。</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2195"/>
        <source>The Minimum Y-Axis value.. Note this can be a negative number if you wish.</source>
        <translation>Y軸の最低値  必要があればこの値に負の値を設定することができます。</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2196"/>
        <source>The Maximum Y-Axis value.. Must be greater than Minimum to work.</source>
        <translation>Y軸の最大値  最低値より大きくなければいけません。</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2231"/>
        <source>Scaling Mode</source>
        <translation>スケーリングモード</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2253"/>
        <source>This button resets the Min and Max to match the Auto-Fit</source>
        <translation>このボタンは最小と最大を自動で合わせる設定と同じにします</translation>
    </message>
</context>
<context>
    <name>NewProfile</name>
    <message>
        <location filename="../oscar/newprofile.ui" line="14"/>
        <source>Edit User Profile</source>
        <translation>ユーザープロフィースを編集</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="73"/>
        <source>I agree to all the conditions above.</source>
        <translation>上記の条件に同意します</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="114"/>
        <source>User Information</source>
        <translation>ユーザー情報</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="140"/>
        <source>User Name</source>
        <translation>ユーザー名</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="158"/>
        <source>Password Protect Profile</source>
        <translation>プロフィールをパスワードで保護する</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="185"/>
        <source>Password</source>
        <translation>パスワード</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="199"/>
        <source>...twice...</source>
        <translation>   2回   </translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="216"/>
        <source>Locale Settings</source>
        <translation>言語設定</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="288"/>
        <source>Country</source>
        <translation>国</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="256"/>
        <source>TimeZone</source>
        <translation>タイムゾーン</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="54"/>
        <source>about:blank</source>
        <translation>blank:について</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="155"/>
        <source>Very weak password protection and not recommended if security is required.</source>
        <translation>とても弱いパスワードが使われており、セキュリティを考えるならお勧めできません。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="243"/>
        <source>DST Zone</source>
        <translation>夏時間帯</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="323"/>
        <source>Personal Information (for reports)</source>
        <translation>（レポート用）個人情報</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="347"/>
        <source>First Name</source>
        <translation>名</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="357"/>
        <source>Last Name</source>
        <translation>姓</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="367"/>
        <source>It&apos;s totally ok to fib or skip this, but your rough age is needed to enhance accuracy of certain calculations.</source>
        <translation>飛ばしたり仮の情報を入力してもかまいませんが、より正確な計算のためには大まかな年齢が必要です。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="370"/>
        <source>D.O.B.</source>
        <translation>D.O.B.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="386"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Biological (birth) gender is sometimes needed to enhance the accuracy of a few calculations, feel free to leave this blank and skip any of them.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;（いくつかの計算を寄り正確に行うには生まれたときの）生物学的な性が必要になりますが、空白にしておいたり飛ばしてもらってもかまいません。 &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="389"/>
        <source>Gender</source>
        <translatorcomment>Japanese don&apos;t have distinctive words to describe gender and sex, thus a bit of foot note in parentheses.</translatorcomment>
        <translation>性別（生物学上の）</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="402"/>
        <source>Male</source>
        <translation>男性</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="407"/>
        <source>Female</source>
        <translation>女性</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="423"/>
        <source>Height</source>
        <translation>身長</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="462"/>
        <source>Metric</source>
        <translation>メートル法</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="467"/>
        <source>English</source>
        <translation>英語</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="480"/>
        <source>Contact Information</source>
        <translation>連絡先</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="507"/>
        <location filename="../oscar/newprofile.ui" line="782"/>
        <source>Address</source>
        <translation>住所</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="524"/>
        <location filename="../oscar/newprofile.ui" line="813"/>
        <source>Email</source>
        <translation>eメール</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="534"/>
        <location filename="../oscar/newprofile.ui" line="803"/>
        <source>Phone</source>
        <translation>電話番号</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="579"/>
        <source>CPAP Treatment Information</source>
        <translation>CPAP治療情報</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="606"/>
        <source>Date Diagnosed</source>
        <translation>診断日</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="620"/>
        <source>Untreated AHI</source>
        <translation>治療していないときのAHI</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="634"/>
        <source>CPAP Mode</source>
        <translation>CPAPモード</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="642"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="647"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="652"/>
        <source>Bi-Level</source>
        <translation>Bi-Level</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="657"/>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="665"/>
        <source>RX Pressure</source>
        <translation>RX圧力</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="711"/>
        <source>Doctors / Clinic Information</source>
        <translation>医師・クリニック情報</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="738"/>
        <source>Doctors Name</source>
        <translation>医師名</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="755"/>
        <source>Practice Name</source>
        <translation>専門</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="765"/>
        <source>Patient ID</source>
        <translation>患者ID</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="957"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cキャンセル</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="973"/>
        <source>&amp;Back</source>
        <translation>&amp;B戻る</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="989"/>
        <location filename="../oscar/newprofile.cpp" line="279"/>
        <location filename="../oscar/newprofile.cpp" line="288"/>
        <source>&amp;Next</source>
        <translation>&amp;N次</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="63"/>
        <source>Select Country</source>
        <translation>国を選択</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="111"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>オープンソースCPAP分析レポートへようこそ</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="118"/>
        <source>PLEASE READ CAREFULLY</source>
        <translation>注意して読んでください</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="122"/>
        <source>Accuracy of any data displayed is not and can not be guaranteed.</source>
        <translation>ここに表示されているデータの正確性は保証されていません。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="124"/>
        <source>Any reports generated are for PERSONAL USE ONLY, and NOT IN ANY WAY fit for compliance or medical diagnostic purposes.</source>
        <translation>すべてのレポートは「個人利用」のためであり、「どんな形であっても」コンプライアンスや医療の診断情報として使うことはできません。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="131"/>
        <source>Use of this software is entirely at your own risk.</source>
        <translation>本ソフトウェアの利用は自己責任です。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="134"/>
        <source>OSCAR is copyright &amp;copy;2011-2018 Mark Watkins and portions &amp;copy;2019-2022 The OSCAR Team</source>
        <translatorcomment>Copyright is often left as is in English, so I leave it as is.</translatorcomment>
        <translation>OSCAR is copyright &amp;copy;2011-2018 Mark Watkins and portions &amp;copy;2019-2022 The OSCAR Team</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="116"/>
        <source>OSCAR has been released freely under the &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt;, and comes with no warranty, and without ANY claims to fitness for any purpose.</source>
        <translation>OSCARは&lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt;でリリースされ、いかなる用途への適合性を保証せず、現状有姿で提供されます。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="113"/>
        <source>This software is being designed to assist you in reviewing the data produced by your CPAP Devices and related equipment.</source>
        <translation>本ソフトウェアは、CPAPや関連機器からデータを取り出し、参照する補助としてデザインされました。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="119"/>
        <source>OSCAR is intended merely as a data viewer, and definitely not a substitute for competent medical guidance from your Doctor.</source>
        <translation>OSCARは単にデータビューワーとして作成され、お医者様の適切なアドバイスを置き換えるものではありません。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="127"/>
        <source>The authors will not be held liable for &lt;u&gt;anything&lt;/u&gt; related to the use or misuse of this software.</source>
        <translation>作者は本ソフトウェアを利用した結果について&lt;u&gt;いかなる場合にも&lt;/u&gt;その責を負いません。</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="157"/>
        <source>Please provide a username for this profile</source>
        <translation>このプロフィールのユーザー名を入力してください</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="167"/>
        <source>Passwords don&apos;t match</source>
        <translation>パスワードが異なっています</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="197"/>
        <source>Profile Changes</source>
        <translation>プロフィール変更</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="197"/>
        <source>Accept and save this information?</source>
        <translation>この値を採用して保存しますか？</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="277"/>
        <source>&amp;Finish</source>
        <translation>&amp;F完了</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="455"/>
        <source>&amp;Close this window</source>
        <translation>&amp;Cウインドウを閉じる</translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <location filename="../oscar/overview.ui" line="68"/>
        <source>Range:</source>
        <translation>範囲:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="82"/>
        <source>Last Week</source>
        <translation>先週</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="87"/>
        <source>Last Two Weeks</source>
        <translation>過去2週間</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="92"/>
        <source>Last Month</source>
        <translation>先月</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="97"/>
        <source>Last Two Months</source>
        <translation>過去2ヶ月</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="102"/>
        <source>Last Three Months</source>
        <translation>過去3ヶ月</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="107"/>
        <source>Last 6 Months</source>
        <translation>過去6ヶ月</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="112"/>
        <source>Last Year</source>
        <translation>昨年</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="117"/>
        <source>Everything</source>
        <translation>すべて</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="122"/>
        <source>Custom</source>
        <translation>カスタム</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="127"/>
        <source>Snapshot</source>
        <translation>スナップショット</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="135"/>
        <source>Start:</source>
        <translation>開始:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="158"/>
        <source>End:</source>
        <translation>終了:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="181"/>
        <source>Reset view to selected date range</source>
        <translation>選んだ日付の範囲に表示をリセット</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="230"/>
        <source>Layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="237"/>
        <source>Save and Restore Graph Layout Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="244"/>
        <source>Drop down to see list of graphs to switch on/off.</source>
        <translation>表示をon/offするグラフをドロップダウンで見る。</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="251"/>
        <source>Graphs</source>
        <translation>グラフ</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="267"/>
        <source>Respiratory
Disturbance
Index</source>
        <translatorcomment>People knows about RDI in abbrebiated form than translation.</translatorcomment>
        <translation>呼吸
障害
指数 (RDI)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="269"/>
        <source>Apnea
Hypopnea
Index</source>
        <translatorcomment>People knows about AHI in abbrebiated form than translation.</translatorcomment>
        <translation>無呼吸
低呼吸
指数 (AHI)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="276"/>
        <source>Usage</source>
        <translation>使用</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="276"/>
        <source>Usage
(hours)</source>
        <translation>使用
（時間）</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="281"/>
        <source>Session Times</source>
        <translation>セッション時間</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="290"/>
        <source>Total Time in Apnea</source>
        <translation>無呼吸合計時間</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="290"/>
        <source>Total Time in Apnea
(Minutes)</source>
        <translation>無呼吸合計時間
（分）</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="347"/>
        <source>Body
Mass
Index</source>
        <translatorcomment>People knows about BMI in abbrebiated form than translation.</translatorcomment>
        <translation>ボディ
マス
指数 (BMI)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="353"/>
        <source>How you felt
(0-10)</source>
        <translation>どう感じたか
(0-10)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.h" line="202"/>
        <source>Hide All Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.h" line="203"/>
        <source>Show All Graphs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OximeterImport</name>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="61"/>
        <location filename="../oscar/oximeterimport.cpp" line="39"/>
        <source>Oximeter Import Wizard</source>
        <translation>オキシメーターインポートウィザード</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="444"/>
        <source>Skip this page next time.</source>
        <translation>次回からこのページを飛ばす。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="499"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Please note: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;First select your correct oximeter type from the pull-down menu below.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;ご注意: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;初回は下のプルダウンメニューからオキシメーターの種類を選んでください。&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="542"/>
        <source>Where would you like to import from?</source>
        <translation>どこからインポートしますか？</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="586"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:700;&quot;&gt;FIRST Select your Oximeter from these groups:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:700;&quot;&gt;最初にオキシメーターをこれらのグループから選んでください:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="648"/>
        <source>CMS50E/F users, when importing directly, please don&apos;t select upload on your device until OSCAR prompts you to.</source>
        <translation>CMS50E/Fのユーザーの方へ&#x3000;直接インポートする際は、OSCARから指示があるまでデバイスでアップロードを選択しないでください。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="685"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If enabled, OSCAR will automatically reset your CMS50&apos;s internal clock using your computers current time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;有効になっていると、OSCARは自動的にCMS50の内部時計をリセットし、コンピュータの現在時刻に合わせます。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="717"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Here you can enter a 7 character name for this oximeter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;ここにオキシメーターの名前として7文字まで入力できます。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="758"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will erase the imported session from your oximeter after import has completed. &lt;/p&gt;&lt;p&gt;Use with caution,  because if something goes wrong before OSCAR saves your session, you can&apos;t get it back.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;このオプションはオキシメーターからのインポートが終わった際にオキシメーターからインポートされたセッションを消去します。&lt;/p&gt;&lt;p&gt;OSCARがセッション情報を保管する前に何かあった場合データを取り戻すことはできないので、注意してご利用ください。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="787"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import (via cable) from your oximeters internal recordings.&lt;/p&gt;&lt;p&gt;After selecting on this option, old Contec oximeters will require you to use the device&apos;s menu to initiate the upload.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;このオプションは（ケーブルを通して）オキシメーター内部の記録をインポートすることを許可します。&lt;/p&gt;&lt;p&gt;このオプションを選んだ後、古い Contec オキシメーターでは、デバイスのメニューからアップロードを開始しければなりません。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="829"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If you don&apos;t mind a being attached to a running computer overnight, this option provide a useful plethysomogram graph, which gives an indication of heart rhythm, on top of the normal oximetry readings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;一晩中コンピュータにつながっている状態をいとわないなら、このオプションは、便利なプレチスモグラムを提供します。プレチスモグラムは通常のオキシメーターの値のみでなく、心拍のリズムを表示します。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="835"/>
        <source>Record attached to computer overnight (provides plethysomogram)</source>
        <translation>接続したコンピュータに夜間記録を取ります（プレチスモグラムが作成されます）</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="868"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import from data files created by software that came with your Pulse Oximeter, such as SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;このオプションは心拍・オキシメーターに付属していたSpO2Reviewのようなソフトウエアで作成されたファイルをインポートすることを許可します。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="874"/>
        <source>Import from a datafile saved by another program, like SpO2Review</source>
        <translation>他の、例えば、SpO2Reviewのようなプログラムで保存されたデータファイルをインポートする</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="953"/>
        <source>Please connect your oximeter device</source>
        <translation>オキシメーターを接続してください</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="971"/>
        <source>If you can read this, you likely have your oximeter type set wrong in preferences.</source>
        <translation>この文章が表示されているので設定で誤ったオキシメーターの種類を設定している可能性があります。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1000"/>
        <source>Please connect your oximeter device, turn it on, and enter the menu</source>
        <translation>オキシメーターを接続し、電源を入れ、メニューに入ってください</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1026"/>
        <source>Press Start to commence recording</source>
        <translation>記録を行うには、Start を押して下さい</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1071"/>
        <source>Show Live Graphs</source>
        <translation>リアルタイムでグラフを表示</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1101"/>
        <location filename="../oscar/oximeterimport.ui" line="1353"/>
        <source>Duration</source>
        <translation>期間</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1217"/>
        <source>Pulse Rate</source>
        <translation>心拍数</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1309"/>
        <source>Multiple Sessions Detected</source>
        <translation>複数のセッションを検出</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1348"/>
        <source>Start Time</source>
        <translation>開始時間</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1358"/>
        <source>Details</source>
        <translation>詳細</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1375"/>
        <source>Import Completed. When did the recording start?</source>
        <translation>インポートが完了しました。記録はいつから始まりましたか？</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1445"/>
        <source>Oximeter Starting time</source>
        <translation>オキシメーター開始時間</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1457"/>
        <source>I want to use the time reported by my oximeter&apos;s built in clock.</source>
        <translation>オキシメーターの内蔵の時計を使う。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1534"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: Syncing to CPAP session starting time will always be more accurate.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;注意: CPAPのセッションの開始時間と同期する方がより正確になります。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1555"/>
        <source>Choose CPAP session to sync to:</source>
        <translation>同期するCPAPセッションを選ぶ:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1675"/>
        <source>You can manually adjust the time here if required:</source>
        <translation>必要があればここで手動で次官を調整できます:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1696"/>
        <source>HH:mm:ssap</source>
        <translation>HH:mm:ssap</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1793"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cキャンセル</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1774"/>
        <source>&amp;Information Page</source>
        <translation>&amp;I 情報ページ</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="688"/>
        <source>Set device date/time</source>
        <translation>デバイスの日付/時刻を設定</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="695"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to enable updating the device identifier next import, which is useful for those who have multiple oximeters lying around.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;次回インポートの際機器の識別子を更新するにはチェックしてください。複数のオキシメーターをお持ちの方には役に立ちます。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="698"/>
        <source>Set device identifier</source>
        <translation>デバイスの識別子を設定する</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="761"/>
        <source>Erase session after successful upload</source>
        <translation>アップロードが正常に完了した場合、セッション情報を消去します</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="793"/>
        <source>Import directly from a recording on a device</source>
        <translation>デバイス上の記録から直接インポート</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="918"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Reminder for CPAP users: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Did you remember to import your CPAP sessions first?&lt;br/&gt;&lt;/span&gt;If you forget, you won&apos;t have a valid time to sync this oximetry session to.&lt;br/&gt;To a ensure good sync between devices, always try to start both at the same time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;CPAPユーザーの皆様へ: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;CPAPセッションを最初にインポートして頂けましたか？&lt;br/&gt;&lt;/span&gt;まだのようであれば、オキシメーターのセッションの時刻と同期することができません。&lt;br/&gt;デバイス間の同期ができるようにするには、同時に接心を始めるようにしてください。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1325"/>
        <source>Please choose which one you want to import into OSCAR</source>
        <translation>OSCARにどれをインポートするか選んでください</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1399"/>
        <source>Day recording (normally would have) started</source>
        <translation>日時の記録が（通常は）開始されました</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1473"/>
        <source>I started this oximeter recording at (or near) the same time as a session on my CPAP device.</source>
        <translation>オキシメーターの記録をCPAPデバイスのセッションと同時（あるいはほぼ同時）に開始しました。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1502"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR needs a starting time to know where to save this oximetry session to.&lt;/p&gt;&lt;p&gt;Choose one of the following options:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;オキシメーターのセッション情報を格納するために OSCAR は開始時間が必要です。&lt;/p&gt;&lt;p&gt;以下のオプションから一つ選んでください:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1812"/>
        <source>&amp;Retry</source>
        <translation>&amp;R 再試行</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1831"/>
        <source>&amp;Choose Session</source>
        <translation>&amp;C セッションを選ぶ</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1850"/>
        <source>&amp;End Recording</source>
        <translation>&amp;E 記録を終了</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1869"/>
        <source>&amp;Sync and Save</source>
        <translation>&amp;S 同期して保存</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1888"/>
        <source>&amp;Save and Finish</source>
        <translation>&amp;S 保存して終了</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1907"/>
        <source>&amp;Start</source>
        <translation>&amp;S 開始</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="190"/>
        <source>Scanning for compatible oximeters</source>
        <translation>互換性のあるオキシメーターを探しています</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="222"/>
        <source>Could not detect any connected oximeter devices.</source>
        <translation>接続されているオキシメーターデバイスを検出できません。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="230"/>
        <source>Connecting to %1 Oximeter</source>
        <translation>%1 オキシメーターに接続しています</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="258"/>
        <source>Renaming this oximeter from &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>オキシメーターの名称を「%1」から「%2」に変更します</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="261"/>
        <source>Oximeter name is different.. If you only have one and are sharing it between profiles, set the name to the same on both profiles.</source>
        <translation>オキシメーターの名称が異なります  1台のみお持ちで、プロフィール間で共有している場合、両方のプロフィールで同じ名称を設定してください。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="304"/>
        <source>&quot;%1&quot;, session %2</source>
        <translation>%1, セッション %2</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="329"/>
        <source>Nothing to import</source>
        <translation>インポートするものがありません</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="330"/>
        <source>Your oximeter did not have any valid sessions.</source>
        <translation>オキシメーターに正常なセッションがありません。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="331"/>
        <source>Close</source>
        <translation>閉じる</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="334"/>
        <source>Waiting for %1 to start</source>
        <translation>%1 が開始するのを待っています</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="335"/>
        <source>Waiting for the device to start the upload process...</source>
        <translation>デバイスがアップロードを開始するのを待っています…</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="337"/>
        <source>Select upload option on %1</source>
        <translation>%1 のアップロードオプションを選んでください</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="338"/>
        <source>You need to tell your oximeter to begin sending data to the computer.</source>
        <translation>オキシメーターにコンピューターにデータを送信開始するよう指示してください。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="339"/>
        <source>Please connect your oximeter, enter it&apos;s menu and select upload to commence data transfer...</source>
        <translation>オキシメーターを接続し、データ転送を開始するためにメニューからアップロードを選んでください…</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="369"/>
        <source>%1 device is uploading data...</source>
        <translation>%1 デバイスはデータをアップロードしています…</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="370"/>
        <source>Please wait until oximeter upload process completes. Do not unplug your oximeter.</source>
        <translation>オキシメーターがアップロード処理を終えるまで待ってください。オキシメーターの電源を切らないでください。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="389"/>
        <source>Oximeter import completed..</source>
        <translation>オキシメーターからのインポートが終わりました。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="414"/>
        <source>Select a valid oximetry data file</source>
        <translation>正しいオキシメーターのデータファイルを選んでください</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="414"/>
        <source>Oximetry Files (*.spo *.spor *.spo2 *.SpO2 *.dat)</source>
        <translation>オキシメーターファイル (*.spo *.spor *.spo2 *.SpO2 *.dat)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="436"/>
        <source>No Oximetry module could parse the given file:</source>
        <translation>オキシメーターのモジュールがファイルの内容を解釈できませんでした:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="484"/>
        <source>Live Oximetry Mode</source>
        <translation>リアルタムオキシメーターモード</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="536"/>
        <source>Live Oximetry Stopped</source>
        <translation>リアルタイムオキシメーター処理を停止</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="537"/>
        <source>Live Oximetry import has been stopped</source>
        <translation>リアルタイムのオキシメーターデータのインポートが停止しました</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1097"/>
        <source>Oximeter Session %1</source>
        <translation>オキシメーターセッション %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1142"/>
        <source>OSCAR gives you the ability to track Oximetry data alongside CPAP session data, which can give valuable insight into the effectiveness of CPAP treatment. It will also work standalone with your Pulse Oximeter, allowing you to store, track and review your recorded data.</source>
        <translation>OSCARはCPAPのセッションデータとともに、オキシメーターのデータを追跡する機能を提供します。これにより、CPAP治療の有効性についての貴重な洞察が得られます。また、パルスオキシメータとスタンドアロンで動作し、記録されたデータを保存、追跡、および確認できます。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1153"/>
        <source>If you are trying to sync oximetry and CPAP data, please make sure you imported your CPAP sessions first before proceeding!</source>
        <translation>オキシメトリと CPAP データを同期しようとしている場合は、先に進む前に CPAP セッションをインポートしたことを確認してください!</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1156"/>
        <source>For OSCAR to be able to locate and read directly from your Oximeter device, you need to ensure the correct device drivers (eg. USB to Serial UART) have been installed on your computer. For more information about this, %1click here%2.</source>
        <translation>OSCAR が Oximeter デバイスを見つけて直接読み取れるようにするには、コンピュータに正しいデバイス ドライバ (USB to Serial UART など) がインストールされていることを確認する必要があります。 詳細については、%1ここをクリック%2してください。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="466"/>
        <source>Oximeter not detected</source>
        <translation>オキシメーターが検出されません</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="473"/>
        <source>Couldn&apos;t access oximeter</source>
        <translation>オキシメーターにアクセスできません</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="487"/>
        <source>Starting up...</source>
        <translation>開始しています…</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="488"/>
        <source>If you can still read this after a few seconds, cancel and try again</source>
        <translation>数秒経ってもこの文章が表示されている場合は、キャンセルしてもう一度お試しください</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="535"/>
        <source>Live Import Stopped</source>
        <translation>ライブインポートの停止</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="588"/>
        <source>%1 session(s) on %2, starting at %3</source>
        <translation>%2 の %1 セッション、%3 に開始</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="592"/>
        <source>No CPAP data available on %1</source>
        <translation>%1 にはCPAPのデータがありません</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="727"/>
        <source>Recording...</source>
        <translation>記録中…</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="734"/>
        <source>Finger not detected</source>
        <translatorcomment>This text itself is a bit of horror :-) maybe it should be &quot;oximeter can&apos;t find the finger.&quot;</translatorcomment>
        <translation>指が見つかりません</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="834"/>
        <source>I want to use the time my computer recorded for this live oximetry session.</source>
        <translation>オキシメーターのセッションをリアルタイムで記録するのにコンピュータの時計を使います。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="837"/>
        <source>I need to set the time manually, because my oximeter doesn&apos;t have an internal clock.</source>
        <translation>オキシメーターに内蔵の時計がないため、時刻を手動でセットします。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="849"/>
        <source>Something went wrong getting session data</source>
        <translation>セッション上データを取得する際何らかの問題が発生しました</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1138"/>
        <source>Welcome to the Oximeter Import Wizard</source>
        <translation>オキシメーターインポートウィザードへようこそ</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1140"/>
        <source>Pulse Oximeters are medical devices used to measure blood oxygen saturation. During extended Apnea events and abnormal breathing patterns, blood oxygen saturation levels can drop significantly, and can indicate issues that need medical attention.</source>
        <translation>パルスオキシメーターは、血液の酸素飽和度を測定するための医療機器です。無呼吸や正常でない呼吸のパターンの間、血液の酸素飽和度は有意に下がることがあり、医療的な措置が必要な問題を示すことがあります。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1144"/>
        <source>OSCAR is currently compatible with Contec CMS50D+, CMS50E, CMS50F and CMS50I serial oximeters.&lt;br/&gt;(Note: Direct importing from bluetooth models is &lt;span style=&quot; font-weight:600;&quot;&gt;probably not&lt;/span&gt; possible yet)</source>
        <translation>OSCAR は現在 Contec CMS50D+, CMS50E, CMS50 および CMS50I シリアルオキシメーターと互換性があります。&lt;br/&gt;（注: Bluetooth から直接インポートすることは&lt;span style=&quot; font-weight:600;&quot;&gt;恐らくできません&lt;/span&gt;）</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1146"/>
        <source>You may wish to note, other companies, such as Pulox, simply rebadge Contec CMS50&apos;s under new names, such as the Pulox PO-200, PO-300, PO-400. These should also work.</source>
        <translation>他社、例えば Pulox の様な会社は、Contec CMS50 シリーズの名称を Pulox PO-200, PO-300, PO-400 などと変更しているだけの場合あります。このような機器も互換性があります。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1149"/>
        <source>It also can read from ChoiceMMed MD300W1 oximeter .dat files.</source>
        <translation>ChoiceMMed MD300W1 オキシメーターの .dat を読むこともできます。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1151"/>
        <source>Please remember:</source>
        <translation>ご注意ください:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1155"/>
        <source>Important Notes:</source>
        <translation>重要な注:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1158"/>
        <source>Contec CMS50D+ devices do not have an internal clock, and do not record a starting time. If you do not have a CPAP session to link a recording to, you will have to enter the start time manually after the import process is completed.</source>
        <translation>Contec CM500D+ は内蔵の時計がないため、開始時間を記録しません。記録をリンクする CPAP セッションがない場合は、インポート処理の完了後に開始時刻を手動で入力する必要があります。</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1160"/>
        <source>Even for devices with an internal clock, it is still recommended to get into the habit of starting oximeter records at the same time as CPAP sessions, because CPAP internal clocks tend to drift over time, and not all can be reset easily.</source>
        <translation>内部に時計がある機器の場合であっても、CPAP の内部クロックは時間の経過とともに送れたり進んだりする傾向があり、すべてを簡単にリセットできるわけではないため、CPAP セッションと同時にオキシメータの記録を開始する習慣をつけることをお勧めします。</translation>
    </message>
</context>
<context>
    <name>Oximetry</name>
    <message>
        <location filename="../oscar/oximetry.ui" line="89"/>
        <source>Date</source>
        <translation>日付</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="102"/>
        <source>d/MM/yy h:mm:ss AP</source>
        <translation>yy/MM/d h:mm:ss AP</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="131"/>
        <source>R&amp;eset</source>
        <translation>&amp;Rリセット</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="245"/>
        <source>Pulse</source>
        <translation>心拍</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="366"/>
        <source>&amp;Open .spo/R File</source>
        <translation>&amp;O .spo/Rファイルを開く</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="385"/>
        <source>Serial &amp;Import</source>
        <translation>&amp;I シリアルからインポート</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="398"/>
        <source>&amp;Start Live</source>
        <translation>&amp;S リアルタイム収集開始</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="421"/>
        <source>Serial Port</source>
        <translation>シリアルポート</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="450"/>
        <source>&amp;Rescan Ports</source>
        <translation>&amp;R ポートを再スキャン</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="29"/>
        <source>Preferences</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="70"/>
        <source>&amp;Import</source>
        <translation>&amp;I インポート</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="160"/>
        <source>Combine Close Sessions </source>
        <translation>閉じたセッションを統合 </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="170"/>
        <location filename="../oscar/preferencesdialog.ui" line="255"/>
        <location filename="../oscar/preferencesdialog.ui" line="753"/>
        <source>Minutes</source>
        <translation>分</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="190"/>
        <source>Multiple sessions closer together than this value will be kept on the same day.
</source>
        <translation>この値より近い複数のセッションは、同じ日として保管されます。
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="245"/>
        <source>Ignore Short Sessions</source>
        <translation>短いセッションを無視</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="311"/>
        <source>Day Split Time</source>
        <translation>一日の始まり時刻</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="321"/>
        <source>Sessions starting before this time will go to the previous calendar day.</source>
        <translation>この時間より前のセッションはカレンダーの前日として扱われます。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="394"/>
        <source>Session Storage Options</source>
        <translation>セッション保管のオプション</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="441"/>
        <source>Compress SD Card Backups (slower first import, but makes backups smaller)</source>
        <translation>SD カードのバックアップを圧縮する (最初のインポートは遅くなりますが、バックアップが小さくなります）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="659"/>
        <source>&amp;CPAP</source>
        <translation>&amp;CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1297"/>
        <source>Regard days with under this usage as &quot;incompliant&quot;. 4 hours is usually considered compliant.</source>
        <translation>この日数以下は「違反」とみなします。 通常、4 時間は準拠していると見なされます。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1300"/>
        <source> hours</source>
        <translation> 時間</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1058"/>
        <source>Flow Restriction</source>
        <translation>流量制限</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1095"/>
        <source>Percentage of restriction in airflow from the median value. 
A value of 20% works well for detecting apneas. </source>
        <translation>中央値からの流量制限のパーセンテージ。
20% の値は、無呼吸の検出に適しています。 </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1116"/>
        <source>Duration of airflow restriction</source>
        <translation>気流制限の時間</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="999"/>
        <location filename="../oscar/preferencesdialog.ui" line="1119"/>
        <location filename="../oscar/preferencesdialog.ui" line="1592"/>
        <location filename="../oscar/preferencesdialog.ui" line="1683"/>
        <location filename="../oscar/preferencesdialog.ui" line="1712"/>
        <source>s</source>
        <translatorcomment>If this is &quot;plural&quot; s, Japanese nouns do not conjugate when it is plural</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1155"/>
        <source>Event Duration</source>
        <translation>イベント期間</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1220"/>
        <source>Adjusts the amount of data considered for each point in the AHI/Hour graph.
Defaults to 60 minutes.. Highly recommend it&apos;s left at this value.</source>
        <translation>AHI/時のグラフで対象とするデータの量を調整します。
デフォルトは60分です。この値のままを強くお勧めします。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1224"/>
        <source> minutes</source>
        <translation> 分</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1262"/>
        <source>Reset the counter to zero at beginning of each (time) window.</source>
        <translation>それぞれの（時間）の範囲の最初でカウンターをゼロにリセットする。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1265"/>
        <source>Zero Reset</source>
        <translation>ゼロにリセット</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="706"/>
        <source>CPAP Clock Drift</source>
        <translation>CPAPの時計のずれ</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="502"/>
        <source>Do not import sessions older than:</source>
        <translation>次より古いセッションをインポートしない:</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="509"/>
        <source>Sessions older than this date will not be imported</source>
        <translation>この日付より古いセッションがインポートされます</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="535"/>
        <source>dd MMMM yyyy</source>
        <translatorcomment>Spelling out month name in English does not render correctly in Japanese locale.  We usually write Year-Month-Day.</translatorcomment>
        <translation>yyyy MMMM dd</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1278"/>
        <source>User definable threshold considered large leak</source>
        <translation>大きな漏れとして扱うユーザー定義可能なしきい値</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1245"/>
        <source>Whether to show the leak redline in the leak graph</source>
        <translation>漏れのグラフに漏れの赤い線を表示する</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1832"/>
        <location filename="../oscar/preferencesdialog.ui" line="1911"/>
        <source>Search</source>
        <translation>検索</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1518"/>
        <source>&amp;Oximetry</source>
        <translation>&amp;O オキシメトリー</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1109"/>
        <source>Show in Event Breakdown Piechart</source>
        <translation>イベント毎円グラフで表示する</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1735"/>
        <source>Percentage drop in oxygen saturation</source>
        <translation>酸素飽和度低下のパーセンテージ</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1728"/>
        <source>Pulse</source>
        <translation>脈拍</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1693"/>
        <source>Sudden change in Pulse Rate of at least this amount</source>
        <translation>この範囲での突然の脈拍数の変化</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1582"/>
        <location filename="../oscar/preferencesdialog.ui" line="1615"/>
        <location filename="../oscar/preferencesdialog.ui" line="1696"/>
        <source> bpm</source>
        <translation> bpm</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1680"/>
        <source>Minimum duration of drop in oxygen saturation</source>
        <translation>酸素飽和度低下の最低の期間</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1709"/>
        <source>Minimum duration of pulse change event.</source>
        <translation>脈拍数の変化の最低の期間。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1589"/>
        <source>Small chunks of oximetry data under this amount will be discarded.</source>
        <translation>この値以下の小さなオキシメトリーのデータのかたまりは破棄されます。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1967"/>
        <source>&amp;General</source>
        <translation>&amp;G 一般</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1345"/>
        <source>Changes to the following settings needs a restart, but not a recalc.</source>
        <translation>以下の設定の変更は、再計算ではなく、再起動が必要です。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1348"/>
        <source>Preferred Calculation Methods</source>
        <translation>計算方法の設定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1377"/>
        <source>Middle Calculations</source>
        <translation>中間計算</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1391"/>
        <source>Upper Percentile</source>
        <translation>上位パーセンタイル</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="97"/>
        <source>Session Splitting Settings</source>
        <translation>セッション分割設定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="357"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;This setting should be used with caution...&lt;/span&gt; Switching it off comes with consequences involving accuracy of summary only days, as certain calculations only work properly provided summary only sessions that came from individual day records are kept together. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ResMed users:&lt;/span&gt; Just because it seems natural to you and I that the 12 noon session restart should be in the previous day, does not mean ResMed&apos;s data agrees with us. The STF.edf summary index format has serious weaknesses that make doing this not a good idea.&lt;/p&gt;&lt;p&gt;This option exists to pacify those who don&apos;t care and want to see this &amp;quot;fixed&amp;quot; no matter the costs, but know it comes with a cost. If you keep your SD card in every night, and import at least once a week, you won&apos;t see problems with this very often.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;この設定は慎重に使用する必要があります...&lt;/span&gt; この設定をオフにすると、要約のみのデータの日の正確性に影響が及びます。これは 特定の計算が適切に機能するのは、各々の日の記録とそのサマリーが同じ場所の保管されていることを前提としているためです。 &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ResMed ユーザー:&lt;/span&gt; 12 時のセッションの再開が前日の正午に行われることは、あなたと私にとって当然のことのように思えますが、そうではありません。 ResMed のデータと私たちの一日が一致するという意味ではありません。 STF.edf サマリー インデックス形式には重大な弱点があり、これを行うことはお勧めできません。&lt;/p&gt;&lt;p&gt;このオプションは、（計算）コストを気にせず、この「修正済み」のインデックスを見たいと思っている人のために存在します。 コストがかかることを知ってください。 SD カードを毎晩持ち、少なくとも週に 1 回はインポートする場合、これに関する問題はあまり見られません。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="360"/>
        <source>Don&apos;t Split Summary Days (Warning: read the tooltip!)</source>
        <translation>概要の日を分けない（注: ツールチップの表示を読んでください!）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="569"/>
        <source>Memory and Startup Options</source>
        <translation>メモリとスタート時のオプション</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="611"/>
        <source>Pre-Load all summary data at startup</source>
        <translation>開始時にすべてのサマリデータを事前の読み込む</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="598"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This setting keeps waveform and event data in memory after use to speed up revisiting days.&lt;/p&gt;&lt;p&gt;This is not really a necessary option, as your operating system caches previously used files too.&lt;/p&gt;&lt;p&gt;Recommendation is to leave it switched off, unless your computer has a ton of memory.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;この設定は、使用後も波形とイベント データをメモリに保持し、同じ日を見る際のの時間を短縮します。&lt;/p&gt;&lt;p&gt;オペレーティング システムが以前に使ったファイルをキャッシュするため、これは実際には必要なオプションではありません。 &lt;/p&gt;&lt;p&gt;コンピュータに大量のメモリがない限り、オフのままにしておくことをお勧めします。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="601"/>
        <source>Keep Waveform/Event data in memory</source>
        <translation>波形とイベントデータをメモリに保持</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="625"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cuts down on any unimportant confirmation dialogs during import.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;インポート中の重要でない確認ダイアログを減らします。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="628"/>
        <source>Import without asking for confirmation</source>
        <translation>確認せずにインポートする</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="810"/>
        <source>Calculate Unintentional Leaks When Not Present</source>
        <translation>（データが）ない場合意図しない漏れを計算する</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="948"/>
        <source>Note: A linear calculation method is used. Changing these values requires a recalculation.</source>
        <translation>注: 線形計算方法が使用されます。 これらの値を変更するには、再計算が必要です。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1184"/>
        <source>General CPAP and Related Settings</source>
        <translation>一般的な CPAP および関連の設定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1193"/>
        <source>Enable Unknown Events Channels</source>
        <translation>不明なイベント チャネルを有効にする</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1323"/>
        <source>AHI</source>
        <extracomment>Apnea Hypopnea Index</extracomment>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1328"/>
        <source>RDI</source>
        <extracomment>Respiratory Disturbance Index</extracomment>
        <translation>RDI</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1200"/>
        <source>AHI/Hour Graph Time Window</source>
        <translation>AHI/時グラフ 時間ウインドウ</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1255"/>
        <source>Preferred major event index</source>
        <translation>主なイベントインデックスの設定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1207"/>
        <source>Compliance defined as</source>
        <translation>コンプライアンスの定義</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1248"/>
        <source>Flag leaks over threshold</source>
        <translation>しきい値を超える漏れを指摘する</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="787"/>
        <source>Seconds</source>
        <translation>秒</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="733"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: This is not intended for timezone corrections! Make sure your operating system clock and timezone is set correctly.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;注: これはタイムゾーンの修正を意図したものではありません! オペレーティング システムの時計とタイムゾーンが正しく設定されていることを確認してください。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="780"/>
        <source>Hours</source>
        <translation>時間</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1354"/>
        <source>For consistancy, ResMed users should use 95% here,
as this is the only value available on summary-only days.</source>
        <translation>一貫性を保つため、ResMed ユーザーはここを 95% としてください。
概要のみの日にはこれが唯一の値です。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1405"/>
        <source>Median is recommended for ResMed users.</source>
        <translation>ResMedユーザーには中央値を推奨します。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1409"/>
        <location filename="../oscar/preferencesdialog.ui" line="1472"/>
        <source>Median</source>
        <translation>中央値</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1414"/>
        <source>Weighted Average</source>
        <translation>加重平均</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1419"/>
        <source>Normal Average</source>
        <translation>単純平均</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1443"/>
        <source>True Maximum</source>
        <translation>真の最大値</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1448"/>
        <source>99% Percentile</source>
        <translation>99% パーセンタイル</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1384"/>
        <source>Maximum Calcs</source>
        <translation>最大の計算</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1981"/>
        <source>General Settings</source>
        <translation>一般設定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2722"/>
        <source>Daily view navigation buttons will skip over days without data records</source>
        <translation>日時表示のボタンはデータが記録されていない日を飛ばします</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2725"/>
        <source>Skip over Empty Days</source>
        <translation>データのない日を飛ばす</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2002"/>
        <source>Allow use of multiple CPU cores where available to improve performance. 
Mainly affects the importer.</source>
        <translation>性能向上のために複数の CPU コアを使うことを許可する 
主にデータをインポートする際に影響があります。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2006"/>
        <source>Enable Multithreading</source>
        <translation>マルチスレッディングを有効にする</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="575"/>
        <source>Bypass the login screen and load the most recent User Profile</source>
        <translation>ログイン画面を表示せず一番最近のユーザープロフィールを読み込む</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="484"/>
        <source>Create SD Card Backups during Import (Turn this off at your own peril!)</source>
        <translation>インポート中に SD カードのバックアップを作成する （自己責任でこれをオフにしてください！）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1439"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;True maximum is the maximum of the data set.&lt;/p&gt;&lt;p&gt;99th percentile filters out the rarest outliers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;True maximum is the maximum of the data set.&lt;/p&gt;&lt;p&gt;99th percentile filters out the rarest outliers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1457"/>
        <source>Combined Count divided by Total Hours</source>
        <translation>合計カウントを合計時間で割った値</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1462"/>
        <source>Time Weighted average of Indice</source>
        <translation>インデックスの時間加重平均</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1467"/>
        <source>Standard average of indice</source>
        <translation>インデックスの標準平均</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="972"/>
        <source>Custom CPAP User Event Flagging</source>
        <translation>カスタム CPAP ユーザー イベントのフラグ設定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1809"/>
        <source>Events</source>
        <translation>イベント</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1773"/>
        <location filename="../oscar/preferencesdialog.ui" line="1862"/>
        <location filename="../oscar/preferencesdialog.ui" line="1941"/>
        <source>Reset &amp;Defaults</source>
        <translation>&amp;D デフォルトにリセットする</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1875"/>
        <location filename="../oscar/preferencesdialog.ui" line="1954"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;Just because you can, does not mean it&apos;s good practice.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;警告: &lt;/span&gt;出来るということだけで、良いやり方とは言えません。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1888"/>
        <source>Waveforms</source>
        <translation>波形</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1661"/>
        <source>Flag rapid changes in oximetry stats</source>
        <translation>オキシメトリーの統計の急な変化にフラグをつける</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1569"/>
        <source>Other oximetry options</source>
        <translation>園のかのオキシメトリーのオプション</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1602"/>
        <source>Discard segments under</source>
        <translation>以下のセグメントを破棄</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1642"/>
        <source>Flag Pulse Rate Above</source>
        <translation>次の値を超える脈拍をフラグする</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1632"/>
        <source>Flag Pulse Rate Below</source>
        <translation>次の値を下回る脈拍をフラグする</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="433"/>
        <source>Compress ResMed (EDF) backups to save disk space.
Backed up EDF files are stored in the .gz format, 
which is common on Mac &amp; Linux platforms.. 

OSCAR can import from this compressed backup directory natively.. 
To use it with ResScan will require the .gz files to be uncompressed first..</source>
        <translation>ディスクの容量を節約するために ResMed (EDF) のバックアップを圧縮します。
バックアップされた EDF ファイルは .gz 形式で保管されます。 
(Mac および Linux では一般的です） 

OSCAR は圧縮されたバックアップディレクトリをそのままインポートすることができます。 
ResScanで利用するには、いったん .gz 形式を解凍する必要があります。.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="453"/>
        <source>The following options affect the amount of disk space OSCAR uses, and have an effect on how long import takes.</source>
        <translation>以下のオプションは OSCAR がどれくらいのディスク容量を使うかに影響があり、また、インポート時間にも影響があります。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="463"/>
        <source>This makes OSCAR&apos;s data take around half as much space.
But it makes import and day changing take longer.. 
If you&apos;ve got a new computer with a small solid state disk, this is a good option.</source>
        <translation>このオプションを利用すると OSCAR のデータが半分くらいの容量になります。
一方、インポートと日付の切り替えにより長い時間がかかります。
もしお使いのコンピュータが容量の小さい SSD を使っているのであれば、これは良いオプションです。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="468"/>
        <source>Compress Session Data (makes OSCAR data smaller, but day changing slower.)</source>
        <translation>セッションデータを圧縮する（OSCAR のデータは小さくなりますが、日付の切り替えは遅くなります）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="608"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Makes starting OSCAR a bit slower, by pre-loading all the summary data in advance, which speeds up overview browsing and a few other calculations later on. &lt;/p&gt;&lt;p&gt;If you have a large amount of data, it might be worth keeping this switched off, but if you typically like to view &lt;span style=&quot; font-style:italic;&quot;&gt;everything&lt;/span&gt; in overview, all the summary data still has to be loaded anyway. &lt;/p&gt;&lt;p&gt;Note this setting doesn&apos;t affect waveform and event data, which is always demand loaded as needed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCARの起動が少し遅くなり余す。あらかじめサマリーのデータを読み込んでおくことで、概要の表示と後に行われる&lt;/p&gt;&lt;p&gt;大きなデータがある場合、この設定はオフにしておく方が良いですが、 ほとんどの場合に &lt;span style=&quot; font-style:italic;&quot;&gt;すべて&lt;/span&gt;を概要画面見たい場合にはで いずれにしてもすべての概要データを読み込む必要があります。&lt;/p&gt;&lt;p&gt;必要に応じて読み込まれる波形とイベントのデータには、この設定には適用されません。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="906"/>
        <source>4 cmH2O</source>
        <translation>4 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="916"/>
        <source>20 cmH2O</source>
        <translation>20 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2013"/>
        <source>Show Remove Card reminder notification on OSCAR shutdown</source>
        <translation>OSCAR終了時にカードを外すリマインダーを表示する</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2121"/>
        <source>Check for new version every</source>
        <translation>更新の確認を次の日数毎に行う</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2144"/>
        <source>days.</source>
        <translation>日。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2174"/>
        <source>Last Checked For Updates: </source>
        <translation>前回の更新確認： </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2187"/>
        <source>TextLabel</source>
        <translation>テキストラベル</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2212"/>
        <source>I want to be notified of test versions. (Advanced users only please.)</source>
        <translation>テストバージョンについて通知してほしい（上級ユーザーのみ）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2255"/>
        <source>&amp;Appearance</source>
        <translation>&amp;A 表示</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2284"/>
        <source>Graph Settings</source>
        <translation>グラフ設定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2300"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Which tab to open on loading a profile. (Note: It will default to Profile if OSCAR is set to not open a profile on startup)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;プロフィール読み込み時に開くタブ（注: OSCARが起動時にプロフィールを開く設定になっていない場合には、プロフィールになります。)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2558"/>
        <source>Bar Tops</source>
        <translation>積み上げ</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2563"/>
        <source>Line Chart</source>
        <translation>折れ線</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2653"/>
        <source>Overview Linecharts</source>
        <translation>概要 - 折れ線</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2052"/>
        <source>Try changing this from the default setting (Desktop OpenGL) if you experience rendering problems with OSCAR&apos;s graphs.</source>
        <translation>OSCARのグラフのレンダリングに省略時の設定（Desktop OpenGL）で問題がある場合、本設定を変更してください。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2598"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This makes scrolling when zoomed in easier on sensitive bidirectional TouchPads&lt;/p&gt;&lt;p&gt;50ms is recommended value.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;この設定を行うと、敏感な双方向タッチパッドでズームインしたときのスクロールが容易になります&lt;/p&gt;&lt;p&gt;推奨値は、50 ミリ秒です。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2492"/>
        <source>How long you want the tooltips to stay visible.</source>
        <translation>ツールチップの表示される時間。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2480"/>
        <source>Scroll Dampening</source>
        <translation>スクロール減衰</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2470"/>
        <source>Tooltip Timeout</source>
        <translation>ツールチップタイムアウト</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2577"/>
        <source>Default display height of graphs in pixels</source>
        <translation>省略時のグラフの高さ（ピクセル）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2550"/>
        <source>Graph Tooltips</source>
        <translation>グラフツールチップ</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2426"/>
        <source>The visual method of displaying waveform overlay flags.
</source>
        <translation>波形にフラグを重ねる表示方法。
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2431"/>
        <source>Standard Bars</source>
        <translation>標準バー</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2436"/>
        <source>Top Markers</source>
        <translation>トップマーカー</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2384"/>
        <source>Graph Height</source>
        <translation>グラフの高さ</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="272"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt;&quot;&gt;Sessions shorter in duration than this will not be displayed&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt;&quot;&gt;これより短いセッションは表示されません。&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="423"/>
        <source>Changing SD Backup compression options doesn&apos;t automatically recompress backup data.</source>
        <translation>SDバックアップの圧縮方法を変更してもバックアップされているデータは自動的に再圧縮されません。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="578"/>
        <source>Auto-Launch CPAP Importer after opening profile</source>
        <translation>プロフィールを開いた後自動的に CPAP インポーターを起動します</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="618"/>
        <source>Automatically load last used profile on start-up</source>
        <translation>起動時に前回使用したプロフィールを読み込みます</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="645"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Provide an alert when importing data that is somehow different from anything previously seen by OSCAR developers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;データのインポート時に、OSCARのデベロッパーが過去に認識した形式のどれとも異なる場合、アラートを表示します。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="648"/>
        <source>Warn when previously unseen data is encountered</source>
        <translation>認識できない形式である場合に警告します</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="819"/>
        <source>Your masks vent rate at 20 cmH2O pressure</source>
        <translation>マスクは 20 cmH2O ｍｐレートでで空気を排出します</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="875"/>
        <source>Your masks vent rate at 4 cmH2O pressure</source>
        <translation>マスクは 4 cmH2O ｍｐレートでで空気を排出します</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1076"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Custom flagging is an experimental method of detecting events missed by the device. They are &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; included in AHI.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;カスタム フラグは、デバイスが見逃したイベントを検出する実験的な方法です。これらはAHIに含まれて &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;いません。&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1281"/>
        <source> l/min</source>
        <translation> リットル/分</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1398"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cumulative Indices&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;累積指数&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1539"/>
        <source>Oximetry Settings</source>
        <translation>オキシメトリー設定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1622"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Flag SpO&lt;span style=&quot; vertical-align:sub;&quot;&gt;2&lt;/span&gt; Desaturations Below&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;SpO&lt;span style=&quot; vertical-align:sub;&quot;&gt;2&lt;/span&gt;が次より低いときにフラグする&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1795"/>
        <source> &lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt; p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Syncing Oximetry and CPAP Data&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;CMS50 data imported from SpO2Review (from .spoR files) or the serial import method do &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; have the correct timestamp needed to sync.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Live view mode (using a serial cable) is one way to acheive an accurate sync on CMS50 oximeters, but does not counter for CPAP clock drift.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;If you start your Oximeters recording mode at &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;exactly &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;the same time you start your CPAP device, you can now also achieve sync. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;The serial import process takes the starting time from last nights first CPAP session. (Remember to import your CPAP data first!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation> &lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt; p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;オキシメトリーと CPAP データを同期しています&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;CMS50 データが SpO2レビュー (あるいは .spoR ファイル)からインポートされたあるいはシリアルインポートが同期のための正しいタイムスタンプを含んで&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;いません&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;ライブビューモード（シリアルケーブル利用）はCM50オキシメーター立と正しく同期する唯一の方法ですが、CPAPの時計のずれに対応するものではありません。&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;もしCPAPデバイスの開始に&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;正確に&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;オキシメーターの記録を開すれば同期することができます。 &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;シリアル インポート プロセスは、昨夜の最初の CPAP セッションから開始時間を取得します。(最初に CPAP データをインポートすることを忘れないでください!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2020"/>
        <source>Always save screenshots in the OSCAR Data folder</source>
        <translation>OSCARのデータフォルダに常にスクリーンショットを保管する</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2074"/>
        <source>Check For Updates</source>
        <translation>更新の確認</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2089"/>
        <source>You are using a test version of OSCAR. Test versions check for updates automatically at least once every seven days.  You may set the interval to less than seven days.</source>
        <translation>テストバージョンのOSCARをお使いです。テストバージョンは7日毎に最低1回更新がないかチェックします。7日より短い間隔に設定することができます。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2106"/>
        <source>Automatically check for updates</source>
        <translation>自動的に更新を確認</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2128"/>
        <source>How often OSCAR should check for updates.</source>
        <translation>OSCARが更新版を確認する間隔です。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2209"/>
        <source>If you are interested in helping test new features and bugfixes early, click here.</source>
        <translation>もしテストや新機能、バグの修正などのご興味がある場合は、ここをクリックしてください。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2225"/>
        <source>If you would like to help test early versions of OSCAR, please see the Wiki page about testing OSCAR.  We welcome everyone who would like to test OSCAR, help develop OSCAR, and help with translations to existing or new languages. https://www.sleepfiles.com/OSCAR</source>
        <translation>OSCARの開発中のバージョンの試験のお手伝いをなさりたい方は、OSCARの試験についてのWikiページをご覧下さい。OSCARを試験なさりたい方、OSCARを開発なさりたい方、既存あるいは新規の言語に対応する翻訳をお手伝いいただける方などを歓迎いたします。 https://www.sleepfiles.com/OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2290"/>
        <source>On Opening</source>
        <translation>オープン時</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2303"/>
        <location filename="../oscar/preferencesdialog.ui" line="2307"/>
        <source>Profile</source>
        <translation>プロフィール</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2312"/>
        <location filename="../oscar/preferencesdialog.ui" line="2351"/>
        <source>Welcome</source>
        <translation>ようこそ</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2317"/>
        <location filename="../oscar/preferencesdialog.ui" line="2356"/>
        <source>Daily</source>
        <translation>日次</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2327"/>
        <location filename="../oscar/preferencesdialog.ui" line="2366"/>
        <source>Statistics</source>
        <translation>統計</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2335"/>
        <source>Switch Tabs</source>
        <translation>タブの切り替え</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2346"/>
        <source>No change</source>
        <translation>変更なし</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2374"/>
        <source>After Import</source>
        <translation>インポート後</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2400"/>
        <source>Overlay Flags</source>
        <translation>オーバーレイフラグ</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2410"/>
        <source>Line Thickness</source>
        <translation>線の太さ</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2446"/>
        <source>The pixel thickness of line plots</source>
        <translation>折れ線グラフのピクセル単位の太さ</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2672"/>
        <source>Other Visual Settings</source>
        <translation>その他の表示設定</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2678"/>
        <source>Anti-Aliasing applies smoothing to graph plots.. 
Certain plots look more attractive with this on. 
This also affects printed reports.

Try it and see if you like it.</source>
        <translation>アンチエイリアシングはグラフの描画をスムーズにします。
グラフの種類によってはこの設定がONの方が見栄えが良くなります。
印刷されるレポートにも影響のある設定です。

試してみて気に入るかどうか確認してください。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2685"/>
        <source>Use Anti-Aliasing</source>
        <translation>アンチエイリアシングを使う</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2692"/>
        <source>Makes certain plots look more &quot;square waved&quot;.</source>
        <translation>特定のプロットをより「方形波」状に見せます。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2695"/>
        <source>Square Wave Plots</source>
        <translation>方形波プロット</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2702"/>
        <source>Pixmap caching is an graphics acceleration technique. May cause problems with font drawing in graph display area on your platform.</source>
        <translation>Pixmap キャッシングは、グラフィックスの描画を速くする技術です。お使いの環境によってはグラフ表示領域でのフォント描画で問題が発生する可能性があります。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2705"/>
        <source>Use Pixmap Caching</source>
        <translation>Pixmap キャッシングを使う</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2712"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These features have recently been pruned. They will come back later. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;これらの機能は最近削除されました。 将来のバージョンで実装される予定です。 &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2715"/>
        <source>Animations &amp;&amp; Fancy Stuff</source>
        <translation>アニメーション &amp;&amp; ファンシーなもの</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2732"/>
        <source>Whether to allow changing yAxis scales by double clicking on yAxis labels</source>
        <translation>Y軸のラベルをダブルクリックした際Y軸がスケールすることを許すことを許可するかどうか</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2735"/>
        <source>Allow YAxis Scaling</source>
        <translation>Y軸のスケールを許可する</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2745"/>
        <source>Include Serial Number</source>
        <translation>シリアル番号を含める</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2046"/>
        <source>Graphics Engine (Requires Restart)</source>
        <translation>グラフィックエンジン（再起動が必要）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="475"/>
        <source>This maintains a backup of SD-card data for ResMed devices, 

ResMed S9 series devices delete high resolution data older than 7 days, 
and graph data older than 30 days..

OSCAR can keep a copy of this data if you ever need to reinstall. 
(Highly recomended, unless your short on disk space or don&apos;t care about the graph data)</source>
        <translation>この設定は、ResMed 装置のSDカードのバックアップについてです。

ResMed S9 シリーズのデバイスは7巻より古いデータについて解像度の高いデータを削除します。
グラフについては30日より古いものが対象です

OSCARは再インストールが必要になった場合に備え、これらのデータの複製を保持することができます。
(ディスク容量が足りなかったり、グラフデータについて気にしない場合を除き、強くお勧めする設定です）</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="635"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Provide an alert when importing data from any device model that has not yet been tested by OSCAR developers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;データのインポート時に、OSCARのデベロッパーが過去に試験した形式のどれとも異なる場合、アラートを表示します。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="638"/>
        <source>Warn when importing data from an untested device</source>
        <translation>試験したことのない機器からのデータからのインポートである場合警告します</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="803"/>
        <source>This calculation requires Total Leaks data to be provided by the CPAP device. (Eg, PRS1, but not ResMed, which has these already)

The Unintentional Leak calculations used here are linear, they don&apos;t model the mask vent curve.

If you use a few different masks, pick average values instead. It should still be close enough.</source>
        <translation>この計算には、CPAP デバイスから提供される合計漏れデータが必要です。 (例 PRS1, ResMedは既にこのデータがあります。)

予期しない漏れの計算は線形であり、マスクのベントカーブに基づくモデルではありません。

複数のマスクをお持ちの場合は、平均値を使ってください。充分に（真の値に）近い値となると思われます。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="967"/>
        <source>Enable/disable experimental event flagging enhancements. 
It allows detecting borderline events, and some the device missed.
This option must be enabled before import, otherwise a purge is required.</source>
        <translation>実験的なイベント フラグ機能の強化を有効/無効にします。
これにより、デバイスが見逃したイベントや境界イベントの検出が可能になります。
このオプションは、インポートの前に有効にする必要があります。もしくは、いったんパージが必要になります。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1019"/>
        <source>This experimental option attempts to use OSCAR&apos;s event flagging system to improve device detected event positioning.</source>
        <translation>この実験的なオプションは、OSCAR のイベント フラグ システムを使用して、デバイスで検出されたイベントの位置を改善しようとします。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1022"/>
        <source>Resync Device Detected Events (Experimental)</source>
        <translation>デバイスが検出したイベントの再同期 (実験的)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1142"/>
        <source>Allow duplicates near device events.</source>
        <translation>デバイス イベント付近の重複を許可する。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1190"/>
        <source>Show flags for device detected events that haven&apos;t been identified yet.</source>
        <translation>まだ識別されていないデバイス検出イベントのフラグを表示します。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1486"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Due to summary design limitations, ResMed devices do not support changing these settings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;注: &lt;/span&gt;概要設計上の制限により、ResMed デバイスはこれらの設定の変更をサポートしていません。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2742"/>
        <source>Whether to include device serial number on device settings changes report</source>
        <translation>デバイス設定変更レポートにデバイスのシリアル番号を含めるかどうか</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2752"/>
        <source>Print reports in black and white, which can be more legible on non-color printers</source>
        <translation>レポートを白黒で印刷します。これにより、カラー以外のプリンターでも読みやすくなります</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2755"/>
        <source>Print reports in black and white (monochrome)</source>
        <translation>レポートを白黒（モノクロ）で印刷する</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2787"/>
        <source>Fonts (Application wide settings)</source>
        <translation>フォント (アプリケーション全体の設定)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2821"/>
        <source>Font</source>
        <translation>フォント</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2839"/>
        <source>Size</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2857"/>
        <source>Bold  </source>
        <translation>太字  </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2878"/>
        <source>Italic</source>
        <translation>イタリック</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2891"/>
        <source>Application</source>
        <translation>アプリケーション</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2955"/>
        <source>Graph Text</source>
        <translation>グラフの文字</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3016"/>
        <source>Graph Titles</source>
        <translation>グラフのタイトル</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3077"/>
        <source>Big  Text</source>
        <translation>大きな文字</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3143"/>
        <location filename="../oscar/preferencesdialog.cpp" line="472"/>
        <location filename="../oscar/preferencesdialog.cpp" line="604"/>
        <source>Details</source>
        <translation>詳細</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3175"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cキャンセル</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3182"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="467"/>
        <location filename="../oscar/preferencesdialog.cpp" line="598"/>
        <source>Name</source>
        <translation>名前</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="468"/>
        <location filename="../oscar/preferencesdialog.cpp" line="599"/>
        <source>Color</source>
        <translation>色</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="470"/>
        <source>Flag Type</source>
        <translation>フラグの種類</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="471"/>
        <location filename="../oscar/preferencesdialog.cpp" line="603"/>
        <source>Label</source>
        <translation>ラベル</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="488"/>
        <source>CPAP Events</source>
        <translation>CPAPイベント</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="489"/>
        <source>Oximeter Events</source>
        <translation>オキシメーターイベント</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="490"/>
        <source>Positional Events</source>
        <translation>位置イベント</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="491"/>
        <source>Sleep Stage Events</source>
        <translation>睡眠段階のイベント</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="492"/>
        <source>Unknown Events</source>
        <translation>未知のイベント</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="664"/>
        <source>Double click to change the descriptive name this channel.</source>
        <translation>ダブルクリックして、このチャネルのわかりやすい名前に変更します。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="542"/>
        <location filename="../oscar/preferencesdialog.cpp" line="671"/>
        <source>Double click to change the default color for this channel plot/flag/data.</source>
        <translation>ダブルクリックして、このチャネルのプロット、フラグ、データのデフォルトの色を変更します。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2322"/>
        <location filename="../oscar/preferencesdialog.ui" line="2361"/>
        <location filename="../oscar/preferencesdialog.cpp" line="469"/>
        <location filename="../oscar/preferencesdialog.cpp" line="600"/>
        <source>Overview</source>
        <translation>概要</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="66"/>
        <source>No CPAP devices detected</source>
        <translation>CPAPデバイスが検出されません</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="67"/>
        <source>Will you be using a ResMed brand device?</source>
        <translation>RedMedブランドの機器を使いますか？</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="74"/>
        <source>&lt;p&gt;&lt;b&gt;Please Note:&lt;/b&gt; OSCAR&apos;s advanced session splitting capabilities are not possible with &lt;b&gt;ResMed&lt;/b&gt; devices due to a limitation in the way their settings and summary data is stored, and therefore they have been disabled for this profile.&lt;/p&gt;&lt;p&gt;On ResMed devices, days will &lt;b&gt;split at noon&lt;/b&gt; like in ResMed&apos;s commercial software.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;ご注意:&lt;/b&gt; OSCAR の高度なセッション分割機能は、&lt;b&gt;ResMed&lt;/b&gt; デバイスでは設定と概要データの保存方法に制限があるため使用できません。そのため、このプロファイルでは無効になっています。&lt;/p&gt;&lt;p&gt;ResMed デバイスでは、ResMed の商用ソフトウェアと同様に、&lt;b&gt;正午に 1 日が分割されます&lt;/b&gt;。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="534"/>
        <source>Double click to change the descriptive name the &apos;%1&apos; channel.</source>
        <translation>ダブルクリックして、&apos;%1&apos; チャネルをわかりやすい名前に変更します。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="547"/>
        <source>Whether this flag has a dedicated overview chart.</source>
        <translation>このフラグに専用の概要チャートがあるかどうか。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="557"/>
        <source>Here you can change the type of flag shown for this event</source>
        <translation>ここで、このイベントについて表示されるフラグのタイムを変更できます</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="562"/>
        <location filename="../oscar/preferencesdialog.cpp" line="695"/>
        <source>This is the short-form label to indicate this channel on screen.</source>
        <translation>これは、画面上でこのチャネルを示す短い形式のラベルです。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="568"/>
        <location filename="../oscar/preferencesdialog.cpp" line="701"/>
        <source>This is a description of what this channel does.</source>
        <translation>これは、このチャネルの機能の説明です。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="601"/>
        <source>Lower</source>
        <translation>下限</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="602"/>
        <source>Upper</source>
        <translation>上限</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="621"/>
        <source>CPAP Waveforms</source>
        <translation>CPAP波形</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="622"/>
        <source>Oximeter Waveforms</source>
        <translation>オキシメーター波形</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="623"/>
        <source>Positional Waveforms</source>
        <translation>位置波形</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="624"/>
        <source>Sleep Stage Waveforms</source>
        <translation>睡眠の段階波形</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="680"/>
        <source>Whether a breakdown of this waveform displays in overview.</source>
        <translation>この波形の内訳を概要に表示するかどうか。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="685"/>
        <source>Here you can set the &lt;b&gt;lower&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>ここで、%1 波形の特定の計算に使用される&lt;b&gt;下限&lt;/b&gt;のしきい値を設定できます</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="690"/>
        <source>Here you can set the &lt;b&gt;upper&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>ここで、%1 波形の特定の計算に使用される&lt;b&gt;上限&lt;/b&gt;のしきい値を設定できます</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="800"/>
        <source>Data Processing Required</source>
        <translation>データの処理が必要です</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="801"/>
        <source>A data re/decompression proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>これらの変更を適用するには、データの再圧縮/圧縮解除手順が必要です。 この操作が完了するまでに数分かかる場合があります。

これらの変更を行ってもよろしいですか?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="809"/>
        <source>Data Reindex Required</source>
        <translation>データの再インデックスが必要</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="810"/>
        <source>A data reindexing proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>これらの変更を適用するには、データの再インデックス手順が必要です。 この操作が完了するまでに数分かかる場合があります。

これらの変更を行ってもよろしいですか?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="816"/>
        <source>Restart Required</source>
        <translation>再起動が必要</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="817"/>
        <source>One or more of the changes you have made will require this application to be restarted, in order for these changes to come into effect.

Would you like do this now?</source>
        <translation>行った変更の 1 つ以上を有効にするには、このアプリケーションを再起動する必要があります。

今すぐ再起動しますか？</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1173"/>
        <source>ResMed S9 devices routinely delete certain data from your SD card older than 7 and 30 days (depending on resolution).</source>
        <translation>ResMed S9 デバイスは、SD カードから 7 日および 30 日より古い特定のデータを定期的に削除します (解像度によって異なります)。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1174"/>
        <source> If you ever need to reimport this data again (whether in OSCAR or ResScan) this data won&apos;t come back.</source>
        <translation> このデータを再度再インポートする必要が生じた場合 (OSCAR か ResScan かに関係なく)、このデータは戻ってきません。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1175"/>
        <source> If you need to conserve disk space, please remember to carry out manual backups.</source>
        <translation> ディスク容量を節約する必要がある場合は、忘れずに手動バックアップを実行してください。</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1176"/>
        <source> Are you sure you want to disable these backups?</source>
        <translation> これらのバックアップを無効にしてもよろしいですか?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1256"/>
        <source>Switching off backups is not a good idea, because OSCAR needs these to rebuild the database if errors are found.

</source>
        <translation>エラーが見つかった場合、OSCAR はデータベースを再構築するためにバックアップを必要とするため、バックアップをオフにすることはお勧めできません。

</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1257"/>
        <source>Are you really sure you want to do this?</source>
        <translation>本当に実行しても良いですか？</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="50"/>
        <source>Flag</source>
        <translation>フラグ</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="51"/>
        <source>Minor Flag</source>
        <translation>マイナーフラグ</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="52"/>
        <source>Span</source>
        <translation>範囲</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="53"/>
        <source>Always Minor</source>
        <translation>常にマイナー</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="295"/>
        <source>Never</source>
        <translation>行わない</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1172"/>
        <source>This may not be a good idea</source>
        <translation>あまり良い考えではありません</translation>
    </message>
</context>
<context>
    <name>ProfileSelector</name>
    <message>
        <location filename="../oscar/profileselector.ui" line="26"/>
        <source>Filter:</source>
        <translation>フィルタ:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="36"/>
        <source>Reset filter to see all profiles</source>
        <translation>すべてのプロフィールを見るためにフィルターをリセット</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="198"/>
        <source>Version</source>
        <translation>バージョン</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="215"/>
        <source>&amp;Open Profile</source>
        <translation>&amp;O プロフィールを開く</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="226"/>
        <source>&amp;Edit Profile</source>
        <translation>&amp;E プロフィールを編集</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="240"/>
        <source>&amp;New Profile</source>
        <translation>&amp;N 新規プロフィール</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="258"/>
        <source>Profile: None</source>
        <translation>プロフィール: なし</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="278"/>
        <source>Please select or create a profile...</source>
        <translation>プロフィールを選択するか作成してください…</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="328"/>
        <source>Destroy Profile</source>
        <translation>プロフィールを消去する</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="90"/>
        <source>Profile</source>
        <translation>プロフィール</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="91"/>
        <source>Ventilator Brand</source>
        <translation>人工呼吸器のブランド</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="92"/>
        <source>Ventilator Model</source>
        <translation>人工呼吸器のモデル</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="93"/>
        <source>Other Data</source>
        <translation>他のデータ</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="94"/>
        <source>Last Imported</source>
        <translation>前回のインポート</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="95"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="170"/>
        <source>You must create a profile</source>
        <translation>プロフィールを作成しなければなりません</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="233"/>
        <location filename="../oscar/profileselector.cpp" line="367"/>
        <source>Enter Password for %1</source>
        <translation>%1 のパスワードを入力してください</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="249"/>
        <location filename="../oscar/profileselector.cpp" line="386"/>
        <source>You entered an incorrect password</source>
        <translation>誤ったパスワードが入力されました</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="252"/>
        <source>Forgot your password?</source>
        <translation>パスワードが分かりませんか?</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="252"/>
        <source>Ask on the forums how to reset it, it&apos;s actually pretty easy.</source>
        <translation>フォーラムでリセットの方法を聞いてください。意外と簡単です。</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="322"/>
        <source>Select a profile first</source>
        <translation>プロフィールを最初に選択してください</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="357"/>
        <source>The selected profile does not appear to contain any data and cannot be removed by OSCAR</source>
        <translation>選択されたプロフィールはデータが含まれておらず OSCAR で削除することができません</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="389"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to either reset it or delete the profile folder manually.</source>
        <translation>パスワードを忘れたために削除しようとしているのであれば、リセットするかプロフィールのフォルダを手動で削除する必要があります。</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="399"/>
        <source>You are about to destroy profile &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</source>
        <translation>プロフィール &lt;b&gt;%1&lt;/b&gt; を削除しようとしています。</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="399"/>
        <source>Think carefully, as this will irretrievably delete the profile along with all &lt;b&gt;backup data&lt;/b&gt; stored under&lt;br/&gt;%2.</source>
        <translation>ご注意ください。この操作はプロフィールと%2に保管されたすべての&lt;b&gt;バックアップデータ&lt;/b&gt;を復元不可能な形で消去します。</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="399"/>
        <source>Enter the word &lt;b&gt;DELETE&lt;/b&gt; below (exactly as shown) to confirm.</source>
        <translation>確認のため「表示されているとおり」&lt;b&gt;DELETE&lt;/b&gt;と入力してください。</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="417"/>
        <source>DELETE</source>
        <translation>削除</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="418"/>
        <source>Sorry</source>
        <translation>申し訳ないのですが</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="418"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>DELETEは大文字で入力してください。</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="431"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>プロフィールのディレクトリを削除する際にエラーが発生しました。手動で削除してください。</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="435"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>プロフィール %1 は正常に削除されました</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>Bytes</source>
        <translation>バイト</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>KB</source>
        <translation>KB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>TB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>PB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="465"/>
        <source>Summaries:</source>
        <translation>概要:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="466"/>
        <source>Events:</source>
        <translation>イベント:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="467"/>
        <source>Backups:</source>
        <translation>バックアップ:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="479"/>
        <location filename="../oscar/profileselector.cpp" line="519"/>
        <source>Hide disk usage information</source>
        <translation>ディスクの使用状況の情報を隠す</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="482"/>
        <source>Show disk usage information</source>
        <translation>ディスクの使用状況の情報を表示する</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="500"/>
        <source>Name: %1, %2</source>
        <translation>名前: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="503"/>
        <source>Phone: %1</source>
        <translation>電話番号: %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="506"/>
        <source>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation>Eメール: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="509"/>
        <source>Address:</source>
        <translation>住所:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="512"/>
        <source>No profile information given</source>
        <translation>プロフィールの情報がありません</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="515"/>
        <source>Profile: %1</source>
        <translation>プロフィール: %1</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="57"/>
        <source>Abort</source>
        <translation>中止</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../oscar/Graphs/MinutesAtPressure.cpp" line="820"/>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1260"/>
        <source>No Data</source>
        <translation>データなし</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="379"/>
        <source>Events</source>
        <translation>イベント</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="377"/>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="380"/>
        <source>Duration</source>
        <translation>範囲</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="394"/>
        <source>(% %1 in events)</source>
        <translation>(% %1 イベント中)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Jan</source>
        <translation>1月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Feb</source>
        <translation>2月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Mar</source>
        <translation>3月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Apr</source>
        <translation>4月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>May</source>
        <translation>5月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Jun</source>
        <translation>6月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Jul</source>
        <translation>7月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Aug</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Sep</source>
        <translation>9月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Oct</source>
        <translation>10月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Nov</source>
        <translation>11月</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Dec</source>
        <translation>12月</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="688"/>
        <source>ft</source>
        <translation>フィート</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="689"/>
        <source>lb</source>
        <translation>ポンド</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="690"/>
        <source>oz</source>
        <translation>オンス</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="692"/>
        <source>cmH2O</source>
        <translation>cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="220"/>
        <source>Med.</source>
        <translation>中央値.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="238"/>
        <source>Min: %1</source>
        <translation>最小: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="269"/>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="279"/>
        <source>Min: </source>
        <translation>最小: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="274"/>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="284"/>
        <source>Max: </source>
        <translation>最大: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="303"/>
        <source>Max: %1</source>
        <translation>最大: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="309"/>
        <source>%1 (%2 days): </source>
        <translation>%1 (%2日): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="311"/>
        <source>%1 (%2 day): </source>
        <translation>%1 (%2日): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="371"/>
        <source>% in %1</source>
        <translation>%1 (%)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="377"/>
        <location filename="../oscar/Graphs/gUsageChart.cpp" line="45"/>
        <location filename="../oscar/SleepLib/common.cpp" line="693"/>
        <source>Hours</source>
        <translation>時間</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="383"/>
        <source>Min %1</source>
        <translation>%1 分</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gUsageChart.cpp" line="30"/>
        <source>
Length: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gUsageChart.cpp" line="96"/>
        <source>%1 low usage, %2 no usage, out of %3 days (%4% compliant.) Length: %5 / %6 / %7</source>
        <translation>%1 低使用率、%2 使用率なし、%3 日のうち (%4% 準拠。) 長さ: %5 / %6 / %7</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="102"/>
        <source>Sessions: %1 / %2 / %3 Length: %4 / %5 / %6 Longest: %7 / %8 / %9</source>
        <translation>セッション: %1 / %2 / %3 長さ: %4 / %5 / %6 最長: %7 / %8 / %9</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="222"/>
        <source>%1
Length: %3
Start: %2
</source>
        <translation>%1
長さ: %3
開始: %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="224"/>
        <source>Mask On</source>
        <translation>マスク着用</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="224"/>
        <source>Mask Off</source>
        <translation>マスク非着用</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="235"/>
        <source>%1
Length: %3
Start: %2</source>
        <translation>%1
長さ: %3
開始: %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gTTIAChart.cpp" line="71"/>
        <source>TTIA:</source>
        <translation>TTIA:</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gTTIAChart.cpp" line="83"/>
        <source>
TTIA: %1</source>
        <translation>
TTIA: %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="694"/>
        <source>Minutes</source>
        <translation>分</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="695"/>
        <source>Seconds</source>
        <translation>秒</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="696"/>
        <source>h</source>
        <translation>時</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="697"/>
        <source>m</source>
        <translation>分</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="698"/>
        <source>s</source>
        <translation>秒</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="699"/>
        <source>ms</source>
        <translation>ミリ秒</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="700"/>
        <source>Events/hr</source>
        <translation>イベント/時</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="702"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="703"/>
        <source>bpm</source>
        <translation>bpm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="705"/>
        <source>Litres</source>
        <translation>リットル</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="706"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="707"/>
        <source>Breaths/min</source>
        <translation>呼吸数/分</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="710"/>
        <source>Severity (0-1)</source>
        <translation>重要度(0-1)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="711"/>
        <source>Degrees</source>
        <translation>度</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="714"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2920"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="715"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="865"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="866"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="867"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="716"/>
        <source>Information</source>
        <translation>お知らせ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="717"/>
        <source>Busy</source>
        <translation>ビジー</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="718"/>
        <source>Please Note</source>
        <translation>ご注意</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="722"/>
        <source>Graphs Switched Off</source>
        <translation>グラフはオフ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="724"/>
        <source>Sessions Switched Off</source>
        <translation>セッションはオフ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="728"/>
        <source>&amp;Yes</source>
        <translation>&amp;Y はい</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="729"/>
        <source>&amp;No</source>
        <translation>&amp;N いいえ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="730"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cキャンセル</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="731"/>
        <source>&amp;Destroy</source>
        <translation>&amp;D消去</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="732"/>
        <source>&amp;Save</source>
        <translation>&amp;S保存</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="734"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>BMI</source>
        <translation>BMI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="735"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="317"/>
        <source>Weight</source>
        <translation>体重</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="736"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Zombie</source>
        <translation>ゾンビ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="737"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>Pulse Rate</source>
        <translation>心拍</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="739"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="211"/>
        <source>Plethy</source>
        <translatorcomment>Refer to https://www.ei-navi.jp/dictionary/content/plethysmograph/</translatorcomment>
        <translation>プレシモグラフ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="740"/>
        <source>Pressure</source>
        <translation>圧力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="742"/>
        <source>Daily</source>
        <translation>日次</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="743"/>
        <source>Profile</source>
        <translation>プロフィール</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="744"/>
        <source>Overview</source>
        <translation>概要</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="745"/>
        <source>Oximetry</source>
        <translation>オキシメトリー</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="747"/>
        <source>Oximeter</source>
        <translation>オキシメーター</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="748"/>
        <source>Event Flags</source>
        <translation>イベントフラグ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="751"/>
        <source>Default</source>
        <translation>デフォルト</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="754"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="779"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2841"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="117"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="755"/>
        <source>BiPAP</source>
        <translation>BiPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="756"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2844"/>
        <source>Bi-Level</source>
        <translation>Bi-Level</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="757"/>
        <source>EPAP</source>
        <translation>EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="758"/>
        <source>EEPAP</source>
        <translation>EEPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="759"/>
        <source>Min EPAP</source>
        <translation>Min EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="760"/>
        <source>Max EPAP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="761"/>
        <source>IPAP</source>
        <translation>IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="762"/>
        <source>Min IPAP</source>
        <translation>Min IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="763"/>
        <source>Max IPAP</source>
        <translation>Max IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="764"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="118"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="765"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2846"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="124"/>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="766"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="311"/>
        <source>AVAPS</source>
        <translation>AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="767"/>
        <source>ST/ASV</source>
        <translation>ST/ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="769"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2905"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2932"/>
        <source>Humidifier</source>
        <translation>加湿器</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="771"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="772"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="162"/>
        <source>OA</source>
        <translation>OA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="773"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="775"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="160"/>
        <source>CA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="776"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="170"/>
        <source>FL</source>
        <translation>FL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="777"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="192"/>
        <source>SA</source>
        <translation>SA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="778"/>
        <source>LE</source>
        <translation>LE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="779"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="189"/>
        <source>EP</source>
        <translation>EP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="780"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>VS</source>
        <translation>VS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="782"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="176"/>
        <source>VS2</source>
        <translation>VS2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="783"/>
        <source>RERA</source>
        <translation>RERA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="784"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2832"/>
        <source>PP</source>
        <translation>PP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="785"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="786"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>RE</source>
        <translation>RE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="787"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>NR</source>
        <translation>NR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="788"/>
        <source>NRI</source>
        <translation>NRI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="789"/>
        <source>O2</source>
        <translation>O2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="790"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2849"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="217"/>
        <source>PC</source>
        <translation>PC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="791"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="792"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="198"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="793"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="201"/>
        <source>UF3</source>
        <translation>UF3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="795"/>
        <source>PS</source>
        <translation>PS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="796"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="273"/>
        <source>AHI</source>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="797"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="282"/>
        <source>RDI</source>
        <translation>RDI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="798"/>
        <source>AI</source>
        <translation>AI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="799"/>
        <source>HI</source>
        <translation>HI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="800"/>
        <source>UAI</source>
        <translation>UAI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="801"/>
        <source>CAI</source>
        <translation>CAI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="802"/>
        <source>FLI</source>
        <translation>FLI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="804"/>
        <source>REI</source>
        <translation>REI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="805"/>
        <source>EPI</source>
        <translation>EPI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="807"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="158"/>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="811"/>
        <source>IE</source>
        <translation>IE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="812"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>Insp. Time</source>
        <translation>吸入時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="813"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="255"/>
        <source>Exp. Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="814"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>Resp. Event</source>
        <translation>呼吸イベント</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="815"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Flow Limitation</source>
        <translation>流量制限値</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="816"/>
        <source>Flow Limit</source>
        <translation>流量制限</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="817"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1024"/>
        <source>SensAwake</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="818"/>
        <source>Pat. Trig. Breath</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="819"/>
        <source>Tgt. Min. Vent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="820"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Target Vent.</source>
        <translation>流出目標。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="821"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="239"/>
        <source>Minute Vent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="822"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Tidal Volume</source>
        <translation>一回換気量</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="823"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="242"/>
        <source>Resp. Rate</source>
        <translation>呼吸レート</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="824"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2804"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="236"/>
        <source>Snore</source>
        <translation>いびき</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="825"/>
        <source>Leak</source>
        <translation>漏れ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="826"/>
        <source>Leaks</source>
        <translation>漏れ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="827"/>
        <source>Large Leak</source>
        <translation>大きな漏れ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="828"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>LL</source>
        <translation>LL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="829"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="276"/>
        <source>Total Leaks</source>
        <translation>漏れ合計</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="830"/>
        <source>Unintentional Leaks</source>
        <translation>意図しない漏れ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="831"/>
        <source>MaskPressure</source>
        <translation>マスク圧力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="832"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="224"/>
        <source>Flow Rate</source>
        <translation>流量</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="833"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="333"/>
        <source>Sleep Stage</source>
        <translation>睡眠の段階</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="834"/>
        <source>Usage</source>
        <translation>使用</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="835"/>
        <source>Sessions</source>
        <translation>セッション</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="836"/>
        <source>Pr. Relief</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="847"/>
        <source>Device</source>
        <translation>機器</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="720"/>
        <source>No Data Available</source>
        <translation>データなし</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="221"/>
        <source>App key:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="209"/>
        <source>Operating system:</source>
        <translation>オペレーティングシステム:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="207"/>
        <source>Built with Qt %1 on %2</source>
        <translation>%2上でQt %1 でビルドされました</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="210"/>
        <source>Graphics Engine:</source>
        <translation>グラフィックエンジン:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="211"/>
        <source>Graphics Engine type:</source>
        <translation>グラフィックエンジンの種類:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="214"/>
        <source>Compiler:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="681"/>
        <source>Software Engine</source>
        <translation>ソフトウェアエンジン</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="682"/>
        <source>ANGLE / OpenGLES</source>
        <translation>ANGLE / OpenGLES</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="683"/>
        <source>Desktop OpenGL</source>
        <translation>デスクトップ OpenGL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="685"/>
        <source> m</source>
        <translation> m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="686"/>
        <source> cm</source>
        <translation> cm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="687"/>
        <source>in</source>
        <translation>の中</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="691"/>
        <source>kg</source>
        <translation>kg</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="704"/>
        <source>l/min</source>
        <translation>l/min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="721"/>
        <source>Only Settings and Compliance Data Available</source>
        <translation>設定とコンプライアンスデータのみ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="723"/>
        <source>Summary Data Only</source>
        <translation>概要データのみ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="838"/>
        <source>Bookmarks</source>
        <translation>ブックマーク</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="842"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="774"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2836"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2838"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="113"/>
        <source>Mode</source>
        <translation>モード</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="843"/>
        <source>Model</source>
        <translation>モデル</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="844"/>
        <source>Brand</source>
        <translation>ブランド</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="845"/>
        <source>Serial</source>
        <translation>シリアル番号</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="846"/>
        <source>Series</source>
        <translation>シリーズ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="848"/>
        <source>Channel</source>
        <translation>チャンネル</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="849"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="851"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="289"/>
        <source>Inclination</source>
        <translation>傾斜</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="852"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="286"/>
        <source>Orientation</source>
        <translation>方向</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="853"/>
        <source>Motion</source>
        <translation>動作</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="855"/>
        <source>Name</source>
        <translation>名前</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="856"/>
        <source>DOB</source>
        <translation>誕生日</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="857"/>
        <source>Phone</source>
        <translation>電話番号</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="858"/>
        <source>Address</source>
        <translation>住所</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="859"/>
        <source>Email</source>
        <translation>電子メール</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="860"/>
        <source>Patient ID</source>
        <translation>患者ID</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="861"/>
        <source>Date</source>
        <translation>日付</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="863"/>
        <source>Bedtime</source>
        <translation>就寝時刻</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="864"/>
        <source>Wake-up</source>
        <translation>起床</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="865"/>
        <source>Mask Time</source>
        <translation>マスク時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="866"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="129"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="206"/>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="126"/>
        <source>Unknown</source>
        <translation>不明</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="867"/>
        <source>None</source>
        <translation>なし</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="868"/>
        <source>Ready</source>
        <translation>準備完了</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="870"/>
        <source>First</source>
        <translation>最初</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="871"/>
        <source>Last</source>
        <translation>最後</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="872"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Start</source>
        <translation>開始</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="873"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>End</source>
        <translation>終了</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="874"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="192"/>
        <source>On</source>
        <translation>オン</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="875"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="792"/>
        <source>Off</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="877"/>
        <source>Yes</source>
        <translation>はい</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="878"/>
        <source>No</source>
        <translation>いいえ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="880"/>
        <source>Min</source>
        <translation>分</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="881"/>
        <source>Max</source>
        <translation>最大</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="882"/>
        <source>Med</source>
        <translation>中間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="884"/>
        <source>Average</source>
        <translation>平均</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="885"/>
        <source>Median</source>
        <translation>中央値</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="224"/>
        <location filename="../oscar/SleepLib/common.cpp" line="886"/>
        <source>Avg</source>
        <translation>平均</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="222"/>
        <location filename="../oscar/SleepLib/common.cpp" line="887"/>
        <source>W-Avg</source>
        <translation>加重平均</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="154"/>
        <source>Your %1 %2 (%3) generated data that OSCAR has never seen before.</source>
        <translation>お使いの%1 %2 (%3) で作成されたデータは、OSCARで認識できない形式です。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="155"/>
        <source>The imported data may not be entirely accurate, so the developers would like a .zip copy of this device&apos;s SD card and matching clinician .pdf reports to make sure OSCAR is handling the data correctly.</source>
        <translation>インポートされたデータは完全に正確ではない可能性があるため、OSCAR がデータを正しく処理していることを確認するために、開発者はこのデバイスの SD カードの .zip コピーと一致する臨床医の .pdf レポートを必要としています。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="164"/>
        <source>Non Data Capable Device</source>
        <translation>非データ対応機器</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="165"/>
        <source>Your %1 CPAP Device (Model %2) is unfortunately not a data capable model.</source>
        <translation>残念ながら、%1 CPAP デバイス (モデル %2) はデータ対応モデルではありません。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="166"/>
        <source>I&apos;m sorry to report that OSCAR can only track hours of use and very basic settings for this device.</source>
        <translation>申し訳ありませんが、OSCAR が追跡できるのは、このデバイスの使用時間と非常に基本的な設定のみです。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="178"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="454"/>
        <source>Device Untested</source>
        <translation>未テストの機器</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="179"/>
        <source>Your %1 CPAP Device (Model %2) has not been tested yet.</source>
        <translation>%1 CPAP デバイス (モデル %2) はまだテストされていません。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="180"/>
        <source>It seems similar enough to other devices that it might work, but the developers would like a .zip copy of this device&apos;s SD card and matching clinician .pdf reports to make sure it works with OSCAR.</source>
        <translation>動作する可能性がある他のデバイスと十分に似ているように見えますが、開発者は、このデバイスの SD カードの .zip コピーと、担当臨床医の .pdf レポートで OSCAR で動作することを確認したいと考えています。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="188"/>
        <source>Device Unsupported</source>
        <translation>サポートされない機器</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="189"/>
        <source>Sorry, your %1 CPAP Device (%2) is not supported yet.</source>
        <translation>申し訳ありませんが、%1 CPAP デバイス (%2) はまだサポートされていません。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="190"/>
        <source>The developers need a .zip copy of this device&apos;s SD card and matching clinician .pdf reports to make it work with OSCAR.</source>
        <translation>開発者は、OSCARで正しく動くようにするため、このデバイスの SD カードの .zip コピーと、担当臨床医の .pdf レポートが必要です。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2694"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="523"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="872"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="225"/>
        <source>Getting Ready...</source>
        <translation>準備中...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="535"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="897"/>
        <source>Scanning Files...</source>
        <translation>ファイルをスキャン中...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="628"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="905"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="743"/>
        <location filename="../oscar/mainwindow.cpp" line="2338"/>
        <source>Importing Sessions...</source>
        <translation>セッションをインポート中...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="778"/>
        <source>UNKNOWN</source>
        <translation>不明</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="780"/>
        <source>APAP (std)</source>
        <translation>APAP (std)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="781"/>
        <source>APAP (dyn)</source>
        <translation>APAP (dyn)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="782"/>
        <source>Auto S</source>
        <translation>Auto S</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="783"/>
        <source>Auto S/T</source>
        <translation>Auto S/T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="784"/>
        <source>AcSV</source>
        <translation>AcSV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="788"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="789"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="790"/>
        <source>SoftPAP Mode</source>
        <translation>SoftPAP モード</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="793"/>
        <source>Slight</source>
        <translation>Slight</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="797"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="798"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="799"/>
        <source>PSoft</source>
        <translation>PSoft</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="803"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="804"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="805"/>
        <source>PSoftMin</source>
        <translation>PSoftMin</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="809"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="810"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="811"/>
        <source>AutoStart</source>
        <translation>自動スタート</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="817"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="818"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="819"/>
        <source>Softstart_Time</source>
        <translation>Softstart_Time</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="823"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="824"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="825"/>
        <source>Softstart_TimeMax</source>
        <translation>Softstart_TimeMax</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="829"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="830"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="831"/>
        <source>Softstart_Pressure</source>
        <translation>Softstart_Pressure</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="835"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="836"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="837"/>
        <source>PMaxOA</source>
        <translation>PMaxOA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="841"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="842"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="843"/>
        <source>EEPAPMin</source>
        <translation>EEPAPMin</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="847"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="848"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="849"/>
        <source>EEPAPMax</source>
        <translation>EEPAPMax</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="853"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="854"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="855"/>
        <source>HumidifierLevel</source>
        <translation>加湿器レベル</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="859"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="860"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="861"/>
        <source>TubeType</source>
        <translation>チューブの種類</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="875"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="878"/>
        <source>ObstructLevel</source>
        <translation>障害物レベル</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="877"/>
        <source>Obstruction Level</source>
        <translation>障害レベル</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="885"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="887"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="888"/>
        <source>rMVFluctuation</source>
        <translation>rMVFluctuation</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="895"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="897"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="898"/>
        <source>rRMV</source>
        <translation>rRMV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="903"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="905"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="906"/>
        <source>PressureMeasured</source>
        <translation>計測された圧力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="911"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="913"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="914"/>
        <source>FlowFull</source>
        <translation>FlowFull</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="919"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="921"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="922"/>
        <source>SPRStatus</source>
        <translation>SPRStatus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="928"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="930"/>
        <source>Artifact</source>
        <translation>SPRStatus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="931"/>
        <source>ART</source>
        <translation>ART</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="936"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="938"/>
        <source>CriticalLeak</source>
        <translation>重大な漏れ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="939"/>
        <source>CL</source>
        <translation>CL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="944"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="946"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="947"/>
        <source>eMO</source>
        <translation>eMO</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="953"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="955"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="956"/>
        <source>eSO</source>
        <translation>eSO</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="962"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="964"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="965"/>
        <source>eS</source>
        <translation>eS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="971"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="973"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="974"/>
        <source>eFL</source>
        <translation>eFL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="980"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="982"/>
        <source>DeepSleep</source>
        <translation>深い眠り</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="983"/>
        <source>DS</source>
        <translation>DS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="990"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="992"/>
        <source>TimedBreath</source>
        <translation>時間による呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2748"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="368"/>
        <location filename="../oscar/mainwindow.cpp" line="754"/>
        <location filename="../oscar/mainwindow.cpp" line="2360"/>
        <source>Finishing up...</source>
        <translation>終了中...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2878"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2880"/>
        <source>Flex Lock</source>
        <translation>フレックスロック</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2879"/>
        <source>Whether Flex settings are available to you.</source>
        <translation>Flex設定が利用できるかどうか。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2888"/>
        <source>Amount of time it takes to transition from EPAP to IPAP, the higher the number the slower the transition</source>
        <translation>EPAPからIPAPに移行するためにの時間で、より大きな数字がゆっくりとした移行です</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2894"/>
        <source>Rise Time Lock</source>
        <translation>立ち上がり時間ロック</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2895"/>
        <source>Whether Rise Time settings are available to you.</source>
        <translation>立ち上がり時間設定が利用できるかどうか。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2896"/>
        <source>Rise Lock</source>
        <translation>立ち上がりロック</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2949"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2950"/>
        <source>Mask Resistance Setting</source>
        <translation>マスク抵抗設定</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2951"/>
        <source>Mask Resist.</source>
        <translation>マスク抵抗.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2959"/>
        <source>Hose Diam.</source>
        <translation>ホース径.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2962"/>
        <source>15mm</source>
        <translation>15mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2961"/>
        <source>22mm</source>
        <translation>22mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="526"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="888"/>
        <source>Backing Up Files...</source>
        <translation>ファイルをバックアップしています...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="153"/>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="92"/>
        <source>Untested Data</source>
        <translation>テストされていないデータ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="963"/>
        <source>model %1</source>
        <translation>モデル %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="966"/>
        <source>unknown model</source>
        <translation>認識できないモデル</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2840"/>
        <source>CPAP-Check</source>
        <translation>CPAPチェック</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2842"/>
        <source>AutoCPAP</source>
        <translation>AutoCPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2843"/>
        <source>Auto-Trial</source>
        <translation>Auto-Trial</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2845"/>
        <source>AutoBiLevel</source>
        <translation>AutoBiLevel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2847"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2848"/>
        <source>S/T</source>
        <translation>S/T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2850"/>
        <source>S/T - AVAPS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2851"/>
        <source>PC - AVAPS</source>
        <translation>PC - AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2854"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2856"/>
        <source>Flex Mode</source>
        <translation>フレックスモード</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2855"/>
        <source>PRS1 pressure relief mode.</source>
        <translation>PRS1圧力リリーフモード。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2859"/>
        <source>C-Flex</source>
        <translation>C-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2860"/>
        <source>C-Flex+</source>
        <translation>C-Flex+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2861"/>
        <source>A-Flex</source>
        <translation>A-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2862"/>
        <source>P-Flex</source>
        <translation>P-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2863"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2887"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2889"/>
        <source>Rise Time</source>
        <translation>立ち上がり時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2864"/>
        <source>Bi-Flex</source>
        <translation>Bi-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2866"/>
        <source>Flex</source>
        <translation>Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2870"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2872"/>
        <source>Flex Level</source>
        <translation>Flexレベル</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2871"/>
        <source>PRS1 pressure relief setting.</source>
        <translation>PRS1圧力リリーフ設定。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2919"/>
        <source>Passover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2940"/>
        <source>Target Time</source>
        <translation>目標時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2941"/>
        <source>PRS1 Humidifier Target Time</source>
        <translation>PRS1加湿器目標時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2942"/>
        <source>Hum. Tgt Time</source>
        <translation>加湿器目標時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2967"/>
        <source>Tubing Type Lock</source>
        <translation>チューブの種類のロック</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2968"/>
        <source>Whether tubing type settings are available to you.</source>
        <translation>チューブ タイプの設定を使用できるかどうか。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2969"/>
        <source>Tube Lock</source>
        <translation>チューブロック</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2976"/>
        <source>Mask Resistance Lock</source>
        <translation>マスク抵抗ロック</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2977"/>
        <source>Whether mask resistance settings are available to you.</source>
        <translation>マスク抵抗設定が利用できるかどうか。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2978"/>
        <source>Mask Res. Lock</source>
        <translation>マスク抵抗ロック</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2986"/>
        <source>A few breaths automatically starts device</source>
        <translation>少し息をすると機器が自動で動き始めます</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2995"/>
        <source>Device automatically switches off</source>
        <translation>デバイスは自動で電源が切れます</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3004"/>
        <source>Whether or not device allows Mask checking.</source>
        <translation>デバイスがマスク チェックを許可するかどうかを示します。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3021"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3023"/>
        <source>Ramp Type</source>
        <translation>ランプの種類</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3022"/>
        <source>Type of ramp curve to use.</source>
        <translation>使用するランプカーブの種類。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3025"/>
        <source>Linear</source>
        <translation>線形</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3026"/>
        <source>SmartRamp</source>
        <translation>スマートランプ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3027"/>
        <source>Ramp+</source>
        <translation>ランプ+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3031"/>
        <source>Backup Breath Mode</source>
        <translation>バックアップ呼吸モード</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3032"/>
        <source>The kind of backup breath rate in use: none (off), automatic, or fixed</source>
        <translation>使用中のバックアップ呼吸数の種類: なし (オフ)、自動、または固定</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3033"/>
        <source>Breath Rate</source>
        <translation>呼吸レート</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3037"/>
        <source>Fixed</source>
        <translation>固定</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3041"/>
        <source>Fixed Backup Breath BPM</source>
        <translation>固定バックアップ呼吸 BPM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3042"/>
        <source>Minimum breaths per minute (BPM) below which a timed breath will be initiated</source>
        <translation>1 分あたりの最小呼吸数 (BPM) を下回ると、時間指定された呼吸が開始されます</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3043"/>
        <source>Breath BPM</source>
        <translation>ブレスBPM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3048"/>
        <source>Timed Inspiration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3049"/>
        <source>The time that a timed breath will provide IPAP before transitioning to EPAP</source>
        <translation>The time that a timed breath will provide IPAP before transitioning to EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3050"/>
        <source>Timed Insp.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3055"/>
        <source>Auto-Trial Duration</source>
        <translation>自動試行時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3057"/>
        <source>Auto-Trial Dur.</source>
        <translation>自動試行時間.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3062"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3064"/>
        <source>EZ-Start</source>
        <translation>EZ-Start</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3063"/>
        <source>Whether or not EZ-Start is enabled</source>
        <translation>EZ-Startrが有効かどうか</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3071"/>
        <source>Variable Breathing</source>
        <translation>Variable Breathing</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3072"/>
        <source>UNCONFIRMED: Possibly variable breathing, which are periods of high deviation from the peak inspiratory flow trend</source>
        <translation>未確認: 呼吸が変動している可能性があります。これは、ピーク吸気フロー トレンドから大きく逸脱している期間です</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3081"/>
        <source>A period during a session where the device could not detect flow.</source>
        <translation>デバイスがフローを検出できなかったセッション中の期間。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3095"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3097"/>
        <source>Peak Flow</source>
        <translation>ピークフロー</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3096"/>
        <source>Peak flow during a 2-minute interval</source>
        <translation>2分毎のピークフロー</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2903"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="153"/>
        <source>Humidifier Status</source>
        <translation>加湿器の状況</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2904"/>
        <source>PRS1 humidifier connected?</source>
        <translation>PRS1加湿器は接続されていますか？</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2907"/>
        <source>Disconnected</source>
        <translation>切断されました</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2908"/>
        <source>Connected</source>
        <translation>接続されました</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2912"/>
        <source>Humidification Mode</source>
        <translation>加湿モード</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2913"/>
        <source>PRS1 Humidification Mode</source>
        <translation>PRS1 加湿器モード</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2914"/>
        <source>Humid. Mode</source>
        <translation>加湿モード</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2916"/>
        <source>Fixed (Classic)</source>
        <translation>固定（クラシック）</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2917"/>
        <source>Adaptive (System One)</source>
        <translation>アダプティブ (システム 1)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2918"/>
        <source>Heated Tube</source>
        <translation>加熱チューブ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2924"/>
        <source>Tube Temperature</source>
        <translation>チューブ温度</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2925"/>
        <source>PRS1 Heated Tube Temperature</source>
        <translation>PRS1 加熱チューブ温度</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2926"/>
        <source>Tube Temp.</source>
        <translation>チューブ温度.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2933"/>
        <source>PRS1 Humidifier Setting</source>
        <translation>PRS1 加湿器設定</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2957"/>
        <source>Hose Diameter</source>
        <translation>ホース径</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2958"/>
        <source>Diameter of primary CPAP hose</source>
        <translation>主の CPAP のホースの直径</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2963"/>
        <source>12mm</source>
        <translation>12mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2985"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2987"/>
        <source>Auto On</source>
        <translation>自動オン</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2994"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2996"/>
        <source>Auto Off</source>
        <translation>自動オフ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3003"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3005"/>
        <source>Mask Alert</source>
        <translation>マスクアラート</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3012"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3014"/>
        <source>Show AHI</source>
        <translation>AHIを表示</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3013"/>
        <source>Whether or not device shows AHI via built-in display.</source>
        <translation>デバイスが内蔵画面にAHIを表示するかどうか。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3056"/>
        <source>The number of days in the Auto-CPAP trial period, after which the device will revert to CPAP</source>
        <translation>デバイスが CPAP に戻るまでの Auto-CPAP 試用期間の日数</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3080"/>
        <source>Breathing Not Detected</source>
        <translation>呼吸が検出されない</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3082"/>
        <source>BND</source>
        <translation>BND</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3087"/>
        <source>Timed Breath</source>
        <translation>呼吸時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3088"/>
        <source>Machine Initiated Breath</source>
        <translation>機械による呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="993"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3089"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="37"/>
        <source>Windows User</source>
        <translation>Windowsユーザー</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="202"/>
        <source>Using </source>
        <translation>使用している </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="202"/>
        <source>, found SleepyHead -
</source>
        <translation>, 検出された Sleepy Head -
</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="203"/>
        <source>You must run the OSCAR Migration Tool</source>
        <translation>You must run the OSCAR Migration Tool</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="485"/>
        <source>Launching Windows Explorer failed</source>
        <translation>Windows エクスプローラーの起動に失敗しました</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="486"/>
        <source>Could not find explorer.exe in path to launch Windows Explorer.</source>
        <translation>Windows エクスプローラーを起動するためのパスに explorer.exe が見つかりませんでした。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="538"/>
        <source>OSCAR %1 needs to upgrade its database for %2 %3 %4</source>
        <translation>OSCAR %1 は %2 %3 %4 のデータベースをアップグレードする必要があります</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="551"/>
        <source>&lt;b&gt;OSCAR maintains a backup of your devices data card that it uses for this purpose.&lt;/b&gt;</source>
        <translation>&lt;b&gt;OSCAR は、この目的のために使用するデバイスのデータ カードのバックアップを保持しています。&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="552"/>
        <source>&lt;i&gt;Your old device data should be regenerated provided this backup feature has not been disabled in preferences during a previous data import.&lt;/i&gt;</source>
        <translation>&lt;i&gt;前回のデータ インポート時に設定でこのバックアップ機能が無効にされていなければ、古いデバイス データを再生成する必要があります。&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="555"/>
        <source>OSCAR does not yet have any automatic card backups stored for this device.</source>
        <translation>OSCAR には、このデバイス用に保存された自動カード バックアップがまだありません。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="556"/>
        <source>This means you will need to import this device data again afterwards from your own backups or data card.</source>
        <translation>これは、後でこのデバイス データをご自分のバックアップまたはデータ カードから再度インポートする必要があることを意味します。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="559"/>
        <source>Important:</source>
        <translation>重要:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="560"/>
        <source>If you are concerned, click No to exit, and backup your profile manually, before starting OSCAR again.</source>
        <translation>心配な場合は、[いいえ] をクリックして終了し、プロフィールを手動でバックアップしてから、OSCAR を再度開始してください。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="561"/>
        <source>Are you ready to upgrade, so you can run the new version of OSCAR?</source>
        <translation>新しいバージョンの OSCAR を実行できるように、アップグレードする準備はできていますか?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="564"/>
        <source>Device Database Changes</source>
        <translation>デバイス データベースの変更</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="575"/>
        <source>Sorry, the purge operation failed, which means this version of OSCAR can&apos;t start.</source>
        <translation>申し訳ありませんが、パージ操作に失敗しました。つまり、このバージョンの OSCAR を開始できません。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="576"/>
        <source>The device data folder needs to be removed manually.</source>
        <translation>デバイス データ フォルダは手動で削除する必要があります。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="594"/>
        <source>Would you like to switch on automatic backups, so next time a new version of OSCAR needs to do so, it can rebuild from these?</source>
        <translation>自動バックアップをオンにしますか?次回 OSCAR の新しいバージョンが必要になったときに、これらから再構築できるようにしますか?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="601"/>
        <source>OSCAR will now start the import wizard so you can reinstall your %1 data.</source>
        <translation>OSCAR はインポート ウィザードを開始し、%1 データを再インストールできるようにします。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="611"/>
        <source>OSCAR will now exit, then (attempt to) launch your computers file manager so you can manually back your profile up:</source>
        <translation>OSCAR が終了し、コンピューターのファイル マネージャーを起動し（ようと試み）、プロファイルを手動でバックアップできるようにします:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="613"/>
        <source>Use your file manager to make a copy of your profile directory, then afterwards, restart OSCAR and complete the upgrade process.</source>
        <translation>ファイル マネージャを使用してプロフィールディレクトリのコピーを作成し、その後、OSCAR を再起動してアップグレード プロセスを完了してください。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="559"/>
        <source>Once you upgrade, you &lt;font size=+1&gt;cannot&lt;/font&gt; use this profile with the previous version anymore.</source>
        <translation>アップグレードすると、以前のバージョンでこのプロファイルを&lt;font size=+1&gt;使用できなくなります&lt;/font&gt;。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="577"/>
        <source>This folder currently resides at the following location:</source>
        <translation>このフォルダーは現在、次の場所にあります:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="585"/>
        <source>Rebuilding from %1 Backup</source>
        <translation>%1 バックアップからの再構築しています</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Therapy Pressure</source>
        <translation>治療圧力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Inspiratory Pressure</source>
        <translation>吸気圧</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="136"/>
        <source>Lower Inspiratory Pressure</source>
        <translation>吸気圧を下げる</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="137"/>
        <source>Higher Inspiratory Pressure</source>
        <translation>吸気圧を上げる</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>Expiratory Pressure</source>
        <translation>呼気圧</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="139"/>
        <source>Lower Expiratory Pressure</source>
        <translation>吸気圧を下げる</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>Higher Expiratory Pressure</source>
        <translation>吸気圧を上げる</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="142"/>
        <source>Pressure Support</source>
        <translation>プレッシャーサポート</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>PS Min</source>
        <translation>プレッシャーサポート分</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>Pressure Support Minimum</source>
        <translation>プレッシャーサポート最低値</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>PS Max</source>
        <translation>プレッシャーサポート最大値</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>Pressure Support Maximum</source>
        <translation>プレッシャーサポート最大値</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Min Pressure</source>
        <translation>最低圧力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Minimum Therapy Pressure</source>
        <translation>治療最低圧力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>Max Pressure</source>
        <translation>最大圧力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>Maximum Therapy Pressure</source>
        <translation>治療最大圧力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>Ramp Time</source>
        <translation>ランプ時間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>Ramp Delay Period</source>
        <translation>ランプ遅延期間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="148"/>
        <source>Ramp Pressure</source>
        <translation>ランプ圧力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="148"/>
        <source>Starting Ramp Pressure</source>
        <translation>開示指示ランプ圧力</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <source>Ramp Event</source>
        <translation>ランプイベント</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="209"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1041"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <source>Ramp</source>
        <translation>ランプ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="156"/>
        <source>An abnormal period of Cheyne Stokes Respiration</source>
        <translation>チェーンストークス呼吸の異常な期間</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="156"/>
        <source>Cheyne Stokes Respiration (CSR)</source>
        <translation>チェーンストークス呼吸</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="158"/>
        <source>Periodic Breathing (PB)</source>
        <translation>周期的な呼吸 (PB)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="160"/>
        <source>Clear Airway (CA)</source>
        <translation>気道確保 (CA)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="162"/>
        <source>Obstructive Apnea (OA)</source>
        <translation>Obstructive Apnea (OA)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>Hypopnea (H)</source>
        <translation>Hypopnea (H)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>An apnea that couldn&apos;t be determined as Central or Obstructive.</source>
        <translation>中枢性または閉塞性と判断できない無呼吸。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>Unclassified Apnea (UA)</source>
        <translation>未分類の無呼吸 (UA)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>Apnea (A)</source>
        <translation>無呼吸 (A)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>An apnea reportred by your CPAP device.</source>
        <translation>CPAP デバイスによって報告された無呼吸。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="170"/>
        <source>A restriction in breathing from normal, causing a flattening of the flow waveform.</source>
        <translation>通常よりも呼吸が制限され、フロー波形が平坦化します。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="170"/>
        <source>Flow Limitation (FL)</source>
        <translation>流量制限 (FL)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>RERA (RE)</source>
        <translation>RERA (RE)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>Vibratory Snore (VS)</source>
        <translation>振動いびき（VS）</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="176"/>
        <source>Vibratory Snore (VS2) </source>
        <translation>振動いびき（VS2） </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="179"/>
        <source>Leak Flag (LF)</source>
        <translation>リークフラグ (LF)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="179"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>A large mask leak affecting device performance.</source>
        <translation>デバイスのパフォーマンスに影響を与える大きなマスクの漏れ。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>Large Leak (LL)</source>
        <translation>大きな漏れ (LL)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>Non Responding Event (NR)</source>
        <translation>Non Responding Event (NR)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="189"/>
        <source>Expiratory Puff (EP)</source>
        <translation>呼気パフ (EP)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="192"/>
        <source>SensAwake (SA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>User Flag #1 (UF1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="198"/>
        <source>User Flag #2 (UF2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="201"/>
        <source>User Flag #3 (UF3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="217"/>
        <source>Pulse Change (PC)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="220"/>
        <source>SpO2 Drop (SD)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>A ResMed data item: Trigger Cycle Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="273"/>
        <source>Apnea Hypopnea Index (AHI)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="282"/>
        <source>Respiratory Disturbance Index (RDI)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="295"/>
        <source>Mask On Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="295"/>
        <source>Time started according to str.edf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="298"/>
        <source>Summary Only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="160"/>
        <source>An apnea where the airway is open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="162"/>
        <source>An apnea caused by airway obstruction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>A partially obstructed airway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="774"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>UA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>A vibratory snore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2830"/>
        <source>Pressure Pulse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2831"/>
        <source>A pulse of pressure &apos;pinged&apos; to detect a closed airway.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>A type of respiratory event that won&apos;t respond to a pressure increase.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="189"/>
        <source>Intellipap event where you breathe out your mouth.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="192"/>
        <source>SensAwake feature will reduce pressure when waking is detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>Heart rate in beats per minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="208"/>
        <source>Blood-oxygen saturation percentage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="211"/>
        <source>Plethysomogram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="211"/>
        <source>An optical Photo-plethysomogram showing heart rhythm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="217"/>
        <source>A sudden (user definable) change in heart rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="220"/>
        <source>A sudden (user definable) drop in blood oxygen saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="220"/>
        <source>SD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="224"/>
        <source>Breathing flow rate waveform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="227"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Mask Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Amount of air displaced per breath</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="236"/>
        <source>Graph displaying snore volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="239"/>
        <source>Minute Ventilation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="239"/>
        <source>Amount of air displaced per minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="242"/>
        <source>Respiratory Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="242"/>
        <source>Rate of breaths per minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="245"/>
        <source>Patient Triggered Breaths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="245"/>
        <source>Percentage of breaths triggered by patient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="245"/>
        <source>Pat. Trig. Breaths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="248"/>
        <source>Leak Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="248"/>
        <source>Rate of detected mask leakage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>I:E Ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>Ratio between Inspiratory and Expiratory time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="709"/>
        <source>ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Pressure Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>Pressure Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>Pressure Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>Pressure Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>IPAP Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>IPAP Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>EPAP Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>EPAP Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="806"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="156"/>
        <source>CSR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="158"/>
        <source>An abnormal period of Periodic Breathing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="179"/>
        <source>LF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="198"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="201"/>
        <source>A user definable event detected by OSCAR&apos;s flow waveform processor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="214"/>
        <source>Perfusion Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="214"/>
        <source>A relative assessment of the pulse strength at the monitoring site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="214"/>
        <source>Perf. Index %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Mask Pressure (High frequency)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="255"/>
        <source>Expiratory Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="255"/>
        <source>Time taken to breathe out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>Inspiratory Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>Time taken to breathe in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>Respiratory Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Graph showing severity of flow limitations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Flow Limit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Target Minute Ventilation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>Maximum Leak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>The maximum rate of mask leakage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>Max Leaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="273"/>
        <source>Graph showing running AHI for the past hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="276"/>
        <source>Total Leak Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="276"/>
        <source>Detected mask leakage including natural Mask leakages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="279"/>
        <source>Median Leak Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="279"/>
        <source>Median rate of detected mask leakage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="279"/>
        <source>Median Leaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="282"/>
        <source>Graph showing running RDI for the past hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="286"/>
        <source>Sleep position in degrees</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="289"/>
        <source>Upright angle in degrees</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="292"/>
        <source>Movement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="292"/>
        <source>Movement detector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="298"/>
        <source>CPAP Session contains summary data only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="775"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="776"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2837"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="301"/>
        <source>PAP Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="121"/>
        <source>Couldn&apos;t parse Channels.xml, OSCAR cannot continue and is exiting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="141"/>
        <source>End Expiratory Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>Respiratory Effort Related Arousal: A restriction in breathing that causes either awakening or sleep disturbance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="176"/>
        <source>A vibratory snore as detected by a System One device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="301"/>
        <source>PAP Device Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="305"/>
        <source>APAP (Variable)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="309"/>
        <source>ASV (Fixed EPAP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="310"/>
        <source>ASV (Variable EPAP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="318"/>
        <source>Height</source>
        <translation type="unfinished">身長</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="318"/>
        <source>Physical Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="319"/>
        <source>Notes</source>
        <translation type="unfinished">ノート</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="319"/>
        <source>Bookmark Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>Body Mass Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>How you feel (0 = like crap, 10 = unstoppable)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Bookmark Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>Bookmark End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Last Updated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>Journal Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>Journal</source>
        <translation type="unfinished">日誌</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="333"/>
        <source>1=Awake 2=REM 3=Light Sleep 4=Deep Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="335"/>
        <source>Brain Wave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="335"/>
        <source>BrainWave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="336"/>
        <source>Awakenings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="336"/>
        <source>Number of Awakenings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="337"/>
        <source>Morning Feel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="337"/>
        <source>How you felt in the morning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="338"/>
        <source>Time Awake</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="338"/>
        <source>Time spent awake</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>Time In REM Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>Time spent in REM Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>Time in REM Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="340"/>
        <source>Time In Light Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="340"/>
        <source>Time spent in light sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="340"/>
        <source>Time in Light Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="341"/>
        <source>Time In Deep Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="341"/>
        <source>Time spent in deep sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="341"/>
        <source>Time in Deep Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="342"/>
        <source>Time to Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="342"/>
        <source>Time taken to get to sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Zeo ZQ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Zeo sleep quality measurement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>ZEO ZQ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="354"/>
        <source>Debugging channel #1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="354"/>
        <source>Test #1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="354"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="355"/>
        <source>For internal use only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="355"/>
        <source>Debugging channel #2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="355"/>
        <source>Test #2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="779"/>
        <source>Zero</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="782"/>
        <source>Upper Threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="785"/>
        <source>Lower Threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="525"/>
        <source>As you did not select a data folder, OSCAR will exit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="194"/>
        <source>or CANCEL to skip migration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="193"/>
        <source>Choose the SleepyHead or OSCAR data folder to migrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="208"/>
        <source>The folder you chose does not contain valid SleepyHead or OSCAR data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="209"/>
        <source>You cannot use this folder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="224"/>
        <source>Migrating </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="224"/>
        <source> files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="225"/>
        <source>from </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="225"/>
        <source>to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="343"/>
        <source>OSCAR crashed due to an incompatibility with your graphics hardware.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="344"/>
        <source>To resolve this, OSCAR has reverted to a slower but more compatible method of drawing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="511"/>
        <source>OSCAR will set up a folder for your data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="512"/>
        <source>If you have been using SleepyHead or an older version of OSCAR,</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="513"/>
        <source>OSCAR can copy your old data to this folder later.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="566"/>
        <source>Migrate SleepyHead or OSCAR Data?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="567"/>
        <source>On the next screen OSCAR will ask you to select a folder with SleepyHead or OSCAR data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="568"/>
        <source>Click [OK] to go to the next screen or [No] if you do not wish to use any SleepyHead or OSCAR data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="514"/>
        <source>We suggest you use this folder: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="515"/>
        <source>Click Ok to accept this, or No if you want to use a different folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="521"/>
        <source>Choose or create a new folder for OSCAR data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="526"/>
        <source>Next time you run OSCAR, you will be asked again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="537"/>
        <source>The folder you chose is not empty, nor does it already contain valid OSCAR data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="558"/>
        <source>Data directory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="577"/>
        <source>Unable to create the OSCAR data folder at</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="587"/>
        <source>Unable to write to OSCAR data directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="588"/>
        <source>Error code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="589"/>
        <source>OSCAR cannot continue and is exiting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="600"/>
        <source>Unable to write to debug log. You can still use the debug pane (Help/Troubleshooting/Show Debug Pane) but the debug log will not be written to disk.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="659"/>
        <source>Version &quot;%1&quot; is invalid, cannot continue!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="668"/>
        <source>The version of OSCAR you are running (%1) is OLDER than the one used to create this data (%2).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="672"/>
        <source>It is likely that doing this will cause data corruption, are you sure you want to do this?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="713"/>
        <source>Question</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="524"/>
        <location filename="../oscar/main.cpp" line="576"/>
        <location filename="../oscar/main.cpp" line="591"/>
        <source>Exiting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="538"/>
        <source>Are you sure you want to use this folder?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="304"/>
        <source>OSCAR Reminder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="304"/>
        <source>Don&apos;t forget to place your datacard back in your CPAP device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="488"/>
        <source>You can only work with one instance of an individual OSCAR profile at a time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="489"/>
        <source>If you are using cloud storage, make sure OSCAR is closed and syncing has completed first on the other computer before proceeding.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="502"/>
        <source>Loading profile &quot;%1&quot;...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="968"/>
        <source>Chromebook file system detected, but no removable device found
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="969"/>
        <source>You must share your SD card with Linux using the ChromeOS Files program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2139"/>
        <source>Recompressing Session Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2666"/>
        <source>Please select a location for your zip other than the data card itself!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2713"/>
        <location filename="../oscar/mainwindow.cpp" line="2763"/>
        <location filename="../oscar/mainwindow.cpp" line="2822"/>
        <source>Unable to create zip!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1209"/>
        <source>Are you sure you want to reset all your channel colors and settings to defaults?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1219"/>
        <source>Are you sure you want to reset all your oximetry settings to defaults?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1298"/>
        <source>Are you sure you want to reset all your waveform channel colors and settings to defaults?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="45"/>
        <source>There are no graphs visible to print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="60"/>
        <source>Would you like to show bookmarked areas in this report?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="104"/>
        <source>Printing %1 Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="137"/>
        <source>%1 Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="195"/>
        <source>: %1 hours, %2 minutes, %3 seconds
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="248"/>
        <source>RDI	%1
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="250"/>
        <source>AHI	%1
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="283"/>
        <source>AI=%1 HI=%2 CAI=%3 </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="289"/>
        <source>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="293"/>
        <source>UAI=%1 </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="295"/>
        <source>NRI=%1 LKI=%2 EPI=%3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="298"/>
        <source>AI=%1 </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="366"/>
        <source>Reporting from %1 to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="431"/>
        <source>Entire Day&apos;s Flow Waveform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="433"/>
        <source>Current Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="443"/>
        <source>Entire Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="568"/>
        <source>Page %1 of %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1009"/>
        <source>Days: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1013"/>
        <source>Low Usage Days: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1014"/>
        <source>(%1% compliant, defined as &gt; %2 hours)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1131"/>
        <source>(Sess: %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1139"/>
        <source>Bedtime: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1141"/>
        <source>Waketime: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1254"/>
        <source>(Summary Only)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="487"/>
        <source>There is a lockfile already present for this profile &apos;%1&apos;, claimed on &apos;%2&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="306"/>
        <source>Fixed Bi-Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="307"/>
        <source>Auto Bi-Level (Fixed PS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>Auto Bi-Level (Variable PS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1537"/>
        <source>varies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1571"/>
        <source>n/a</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1588"/>
        <source>Fixed %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1591"/>
        <source>Min %1 Max %2 (%3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1595"/>
        <source>EPAP %1 IPAP %2 (%3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1599"/>
        <source>PS %1 over %2-%3 (%4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1604"/>
        <location filename="../oscar/SleepLib/day.cpp" line="1613"/>
        <source>Min EPAP %1 Max IPAP %2 PS %3-%4 (%5)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1609"/>
        <source>EPAP %1 PS %2-%3 (%4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1624"/>
        <source>EPAP %1 IPAP %2-%3 (%4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1630"/>
        <source>EPAP %1-%2 IPAP %3-%4 (%5)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="360"/>
        <source>Most recent Oximetry data: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="361"/>
        <source>(last night)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="362"/>
        <source>(1 day ago)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="363"/>
        <source>(%2 days ago)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="368"/>
        <source>No oximetry data has been imported yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="41"/>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="43"/>
        <source>Contec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="41"/>
        <source>CMS50</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="78"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.h" line="88"/>
        <source>Fisher &amp; Paykel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="78"/>
        <source>ICON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="78"/>
        <source>DeVilbiss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="78"/>
        <source>Intellipap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="86"/>
        <source>SmartFlex Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="41"/>
        <source>ChoiceMMed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="41"/>
        <source>MD300</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="70"/>
        <source>Respironics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="70"/>
        <source>M-Series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="187"/>
        <source>Philips Respironics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="187"/>
        <source>System One</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="92"/>
        <source>ResMed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="93"/>
        <source>S9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="126"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.h" line="98"/>
        <source>EPR: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="39"/>
        <source>Somnopose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="39"/>
        <source>Somnopose Software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="40"/>
        <source>Zeo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="40"/>
        <source>Personal Sleep Coach</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="196"/>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="206"/>
        <source>Selection Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="249"/>
        <source>Database Outdated
Please Rebuild CPAP Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="430"/>
        <source> (%2 min, %3 sec)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="432"/>
        <source> (%3 sec)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="552"/>
        <source>Pop out Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="634"/>
        <source>The popout window is full. You should capture the existing
popout window, delete it, then pop out this graph again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1621"/>
        <source>Your machine doesn&apos;t record data to graph in Daily View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1624"/>
        <source>There is no data to graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1771"/>
        <source>d MMM yyyy [ %1 - %2 ]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2435"/>
        <source>Hide All Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2436"/>
        <source>Show All Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2777"/>
        <source>Unpin %1 Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2779"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2854"/>
        <source>Popout %1 Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2856"/>
        <source>Pin %1 Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/MinutesAtPressure.cpp" line="809"/>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1034"/>
        <source>Plots Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1117"/>
        <source>Duration %1:%2:%3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1118"/>
        <source>AHI %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="155"/>
        <source>Relief: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="161"/>
        <source>Hours: %1h, %2m, %3s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="264"/>
        <source>Machine Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="29"/>
        <source>Journal Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="47"/>
        <source>OSCAR found an old Journal folder, but it looks like it&apos;s been renamed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="49"/>
        <source>OSCAR will not touch this folder, and will create a new one instead.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="50"/>
        <source>Please be careful when playing in OSCAR&apos;s profile folders :-P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="57"/>
        <source>For some reason, OSCAR couldn&apos;t find a journal object record in your profile, but did find multiple Journal data folders.

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="58"/>
        <source>OSCAR picked only the first one of these, and will use it in future:

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="60"/>
        <source>If your old data is missing, copy the contents of all the other Journal_XXXXXXX folders to this one manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="43"/>
        <source>CMS50F3.7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="43"/>
        <source>CMS50F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2717"/>
        <source>Backing up files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2724"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="249"/>
        <source>Reading data files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2786"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2788"/>
        <source>SmartFlex Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2787"/>
        <source>Intellipap pressure relief mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2793"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="135"/>
        <source>Ramp Only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2794"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="136"/>
        <source>Full Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2797"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2799"/>
        <source>SmartFlex Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2798"/>
        <source>Intellipap pressure relief level.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2805"/>
        <source>Snoring event.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2806"/>
        <source>SN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="547"/>
        <source>Locating STR.edf File(s)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="708"/>
        <source>Cataloguing EDF Files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="727"/>
        <source>Queueing Import Tasks...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="756"/>
        <source>Finishing Up...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="113"/>
        <source>CPAP Mode</source>
        <translation type="unfinished">CPAPモード</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="123"/>
        <source>VPAPauto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="125"/>
        <source>ASVAuto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="126"/>
        <source>iVAPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="127"/>
        <source>PAC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="128"/>
        <source>Auto for Her</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="132"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1030"/>
        <source>EPR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="132"/>
        <source>ResMed Exhale Pressure Relief</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="137"/>
        <source>Patient???</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="140"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1036"/>
        <source>EPR Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="140"/>
        <source>Exhale Pressure Relief Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="147"/>
        <source>Device auto starts by breathing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="216"/>
        <source>Response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="222"/>
        <source>Device auto stops by breathing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="228"/>
        <source>Patient View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="455"/>
        <source>Your ResMed CPAP device (Model %1) has not been tested yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="456"/>
        <source>It seems similar enough to other devices that it might work, but the developers would like a .zip copy of this device&apos;s SD card to make sure it works with OSCAR.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="147"/>
        <source>SmartStart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="147"/>
        <source>Smart Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="153"/>
        <source>Humid. Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="153"/>
        <source>Humidifier Enabled Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2934"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="159"/>
        <source>Humid. Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="159"/>
        <source>Humidity Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="172"/>
        <source>Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="172"/>
        <source>ClimateLine Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="176"/>
        <source>Temp. Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="176"/>
        <source>ClimateLine Temperature Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="176"/>
        <source>Temperature Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="183"/>
        <source>AB Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="183"/>
        <source>Antibacterial Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="189"/>
        <source>Pt. Access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="189"/>
        <source>Essentials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="191"/>
        <source>Plus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="195"/>
        <source>Climate Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="198"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="219"/>
        <source>Soft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="794"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="218"/>
        <source>Standard</source>
        <translation type="unfinished">標準</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="119"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="122"/>
        <source>BiPAP-T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="120"/>
        <source>BiPAP-S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="121"/>
        <source>BiPAP-S/T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="222"/>
        <source>SmartStop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="222"/>
        <source>Smart Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="231"/>
        <source>Simple</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="230"/>
        <source>Advanced</source>
        <translation type="unfinished">アドバンス</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1225"/>
        <source>Parsing STR.edf records...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="876"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2945"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3036"/>
        <source>Auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="201"/>
        <source>Mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="201"/>
        <source>ResMed Mask Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="203"/>
        <source>Pillows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="204"/>
        <source>Full Face</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="205"/>
        <source>Nasal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="209"/>
        <source>Ramp Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="118"/>
        <source>Weinmann</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="118"/>
        <source>SOMNOsoft2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="430"/>
        <source>Snapshot %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="280"/>
        <source>CMS50D+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="280"/>
        <source>CMS50E/F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="695"/>
        <source>Loading %1 data for %2...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="706"/>
        <source>Scanning Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="740"/>
        <source>Migrating Summary File Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="910"/>
        <source>Loading Summaries.xml.gz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="1042"/>
        <source>Loading Summary Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="15"/>
        <source>Please Wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="168"/>
        <source>Updating Statistics cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="715"/>
        <source>Usage Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="752"/>
        <source>Loading summaries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/dreem_loader.h" line="37"/>
        <source>Dreem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="93"/>
        <source>Your Viatom device generated data that OSCAR has never seen before.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="94"/>
        <source>The imported data may not be entirely accurate, so the developers would like a copy of your Viatom files to make sure OSCAR is handling the data correctly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.h" line="40"/>
        <source>Viatom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.h" line="40"/>
        <source>Viatom Software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="152"/>
        <source>New versions file improperly formed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="175"/>
        <source>A more recent version of OSCAR is available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="170"/>
        <source>release</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="170"/>
        <source>test version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="171"/>
        <source>You are running the latest %1 of OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="172"/>
        <location filename="../oscar/checkupdates.cpp" line="176"/>
        <source>You are running OSCAR %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="178"/>
        <source>OSCAR %1 is available &lt;a href=&apos;%2&apos;&gt;here&lt;/a&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="181"/>
        <source>Information about more recent test version %1 is available at &lt;a href=&apos;%2&apos;&gt;%2&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="209"/>
        <source>Check for OSCAR Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="276"/>
        <source>Unable to check for updates. Please try again later.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1022"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1023"/>
        <source>SensAwake level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1030"/>
        <source>Expiratory Relief</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1036"/>
        <source>Expiratory Relief Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1048"/>
        <source>Humidity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.h" line="88"/>
        <source>SleepStyle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="81"/>
        <source>This page in other languages:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2643"/>
        <location filename="../oscar/overview.cpp" line="471"/>
        <source>%1 Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2647"/>
        <location filename="../oscar/overview.cpp" line="475"/>
        <source>%1 of %2 Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2674"/>
        <source>%1 Event Types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2678"/>
        <source>%1 of %2 Event Types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.h" line="223"/>
        <source>Löwenstein</source>
        <translation>レーベンシュタイン</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.h" line="223"/>
        <source>Prisma Smart</source>
        <translation>プリズスマート</translation>
    </message>
</context>
<context>
    <name>SaveGraphLayoutSettings</name>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="179"/>
        <source>Manage Save Layout Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="186"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="187"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="186"/>
        <source>Add Feature inhibited. The maximum number of Items has been exceeded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="187"/>
        <source>creates new copy of current settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="188"/>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="188"/>
        <source>Restores saved settings from selection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="189"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="189"/>
        <source>Renames the selection. Must edit existing name then press enter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="190"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="190"/>
        <source>Updates the selection with current settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="191"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="191"/>
        <source>Deletes the selection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="192"/>
        <source>Expanded Help menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="193"/>
        <source>Exits the Layout menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="228"/>
        <source>&lt;h4&gt;Help Menu - Manage Layout Settings&lt;/h4&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="237"/>
        <source>Exits the help menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="238"/>
        <source>Exits the dialog menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="256"/>
        <source>     &lt;p style=&quot;color:black;&quot;&gt;        This feature manages the saving and restoring of Layout Settings.      &lt;br&gt;      Layout Settings control the layout of a graph or chart.      &lt;br&gt;      Different Layouts Settings can be saved and later restored.      &lt;br&gt;     &lt;/p&gt;     &lt;table width=&quot;100%&quot;&gt;         &lt;tr&gt;&lt;td&gt;&lt;b&gt;Button&lt;/b&gt;&lt;/td&gt;             &lt;td&gt;&lt;b&gt;Description&lt;/b&gt;&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td valign=&quot;top&quot;&gt;Add&lt;/td&gt; 			&lt;td&gt;Creates a copy of the current Layout Settings. &lt;br&gt; 				The default description is the current date. &lt;br&gt; 				The description may be changed. &lt;br&gt; 				The Add button will be greyed out when maximum number is reached.&lt;/td&gt;&lt;/tr&gt;                 &lt;br&gt;         &lt;tr&gt;&lt;td&gt;&lt;i&gt;&lt;u&gt;Other Buttons&lt;/u&gt; &lt;/i&gt;&lt;/td&gt;              &lt;td&gt;Greyed out when there are no selections&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;Restore&lt;/td&gt; 			&lt;td&gt;Loads the Layout Settings from the selection. Automatically exits. &lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;Rename &lt;/td&gt;         			&lt;td&gt;Modify the description of the selection. Same as a double click.&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td valign=&quot;top&quot;&gt;Update&lt;/td&gt;&lt;td&gt; Saves the current Layout Settings to the selection.&lt;br&gt; 		        Prompts for confirmation.&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td valign=&quot;top&quot;&gt;Delete&lt;/td&gt; 			&lt;td&gt;Deletes the selecton. &lt;br&gt; 			    Prompts for confirmation.&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;&lt;i&gt;&lt;u&gt;Control&lt;/u&gt; &lt;/i&gt;&lt;/td&gt;              &lt;td&gt;&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;Exit &lt;/td&gt; 			&lt;td&gt;(Red circle with a white &quot;X&quot;.) Returns to OSCAR menu.&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;Return&lt;/td&gt; 			&lt;td&gt;Next to Exit icon. Only in Help Menu. Returns to Layout menu.&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;Escape Key&lt;/td&gt; 			&lt;td&gt;Exit the Help or Layout menu.&lt;/td&gt;&lt;/tr&gt;       &lt;/table&gt;        &lt;p&gt;&lt;b&gt;Layout Settings&lt;/b&gt;&lt;/p&gt;       &lt;table width=&quot;100%&quot;&gt;          &lt;tr&gt; 			&lt;td&gt;* Name&lt;/td&gt; 			&lt;td&gt;* Pinning&lt;/td&gt; 			&lt;td&gt;* Plots Enabled &lt;/td&gt; 			&lt;td&gt;* Height&lt;/td&gt; 		&lt;/tr&gt;         &lt;tr&gt; 			&lt;td&gt;* Order&lt;/td&gt; 			&lt;td&gt;* Event Flags&lt;/td&gt; 			&lt;td&gt;* Dotted Lines&lt;/td&gt; 			&lt;td&gt;* Height Options&lt;/td&gt; 		&lt;/tr&gt;       &lt;/table&gt;        &lt;p&gt;&lt;b&gt;General Information&lt;/b&gt;&lt;/p&gt; 	  &lt;ul style=margin-left=&quot;20&quot;; &gt;  		&lt;li&gt; Maximum description size = 80 characters.	&lt;/li&gt;  		&lt;li&gt; Maximum Saved Layout Settings = 30.	&lt;/li&gt;  		&lt;li&gt; Saved Layout Settings can be accessed by all profiles.  		&lt;li&gt; Layout Settings only control the layout of a graph or chart. &lt;br&gt;               They do not contain any other data. &lt;br&gt;              They do not control if a graph is displayed or not. &lt;/li&gt; 		&lt;li&gt; Layout Settings for daily and overview are managed independantly. &lt;/li&gt;	  &lt;/ul&gt;   </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="458"/>
        <source>Maximum number of Items exceeded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="464"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="473"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="482"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="509"/>
        <source>No Item Selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="465"/>
        <source>Ok to Update?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="510"/>
        <source>Ok To Delete?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SessionBar</name>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="247"/>
        <source>%1h %2m</source>
        <translation>%1時間 %2分</translation>
    </message>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="290"/>
        <source>No Sessions Present</source>
        <translation>セッションがありません</translation>
    </message>
</context>
<context>
    <name>SleepStyleLoader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="213"/>
        <source>Import Error</source>
        <translation>インポート失敗</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="214"/>
        <source>This device Record cannot be imported in this profile.</source>
        <translation>このプロフィールにはこのデバイスの記録がインポートできません。</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="214"/>
        <source>The Day records overlap with already existing content.</source>
        <translation>日次の記録が既にある内容と重なります。</translation>
    </message>
</context>
<context>
    <name>Statistics</name>
    <message>
        <location filename="../oscar/statistics.cpp" line="536"/>
        <source>CPAP Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="539"/>
        <location filename="../oscar/statistics.cpp" line="1400"/>
        <source>CPAP Usage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="540"/>
        <source>Average Hours per Night</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="543"/>
        <source>Therapy Efficacy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="556"/>
        <source>Leak Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="561"/>
        <source>Pressure Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="586"/>
        <source>Oximeter Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="590"/>
        <source>Blood Oxygen Saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="595"/>
        <source>Pulse Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="606"/>
        <source>%1 Median</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="607"/>
        <location filename="../oscar/statistics.cpp" line="608"/>
        <source>Average %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="610"/>
        <source>Min %1</source>
        <translation type="unfinished">%1 分</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="611"/>
        <source>Max %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="612"/>
        <source>%1 Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="613"/>
        <source>% of time in %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="614"/>
        <source>% of time above %1 threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="615"/>
        <source>% of time below %1 threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="637"/>
        <source>Name: %1, %2</source>
        <translation type="unfinished">名前: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="639"/>
        <source>DOB: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="642"/>
        <source>Phone: %1</source>
        <translation type="unfinished">電話番号: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="645"/>
        <source>Email: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="648"/>
        <source>Address:</source>
        <translation type="unfinished">住所:</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="734"/>
        <source>This report was prepared on %1 by OSCAR %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="919"/>
        <source>Device Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="978"/>
        <source>Changes to Device Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1401"/>
        <source>Days Used: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1402"/>
        <source>Low Use Days: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1403"/>
        <source>Compliance: %1%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1427"/>
        <source>Days AHI of 5 or greater: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1434"/>
        <source>Best AHI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1438"/>
        <location filename="../oscar/statistics.cpp" line="1450"/>
        <source>Date: %1 AHI: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1444"/>
        <source>Worst AHI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1481"/>
        <source>Best Flow Limitation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1485"/>
        <location filename="../oscar/statistics.cpp" line="1498"/>
        <source>Date: %1 FL: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1491"/>
        <source>Worst Flow Limtation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1503"/>
        <source>No Flow Limitation on record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1524"/>
        <source>Worst Large Leaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1532"/>
        <source>Date: %1 Leak: %2%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1538"/>
        <source>No Large Leaks on record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1561"/>
        <source>Worst CSR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1569"/>
        <source>Date: %1 CSR: %2%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1574"/>
        <source>No CSR on record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1591"/>
        <source>Worst PB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1599"/>
        <source>Date: %1 PB: %2%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1604"/>
        <source>No PB on record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1612"/>
        <source>Want more information?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1613"/>
        <source>OSCAR needs all summary data loaded to calculate best/worst data for individual days.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1614"/>
        <source>Please enable Pre-Load Summaries checkbox in preferences to make sure this data is available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1634"/>
        <source>Best RX Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1637"/>
        <location filename="../oscar/statistics.cpp" line="1649"/>
        <source>Date: %1 - %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1640"/>
        <location filename="../oscar/statistics.cpp" line="1652"/>
        <source>AHI: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1641"/>
        <location filename="../oscar/statistics.cpp" line="1653"/>
        <source>Total Hours: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1646"/>
        <source>Worst RX Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1166"/>
        <source>Most Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="541"/>
        <source>Compliance (%1 hrs/day)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="737"/>
        <source>OSCAR is free open-source CPAP report software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1064"/>
        <source>No data found?!?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1066"/>
        <source>Oscar has no data to report :(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1167"/>
        <source>Last Week</source>
        <translation type="unfinished">先週</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1168"/>
        <source>Last 30 Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1169"/>
        <source>Last 6 Months</source>
        <translation type="unfinished">過去6ヶ月</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1170"/>
        <source>Last Year</source>
        <translation type="unfinished">昨年</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1174"/>
        <source>Last Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1219"/>
        <source>Details</source>
        <translation type="unfinished">詳細</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1233"/>
        <source>No %1 data available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1236"/>
        <source>%1 day of %2 Data on %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1242"/>
        <source>%1 days of %2 Data, between %3 and %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="986"/>
        <source>Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="990"/>
        <source>Pressure Relief</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="992"/>
        <source>Pressure Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="925"/>
        <source>First Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="926"/>
        <source>Last Use</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../oscar/welcome.ui" line="127"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>オープンソースCPAP分析レポートへようこそ</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="142"/>
        <source>What would you like to do?</source>
        <translation>何をしたいですか？</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="185"/>
        <source>CPAP Importer</source>
        <translation>CPAPインポーター</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="223"/>
        <source>Oximetry Wizard</source>
        <translation>オキシメーターウィザード</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="261"/>
        <source>Daily View</source>
        <translation>日次ビュー</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="299"/>
        <source>Overview</source>
        <translation>概要</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="337"/>
        <source>Statistics</source>
        <translation>統計</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="580"/>
        <source>&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;ResMed S9 SDCards need to be locked &lt;/span&gt;&lt;span style=&quot; font-weight:600; color:#ff0000;&quot;&gt;before inserting into your computer.&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br&gt;Some operating systems write index files to the card without asking, which can render your card unreadable by your cpap device.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;span style=&quot; font-weight:600;&quot;&gt;警告:&lt;/span&gt;&lt;span style=&quot; font-weight:600; color:#ff0000;&quot;&gt;コンピュータに挿入する前に&lt;/span&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;ResMed S9 SDカードはロックする必要があります &lt;/span&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br&gt;OSによっては許可を求めずインデックスファイルをカードの書き込み、CPAPデバイスで読み込めなくなることがあります。&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="160"/>
        <source>It would be a good idea to check File-&gt;Preferences first,</source>
        <translation>ファイル -&gt; 設定を先に確認することをお勧めします,</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="161"/>
        <source>as there are some options that affect import.</source>
        <translation>インポートに影響のあるオプションがあるため。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="162"/>
        <source>Note that some preferences are forced when a ResMed device is detected</source>
        <translation>ResMedデバイスが見つかったため、いくつかの設定が強制されます</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="163"/>
        <source>First import can take a few minutes.</source>
        <translation>最初のインポートは数分かかります。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="176"/>
        <source>The last time you used your %1...</source>
        <translation>前回%1を使ったのは…</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="180"/>
        <source>last night</source>
        <translation>昨晩</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="182"/>
        <source>today</source>
        <translation>今日</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="183"/>
        <source>%2 days ago</source>
        <translation>%2日前</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="185"/>
        <source>was %1 (on %2)</source>
        <translation>%1です（%2)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="193"/>
        <source>%1 hours, %2 minutes and %3 seconds</source>
        <translation>%1時間 %2 分 %3 秒</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="197"/>
        <source>&lt;font color = red&gt;You only had the mask on for %1.&lt;/font&gt;</source>
        <translation>&lt;font color = red&gt;%1にマスクをしていました。&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="210"/>
        <source>under</source>
        <translation>下</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="211"/>
        <source>over</source>
        <translation>上</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="212"/>
        <source>reasonably close to</source>
        <translation>比較的近い</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="213"/>
        <source>equal to</source>
        <translation>等しい</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="227"/>
        <source>You had an AHI of %1, which is %2 your %3 day average of %4.</source>
        <translation>あなたの AHI は %1.%2で、%3平均は%4でした。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="263"/>
        <source>Your pressure was under %1 %2 for %3% of the time.</source>
        <translation>気圧は%3%で %1 %2でした。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="286"/>
        <source>Your EPAP pressure fixed at %1 %2.</source>
        <translation>EPAPの気圧は、%1 %2 に固定されています。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="289"/>
        <location filename="../oscar/welcome.cpp" line="298"/>
        <source>Your IPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>IPAPの気圧は %3%の時間で %1 %2 以下でした。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="297"/>
        <source>Your EPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>EPAPの気圧は %3%の時間で %1 %2 以下でした。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="181"/>
        <source>1 day ago</source>
        <translation>昨日</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="196"/>
        <source>Your device was on for %1.</source>
        <translation>デバイス稼動時間 %1。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="258"/>
        <source>Your CPAP device used a constant %1 %2 of air</source>
        <translation>CPAPデバイスは、%1 %2 の空気を消費</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="271"/>
        <source>Your device used a constant %1-%2 %3 of air.</source>
        <translation>デバイスは、%1-%2 %3の空気を消費。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="278"/>
        <source>Your device was under %1-%2 %3 for %4% of the time.</source>
        <translation>デバイスは%4%で%1-%2 %3以下でした。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="318"/>
        <source>Your average leaks were %1 %2, which is %3 your %4 day average of %5.</source>
        <translation>平均のリークは %1 %2で、%4平均の%5 %3でした。</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="324"/>
        <source>No CPAP data has been imported yet.</source>
        <translation>CPAPのデータはまだインポートされていません。</translation>
    </message>
</context>
<context>
    <name>gGraph</name>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="652"/>
        <source>Double click Y-axis: Return to AUTO-FIT Scaling</source>
        <translation>Y軸をダブルクリック: スケールを自動調整に戻る</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="654"/>
        <source>Double click Y-axis: Return to DEFAULT Scaling</source>
        <translation>Y 軸をダブルクリック: デフォルトのスケールに戻る</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="656"/>
        <source>Double click Y-axis: Return to OVERRIDE Scaling</source>
        <translation>Y 軸をダブルクリック: 書き換えたスケールに戻る</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="659"/>
        <source>Double click Y-axis: For Dynamic Scaling</source>
        <translation>Y 軸をダブルクリック: 動的なスケールに戻る</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="663"/>
        <source>Double click Y-axis: Select DEFAULT Scaling</source>
        <translation>Y 軸をダブルクリック: デフォルトのスケールを選ぶ</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="665"/>
        <source>Double click Y-axis: Select AUTO-FIT Scaling</source>
        <translation>Y軸をダブルクリック: スケールを自動調整を選ぶ</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="924"/>
        <source>%1 days</source>
        <translation>%1日</translation>
    </message>
</context>
<context>
    <name>gGraphView</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="557"/>
        <source>100% zoom level</source>
        <translation>100% 表示</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="559"/>
        <source>Restore X-axis zoom to 100% to view entire selected period.</source>
        <translation>選択した範囲すべてのデータを表示するためX 軸にの拡大率に 100% に戻す。</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="561"/>
        <source>Restore X-axis zoom to 100% to view entire day&apos;s data.</source>
        <translation>一日全体のデータを表示するためX 軸にの拡大率に 100% に戻す。</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="563"/>
        <source>Reset Graph Layout</source>
        <translation>グラフのレイアウトをリセットする</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="564"/>
        <source>Resets all graphs to a uniform height and default order.</source>
        <translation>すべてのグラフをデフォルトの高さと順序に戻す</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="567"/>
        <source>Y-Axis</source>
        <translation>Y軸</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="568"/>
        <source>Plots</source>
        <translation>プロット</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="573"/>
        <source>CPAP Overlays</source>
        <translation>CPAPオーバーレイ</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="576"/>
        <source>Oximeter Overlays</source>
        <translation>オキシメーターオーバーレイ</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="579"/>
        <source>Dotted Lines</source>
        <translation>点線</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1967"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2020"/>
        <source>Double click title to pin / unpin
Click and drag to reorder graphs</source>
        <translation>タイトルをダブルクリックして固定 / 固定解除
クリックしてドラッグでグラフの順序変更</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2266"/>
        <source>Remove Clone</source>
        <translation>複製を削除</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2270"/>
        <source>Clone %1 Graph</source>
        <translation>%1 のグラフを複製</translation>
    </message>
</context>
</TS>
