<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="88"/>
        <source>Sorry, could not locate About file.</source>
        <translation>Leider konnte die Über OSCAR-Datei nicht gefunden werden.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="238"/>
        <source>Close</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="35"/>
        <source>&amp;About</source>
        <translation>&amp;Über</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="77"/>
        <source>GPL License</source>
        <translation>GPL Lizenz</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="103"/>
        <source>Sorry, could not locate Credits file.</source>
        <translation>Entschuldigung, die Credits-Datei konnte nicht gefunden werden.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="133"/>
        <source>Important:</source>
        <translation>Wichtig:</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="63"/>
        <source>Credits</source>
        <translation>Verdienst</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="147"/>
        <source>To see if the license text is available in your language, see %1.</source>
        <translation>Informationen dazu, ob der Lizenztext in Ihrer Sprache verfügbar ist, finden Sie unter %1.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="49"/>
        <location filename="../oscar/aboutdialog.cpp" line="129"/>
        <source>Release Notes</source>
        <translation>Versionshinweise</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="36"/>
        <source>Show data folder</source>
        <translation>Datenordner anzeigen</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="118"/>
        <source>Sorry, could not locate Release Notes.</source>
        <translation>Sorry, konnte die Release Notes nicht finden.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="134"/>
        <source>As this is a pre-release version, it is recommended that you &lt;b&gt;back up your data folder manually&lt;/b&gt; before proceeding, because attempting to roll back later may break things.</source>
        <translation>Da es sich um eine Vorabversion handelt, wird empfohlen, dass Sie &lt;b&gt;Ihre Daten manuell sichern&lt;/b&gt; bevor Sie fortfahren, denn der Versuch die Daten später zurückzuholen, kann scheitern.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="40"/>
        <source>About OSCAR %1</source>
        <translation>Über OSCAR %1</translation>
    </message>
</context>
<context>
    <name>CMS50F37Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="890"/>
        <source>Could not find the oximeter file:</source>
        <translation>Die Oxymeter-Datei konnte nicht gefunden werden:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="896"/>
        <source>Could not open the oximeter file:</source>
        <translation>Die Oxymeter-Datei konnte nicht geöffnet werden:</translation>
    </message>
</context>
<context>
    <name>CMS50Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="492"/>
        <source>Could not get data transmission from oximeter.</source>
        <translation>Keine Datenübertragung vom Oxymeter möglich.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="492"/>
        <source>Please ensure you select &apos;upload&apos; from the oximeter devices menu.</source>
        <translation>Bitte stellen Sie sicher, dass Sie den&apos;Upload&apos; aus dem Menü des Oxymeter Gerätes auswählt haben. Beim PO400 geht das automatisch.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="560"/>
        <source>Could not find the oximeter file:</source>
        <translation>Die Oxymeter-Datei konnte nicht gefunden werden:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="566"/>
        <source>Could not open the oximeter file:</source>
        <translation>Die Oxymeter-Datei konnte nicht geöffnet werden:</translation>
    </message>
</context>
<context>
    <name>CheckUpdates</name>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="250"/>
        <source>Checking for newer OSCAR versions</source>
        <translation>Prüfung auf neuere OSCAR-Versionen</translation>
    </message>
</context>
<context>
    <name>Daily</name>
    <message>
        <location filename="../oscar/daily.ui" line="1062"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1074"/>
        <source>u</source>
        <translation>u</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1126"/>
        <source>Big</source>
        <translation>Groß</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1500"/>
        <source>End</source>
        <translation>Ende</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="310"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="311"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1055"/>
        <source>Oximetry Sessions</source>
        <translation>Oxymeter Sitzung</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1084"/>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1407"/>
        <source>Search</source>
        <translation>Suche</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="968"/>
        <location filename="../oscar/daily.ui" line="1391"/>
        <source>Notes</source>
        <translation>Aufzeichnungen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1106"/>
        <location filename="../oscar/daily.ui" line="1116"/>
        <source>Small</source>
        <translation>Klein</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1500"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1308"/>
        <source>PAP Mode: %1</source>
        <translation>PAP Modus: %1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1196"/>
        <source>I&apos;m feeling ...</source>
        <translation>Ich fühle mich ...</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1022"/>
        <source>Journal</source>
        <translation>Journal</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1439"/>
        <source>Total time in apnea</source>
        <translation>Gesamtzeit des Apnoe</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1061"/>
        <source>Position Sensor Sessions</source>
        <translation>Position Sensor Sitzungen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1363"/>
        <source>Add Bookmark</source>
        <translation>Neues Lesezeichen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1399"/>
        <source>Remove Bookmark</source>
        <translation>Lesezeichen entfernen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2129"/>
        <source>Pick a Colour</source>
        <translation>Wählen Sie eine Farbe</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1809"/>
        <source>Complain to your Equipment Provider!</source>
        <translation>Beschweren Sie sich bei Ihren Anbieter!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1032"/>
        <source>Session Information</source>
        <translation>Sitzungsinformationen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1798"/>
        <source>Sessions all off!</source>
        <translation>Alle Sitzungen schließen!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="791"/>
        <source>%1 event</source>
        <translation>%1 Ereignis</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="674"/>
        <source>Go to the most recent day with data records</source>
        <translation>Zum letzten Tag mit Datensätzen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1326"/>
        <source>B.M.I.</source>
        <translation>B.M.I.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1058"/>
        <source>Sleep Stage Sessions</source>
        <translation>Schlafstadium Sitzungen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1276"/>
        <source>Oximeter Information</source>
        <translation>Oxymeter Informationen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="861"/>
        <source>Events</source>
        <translation>Ereignisse</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1052"/>
        <source>CPAP Sessions</source>
        <translation>CPAP Sitzung</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1121"/>
        <source>Medium</source>
        <translation>Mittel</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1386"/>
        <source>Starts</source>
        <translation>Startet</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1212"/>
        <source>Weight</source>
        <translation>Gewicht</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1050"/>
        <source> i </source>
        <translation> i </translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1184"/>
        <source>Zombie</source>
        <translation>Nicht gut</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1342"/>
        <source>Bookmarks</source>
        <translation>Lesezeichen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1495"/>
        <source>Layout</source>
        <translation>Layout</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1502"/>
        <source>Save and Restore Graph Layout Settings</source>
        <translation>Diagrammlayouteinstellungen speichern und wiederherstellen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="836"/>
        <source>Session End Times</source>
        <translation>Sitzungsendzeit</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1095"/>
        <source>enable</source>
        <translation>aktivieren</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="792"/>
        <source>%1 events</source>
        <translation>%1 Ereignisse</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="298"/>
        <source>events</source>
        <translation>Ereignisse</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1524"/>
        <source>Event Breakdown</source>
        <translation>Ereignis Pannen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1095"/>
        <source>Click to %1 this session.</source>
        <translation>Klicken Sie auf %1 dieser Sitzung.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1281"/>
        <source>SpO2 Desaturations</source>
        <translation>SpO2 Entsättigungen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1827"/>
        <source>&quot;Nothing&apos;s here!&quot;</source>
        <translation>&quot;Keine Daten vorhanden!&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1112"/>
        <source>%1h %2m %3s</source>
        <translation>%1h %2m %3s</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1288"/>
        <source>Awesome</source>
        <translation>Sehr gut</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1282"/>
        <source>Pulse Change events</source>
        <translation>Pulsereignis ändern</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1283"/>
        <source>SpO2 Baseline Used</source>
        <translation>SpO2-Baseline verwendet</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1804"/>
        <source>Zero hours??</source>
        <translation>Null-Stunden??</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="506"/>
        <source>Go to the previous day</source>
        <translation>Zum vorherigen Tag</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="190"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1449"/>
        <source>Time over leak redline</source>
        <translation>Zeit über Leck rote Linie</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1095"/>
        <source>disable</source>
        <translation>deaktivieren</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1807"/>
        <source>no data :(</source>
        <translation>keine Daten :(</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1808"/>
        <source>Sorry, this device only provides compliance data.</source>
        <translation>Tut mir leid, dieses Gerät liefert nur Konformitätsdaten.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2466"/>
        <source>Bookmark at %1</source>
        <translation>Lesezeichen bei %1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1349"/>
        <source>Statistics</source>
        <translation>Statistiken</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="298"/>
        <source>Breakdown</source>
        <translation>Aufschlüsselung</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="584"/>
        <source>Disable Warning</source>
        <translation>Warnung deaktivieren</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="585"/>
        <source>Disabling a session will remove this session data 
from all  graphs, reports and statistics.

The Search tab can find disabled sessions

Continue ?</source>
        <translation>Durch das Deaktivieren einer Sitzung werden diese Sitzungsdaten entfernt
aus allen Grafiken, Berichten und Statistiken.

Die Registerkarte „Suchen“ kann deaktivierte Sitzungen finden

Weitermachen ?</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1066"/>
        <source>Unknown Session</source>
        <translation>Unbekannte Sitzung</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1143"/>
        <source>Device Settings</source>
        <translation>Geräteeinstellungen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1727"/>
        <source>This CPAP device does NOT record detailed data</source>
        <translation>Dieses CPAP-Gerät zeichnet KEINE detaillierten Daten auf</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1800"/>
        <source>Sessions exist for this day but are switched off.</source>
        <translation>Sitzungen existieren heute, sind aber ausgeschaltet.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1303"/>
        <source>Model %1 - %2</source>
        <translation>Model %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1075"/>
        <source>Duration</source>
        <translation>Dauer</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="923"/>
        <source>View Size</source>
        <translation>Größen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1803"/>
        <source>Impossibly short session</source>
        <translation>Sehr kurze Sitzung</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1111"/>
        <source>%1 Session #%2</source>
        <translation>%1 Sitzung #%2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1515"/>
        <source>Show/hide available graphs.</source>
        <translation>Verfügbare Diagramme anzeigen/ausblenden.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="673"/>
        <source>No %1 events are recorded this day</source>
        <translation>Keine %1 Ereignisse werden an diesem Tag aufgezeichnet</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="551"/>
        <source>Show or hide the calender</source>
        <translation>Ein/Aus Kalender</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1459"/>
        <source>Time outside of ramp</source>
        <translation>Außerhalb der Rampenzeit</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1537"/>
        <source>Unable to display Pie Chart on this system</source>
        <translation>Das Kreisdiagramm kann auf diesem System nicht angezeigt werden</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1455"/>
        <source>Total ramp time</source>
        <translation>Gesamte Rampenzeit</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1429"/>
        <source>This day just contains summary data, only limited information is available.</source>
        <translation>Dieser Tag enthält nur zusammenfassende Daten. Es stehen nur begrenzte Informationen zur Verfügung.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="370"/>
        <source>Time at Pressure</source>
        <translation>Zeit in Druck</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="614"/>
        <source>Go to the next day</source>
        <translation>Gehen Sie auf den nächsten Tag</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="835"/>
        <source>Session Start Times</source>
        <translation>Sitzungsstartzeit</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1830"/>
        <source>No data is available for this day.</source>
        <translation>Für diesen Tag sind keine Daten verfügbar.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1219"/>
        <source>If height is greater than zero in Preferences Dialog, setting weight here will show Body Mass Index (BMI) value</source>
        <translation>Wenn die Größe im Einstellungsdialog größer als Null ist, zeigt die Gewichtseinstellung hier den Wert des Body Mass Index (BMI) an</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1146"/>
        <source>&lt;b&gt;Please Note:&lt;/b&gt; All settings shown below are based on assumptions that nothing has changed since previous days.</source>
        <translation>&lt;b&gt;Bitte beachten Sie:&lt;/b&gt; Alle nachfolgend dargestellten Einstellungen basieren auf der Annahme, dass sich gegenüber den Vortagen nichts geändert hat.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2423"/>
        <source>This bookmark is in a currently disabled area..</source>
        <translation>Dieses Lesezeichen befindet sich in einem derzeit deaktivierten Bereich.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1312"/>
        <source>(Mode and Pressure settings missing; yesterday&apos;s shown.)</source>
        <translation>(Modus- und Druckeinstellungen fehlen; die von gestern werden gezeigt.)</translation>
    </message>
    <message>
        <location filename="../oscar/daily.h" line="148"/>
        <source>Hide All Events</source>
        <translation>Alle Ereignisse verbergen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.h" line="149"/>
        <source>Show All Events</source>
        <translation>Alle Ereignisse anzeigen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.h" line="150"/>
        <source>Hide All Graphs</source>
        <translation>Alle Diagramme ausblenden</translation>
    </message>
    <message>
        <location filename="../oscar/daily.h" line="151"/>
        <source>Show All Graphs</source>
        <translation>Alle Diagramme anzeigen</translation>
    </message>
</context>
<context>
    <name>DailySearchTab</name>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="180"/>
        <source>Match:</source>
        <translation>Übereinstimmung:</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="192"/>
        <location filename="../oscar/dailySearchTab.cpp" line="985"/>
        <source>Select Match</source>
        <translation>Wählen Sie Übereinstimmung aus</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="227"/>
        <source>Clear</source>
        <translation>Klar</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="228"/>
        <location filename="../oscar/dailySearchTab.cpp" line="977"/>
        <location filename="../oscar/dailySearchTab.cpp" line="1087"/>
        <source>Start Search</source>
        <translation>Suche starten</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="250"/>
        <source>DATE
Jumps to Date</source>
        <translation>DATUM
Springt zum Datum</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="262"/>
        <source>Notes</source>
        <translation>Notizen</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="263"/>
        <source>Notes containing</source>
        <translation>Notizen enthalten</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="264"/>
        <source>Bookmarks</source>
        <translation>Lesezeichen</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="265"/>
        <source>Bookmarks containing</source>
        <translation>Lesezeichen enthalten</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="266"/>
        <source>AHI </source>
        <translation>AHI </translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="267"/>
        <source>Daily Duration</source>
        <translation>Tägliche Dauer</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="268"/>
        <source>Session Duration</source>
        <translation>Sitzungsdauer</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="269"/>
        <source>Days Skipped</source>
        <translation>Tage übersprungen</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="270"/>
        <source>Disabled Sessions</source>
        <translation>Deaktivierte Sitzungen</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="271"/>
        <source>Number of Sessions</source>
        <translation>Anzahl der Sitzungen</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="318"/>
        <source>Click HERE to close Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="322"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="526"/>
        <source>No Data
Jumps to Date&apos;s Details </source>
        <translation>Keine Daten
Springt zu den Details des Datums </translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="531"/>
        <source>Number Disabled Session
Jumps to Date&apos;s Details </source>
        <translation>Nummer deaktivierte Sitzung
Springt zu den Details des Datums </translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="537"/>
        <location filename="../oscar/dailySearchTab.cpp" line="554"/>
        <source>Note
Jumps to Date&apos;s Notes</source>
        <translation>Notiz
Springt zu den Notizen des Datums</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="542"/>
        <location filename="../oscar/dailySearchTab.cpp" line="547"/>
        <source>Jumps to Date&apos;s Bookmark</source>
        <translation>Springt zum Lesezeichen von Daten</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="561"/>
        <source>AHI
Jumps to Date&apos;s Details</source>
        <translation>AHI
Springt zu den Details des Datums</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="567"/>
        <source>Session Duration
Jumps to Date&apos;s Details</source>
        <translation>Sitzungsdauer
Springt zu den Details des Datums</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="574"/>
        <source>Number of Sessions
Jumps to Date&apos;s Details</source>
        <translation>Anzahl der Sitzungen
Springt zu den Details des Datums</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="581"/>
        <source>Daily Duration
Jumps to Date&apos;s Details</source>
        <translation>Tägliche Dauer
Springt zu den Details des Datums</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="589"/>
        <source>Number of events
Jumps to Date&apos;s Events</source>
        <translation>Anzahl der Ereignisse
Springt zu den Ereignissen von Daten</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="599"/>
        <source>Automatic start</source>
        <translation>Automatischer Start</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="819"/>
        <source>More to Search</source>
        <translation>Mehr zum Suchen</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="822"/>
        <source>Continue Search</source>
        <translation>Suche fortsetzen</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="825"/>
        <source>End of Search</source>
        <translation>Ende der Suche</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="830"/>
        <source>No Matches</source>
        <translation>Keine Treffer</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1035"/>
        <source> Skip:%1</source>
        <translation> Überspringen:%1</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1036"/>
        <source>%1/%2%3 days.</source>
        <translation>%1/%2%3 Tage.</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1127"/>
        <source>Finds days that match specified criteria.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1129"/>
        <source>  Searches from last day to first day.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1132"/>
        <source>First click on Match Button then select topic.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1134"/>
        <source>  Then click on the operation to modify it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1136"/>
        <source>  or update the value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1138"/>
        <source>Topics without operations will automatically start.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1141"/>
        <source>Compare Operations: numberic or character. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1143"/>
        <source>  Numberic  Operations: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1146"/>
        <source>  Character Operations: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1150"/>
        <source>Summary Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1152"/>
        <source>  Left:Summary - Number of Day searched</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1154"/>
        <source>  Center:Number of Items Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1156"/>
        <source>  Right:Minimum/Maximum for item searched</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1158"/>
        <source>Result Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1160"/>
        <source>  Column One: Date of match. Click selects date.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1162"/>
        <source>  Column two: Information. Click selects date.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1164"/>
        <source>    Then Jumps the appropiate tab.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1167"/>
        <source>Wildcard Pattern Matching:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1170"/>
        <source>  Wildcards use 3 characters:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1172"/>
        <source>  Asterisk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1175"/>
        <source> Question Mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1178"/>
        <source> Backslash.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1181"/>
        <source>  Asterisk matches any number of characters.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1183"/>
        <source>  Question Mark matches a single character.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1185"/>
        <source>  Backslash matches next character.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1039"/>
        <source>Found %1.</source>
        <translation>%1 gefunden.</translation>
    </message>
</context>
<context>
    <name>DateErrorDisplay</name>
    <message>
        <location filename="../oscar/overview.cpp" line="814"/>
        <source>ERROR
The start date MUST be before the end date</source>
        <translation>ERROR
Das Startdatum MUSS vor dem Enddatum liegen</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="817"/>
        <source>The entered start date %1 is after the end date %2</source>
        <translation>Das eingegebene Startdatum %1 liegt nach dem Enddatum %2</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="818"/>
        <source>
Hint: Change the end date first</source>
        <translation>
Tipp: Ändern Sie zuerst das Enddatum</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="820"/>
        <source>The entered end date %1 </source>
        <translation>Das eingegebene Enddatum %1 </translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="821"/>
        <source>is before the start date %1</source>
        <translation>liegt vor dem Startdatum %1</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="822"/>
        <source>
Hint: Change the start date first</source>
        <translation>
Tipp: Ändern Sie zuerst das Startdatum</translation>
    </message>
</context>
<context>
    <name>ExportCSV</name>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>AHI</source>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>End</source>
        <translation>Ende</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="154"/>
        <source>End:</source>
        <translation>Ende:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="192"/>
        <source>Quick Range:</source>
        <translation>Zeitspanne:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="60"/>
        <source>Daily</source>
        <translation>Täglich</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Event</source>
        <translation>Ereignis</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="210"/>
        <location filename="../oscar/exportcsv.cpp" line="129"/>
        <source>Last Fortnight</source>
        <translation>Letzten Vierzehn Tage</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="200"/>
        <location filename="../oscar/exportcsv.cpp" line="61"/>
        <location filename="../oscar/exportcsv.cpp" line="123"/>
        <source>Most Recent Day</source>
        <translation>Neuste Tag</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="214"/>
        <source> Count</source>
        <translation> Land</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="85"/>
        <source>Filename:</source>
        <translation>Dateiname:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="88"/>
        <source>Select file to export to</source>
        <translation>Wählen Sie die Datei, um zu Exportieren</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="37"/>
        <source>Resolution:</source>
        <translation>Auflösung:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="107"/>
        <source>Cancel</source>
        <translation>Aufheben</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="24"/>
        <source>Dates:</source>
        <translation>Termine:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="235"/>
        <location filename="../oscar/exportcsv.cpp" line="109"/>
        <source>Custom</source>
        <translation>In Gebrauch</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="114"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="131"/>
        <source>Start:</source>
        <translation>Start:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Data/Duration</source>
        <translation>Daten/Dauer</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="89"/>
        <source>CSV Files (*.csv)</source>
        <translation>CSV Dateien (*.csv)</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="215"/>
        <location filename="../oscar/exportcsv.cpp" line="132"/>
        <source>Last Month</source>
        <translation>Letzter Monat</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="220"/>
        <location filename="../oscar/exportcsv.cpp" line="135"/>
        <source>Last 6 Months</source>
        <translation>Letzten 6 Monate</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>Total Time</source>
        <translation>Gesamte Zeit</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>DateTime</source>
        <translation>Datum-Zeit</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <source>Session Count</source>
        <translation>Sitzungs Anzahl</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Session</source>
        <translation>Sitzung</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="230"/>
        <location filename="../oscar/exportcsv.cpp" line="120"/>
        <source>Everything</source>
        <translation>Alles</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="205"/>
        <location filename="../oscar/exportcsv.cpp" line="126"/>
        <source>Last Week</source>
        <translation>Letzte Woche</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="225"/>
        <location filename="../oscar/exportcsv.cpp" line="138"/>
        <source>Last Year</source>
        <translation>Letztes Jahr</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="14"/>
        <source>Export as CSV</source>
        <translation>Export in CSV Datei</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="79"/>
        <source>Sessions_</source>
        <translation>Sitzungen_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="46"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="81"/>
        <source>Summary_</source>
        <translation>Zusammenfassung_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="77"/>
        <source>Details_</source>
        <translation>Details_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="53"/>
        <source>Sessions</source>
        <translation>Sitzungen</translation>
    </message>
</context>
<context>
    <name>FPIconLoader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="242"/>
        <source>Import Error</source>
        <translation>Import Fehler</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="243"/>
        <source>The Day records overlap with already existing content.</source>
        <translation>Die Aufzeichnungen dieses Tages überschneiden sich mit bereits vorhandenen Inhalt.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="243"/>
        <source>This device Record cannot be imported in this profile.</source>
        <translation>Dieser Gerätedatensatz kann nicht in dieses Profil importiert werden.</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../oscar/help.cpp" line="240"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="112"/>
        <source>Index</source>
        <translation>Index</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="242"/>
        <source>clear</source>
        <translation>klar</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="85"/>
        <source>HelpEngine did not set up correctly</source>
        <translation>Hilfe Anwendung wurde nicht richtig eingerichtet</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="69"/>
        <source>Help files do not appear to be present.</source>
        <translation>Hilfedateien scheinen nicht vorhanden zu sein.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="100"/>
        <source>HelpEngine could not register documentation correctly.</source>
        <translation>Die Hilfe Anwendung konnte die Dokumentation nicht korrekt registrieren.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="59"/>
        <source>Help Files are not yet available for %1 and will display in %2.</source>
        <translation>Hilfedateien sind noch nicht verfügbar für %1 und werden angezeigt in %2.</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="91"/>
        <source>Hide this message</source>
        <translation>Verberge diese Nachricht</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="214"/>
        <source>Please wait a bit.. Indexing still in progress</source>
        <translation>Bitte warten Sie etwas.. Die Indizierung läuft noch</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="116"/>
        <source>Search</source>
        <translation>Suche</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="111"/>
        <source>Contents</source>
        <translation>Inhalt</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="241"/>
        <source>%1 result(s) for &quot;%2&quot;</source>
        <translation>%1 Resultat(e) für &quot;%2&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="126"/>
        <source>No documentation available</source>
        <translation>Keine Dokumentation verfügbar</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="196"/>
        <source>Search Topic:</source>
        <translation>Suchthema:</translation>
    </message>
</context>
<context>
    <name>MD300W1Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="166"/>
        <source>Could not find the oximeter file:</source>
        <translation>Die Oxymeter-Datei konnte nicht gefunden werden:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="172"/>
        <source>Could not open the oximeter file:</source>
        <translation>Die Oxymeter-Datei konnte nicht geöffnet werden:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2506"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1194"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="931"/>
        <source>Please insert your CPAP data card...</source>
        <translation>Bitte benutzen Sie Ihre CPAP-Datenkarte...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2762"/>
        <source>Daily Calendar</source>
        <translation>Kalender täglich</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2429"/>
        <source>&amp;Data</source>
        <translation>&amp;Daten</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2345"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2399"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2370"/>
        <source>&amp;View</source>
        <translation>&amp;Ansicht</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="119"/>
        <source>E&amp;xit</source>
        <translation>&amp;Schließen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1006"/>
        <source>Daily</source>
        <translation>Täglich</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="542"/>
        <source>Loading profile &quot;%1&quot;</source>
        <translation>Profil laden &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2678"/>
        <source>Import &amp;ZEO Data</source>
        <translation>Import &amp;ZEO Daten</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2275"/>
        <source>MSeries Import complete</source>
        <translation>M-Serie komplett Importiert</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1501"/>
        <source>There was an error saving screenshot to file &quot;%1&quot;</source>
        <translation>Es gab einen Fehler beim Speichern des Screenshot in eine Datei &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="766"/>
        <source>Couldn&apos;t find any valid Device Data at

%1</source>
        <translation>Konnte keine gültigen Gerätedaten finden unter

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="938"/>
        <source>Choose a folder</source>
        <translation>Wählen Sie einen Ordner</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1066"/>
        <source>A %1 file structure for a %2 was located at:</source>
        <translation>Eine%1 Dateistruktur für eine %2 wurde in:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1189"/>
        <source>Importing Data</source>
        <translation>Importieren von Daten</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2627"/>
        <source>Online Users &amp;Guide</source>
        <translation>Online &amp;Handbuch</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2533"/>
        <source>View &amp;Welcome</source>
        <translation>&amp;Willkommensansicht</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2781"/>
        <source>Show Performance Information</source>
        <translation>Anzeige der Performance- Informationen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2271"/>
        <source>There was a problem opening MSeries block File: </source>
        <translation>Es gab ein Problem beim Öffnen einer M-Serie Block-Datei: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2718"/>
        <source>Current Days</source>
        <translation>Aktueller Tag</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="116"/>
        <source>&amp;About</source>
        <translation>&amp;Über</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2511"/>
        <source>View &amp;Daily</source>
        <translation>&amp;Tagesansicht</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2522"/>
        <source>View &amp;Overview</source>
        <translation>&amp;Übersichtsansicht</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1385"/>
        <source>Access to Preferences has been blocked until recalculation completes.</source>
        <translation>Zugriff auf Einstellungen wurde blockiert, bis die Neuberechnung abgeschlossen ist.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2688"/>
        <source>Import RemStar &amp;MSeries Data</source>
        <translation>Import REMSTAR &amp;M-Serie Daten</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2745"/>
        <source>Daily Sidebar</source>
        <translation>Randleiste täglich</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1956"/>
        <source>Note as a precaution, the backup folder will be left in place.</source>
        <translation>Als Vorsichtsmaßnahme werden die Backup Ordner an Ort und Stelle belassen.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2642"/>
        <source>Change &amp;User</source>
        <translation>&amp;Benutzer ändern</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2602"/>
        <source>%1&apos;s Journal</source>
        <translation>%1&apos;s Journal</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="766"/>
        <source>Import Problem</source>
        <translation>Importproblem</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2512"/>
        <source>&lt;b&gt;Please be aware you can not undo this operation!&lt;/b&gt;</source>
        <translation>&lt;b&gt;Bitte beachten Sie, dass Sie diesen Vorgang nicht rückgängig machen können!&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2664"/>
        <source>View S&amp;tatistics</source>
        <translation>Statistik &amp;anzeigen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="578"/>
        <source>Monthly</source>
        <translation>Monatlich</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2698"/>
        <source>Change &amp;Language</source>
        <translation>&amp;Sprache auswählen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2563"/>
        <source>&amp;About OSCAR</source>
        <translation>&amp;Über OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1144"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1906"/>
        <source>Because there are no internal backups to rebuild from, you will have to restore from your own.</source>
        <translation>Es existiert keine interne Datensicherung. Sie müssen Ihre eigene verwenden.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="830"/>
        <location filename="../oscar/mainwindow.cpp" line="1903"/>
        <source>Please wait, importing from backup folder(s)...</source>
        <translation>Bitte warten, Import von Backup-Ordner (n)...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2510"/>
        <source>Are you sure you want to delete oximetry data for %1</source>
        <translation>Sind Sie sicher, dass Sie die Oxymetriedaten löschen möchten %1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2609"/>
        <source>O&amp;ximetry Wizard</source>
        <translation>O&amp;xymetrie Assistent</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1316"/>
        <source>Bookmarks</source>
        <translation>Lesezeichen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2653"/>
        <source>Right &amp;Sidebar</source>
        <translation>&amp;Seitenleiste rechts</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2465"/>
        <source>Rebuild CPAP Data</source>
        <translation>Wiederherstellung der CPAP Daten</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2604"/>
        <source>XML Files (*.xml)</source>
        <translation>XML Datei (*.xml)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1573"/>
        <source>The FAQ is not yet implemented</source>
        <translation>Die FAQ ist noch nicht implementiert</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2624"/>
        <source>Export review is not yet implemented</source>
        <translation>Die Exportprüfung ist noch nicht implementiert</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2811"/>
        <source>Report an Issue</source>
        <translation>Ein Problem melden</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="585"/>
        <source>Date Range</source>
        <translation>Datumsbereich</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2667"/>
        <source>View Statistics</source>
        <translation>Statistiken anzeigen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1073"/>
        <source>CPAP Data Located</source>
        <translation>CPAP-Daten liegen an</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1021"/>
        <source>Access to Import has been blocked while recalculations are in progress.</source>
        <translation>Der Zugang zu Import wurde blockiert, während Neuberechnungen im Gange sind.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2693"/>
        <source>Sleep Disorder Terms &amp;Glossary</source>
        <translation>Schlafstörungen Nutzungswörterbuch &amp;Wörterverzeichnis</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1891"/>
        <source>Are you really sure you want to do this?</source>
        <translation>Sind Sie wirklich sicher, dass Sie das tun wollen?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2542"/>
        <source>Select the day with valid oximetry data in daily view first.</source>
        <translation>Wählen Sie zuerst den Tag mit gültigen Oximetriedaten in der Tagesansicht aus.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2437"/>
        <source>Purge Oximetry Data</source>
        <translation>Oxymetriedaten löschen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2285"/>
        <source>Records</source>
        <translation>Zusammenfassung</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2558"/>
        <source>Use &amp;AntiAliasing</source>
        <translation>Verwenden Sie &amp;Antialiasing</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1033"/>
        <source>Would you like to import from this location?</source>
        <translation>Möchten Sie von diesem Ort impoertieren?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="561"/>
        <source>Report Mode</source>
        <translation>Report Modus</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2501"/>
        <source>&amp;Profiles</source>
        <translation>&amp;Profile</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="912"/>
        <source>Profiles</source>
        <translation>Profile</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2801"/>
        <source>CSV Export Wizard</source>
        <translation>CSV-Export-Assistent</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2637"/>
        <source>&amp;Automatic Oximetry Cleanup</source>
        <translation>&amp;Automatische Bereinigung der Oxymetrie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1025"/>
        <source>Import is already running in the background.</source>
        <translation>Import läuft bereits im Hintergrund.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1077"/>
        <source>Specify</source>
        <translation>einzeln Ausführen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="568"/>
        <source>Standard</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2073"/>
        <source>No help is available.</source>
        <translation>Es ist keine Hilfe verfügbar.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="956"/>
        <source>Statistics</source>
        <translation>Statistiken</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="764"/>
        <source>Up to date</source>
        <translation>Neuster Stand</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1380"/>
        <source>Please open a profile first.</source>
        <translation>Bitte öffnen Sie zuerst ein Profil.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="507"/>
        <source>&amp;Statistics</source>
        <translation>&amp;Statistiken</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2773"/>
        <source>Backup &amp;Journal</source>
        <translation>Sicherungskopie &amp;Journal</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="762"/>
        <source>Imported %1 CPAP session(s) from

%2</source>
        <translation>Importiert %1 CPAP-Sitzung(en) von

%2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2834"/>
        <source>Reporting issues is not yet implemented</source>
        <translation>Berichterstattungsprobleme sind noch nicht implementiert</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2448"/>
        <source>Purge &amp;Current Selected Day</source>
        <translation>Bereinigen, &amp;Aktualisieren des aktuell ausgewählten Tages</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1890"/>
        <source>Provided you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your CPAP data&lt;/i&gt;, you can still complete this operation, but you will have to restore from your backups manually.</source>
        <translation>Vorausgesetzt, Sie haben &lt;i&gt;&lt;b&gt;eigene &lt;/b&gt; Backups für ALLE Ihre CPAP-Daten&lt;/i&gt;, die Sie noch vervollständigen können erstellt. Aber Sie müssen die Backups manuell wiederherstellen.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2433"/>
        <source>&amp;Advanced</source>
        <translation>&amp;Fortgeschrittene</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2617"/>
        <source>Print &amp;Report</source>
        <translation>&amp;Drucken</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2806"/>
        <source>Export for Review</source>
        <translation>Export für Bewertung</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2601"/>
        <source>Take &amp;Screenshot</source>
        <translation>&amp;Bildschirmverwaltung</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1050"/>
        <source>Overview</source>
        <translation>Übersicht</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2585"/>
        <source>Show Debug Pane</source>
        <translation>Debug-Fenster anzeigen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2622"/>
        <source>&amp;Edit Profile</source>
        <translation>&amp;Profil bearbeiten</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1108"/>
        <source>Import Reminder</source>
        <translation>Import Erinnerung</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="288"/>
        <source>Help Browser</source>
        <translation>Hilfe Browser</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1715"/>
        <location filename="../oscar/mainwindow.cpp" line="1742"/>
        <source>If you can read this, the restart command didn&apos;t work. You will have to do it yourself manually.</source>
        <translation>Wenn Sie dies lesen können, hat der Neustartbefehl nicht funktioniert. Sie müssen es manuell tun.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2349"/>
        <source>Exp&amp;ort Data</source>
        <translation>Exp&amp;ort Daten</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="560"/>
        <location filename="../oscar/mainwindow.cpp" line="2227"/>
        <source>Welcome</source>
        <translation>Willkommen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2708"/>
        <source>Import &amp;Somnopose Data</source>
        <translation>Import &amp;CSV Daten</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1503"/>
        <source>Screenshot saved to file &quot;%1&quot;</source>
        <translation>Screenshot Datei gespeichert &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2496"/>
        <source>&amp;Preferences</source>
        <translation>&amp;Einstellungen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1968"/>
        <source>Are you &lt;b&gt;absolutely sure&lt;/b&gt; you want to proceed?</source>
        <translation>Sind Sie &lt;b&gt;absolut sicher&lt;/b&gt; das Sie fortfahren möchten?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="762"/>
        <source>Import Success</source>
        <translation>Erfolgreicher Import</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2604"/>
        <source>Choose where to save journal</source>
        <translation>Wählen, wo das Blatt gespeichert werden soll</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2632"/>
        <source>&amp;Frequently Asked Questions</source>
        <translation>&amp;Häufig gestellte Fragen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1094"/>
        <source>Oximetry</source>
        <translation>Oxymetrie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1068"/>
        <source>A %1 file structure was located at:</source>
        <translation>Eine%1 Dateistruktur befindet sich unter:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2703"/>
        <source>Change &amp;Data Folder</source>
        <translation>&amp;Datenordner ändern</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="885"/>
        <source>Navigation</source>
        <translation>Navigation</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="764"/>
        <source>Already up to date with CPAP data at

%1</source>
        <translation>Bereits aktuelle CPAP Daten

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1017"/>
        <source>No profile has been selected for Import.</source>
        <translation>Es wurde kein Profil für den Import ausgewählt.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1881"/>
        <source>Please note, that this could result in loss of data if OSCAR&apos;s backups have been disabled.</source>
        <translation>Bitte beachten Sie, dass dies zu Datenverlust führen kann, wenn die Backups von OSCAR deaktiviert wurden.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2571"/>
        <source>&amp;Maximize Toggle</source>
        <translation>&amp;Maximieren des Umschalters</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1565"/>
        <source>The User&apos;s Guide will open in your default browser</source>
        <translation>Das Benutzerhandbuch wird in Ihrem Standardbrowser geöffnet</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2286"/>
        <source>The Glossary will open in your default browser</source>
        <translation>Das Glossar wird in Ihrem Standardbrowser geöffnet</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2514"/>
        <source>Show Daily view</source>
        <translation>Tagesansicht anzeigen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2525"/>
        <source>Show Overview view</source>
        <translation>Übersichtsansicht anzeigen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2574"/>
        <source>Maximize window</source>
        <translation>Fenster maximieren</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2596"/>
        <source>Reset sizes of graphs</source>
        <translation>Größen von Diagrammen zurücksetzen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2656"/>
        <source>Show Right Sidebar</source>
        <translation>Rechte Seitenleiste anzeigen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2670"/>
        <source>Show Statistics view</source>
        <translation>Statistikansicht anzeigen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2726"/>
        <source>Show &amp;Line Cursor</source>
        <translation>Zeigt &amp;Cursorlinie an</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2748"/>
        <source>Show Daily Left Sidebar</source>
        <translation>Tägliche Ansicht in linker Sidebar anzeigen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2765"/>
        <source>Show Daily Calendar</source>
        <translation>Tageskalender anzeigen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2816"/>
        <source>System Information</source>
        <translation>Systeminformationen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2827"/>
        <source>Show &amp;Pie Chart</source>
        <translation>Torten -&amp;Diagramm anzeigen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2830"/>
        <source>Show Pie Chart on Daily page</source>
        <translation>Tortendiagramm auf der Tagesseite anzeigen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1168"/>
        <location filename="../oscar/mainwindow.cpp" line="2843"/>
        <source>OSCAR Information</source>
        <translation>OSCAR Information</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2374"/>
        <source>&amp;Reset Graphs</source>
        <translation>&amp;Grafiken zurücksetzen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2443"/>
        <source>Purge ALL Device Data</source>
        <translation>Löschen Sie ALLE Gerätedaten</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2593"/>
        <source>Reset Graph &amp;Heights</source>
        <translation>Graph &amp;Höhen zurücksetzen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2403"/>
        <source>Troubleshooting</source>
        <translation>Fehlerbehebung</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2488"/>
        <source>&amp;Import CPAP Card Data</source>
        <translation>&amp;CPAP-Kartendaten importieren</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2683"/>
        <source>Import &amp;Dreem Data</source>
        <translation>Import &amp;Dreem Daten</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2786"/>
        <source>Create zip of CPAP data card</source>
        <translation>Zip der CPAP-Datenkarte erstellen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2796"/>
        <source>Create zip of all OSCAR data</source>
        <translation>Zip von allen OSCAR-Daten erstellen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="581"/>
        <source>%1 (Profile: %2)</source>
        <translation>%1 (Profil: %2)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1107"/>
        <source>Please remember to select the root folder or drive letter of your data card, and not a folder inside it.</source>
        <translation>Bitte denken Sie daran, den Stammordner oder Laufwerksbuchstaben Ihrer Datenkarte zu wählen und nicht einen Ordner darin.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1491"/>
        <source>Choose where to save screenshot</source>
        <translation>Wählen Sie, wo der Screenshot gespeichert werden soll</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1491"/>
        <source>Image files (*.png)</source>
        <translation>Bilddateien (*.png)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2589"/>
        <source>You must select and open the profile you wish to modify</source>
        <translation>Sie müssen das Profil, das Sie ändern möchten, auswählen und öffnen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2634"/>
        <source>Would you like to zip this card?</source>
        <translation>Möchten Sie diese Karte verschließen?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2656"/>
        <location filename="../oscar/mainwindow.cpp" line="2727"/>
        <location filename="../oscar/mainwindow.cpp" line="2778"/>
        <source>Choose where to save zip</source>
        <translation>Wählen Sie, wo die Zip-Datei gespeichert werden soll</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2656"/>
        <location filename="../oscar/mainwindow.cpp" line="2727"/>
        <location filename="../oscar/mainwindow.cpp" line="2778"/>
        <source>ZIP files (*.zip)</source>
        <translation>ZIP Dateien (*.zip)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2703"/>
        <location filename="../oscar/mainwindow.cpp" line="2741"/>
        <location filename="../oscar/mainwindow.cpp" line="2812"/>
        <source>Creating zip...</source>
        <translation>Zip erstellen...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2688"/>
        <location filename="../oscar/mainwindow.cpp" line="2796"/>
        <source>Calculating size...</source>
        <translation>Größe berechnen...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2860"/>
        <source>Show Personal Data</source>
        <translation>Persönliche Daten anzeigen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2791"/>
        <source>Create zip of OSCAR diagnostic logs</source>
        <translation>Zip von OSCAR-Diagnoseprotokollen erstellen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2868"/>
        <source>Check For &amp;Updates</source>
        <translation>Nach &amp;Updates suchen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1442"/>
        <source>Check for updates not implemented</source>
        <translation>Prüfung auf nicht implementierte Updates</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2713"/>
        <source>Import &amp;Viatom/Wellue Data</source>
        <translation>Import &amp;Viatom/Wellendaten</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2838"/>
        <source>Standard - CPAP, APAP</source>
        <translation>Standard - CPAP, APAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2841"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Standard graph order, good for CPAP, APAP,  Basic BPAP&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Standarddiagrammreihenfolge, gut für CPAP, APAP, Basic BPAP&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2846"/>
        <source>Advanced - BPAP, ASV</source>
        <translation>Erweitert - BPAP, ASV</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2849"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Advanced graph order, good for BPAP w/BU, ASV, AVAPS, IVAPS&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Erweiterte Diagrammreihenfolge, gut für BPAP mit BU, ASV, AVAPS, IVAPS&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2876"/>
        <source>Purge Current Selected Day</source>
        <translation>Aktuellen ausgewählten Tag bereinigen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2881"/>
        <source>&amp;CPAP</source>
        <translation>&amp;CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2886"/>
        <source>&amp;Oximetry</source>
        <translation>&amp;Oxymetrie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2891"/>
        <source>&amp;Sleep Stage</source>
        <translation>&amp;Schlafphase</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2896"/>
        <source>&amp;Position</source>
        <translation>&amp;Position</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2901"/>
        <source>&amp;All except Notes</source>
        <translation>&amp;Alle außer Anmerkungen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2906"/>
        <source>All including &amp;Notes</source>
        <translation>Alle einschließlich &amp;Notizen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1123"/>
        <source>Find your CPAP data card</source>
        <translation>Finden Sie Ihre CPAP-Datenkarte</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1168"/>
        <source>No supported data was found</source>
        <translation>Es wurden keine unterstützten Daten gefunden</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1878"/>
        <source>Are you sure you want to rebuild all CPAP data for the following device:

</source>
        <translation>Möchten Sie wirklich alle CPAP-Daten für das folgende Gerät neu erstellen:

</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1888"/>
        <source>For some reason, OSCAR does not have any backups for the following device:</source>
        <translation>Aus irgendeinem Grund hat OSCAR keine Sicherungen für das folgende Gerät:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1907"/>
        <source>Would you like to import from your own backups now? (you will have no data visible for this device until you do)</source>
        <translation>Möchten Sie jetzt aus Ihren eigenen Backups importieren? (Bis dahin sind keine Daten für dieses Gerät sichtbar)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1958"/>
        <source>OSCAR does not have any backups for this device!</source>
        <translation>OSCAR hat keine Backups für dieses Gerät!</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1959"/>
        <source>Unless you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your data for this device&lt;/i&gt;, &lt;font size=+2&gt;you will lose this device&apos;s data &lt;b&gt;permanently&lt;/b&gt;!&lt;/font&gt;</source>
        <translation>Sofern Sie nicht &lt;i&gt;Ihre &lt;b&gt;eigenen&lt;/b&gt; Sicherungen für ALLE Ihre Daten für dieses Gerät erstellt haben&lt;/i&gt;, &lt;font size=+2&gt;werden Sie die Daten dieses Geräts &lt;b&gt;dauerhaft&lt;/b verlieren &gt;!&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1965"/>
        <source>You are about to &lt;font size=+2&gt;obliterate&lt;/font&gt; OSCAR&apos;s device database for the following device:&lt;/p&gt;</source>
        <translation>Sie sind dabei, die Gerätedatenbank von OSCAR für das folgende Gerät &lt;font size=+2&gt;zu löschen&lt;/font&gt;:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2023"/>
        <source>A file permission error caused the purge process to fail; you will have to delete the following folder manually:</source>
        <translation>Ein Dateiberechtigungsfehler führte dazu, dass der Löschvorgang fehlschlug; Sie müssen den folgenden Ordner manuell löschen:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2374"/>
        <location filename="../oscar/mainwindow.cpp" line="2378"/>
        <source>There was a problem opening %1 Data File: %2</source>
        <translation>Es gab ein Problem beim Öffnen von %1 Data File: %2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2377"/>
        <source>%1 Data Import of %2 file(s) complete</source>
        <translation>%1 Datenimport von %2 Datei(en) abgeschlossen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2379"/>
        <source>%1 Import Partial Success</source>
        <translation>%1 Teilweise erfolgreicher Import</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2381"/>
        <source>%1 Data Import complete</source>
        <translation>%1 Datenimport abgeschlossen</translation>
    </message>
</context>
<context>
    <name>MinMaxWidget</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2231"/>
        <source>Scaling Mode</source>
        <translation>Skalierungsmodus</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2196"/>
        <source>The Maximum Y-Axis value.. Must be greater than Minimum to work.</source>
        <translation>Um damit zu arbeiten, muss der max. Y-Achsen Wert größer sein als der minimale Wert.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2253"/>
        <source>This button resets the Min and Max to match the Auto-Fit</source>
        <translation>Diese Schaltfläche setzt die Automatische Anpassung für Min. und Max</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2189"/>
        <source>The Y-Axis scaling mode, &apos;Auto-Fit&apos; for automatic scaling, &apos;Defaults&apos; for settings according to manufacturer, and &apos;Override&apos; to choose your own.</source>
        <translation>Der Y-Achsen Skalierungsmodus &quot;Automatische Anpassung&quot;, für die automatische Skalierung, sind Vorgaben vom Hersteller. Die &quot;Übersteuerung&quot; können Sie selbst wählen.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2195"/>
        <source>The Minimum Y-Axis value.. Note this can be a negative number if you wish.</source>
        <translation>Wenn Sie es wünschen kann der Y-Achsen-Mindestwert eine negative Zahl sein.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2187"/>
        <source>Defaults</source>
        <translation>Standardwerte</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2186"/>
        <source>Auto-Fit</source>
        <translation>Automatische Anpassung</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2188"/>
        <source>Override</source>
        <translation>Übersteuerung</translation>
    </message>
</context>
<context>
    <name>NewProfile</name>
    <message>
        <location filename="../oscar/newprofile.ui" line="657"/>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="647"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="642"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="402"/>
        <source>Male</source>
        <translation>Männlich</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="973"/>
        <source>&amp;Back</source>
        <translation>&amp;Zurück</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="989"/>
        <location filename="../oscar/newprofile.cpp" line="279"/>
        <location filename="../oscar/newprofile.cpp" line="288"/>
        <source>&amp;Next</source>
        <translation>&amp;Weiter</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="256"/>
        <source>TimeZone</source>
        <translation>Zeitzone</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="524"/>
        <location filename="../oscar/newprofile.ui" line="813"/>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="534"/>
        <location filename="../oscar/newprofile.ui" line="803"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="124"/>
        <source>Any reports generated are for PERSONAL USE ONLY, and NOT IN ANY WAY fit for compliance or medical diagnostic purposes.</source>
        <translation>Alle Berichte die erzeugt werden, sind nur zum PERSÖNLICHEN GEBRAUCH und  in keiner Weise für die Einhaltung medizinischer Diagnosezwecke geeignet.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="134"/>
        <source>OSCAR is copyright &amp;copy;2011-2018 Mark Watkins and portions &amp;copy;2019-2022 The OSCAR Team</source>
        <translation>OSCAR ist urheberrechtlich geschützt &amp;copy;2011-2018 Mark Watkins und Teile &amp;copy;2019-2022 Das OSCAR-Team</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="455"/>
        <source>&amp;Close this window</source>
        <translation>&amp;Fenster schließen</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="14"/>
        <source>Edit User Profile</source>
        <translation>Benutzerprofil bearbeiten</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="127"/>
        <source>The authors will not be held liable for &lt;u&gt;anything&lt;/u&gt; related to the use or misuse of this software.</source>
        <translation>Die Autoren haften nicht für &lt;u&gt;irgendetwas&lt;/u&gt; im Zusammenhang mit der Verwendung oder dem Missbrauch dieser Software.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="157"/>
        <source>Please provide a username for this profile</source>
        <translation>Bitte geben Sie einen Benutzernamen für dieses Profil an</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="579"/>
        <source>CPAP Treatment Information</source>
        <translation>CPAP-Behandlungsinformationen</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="158"/>
        <source>Password Protect Profile</source>
        <translation>Profilpasswort</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="119"/>
        <source>OSCAR is intended merely as a data viewer, and definitely not a substitute for competent medical guidance from your Doctor.</source>
        <translation>OSCAR ist lediglich als Datenanzeige gedacht und ersetzt keinesfalls die kompetente medizinische Anleitung Ihres Arztes.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="116"/>
        <source>OSCAR has been released freely under the &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt;, and comes with no warranty, and without ANY claims to fitness for any purpose.</source>
        <translation>OSCAR wurde frei veröffentlicht unter &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt;, und kommt ohne Gewähr und ohne irgendwelche Ansprüche auf Eignung für irgendeinen Zweck.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="113"/>
        <source>This software is being designed to assist you in reviewing the data produced by your CPAP Devices and related equipment.</source>
        <translation>Diese Software wurde entwickelt, um Sie bei der Überprüfung der von Ihren CPAP-Geräten und zugehörigen Geräten erzeugten Daten zu unterstützen.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="122"/>
        <source>Accuracy of any data displayed is not and can not be guaranteed.</source>
        <translation>Für die Korrektheit der Daten kann nicht garantiert werden.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="370"/>
        <source>D.O.B.</source>
        <translation>Geb. Dat.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="407"/>
        <source>Female</source>
        <translation>Weiblich</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="389"/>
        <source>Gender</source>
        <translation>Geschlecht</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="423"/>
        <source>Height</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="480"/>
        <source>Contact Information</source>
        <translation>Kontaktinformationen</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="216"/>
        <source>Locale Settings</source>
        <translation>Ländereinstellungen</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="634"/>
        <source>CPAP Mode</source>
        <translation>CPAP Modus</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="63"/>
        <source>Select Country</source>
        <translation>Land wählen</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="118"/>
        <source>PLEASE READ CAREFULLY</source>
        <translation>BITTE LESEN SIE</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="620"/>
        <source>Untreated AHI</source>
        <translation>Unbehandelter AHI</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="507"/>
        <location filename="../oscar/newprofile.ui" line="782"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="73"/>
        <source>I agree to all the conditions above.</source>
        <translation>Ich bin mit allen oben genannten Bedingungen einverstanden.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="243"/>
        <source>DST Zone</source>
        <translation>Automatische Sommerzeit</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="54"/>
        <source>about:blank</source>
        <translation>Leere Seite</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="665"/>
        <source>RX Pressure</source>
        <translation>RX Druck</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="185"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="131"/>
        <source>Use of this software is entirely at your own risk.</source>
        <translation>Die Nutzung der Software erfolgt auf eigene Gefahr.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="167"/>
        <source>Passwords don&apos;t match</source>
        <translation>Passwörter stimmen nicht überein</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="347"/>
        <source>First Name</source>
        <translation>Vorname</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="357"/>
        <source>Last Name</source>
        <translation>Nachname</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="288"/>
        <source>Country</source>
        <translation>Land</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="957"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Schließen</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="277"/>
        <source>&amp;Finish</source>
        <translation>&amp;Ende</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="652"/>
        <source>Bi-Level</source>
        <translation>Bi-Level</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="197"/>
        <source>Profile Changes</source>
        <translation>Profiländerungen</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="323"/>
        <source>Personal Information (for reports)</source>
        <translation>Persönliche Informationen (für Berichte)</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="140"/>
        <source>User Name</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="386"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Biological (birth) gender is sometimes needed to enhance the accuracy of a few calculations, feel free to leave this blank and skip any of them.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Manchmal ist ein biologisches (Geburts-) Geschlecht erforderlich, um die Genauigkeit einiger Berechnungen zu verbessern. Lassen Sie dieses Feld leer können Sie alles überspringen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="114"/>
        <source>User Information</source>
        <translation>Benutzerinformationen</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="199"/>
        <source>...twice...</source>
        <translation>...wiederholen...</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="738"/>
        <source>Doctors Name</source>
        <translation>Name des Arztes</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="367"/>
        <source>It&apos;s totally ok to fib or skip this, but your rough age is needed to enhance accuracy of certain calculations.</source>
        <translation>Dies ist jedoch erforderlich, um die Genauigkeit bestimmter Berechnungen zu verbessern.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="711"/>
        <source>Doctors / Clinic Information</source>
        <translation>Name des Arztes in der Klinik</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="755"/>
        <source>Practice Name</source>
        <translation>Name der Praxis</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="606"/>
        <source>Date Diagnosed</source>
        <translation>Diagnosedatum</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="197"/>
        <source>Accept and save this information?</source>
        <translation>Akzeptieren und speichern Sie diese Informationen?</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="765"/>
        <source>Patient ID</source>
        <translation>Patient-ID</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="111"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>Willkommen beim Open Source CPAP Analysis Reporter</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="462"/>
        <source>Metric</source>
        <translation>Metrisch</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="467"/>
        <source>English</source>
        <translation>Englisch</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="155"/>
        <source>Very weak password protection and not recommended if security is required.</source>
        <translation>Sehr schwacher Passwortschutz und nicht empfehlenswert, wenn Sicherheit erforderlich ist.</translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <location filename="../oscar/overview.ui" line="158"/>
        <source>End:</source>
        <translation>Ende:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="276"/>
        <source>Usage</source>
        <translation>Verwendung</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="267"/>
        <source>Respiratory
Disturbance
Index</source>
        <translation>Atem-
Störung
Index</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="181"/>
        <source>Reset view to selected date range</source>
        <translation>Ansicht auf den ausgewählten Datumsbereich zurücksetzen</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="290"/>
        <source>Total Time in Apnea</source>
        <translation>Gesamtzeit im Apnoe</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="244"/>
        <source>Drop down to see list of graphs to switch on/off.</source>
        <translation>Drop-Down-Liste, Diagramme, Ein/Ausschalten.</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="276"/>
        <source>Usage
(hours)</source>
        <translation>Verwendung
(Stunden)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="102"/>
        <source>Last Three Months</source>
        <translation>Letzten 3 Monate</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="290"/>
        <source>Total Time in Apnea
(Minutes)</source>
        <translation>Gesamtzeit im Apnoe
(Minuten)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="122"/>
        <source>Custom</source>
        <translation>Gebrauch</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="353"/>
        <source>How you felt
(0-10)</source>
        <translation>Wie fühlen Sie sich?
(0-10)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="251"/>
        <source>Graphs</source>
        <translation>Diagramme</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="68"/>
        <source>Range:</source>
        <translation>Zeitraum:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="135"/>
        <source>Start:</source>
        <translation>Start:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="92"/>
        <source>Last Month</source>
        <translation>Letzter Monat</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="269"/>
        <source>Apnea
Hypopnea
Index</source>
        <translation>Apnoe
Hypopnoe
Index</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="107"/>
        <source>Last 6 Months</source>
        <translation>Letzten 6 Monate</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="347"/>
        <source>Body
Mass
Index</source>
        <translation>Body
Masse
Index</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="281"/>
        <source>Session Times</source>
        <translation>Anwendungszeit</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="87"/>
        <source>Last Two Weeks</source>
        <translation>Letzten zwei Wochen</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="117"/>
        <source>Everything</source>
        <translation>Alles</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="82"/>
        <source>Last Week</source>
        <translation>Letzte Woche</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="112"/>
        <source>Last Year</source>
        <translation>Letztes Jahr</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="230"/>
        <source>Layout</source>
        <translation>Layout</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="237"/>
        <source>Save and Restore Graph Layout Settings</source>
        <translation>Diagrammlayouteinstellungen speichern und wiederherstellen</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="97"/>
        <source>Last Two Months</source>
        <translation>Letzten 2 Monate</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="127"/>
        <source>Snapshot</source>
        <translation>Schnappschuss</translation>
    </message>
    <message>
        <location filename="../oscar/overview.h" line="202"/>
        <source>Hide All Graphs</source>
        <translation>Alle Diagramme ausblenden</translation>
    </message>
    <message>
        <location filename="../oscar/overview.h" line="203"/>
        <source>Show All Graphs</source>
        <translation>Alle Diagramme anzeigen</translation>
    </message>
</context>
<context>
    <name>OximeterImport</name>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="695"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to enable updating the device identifier next import, which is useful for those who have multiple oximeters lying around.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wer mehrere verschiedene Oxymeter benutzt, muss die Aktualisierung der Geräteerkennung ermöglichen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="537"/>
        <source>Live Oximetry import has been stopped</source>
        <translation>Der Live-Oxymetrie-Import wurde gestoppt</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1026"/>
        <source>Press Start to commence recording</source>
        <translation>Drücken Sie Start, um die Aufnahme zu beginnen</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="331"/>
        <source>Close</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="592"/>
        <source>No CPAP data available on %1</source>
        <translation>Keine CPAP Daten auf%1 verfügbar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1149"/>
        <source>It also can read from ChoiceMMed MD300W1 oximeter .dat files.</source>
        <translation>Es kann auch von Choicemmed MD300W1 Oxymeter DAT-Dateien gelesen werden.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="370"/>
        <source>Please wait until oximeter upload process completes. Do not unplug your oximeter.</source>
        <translation>Bitte warten Sie, bis der Oxymeter Upload-Vorgang abgeschlossen ist. Das Oxymeter nicht trennen.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="734"/>
        <source>Finger not detected</source>
        <translation>Kein Fingerchlip angeschlossen</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="338"/>
        <source>You need to tell your oximeter to begin sending data to the computer.</source>
        <translation>Sie müssen im Menü Ihres Oxymeters den Upload starten.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="436"/>
        <source>No Oximetry module could parse the given file:</source>
        <translation>Kein Oxymetriemodul kann die angegebene Datei analysieren:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="258"/>
        <source>Renaming this oximeter from &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>Dieses Oxymeter umbenennen aus &apos;%1&apos; to &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="868"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import from data files created by software that came with your Pulse Oximeter, such as SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Diese Option ermöglicht den Import von Dateien, welche durch Software von SpO2 Review erzeugt wurde.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="389"/>
        <source>Oximeter import completed..</source>
        <translation>Oxymeterdatenimport abgeschlossen..</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1812"/>
        <source>&amp;Retry</source>
        <translation>&amp;Wiederholen</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1907"/>
        <source>&amp;Start</source>
        <translation>&amp;Start</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1146"/>
        <source>You may wish to note, other companies, such as Pulox, simply rebadge Contec CMS50&apos;s under new names, such as the Pulox PO-200, PO-300, PO-400. These should also work.</source>
        <translation>Sie werden darauf hingewiesen, dass andere Unternehmen, wie Pulox identisch mit dem CMS50 sind. (wie der Pulox PO-200, PO-300, PO-400). Diese sollten auch funktionieren.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="588"/>
        <source>%1 session(s) on %2, starting at %3</source>
        <translation>%1 Sitzung(en) an %2, Starten ab %3</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="837"/>
        <source>I need to set the time manually, because my oximeter doesn&apos;t have an internal clock.</source>
        <translation>Ich muss die Zeit manuell einstellen, denn mein Oxymeter hat keine eigebaute Uhr.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1675"/>
        <source>You can manually adjust the time here if required:</source>
        <translation>Falls erforderlich, können Sie hier die Zeit manuell einstellen:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1142"/>
        <source>OSCAR gives you the ability to track Oximetry data alongside CPAP session data, which can give valuable insight into the effectiveness of CPAP treatment. It will also work standalone with your Pulse Oximeter, allowing you to store, track and review your recorded data.</source>
        <translation>Mit OSCAR können Sie neben CPAP-Sitzungsdaten auch Oxymetriedaten nachverfolgen, wodurch Sie wertvolle Einblicke in die Wirksamkeit der CPAP-Behandlung erhalten. Es funktioniert auch eigenständig mit Ihrem Pulsoxymeter, sodass Sie Ihre aufgezeichneten Daten speichern, verfolgen und überprüfen können.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1097"/>
        <source>Oximeter Session %1</source>
        <translation>Oxymetersitzung %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="473"/>
        <source>Couldn&apos;t access oximeter</source>
        <translation>Konnte nicht auf das Oxymeter zugreifen</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1325"/>
        <source>Please choose which one you want to import into OSCAR</source>
        <translation>Bitte wählen Sie aus, welche Sie in OSCAR importieren möchten</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="953"/>
        <source>Please connect your oximeter device</source>
        <translation>Bitte verbinden Sie Ihr Oxymeter-Gerät</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1155"/>
        <source>Important Notes:</source>
        <translation>Wichtige Hinweise:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="487"/>
        <source>Starting up...</source>
        <translation>Sarten Sie...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1158"/>
        <source>Contec CMS50D+ devices do not have an internal clock, and do not record a starting time. If you do not have a CPAP session to link a recording to, you will have to enter the start time manually after the import process is completed.</source>
        <translation>Contec CMS50D + Geräte verfügen über keine interne Uhr und notieren keine Startzeit. Hier müssen Sie die Startzeit manuell eingeben, nachdem der Importvorgang abgeschlossen ist.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1696"/>
        <source>HH:mm:ssap</source>
        <translation>HH:mm:ssap</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="793"/>
        <source>Import directly from a recording on a device</source>
        <translation>Importieren direkt aus einer Aufzeichnung auf einem Gerät</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="261"/>
        <source>Oximeter name is different.. If you only have one and are sharing it between profiles, set the name to the same on both profiles.</source>
        <translation>Oxymeter Name ist anders. Wenn Sie mit mehreren Profilen arbeiten setzen Sie den Namen auf allen Profilen gleich.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1160"/>
        <source>Even for devices with an internal clock, it is still recommended to get into the habit of starting oximeter records at the same time as CPAP sessions, because CPAP internal clocks tend to drift over time, and not all can be reset easily.</source>
        <translation>Auch bei Geräten mit einer internen Uhr wird empfohlen die CPAP-Sitzung gleichzeitig mit der Oxymetrie-Sitzung zu beginnen. Manche CPAP Geräte neigen mit der Zeit dazu ungenaue Zeitdaten zu liefern.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1774"/>
        <source>&amp;Information Page</source>
        <translation>&amp;Informationsseite</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1457"/>
        <source>I want to use the time reported by my oximeter&apos;s built in clock.</source>
        <translation>Ich will die Zeit von meiner im Oxymeter eingebauten Uhr verwenden.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="536"/>
        <source>Live Oximetry Stopped</source>
        <translation>Live-Oxymetrie gestoppt</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="334"/>
        <source>Waiting for %1 to start</source>
        <translation>Warten auf %1 zu starten</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="918"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Reminder for CPAP users: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Did you remember to import your CPAP sessions first?&lt;br/&gt;&lt;/span&gt;If you forget, you won&apos;t have a valid time to sync this oximetry session to.&lt;br/&gt;To a ensure good sync between devices, always try to start both at the same time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Erinnerung für CPAP Benutzer: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Haben Sie daran gedacht erst Ihre CPAP Daten zu importieren?&lt;br/&gt;&lt;/span&gt;Bitte versuchen Sie immer die CPAP Sitzung gleichzeitig mir der Oxymetrie Sitzung zu starten.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="414"/>
        <source>Select a valid oximetry data file</source>
        <translation>Wählen Sie eine gültige Oxymetriedatendatei aus</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="369"/>
        <source>%1 device is uploading data...</source>
        <translation>%1 Gerät Hochladen von Daten...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1850"/>
        <source>&amp;End Recording</source>
        <translation>&amp;Aufnahme Beenden</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="648"/>
        <source>CMS50E/F users, when importing directly, please don&apos;t select upload on your device until OSCAR prompts you to.</source>
        <translation>CMS50E / F-Benutzer: Wenn Sie direkt importieren, wählen Sie bitte keinen Upload auf Ihrem Gerät aus, bis Sie von OSCAR dazu aufgefordert werden.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1831"/>
        <source>&amp;Choose Session</source>
        <translation>&amp;Wählen Sie die Sitzung</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="329"/>
        <source>Nothing to import</source>
        <translation>Nichts zu importieren</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="337"/>
        <source>Select upload option on %1</source>
        <translation>Wählen Sie eine Uploadfunktion %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="335"/>
        <source>Waiting for the device to start the upload process...</source>
        <translation>Warten auf das Gerät, um den Upload-Vorgang zu starten...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="222"/>
        <source>Could not detect any connected oximeter devices.</source>
        <translation>Konnte keine angeschlossenen Oxymeter Geräte erkennen.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="61"/>
        <location filename="../oscar/oximeterimport.cpp" line="39"/>
        <source>Oximeter Import Wizard</source>
        <translation>Oxymeter Import-Assistent</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="499"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Please note: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;First select your correct oximeter type from the pull-down menu below.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Hinweis: &lt;/span&gt;&lt;span style=&quot; font-style:italic; &quot;&gt;Wählen Sie zuerst Ihren richtigen Oximetertyp aus dem Pulldown-Menü unten aus.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="586"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:700;&quot;&gt;FIRST Select your Oximeter from these groups:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:700;&quot;&gt;Wählen Sie ZUERST Ihr Oximeter aus diesen Gruppen aus:&lt;/span&gt;&lt;/p&gt;&lt;/ body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1399"/>
        <source>Day recording (normally would have) started</source>
        <translation>Tagesaufzeichnung (hätte normalerweise) begonnen</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1473"/>
        <source>I started this oximeter recording at (or near) the same time as a session on my CPAP device.</source>
        <translation>Ich habe diese Oximeteraufzeichnung zur (oder nahezu) gleichen Zeit wie eine Sitzung auf meinem CPAP-Gerät gestartet.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1888"/>
        <source>&amp;Save and Finish</source>
        <translation>&amp;Speichern und Beenden</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1140"/>
        <source>Pulse Oximeters are medical devices used to measure blood oxygen saturation. During extended Apnea events and abnormal breathing patterns, blood oxygen saturation levels can drop significantly, and can indicate issues that need medical attention.</source>
        <translation>Pulsoxymeter sind medizinische Geräte und werden zur Feststellung der Sauerstoffsättigung im Blut benutzt. Bei längeren Apnea Ereignissen und abnormalen Atemmustern, kann die Blutsauerstoffsättigung deutlich sinken. Dann sollten Sie Ihren Arzt informieren.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1156"/>
        <source>For OSCAR to be able to locate and read directly from your Oximeter device, you need to ensure the correct device drivers (eg. USB to Serial UART) have been installed on your computer. For more information about this, %1click here%2.</source>
        <translation>Damit OSCAR direkt von Ihrem Oxymeter-Gerät aus suchen und lesen kann, müssen Sie sicherstellen, dass die richtigen Gerätetreiber (z. B. USB zu Serial UART) auf Ihrem Computer installiert sind. Weitere Informationen dazu, %1klick hier%2.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1348"/>
        <source>Start Time</source>
        <translation>Startzeit</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1502"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR needs a starting time to know where to save this oximetry session to.&lt;/p&gt;&lt;p&gt;Choose one of the following options:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR benötigt eine Startzeit, um zu wissen, wo die Oxymetriesitzung gespeichert werden soll.&lt;/p&gt;&lt;p&gt;Wählen Sie eine der folgenden Optionen:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1217"/>
        <source>Pulse Rate</source>
        <translation>Pulsrate</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1534"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: Syncing to CPAP session starting time will always be more accurate.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Hinweis: Synchronisieren Sie die CPAP-Sitzungs- Startzeit. Danach wird das Ergebnis immer genauer sein.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="688"/>
        <source>Set device date/time</source>
        <translation>Datum/Uhrzeit im Gerät gesetzt</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="414"/>
        <source>Oximetry Files (*.spo *.spor *.spo2 *.SpO2 *.dat)</source>
        <translation>Oxymetriedateien (*.spo *.spor *.spo2 *.SpO2 *.dat)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1375"/>
        <source>Import Completed. When did the recording start?</source>
        <translation>Import abgeschlossen. Wann hat die Aufnahme zu starten?</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="874"/>
        <source>Import from a datafile saved by another program, like SpO2Review</source>
        <translation>Import aus einer Datendatei von einem anderen Programm, welche von SpO2Review gespeichert wurden</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1309"/>
        <source>Multiple Sessions Detected</source>
        <translation>Mehrere Sitzungen erkannt</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="835"/>
        <source>Record attached to computer overnight (provides plethysomogram)</source>
        <translation>Waren Sie über Nacht an den PC angeschlossen benutzen Sie das (Plethysonogramm)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="761"/>
        <source>Erase session after successful upload</source>
        <translation>Löschen der Sitzung nach erfolgreichem Upload</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="484"/>
        <source>Live Oximetry Mode</source>
        <translation>Live-Oxymetriemodus</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1793"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Schließen</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="698"/>
        <source>Set device identifier</source>
        <translation>Geräteidentifizierung</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1358"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="685"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If enabled, OSCAR will automatically reset your CMS50&apos;s internal clock using your computers current time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wenn aktiviert, setzt OSCAR die interne Uhr des CMS50 automatisch auf die aktuelle Uhrzeit Ihres Computers zurück.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="466"/>
        <source>Oximeter not detected</source>
        <translation>Kein Oxymeter angeschlossen</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1151"/>
        <source>Please remember:</source>
        <translation>Bitte beachten Sie:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="542"/>
        <source>Where would you like to import from?</source>
        <translation>Von wo wollen Sie importieren?</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="971"/>
        <source>If you can read this, you likely have your oximeter type set wrong in preferences.</source>
        <translation>Wenn Sie das lesen, haben Sie nicht das richtige Oxymeter in den Einstellungen gewählt.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="834"/>
        <source>I want to use the time my computer recorded for this live oximetry session.</source>
        <translation>Ich möchte die Zeit von meinem Computer für diese Live-Oxymetrie-Sitzung benutzen.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="190"/>
        <source>Scanning for compatible oximeters</source>
        <translation>Scannen von kompatieblen Oxymetern</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="339"/>
        <source>Please connect your oximeter, enter it&apos;s menu and select upload to commence data transfer...</source>
        <translation>Bitte schließen Sie Ihr Oxymeter an und wählen Sie im Menü Upload mit Datenübertragung beginnen...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1555"/>
        <source>Choose CPAP session to sync to:</source>
        <translation>Wählen Sie aus um die CPAP-Sitzung zu synchronisieren:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1101"/>
        <location filename="../oscar/oximeterimport.ui" line="1353"/>
        <source>Duration</source>
        <translation>Dauer</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1138"/>
        <source>Welcome to the Oximeter Import Wizard</source>
        <translation>Herzlich Willkommen beim Oxymeter-Import-Assistenten</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="829"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If you don&apos;t mind a being attached to a running computer overnight, this option provide a useful plethysomogram graph, which gives an indication of heart rhythm, on top of the normal oximetry readings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sollten Sie das Pulsoxymeter über Nacht an einem PC betreiben, kann diese Option sehr nützlich sein um Herzrhythmusstörungen zu erkennen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1153"/>
        <source>If you are trying to sync oximetry and CPAP data, please make sure you imported your CPAP sessions first before proceeding!</source>
        <translation>Wenn Sie versuchen, Oxymetrie- und CPAP-Daten zu synchronisieren, stellen Sie sicher, dass Sie Ihre CPAP-Sitzungen zuerst importiert haben, bevor Sie fortfahren!</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="304"/>
        <source>&quot;%1&quot;, session %2</source>
        <translation>&quot;%1&quot;, Sitzung %2</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1071"/>
        <source>Show Live Graphs</source>
        <translation>Live-Diagramme anzeigen</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="535"/>
        <source>Live Import Stopped</source>
        <translation>Live Import stoppen</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1445"/>
        <source>Oximeter Starting time</source>
        <translation>Oxymeter Startzeit</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="444"/>
        <source>Skip this page next time.</source>
        <translation>Überspringen Sie diese Seite das nächste Mal.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="488"/>
        <source>If you can still read this after a few seconds, cancel and try again</source>
        <translation>Sollte der Vorgang zu lange dauern, starten Sie Ihn nach ein paar Sekunden erneut</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1869"/>
        <source>&amp;Sync and Save</source>
        <translation>&amp;Sync und Speichern</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="330"/>
        <source>Your oximeter did not have any valid sessions.</source>
        <translation>Ihre Oxymeter hatten keine gültigen Sitzungen.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="849"/>
        <source>Something went wrong getting session data</source>
        <translation>Ein Fehler ist immer wenn Sitzungs-Daten nicht übereinstimmen</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="230"/>
        <source>Connecting to %1 Oximeter</source>
        <translation>Anschließen an ein %1 Oxymeter</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="727"/>
        <source>Recording...</source>
        <translation>Aufnahme...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1144"/>
        <source>OSCAR is currently compatible with Contec CMS50D+, CMS50E, CMS50F and CMS50I serial oximeters.&lt;br/&gt;(Note: Direct importing from bluetooth models is &lt;span style=&quot; font-weight:600;&quot;&gt;probably not&lt;/span&gt; possible yet)</source>
        <translation>OSCAR ist derzeit kompatibel mit den seriellen Oximetern Contec CMS50D+, CMS50E, CMS50F und CMS50I.&lt;br/&gt;(Hinweis: Der direkte Import aus Bluetooth-Modellen ist möglich. &lt;span style=&quot; font-weight:600;&quot;&gt;wahrscheinlich noch nicht&lt;/span&gt; möglich)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="717"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Here you can enter a 7 character name for this oximeter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Hier können Sie einen 7-stelligen Namen für dieses Oximeter eingeben.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="758"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will erase the imported session from your oximeter after import has completed. &lt;/p&gt;&lt;p&gt;Use with caution,  because if something goes wrong before OSCAR saves your session, you can&apos;t get it back.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Diese Option löscht die importierte Sitzung aus Ihrem Oximeter, nachdem der Import abgeschlossen ist. &lt;/p&gt;&lt;p&gt;Seien Sie vorsichtig, denn wenn etwas schief geht, bevor OSCAR Ihre Sitzung speichert, können Sie es nicht zurückbekommen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="787"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import (via cable) from your oximeters internal recordings.&lt;/p&gt;&lt;p&gt;After selecting on this option, old Contec oximeters will require you to use the device&apos;s menu to initiate the upload.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Mit dieser Option können Sie (per Kabel) interne Aufzeichnungen aus dem Oximeter importieren.&lt;/p&gt;&lt;p&gt;Nachdem Sie diese Option ausgewählt haben, müssen Sie bei alten Contec-Oximetern das Menü des Geräts verwenden, um den Upload zu starten.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1000"/>
        <source>Please connect your oximeter device, turn it on, and enter the menu</source>
        <translation>Bitte schließen Sie Ihr Oximetergerät an, schalten Sie es ein und öffnen Sie das Menü</translation>
    </message>
</context>
<context>
    <name>Oximetry</name>
    <message>
        <location filename="../oscar/oximetry.ui" line="89"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="245"/>
        <source>Pulse</source>
        <translation>Puls</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="366"/>
        <source>&amp;Open .spo/R File</source>
        <translation>&amp;Öffnen .spo/R Datei</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="131"/>
        <source>R&amp;eset</source>
        <translation>R&amp;eset</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="385"/>
        <source>Serial &amp;Import</source>
        <translation>Serien &amp;Import</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="421"/>
        <source>Serial Port</source>
        <translation>Serien Port</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="102"/>
        <source>d/MM/yy h:mm:ss AP</source>
        <translation>d/MM/yy h:mm:ss AP</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="398"/>
        <source>&amp;Start Live</source>
        <translation>&amp;Start-Live</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="450"/>
        <source>&amp;Rescan Ports</source>
        <translation>&amp;Ports neu scannen</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="999"/>
        <location filename="../oscar/preferencesdialog.ui" line="1119"/>
        <location filename="../oscar/preferencesdialog.ui" line="1592"/>
        <location filename="../oscar/preferencesdialog.ui" line="1683"/>
        <location filename="../oscar/preferencesdialog.ui" line="1712"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3182"/>
        <source>&amp;Ok</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1323"/>
        <source>AHI</source>
        <extracomment>Apnea Hypopnea Index</extracomment>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1328"/>
        <source>RDI</source>
        <extracomment>Respiratory Disturbance Index</extracomment>
        <translation>RDI</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1256"/>
        <source>Switching off backups is not a good idea, because OSCAR needs these to rebuild the database if errors are found.

</source>
        <translation>Das Abschalten von Sicherungen ist keine gute Idee, da OSCAR diese benötigt, um die Datenbank neu zu erstellen, falls Fehler gefunden werden.

</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1582"/>
        <location filename="../oscar/preferencesdialog.ui" line="1615"/>
        <location filename="../oscar/preferencesdialog.ui" line="1696"/>
        <source> bpm</source>
        <translation> bpm</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2384"/>
        <source>Graph Height</source>
        <translation>Diagrammhöhe</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="50"/>
        <source>Flag</source>
        <translation>Flag</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2821"/>
        <source>Font</source>
        <translation>Schriftart</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="467"/>
        <location filename="../oscar/preferencesdialog.cpp" line="598"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2839"/>
        <source>Size</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="52"/>
        <source>Span</source>
        <translation>Spannweite</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="66"/>
        <source>No CPAP devices detected</source>
        <translation>Keine CPAP-Geräte erkannt</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="67"/>
        <source>Will you be using a ResMed brand device?</source>
        <translation>Werden Sie ein Gerät der Marke ResMed verwenden?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="74"/>
        <source>&lt;p&gt;&lt;b&gt;Please Note:&lt;/b&gt; OSCAR&apos;s advanced session splitting capabilities are not possible with &lt;b&gt;ResMed&lt;/b&gt; devices due to a limitation in the way their settings and summary data is stored, and therefore they have been disabled for this profile.&lt;/p&gt;&lt;p&gt;On ResMed devices, days will &lt;b&gt;split at noon&lt;/b&gt; like in ResMed&apos;s commercial software.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Bitte beachten Sie:&lt;/b&gt; Die erweiterten Sitzungsaufteilungsfunktionen von OSCAR sind mit &lt;b&gt;ResMed&lt;/b&gt;-Geräten aufgrund einer Einschränkung in der Art und Weise, wie ihre Einstellungen und Zusammenfassungsdaten gespeichert werden, nicht möglich, und deshalb sind sie es wurde für dieses Profil deaktiviert.&lt;/p&gt;&lt;p&gt;Auf ResMed-Geräten werden die Tage &lt;b&gt;mittags geteilt&lt;/b&gt; wie in der kommerziellen Software von ResMed.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1173"/>
        <source>ResMed S9 devices routinely delete certain data from your SD card older than 7 and 30 days (depending on resolution).</source>
        <translation>ResMed S9-Geräte löschen routinemäßig bestimmte Daten von Ihrer SD-Karte, die älter als 7 und 30 Tage sind (je nach Auflösung).</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="659"/>
        <source>&amp;CPAP</source>
        <translation>&amp;CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1981"/>
        <source>General Settings</source>
        <translation>Allgemeine Einstellungen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2598"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This makes scrolling when zoomed in easier on sensitive bidirectional TouchPads&lt;/p&gt;&lt;p&gt;50ms is recommended value.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Dies erleichtert das Scrollen beim Vergrößern auf empfindlichen bidirektionalen TouchPads&lt;/p&gt;&lt;p&gt;50ms ist der empfohlenen Wert.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="468"/>
        <location filename="../oscar/preferencesdialog.cpp" line="599"/>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2317"/>
        <location filename="../oscar/preferencesdialog.ui" line="2356"/>
        <source>Daily</source>
        <translation>Täglich</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1155"/>
        <source>Event Duration</source>
        <translation>Sitzungsdauer</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="780"/>
        <source>Hours</source>
        <translation>Stunden</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="471"/>
        <location filename="../oscar/preferencesdialog.cpp" line="603"/>
        <source>Label</source>
        <translation>Aufschrift</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="601"/>
        <source>Lower</source>
        <translation>untere</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="295"/>
        <source>Never</source>
        <translation>Niemals</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1539"/>
        <source>Oximetry Settings</source>
        <translation>Oxymetrieeinstellungen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1728"/>
        <source>Pulse</source>
        <translation>Puls</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2046"/>
        <source>Graphics Engine (Requires Restart)</source>
        <translation>Grafikanwendung (neu starten)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="602"/>
        <source>Upper</source>
        <translation>obere</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2144"/>
        <source>days.</source>
        <translation>tägl.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="690"/>
        <source>Here you can set the &lt;b&gt;upper&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>Hier können Sie einstellen &lt;b&gt;obere&lt;/b&gt; Schwelle für bestimmte Berechnungen welche die Wellenform verwendet %1</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2374"/>
        <source>After Import</source>
        <translation>Nach dem Import</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="245"/>
        <source>Ignore Short Sessions</source>
        <translation>Ignorieren von kurzen Sitzungen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="624"/>
        <source>Sleep Stage Waveforms</source>
        <translation>Schlafstadium-Wellenform</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1095"/>
        <source>Percentage of restriction in airflow from the median value. 
A value of 20% works well for detecting apneas. </source>
        <translation>Prozentualer Anteil der Einschränkung des Luftstroms aus dem Medianwert.
Ein Wert von 20% eignet sich gut zum Nachweis von Apnoen. </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="321"/>
        <source>Sessions starting before this time will go to the previous calendar day.</source>
        <translation>Sitzungen vor dieser Zeit werden auf den voangegangenen Tag genommen.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="394"/>
        <source>Session Storage Options</source>
        <translation>Sitzungs Speicher Optonen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3016"/>
        <source>Graph Titles</source>
        <translation>Diagrammtitel</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1265"/>
        <source>Zero Reset</source>
        <translation>Nullsetzung</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="801"/>
        <source>A data re/decompression proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>Ein Daten-Dekomprimierungsvorgang ist erforderlich, um diese Änderungen anzuwenden. Dieser Vorgang kann einige Minuten dauern.

Möchten Sie diese Änderungen wirklich vornehmen?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1058"/>
        <source>Flow Restriction</source>
        <translation>Durchflussbegrenzung</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1076"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Custom flagging is an experimental method of detecting events missed by the device. They are &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; included in AHI.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Benutzerdefinierte Kennzeichnung ist eine experimentelle Methode zur Erkennung von Ereignissen, die vom Gerät übersehen wurden. Sie sind &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;nicht&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos; ; font-size:10pt;&quot;&gt; in AHI enthalten.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1193"/>
        <source>Enable Unknown Events Channels</source>
        <translation>Aktivieren Sie die Kanäle für unbekannte Ereignisse</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1680"/>
        <source>Minimum duration of drop in oxygen saturation</source>
        <translation>Mindestdauer des Abfalls der Sauerstoffsättigung</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2212"/>
        <source>I want to be notified of test versions. (Advanced users only please.)</source>
        <translation>Ich möchte über Testversionen informiert werden. (Bitte nur fortgeschrittene Benutzer.)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2653"/>
        <source>Overview Linecharts</source>
        <translation>Übersicht Liniendiagramme</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2732"/>
        <source>Whether to allow changing yAxis scales by double clicking on yAxis labels</source>
        <translation>Ob sich ändernde yAchse Skalen durch Doppelklick auf yAchse Etiketten ermöglichen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="53"/>
        <source>Always Minor</source>
        <translation>immer Klein</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="492"/>
        <source>Unknown Events</source>
        <translation>Unbekannte Ereignisse</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2702"/>
        <source>Pixmap caching is an graphics acceleration technique. May cause problems with font drawing in graph display area on your platform.</source>
        <translation>Pixmap-Caching ist eine Grafikbeschleunigungstechnik, welche zu Problemen mit der Anzeige von Schrift in dem Grafik-Anzeigebereich auf Ihrer Plattform führen kann.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1773"/>
        <location filename="../oscar/preferencesdialog.ui" line="1862"/>
        <location filename="../oscar/preferencesdialog.ui" line="1941"/>
        <source>Reset &amp;Defaults</source>
        <translation>Auf &amp;Standardwerte zurücksetzen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="575"/>
        <source>Bypass the login screen and load the most recent User Profile</source>
        <translation>Umgehen Sie den Login-Bildschirm und laden Sie das neueste Benutzerprofil</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="809"/>
        <source>Data Reindex Required</source>
        <translation>Erforderliche Daten indizieren</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2480"/>
        <source>Scroll Dampening</source>
        <translation>Bildlauf Dämpfung</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1176"/>
        <source> Are you sure you want to disable these backups?</source>
        <translation> Möchten Sie diese Sicherungen wirklich deaktivieren?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1248"/>
        <source>Flag leaks over threshold</source>
        <translation>Leck-Flag über dem Schwellenwert</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="916"/>
        <source>20 cmH2O</source>
        <translation>20 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1300"/>
        <source> hours</source>
        <translation> Stunden</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="664"/>
        <source>Double click to change the descriptive name this channel.</source>
        <translation>Klicken Sie doppelt auf den beschreibenden Namen um diesen Kanal zu ändern.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="509"/>
        <source>Sessions older than this date will not be imported</source>
        <translation>Sitzungen, die älter als dieses Datum sind werden nicht importiert</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2431"/>
        <source>Standard Bars</source>
        <translation>Standardbalken</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1448"/>
        <source>99% Percentile</source>
        <translation>99% Prozentuale</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="272"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt;&quot;&gt;Sessions shorter in duration than this will not be displayed&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt;&quot;&gt;Sitzungen, die kürzer als diese Dauer sind, werden nicht angezeigt.&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="569"/>
        <source>Memory and Startup Options</source>
        <translation>Speicher und Startoptionen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="625"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cuts down on any unimportant confirmation dialogs during import.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;verkürzt sich auf irgendwelche unwichtigen Bestätigungsdialoge beim Import.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1589"/>
        <source>Small chunks of oximetry data under this amount will be discarded.</source>
        <translation>Kleine Abschnitte von Oxymetriedaten unter diesem Betrag, werden verworfen.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="622"/>
        <source>Oximeter Waveforms</source>
        <translation>Oxymeter Wellenformen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1278"/>
        <source>User definable threshold considered large leak</source>
        <translation>Frei definierbare Schwelle als großes Leck</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1262"/>
        <source>Reset the counter to zero at beginning of each (time) window.</source>
        <translation>Setzen Sie den Zähler zu Beginn eines jeden (Zeit-) Abschnitte s auf Null.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1207"/>
        <source>Compliance defined as</source>
        <translation>Therapietreue definiert als</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="608"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Makes starting OSCAR a bit slower, by pre-loading all the summary data in advance, which speeds up overview browsing and a few other calculations later on. &lt;/p&gt;&lt;p&gt;If you have a large amount of data, it might be worth keeping this switched off, but if you typically like to view &lt;span style=&quot; font-style:italic;&quot;&gt;everything&lt;/span&gt; in overview, all the summary data still has to be loaded anyway. &lt;/p&gt;&lt;p&gt;Note this setting doesn&apos;t affect waveform and event data, which is always demand loaded as needed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Startet OSCAR etwas langsamer, indem alle Übersichtsdaten vorab geladen werden, wodurch das Durchsuchen von Übersichten und einige andere Berechnungen später beschleunigt wird. &lt;/p&gt;&lt;p&gt;Wenn Sie über eine große Datenmenge verfügen, kann es sich lohnen, diese Option deaktiviert zu lassen, wenn Sie sie jedoch normalerweise anzeigen möchten &lt;span style=&quot; font-style:italic;&quot;&gt;alles&lt;/span&gt; in der Übersicht müssen alle zusammenfassenden Daten trotzdem noch geladen werden. &lt;/p&gt;&lt;p&gt;Beachten Sie, dass sich diese Einstellung nicht auf Wellenform- und Ereignisdaten auswirkt, die bei Bedarf immer nach Bedarf geladen werden.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="557"/>
        <source>Here you can change the type of flag shown for this event</source>
        <translation>Hier können Sie die Art der Markierung für das gezeigte Ereigniss ändern</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2436"/>
        <source>Top Markers</source>
        <translation>Obere Markierung</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="817"/>
        <source>One or more of the changes you have made will require this application to be restarted, in order for these changes to come into effect.

Would you like do this now?</source>
        <translation>Für eine oder mehrere der von Ihnen vorgenommenen Änderungen muss diese Anwendung neu gestartet werden, damit die Änderungen wirksam werden.

Möchten Sie das jetzt tun?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1224"/>
        <source> minutes</source>
        <translation> minuten</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="170"/>
        <location filename="../oscar/preferencesdialog.ui" line="255"/>
        <location filename="../oscar/preferencesdialog.ui" line="753"/>
        <source>Minutes</source>
        <translation>Minuten</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="484"/>
        <source>Create SD Card Backups during Import (Turn this off at your own peril!)</source>
        <translation>Erstellen Sie ein SD-Karten Backup während des Imports (Deaktivieren Sie dieses auf eigene Gefahr!)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2284"/>
        <source>Graph Settings</source>
        <translation>Diagramm-Einstellungen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="562"/>
        <location filename="../oscar/preferencesdialog.cpp" line="695"/>
        <source>This is the short-form label to indicate this channel on screen.</source>
        <translation>Das ist das Kurzform-Label, um diesen Kanal auf dem Bildschirm anzuzeigen.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="453"/>
        <source>The following options affect the amount of disk space OSCAR uses, and have an effect on how long import takes.</source>
        <translation>Die folgenden Optionen wirken sich auf den von OSCAR verwendeten Festplattenspeicherplatz aus und wirken sich auch darauf aus, wie lange der Import dauert.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="488"/>
        <source>CPAP Events</source>
        <translation>CPAP Ereignisse</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2857"/>
        <source>Bold  </source>
        <translation>Fett  </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2300"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Which tab to open on loading a profile. (Note: It will default to Profile if OSCAR is set to not open a profile on startup)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Welche Registerkarte wird beim Laden eines Profils geöffnet? (Hinweis: Standardmäßig wird ein Profil festgelegt, wenn OSCAR beim Starten nicht zum Öffnen eines Profils konfiguriert ist.)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1709"/>
        <source>Minimum duration of pulse change event.</source>
        <translation>Mindestdauer von Pulswechsel-Ereignissen.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2678"/>
        <source>Anti-Aliasing applies smoothing to graph plots.. 
Certain plots look more attractive with this on. 
This also affects printed reports.

Try it and see if you like it.</source>
        <translation>Anti-Aliasing gilt für eine Verbesserung der grphischen Darstellung..
Bestimmte Darstellungen sehen besser aus wenn Sie das aktivieren.
Dies wirkt sich auch auf gedruckte Berichte Berichte aus.

Probieren Sie es aus und sehen, ob es Ihnen gefällt.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="491"/>
        <source>Sleep Stage Events</source>
        <translation>Schlafstadium Ereignisse</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1809"/>
        <source>Events</source>
        <translation>Ereignisse</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="357"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;This setting should be used with caution...&lt;/span&gt; Switching it off comes with consequences involving accuracy of summary only days, as certain calculations only work properly provided summary only sessions that came from individual day records are kept together. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ResMed users:&lt;/span&gt; Just because it seems natural to you and I that the 12 noon session restart should be in the previous day, does not mean ResMed&apos;s data agrees with us. The STF.edf summary index format has serious weaknesses that make doing this not a good idea.&lt;/p&gt;&lt;p&gt;This option exists to pacify those who don&apos;t care and want to see this &amp;quot;fixed&amp;quot; no matter the costs, but know it comes with a cost. If you keep your SD card in every night, and import at least once a week, you won&apos;t see problems with this very often.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Diese Einstellung sollte mit Vorsicht angewendet werden...&lt;/span&gt; Das Ausschalten hat Konsequenzen auf die Genauigkeit der Zusammenfassung von Tagen, da bestimmte Berechnungen nur
ordnungsgemäß auf die Zusammenfassung von Sitzungen funktionieren. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ResMed Anwender:&lt;/span&gt; Nur weil es so scheint, dass die 12 Uhr-Sitzung der Neustart in den vorherigen Tag sein sollte, bedeutet es nicht, dass die ResMed Daten mit uns übereinstimmen. 
Das STF.edf Zusammenfassung Indexformat hat gravierende Schwächen. Es ist keine gute Idee, es zu tun.&lt;/p&gt;&lt;p&gt;Diese Option gibt es für diejenigen, denen es egal ist ob Sie es sehen oder nicht &amp;quot;feststehend&amp;quot; Wenn Sie die SD-Karte in jeder Nacht benutzen, und den Import mindestens einmal pro Woche erledigen, 
werden Sie sehen, dass es nicht sehr oft zu Problemen kommt.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1405"/>
        <source>Median is recommended for ResMed users.</source>
        <translation>Medianwert ist für ResMed Benutzer empfohlen.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="489"/>
        <source>Oximeter Events</source>
        <translation>Oxymeter Ereignisse</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2878"/>
        <source>Italic</source>
        <translation>Kursiv</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2006"/>
        <source>Enable Multithreading</source>
        <translation>aktivieren Sie Multithreading</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1172"/>
        <source>This may not be a good idea</source>
        <translation>Das ist keine gute Idee</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1414"/>
        <source>Weighted Average</source>
        <translation>Gewichteter Durchschnitt</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1409"/>
        <location filename="../oscar/preferencesdialog.ui" line="1472"/>
        <source>Median</source>
        <translation>Medianwert</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1661"/>
        <source>Flag rapid changes in oximetry stats</source>
        <translation>Flag bei raschen Veränderungen in der Oxymetrie-Statistik</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1693"/>
        <source>Sudden change in Pulse Rate of at least this amount</source>
        <translation>Plötzliche Änderung in Pulsfrequenz von mindestens diesen Betrag</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1795"/>
        <source> &lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt; p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Syncing Oximetry and CPAP Data&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;CMS50 data imported from SpO2Review (from .spoR files) or the serial import method do &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; have the correct timestamp needed to sync.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Live view mode (using a serial cable) is one way to acheive an accurate sync on CMS50 oximeters, but does not counter for CPAP clock drift.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;If you start your Oximeters recording mode at &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;exactly &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;the same time you start your CPAP device, you can now also achieve sync. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;The serial import process takes the starting time from last nights first CPAP session. (Remember to import your CPAP data first!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation> &lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt; p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Synchronisierung von Oximetrie- und CPAP-Daten&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;CMS50-Daten, die von SpO2Review (aus .spoR-Dateien) oder der seriellen Importmethode importiert werden, haben &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;nicht&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; den korrekten Zeitstempel, der für die Synchronisierung benötigt wird.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Der Live-View-Modus (unter Verwendung eines seriellen Kabels) ist eine Möglichkeit, eine genaue Synchronisierung bei CMS50-Oximetern zu erreichen, gleicht jedoch nicht die CPAP-Taktdrift aus.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Wenn Sie den Aufzeichnungsmodus Ihres Oximeters &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;genau &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;zu dem Zeitpunkt starten, zu dem Sie Ihr CPAP-Gerät einschalten, können Sie nun auch eine Synchronisierung erreichen. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Für den Serienimport wird die Startzeit der ersten CPAP-Sitzung der letzten Nacht verwendet. (Denken Sie daran, Ihre CPAP-Daten zuerst zu importieren!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1832"/>
        <location filename="../oscar/preferencesdialog.ui" line="1911"/>
        <source>Search</source>
        <translation>Suche</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1462"/>
        <source>Time Weighted average of Indice</source>
        <translation>Zeit Gewichteter Durchschnitt der Indice</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1377"/>
        <source>Middle Calculations</source>
        <translation>Mittel Berechnungen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2725"/>
        <source>Skip over Empty Days</source>
        <translation>Leere Tage überspringen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2426"/>
        <source>The visual method of displaying waveform overlay flags.
</source>
        <translation>Das visuelle Verfahren zur Darstellung von Wellenüberlagerungsansichten.
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1391"/>
        <source>Upper Percentile</source>
        <translation>Obere Prozentuale</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="816"/>
        <source>Restart Required</source>
        <translation>Neustart erforderlich</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1245"/>
        <source>Whether to show the leak redline in the leak graph</source>
        <translation>Ob die rote Linie im Leck Graphen angezeigt werden soll</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1174"/>
        <source> If you ever need to reimport this data again (whether in OSCAR or ResScan) this data won&apos;t come back.</source>
        <translation> Wenn Sie diese Daten jemals erneut importieren müssen (ob in OSCAR oder ResScan), werden diese Daten nicht zurückgegeben.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1443"/>
        <source>True Maximum</source>
        <translation>Echte Maximum</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="51"/>
        <source>Minor Flag</source>
        <translation>Kleinere Flag</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="800"/>
        <source>Data Processing Required</source>
        <translation>Datenverarbeitung erforderlich</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1354"/>
        <source>For consistancy, ResMed users should use 95% here,
as this is the only value available on summary-only days.</source>
        <translation>Für Konsistenz sollten ResMed Nutzer hier 95% verwenden,
denn dies ist der einzige Wert in der Tageszusammenfassung der lieferbar ist.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="906"/>
        <source>4 cmH2O</source>
        <translation>4 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2290"/>
        <source>On Opening</source>
        <translation>Beim Öffnen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="611"/>
        <source>Pre-Load all summary data at startup</source>
        <translation>Übersichtsdaten beim Start vorladen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2013"/>
        <source>Show Remove Card reminder notification on OSCAR shutdown</source>
        <translation>Show Reminder Card Reminder-Benachrichtigung beim Herunterfahren von OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2346"/>
        <source>No change</source>
        <translation>Keine Änderung</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2955"/>
        <source>Graph Text</source>
        <translation>Diagrammtext</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="542"/>
        <location filename="../oscar/preferencesdialog.cpp" line="671"/>
        <source>Double click to change the default color for this channel plot/flag/data.</source>
        <translation>Klicken Sie doppelt auf die Standardfarbe für diese Kanal Parzelle/Markierung/Daten ändern.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1200"/>
        <source>AHI/Hour Graph Time Window</source>
        <translation>AHI/Stunde Diagramm Zeitfenster</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="628"/>
        <source>Import without asking for confirmation</source>
        <translation>Import ohne weitere Bestätigung</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="475"/>
        <source>This maintains a backup of SD-card data for ResMed devices, 

ResMed S9 series devices delete high resolution data older than 7 days, 
and graph data older than 30 days..

OSCAR can keep a copy of this data if you ever need to reinstall. 
(Highly recomended, unless your short on disk space or don&apos;t care about the graph data)</source>
        <translation>Dadurch wird eine Sicherungskopie der SD-Kartendaten für ResMed-Geräte verwaltet,

Geräte der ResMed S9-Serie löschen hochauflösende Daten, die älter als 7 Tage sind,
und Diagrammdaten, die älter als 30 Tage sind.

OSCAR kann eine Kopie dieser Daten aufbewahren, falls Sie jemals eine Neuinstallation durchführen müssen.
(Sehr empfehlenswert, es sei denn, Sie haben wenig Speicherplatz oder interessieren sich nicht für die Diagrammdaten)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="635"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Provide an alert when importing data from any device model that has not yet been tested by OSCAR developers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Seien Sie gewarnt, wenn Sie Daten von einem Gerätemodell importieren, das noch nicht von OSCAR-Entwicklern getestet wurde.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="638"/>
        <source>Warn when importing data from an untested device</source>
        <translation>Warnung beim Importieren von Daten von einem ungetesteten Gerät</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="803"/>
        <source>This calculation requires Total Leaks data to be provided by the CPAP device. (Eg, PRS1, but not ResMed, which has these already)

The Unintentional Leak calculations used here are linear, they don&apos;t model the mask vent curve.

If you use a few different masks, pick average values instead. It should still be close enough.</source>
        <translation>Für diese Berechnung müssen vom CPAP-Gerät Gesamtleckagedaten bereitgestellt werden. (z. B. PRS1, aber nicht ResMed, das diese bereits hat)

Die hier verwendeten unbeabsichtigten Leckageberechnungen sind linear, sie modellieren nicht die Maskenentlüftungskurve.

Wenn Sie einige verschiedene Masken verwenden, wählen Sie stattdessen Durchschnittswerte. Es sollte noch nah genug sein.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="967"/>
        <source>Enable/disable experimental event flagging enhancements. 
It allows detecting borderline events, and some the device missed.
This option must be enabled before import, otherwise a purge is required.</source>
        <translation>Aktivieren/deaktivieren Sie experimentelle Verbesserungen der Ereigniskennzeichnung.
Es ermöglicht die Erkennung grenzwertiger Ereignisse und einige, die das Gerät übersehen hat.
Diese Option muss vor dem Import aktiviert werden, andernfalls ist eine Bereinigung erforderlich.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1019"/>
        <source>This experimental option attempts to use OSCAR&apos;s event flagging system to improve device detected event positioning.</source>
        <translation>Diese experimentelle Option versucht, das Ereignismarkierungssystem von OSCAR zu verwenden, um die Positionierung von vom Gerät erkannten Ereignissen zu verbessern.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1022"/>
        <source>Resync Device Detected Events (Experimental)</source>
        <translation>Neue Syncronisierung vom Gerät erkannte Ereignisse (experimentell)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1142"/>
        <source>Allow duplicates near device events.</source>
        <translation>Duplikate in der Nähe von Geräteereignissen zulassen.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1190"/>
        <source>Show flags for device detected events that haven&apos;t been identified yet.</source>
        <translation>Markierungen für noch nicht identifizierte Geräteerkennungsereignisse anzeigen.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1281"/>
        <source> l/min</source>
        <translation> l/min</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1398"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cumulative Indices&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Kumulative Indizes&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1486"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Due to summary design limitations, ResMed devices do not support changing these settings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Hinweis: &lt;/span&gt;Aufgrund von Zusammenfassungsdesignbeschränkungen unterstützen ResMed-Geräte das Ändern dieser Einstellungen nicht.&lt;/p &gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1602"/>
        <source>Discard segments under</source>
        <translation>Untere Segmente verwerfen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1622"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Flag SpO&lt;span style=&quot; vertical-align:sub;&quot;&gt;2&lt;/span&gt; Desaturations Below&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Flag SpO&lt;span style=&quot; vertical-align:sub;&quot;&gt;2&lt;/span&gt; Entsättigungen unten&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2002"/>
        <source>Allow use of multiple CPU cores where available to improve performance. 
Mainly affects the importer.</source>
        <translation>Erlaubt die Verwendung von mehreren CPU-Kernen, wenn verfügbar, um die Leistung zu verbessern.
Vor allem wirkt sich das auf den Import von Daten aus.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2563"/>
        <source>Line Chart</source>
        <translation>Liniendiagramm</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="535"/>
        <source>dd MMMM yyyy</source>
        <translation>dd MMMM yyyy</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1439"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;True maximum is the maximum of the data set.&lt;/p&gt;&lt;p&gt;99th percentile filters out the rarest outliers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;True Maximum liegt das Maximum des Datensatzes.&lt;/p&gt;&lt;p&gt;99. Perzentile filtert den seltensten Ausreißer heraus.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2303"/>
        <location filename="../oscar/preferencesdialog.ui" line="2307"/>
        <source>Profile</source>
        <translation>Profile</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="470"/>
        <source>Flag Type</source>
        <translation>Flag-Typ</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="810"/>
        <source>Calculate Unintentional Leaks When Not Present</source>
        <translation>Berechnung unbeabsichtigter Lecks, wenn nicht vorhanden</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="618"/>
        <source>Automatically load last used profile on start-up</source>
        <translation>Das zuletzt verwendete Profil wird beim Start automatisch geladen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2492"/>
        <source>How long you want the tooltips to stay visible.</source>
        <translation>Wie lange sollen die Tooltips sichtbar bleiben.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="534"/>
        <source>Double click to change the descriptive name the &apos;%1&apos; channel.</source>
        <translation>Klicken Sie doppelt auf den beschreibenden Namen des &apos;%1&apos; Kanal zu wechseln.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="190"/>
        <source>Multiple sessions closer together than this value will be kept on the same day.
</source>
        <translation>Mehrere Sitzungen, die näher als dieser Wert liegen, werden am selben Tag gehalten.
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1257"/>
        <source>Are you really sure you want to do this?</source>
        <translation>Sind Sie wirklich sicher, dass Sie das tun wollen?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1116"/>
        <source>Duration of airflow restriction</source>
        <translation>Dauer der Behinderung des Luftstroms</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2558"/>
        <source>Bar Tops</source>
        <translation>Balkendiagramme</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="463"/>
        <source>This makes OSCAR&apos;s data take around half as much space.
But it makes import and day changing take longer.. 
If you&apos;ve got a new computer with a small solid state disk, this is a good option.</source>
        <translation>Dadurch benötigen OSCAR-Daten etwa die Hälfte des Speicherplatzes.
Import und Tageswechsel dauern jedoch länger. 
Wenn Sie einen neuen Computer mit einer kleinen Solid-State-Diskette haben, ist dies eine gute Option.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="97"/>
        <source>Session Splitting Settings</source>
        <translation>Sitzung Splitting-Einstellungen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="733"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: This is not intended for timezone corrections! Make sure your operating system clock and timezone is set correctly.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Hinweis: Dies ist nicht für Zeitzone Korrekturen bestimmt! Stellen Sie sicher, dass Ihre Betriebssystem Uhr und Zeitzone richtig eingestellt ist.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2672"/>
        <source>Other Visual Settings</source>
        <translation>Andere Visuelle Einstellungen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="311"/>
        <source>Day Split Time</source>
        <translation>Tages Zwischenzeit</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="621"/>
        <source>CPAP Waveforms</source>
        <translation>CPAP Wellenform</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="468"/>
        <source>Compress Session Data (makes OSCAR data smaller, but day changing slower.)</source>
        <translation>Sitzungsdaten komprimieren (OSCAR-Daten werden dadurch kleiner, jedoch die Tagesansicht wird langsamer.)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3077"/>
        <source>Big  Text</source>
        <translation>Großer Text</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2712"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These features have recently been pruned. They will come back later. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Diese Eigenschaften sind vor kurzem eingestellt worden. Sie werden später wieder zu benutzen sein. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="948"/>
        <source>Note: A linear calculation method is used. Changing these values requires a recalculation.</source>
        <translation>Hinweis:Hier wird ein lineares Berechnungsverfahren verwendet. Das Ändern dieser Werte erfordert eine Neuberechnung.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1297"/>
        <source>Regard days with under this usage as &quot;incompliant&quot;. 4 hours is usually considered compliant.</source>
        <translation>Tage mit der Benutzung des Gerätes mit unter 4 Stunden ist &quot;nicht konform&quot;.mehr als 4 Stnden sind in Ordnung.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="502"/>
        <source>Do not import sessions older than:</source>
        <translation>Keine älteren als diese Sitzung importieren:</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2722"/>
        <source>Daily view navigation buttons will skip over days without data records</source>
        <translation>Tagesansicht Navigationstasten wird die Tage ohne Datensätze überspringen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1642"/>
        <source>Flag Pulse Rate Above</source>
        <translation>Flag bei Pulsrate über</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1632"/>
        <source>Flag Pulse Rate Below</source>
        <translation>Flag bei Pulsrate unter</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="787"/>
        <source>Seconds</source>
        <translation>Sekunden</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1220"/>
        <source>Adjusts the amount of data considered for each point in the AHI/Hour graph.
Defaults to 60 minutes.. Highly recommend it&apos;s left at this value.</source>
        <translation>Stellt die Datenmenge für jeden Punkt in dem AHI/ Stunde Diagramm bereit.
Standardwerte auf 60 Minuten.. Sehr zu empfehlen.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1109"/>
        <source>Show in Event Breakdown Piechart</source>
        <translation>Die Ereignispannen als Kreisdiagramm anzeigen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1569"/>
        <source>Other oximetry options</source>
        <translation>Andere Oxymetrie Optionen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2335"/>
        <source>Switch Tabs</source>
        <translation>Tabs wechseln</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3175"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="360"/>
        <source>Don&apos;t Split Summary Days (Warning: read the tooltip!)</source>
        <translation>Übersicht der Tage nicht spalten (Warnung: Lesen Sie die Tooltipps!)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2174"/>
        <source>Last Checked For Updates: </source>
        <translation>Letzte Kontrolle Updates: </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="433"/>
        <source>Compress ResMed (EDF) backups to save disk space.
Backed up EDF files are stored in the .gz format, 
which is common on Mac &amp; Linux platforms.. 

OSCAR can import from this compressed backup directory natively.. 
To use it with ResScan will require the .gz files to be uncompressed first..</source>
        <translation>Komprimieren Sie ResMed (EDF) -Sicherungen, um Speicherplatz zu sparen.
Gesicherte EDF-Dateien werden im .gz-Format gespeichert. 
Das ist auf Mac- und Linux-Plattformen üblich. 

OSCAR kann nativ aus diesem komprimierten Sicherungsverzeichnis importieren. 
Um es mit ResScan zu verwenden, müssen die .gz-Dateien zuerst dekomprimiert werden.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3143"/>
        <location filename="../oscar/preferencesdialog.cpp" line="472"/>
        <location filename="../oscar/preferencesdialog.cpp" line="604"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2685"/>
        <source>Use Anti-Aliasing</source>
        <translation>Verwenden Sie Anti-Aliasing</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2715"/>
        <source>Animations &amp;&amp; Fancy Stuff</source>
        <translation>Animationen &amp;&amp; gutes Material</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="70"/>
        <source>&amp;Import</source>
        <translation>&amp;Importieren</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2327"/>
        <location filename="../oscar/preferencesdialog.ui" line="2366"/>
        <source>Statistics</source>
        <translation>Statistiken</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="598"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This setting keeps waveform and event data in memory after use to speed up revisiting days.&lt;/p&gt;&lt;p&gt;This is not really a necessary option, as your operating system caches previously used files too.&lt;/p&gt;&lt;p&gt;Recommendation is to leave it switched off, unless your computer has a ton of memory.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Diese Einstellung hält Wellenform und Ereignisdaten im Speicher. Bei einem neuen Besuch des Programms werden Sie eine erhebliche Beschleunigung bemerken.&lt;/p&gt;&lt;p&gt;Das ist nicht wirklich eine notwendige Option.&lt;/p&gt;&lt;p&gt;Empfohlen wird, diese Option ausgeschaltet zu lassen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1345"/>
        <source>Changes to the following settings needs a restart, but not a recalc.</source>
        <translation>Änderungen an den folgenden Einstellungen benötigt einen Neustart, aber keine Neuberechnung.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2255"/>
        <source>&amp;Appearance</source>
        <translation>&amp;Erscheinungsbild</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2446"/>
        <source>The pixel thickness of line plots</source>
        <translation>Die Pixeldicke von Liniendiagrammen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="547"/>
        <source>Whether this flag has a dedicated overview chart.</source>
        <translation>Egal, diese Markierung hat eine eigene Übersichtskarte.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="568"/>
        <location filename="../oscar/preferencesdialog.cpp" line="701"/>
        <source>This is a description of what this channel does.</source>
        <translation>Dies ist eine Beschreibung der Funktion dieses Kanals.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="160"/>
        <source>Combine Close Sessions </source>
        <translation>Kombinieren Schließen Sitzung </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="972"/>
        <source>Custom CPAP User Event Flagging</source>
        <translation>Benutzerdefinierte CPAP Benutzerereignis Flag</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1875"/>
        <location filename="../oscar/preferencesdialog.ui" line="1954"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;Just because you can, does not mean it&apos;s good practice.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warnung: &lt;/span&gt;Nur weil Sie auf die Standardwerte zurücksetzen können, bedeutet Dies nicht immer, dass das gut ist.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2735"/>
        <source>Allow YAxis Scaling</source>
        <translation>Erlauben Sie YAxis Skalierung</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2787"/>
        <source>Fonts (Application wide settings)</source>
        <translation>Schriften (Application Größeneinstellungen)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2705"/>
        <source>Use Pixmap Caching</source>
        <translation>Verwenden Pixmap Zwischenspeicherung</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2121"/>
        <source>Check for new version every</source>
        <translation>Alle auf neue Version prüfen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1888"/>
        <source>Waveforms</source>
        <translation>Wellenformen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1384"/>
        <source>Maximum Calcs</source>
        <translation>Maximale Berechnungen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2322"/>
        <location filename="../oscar/preferencesdialog.ui" line="2361"/>
        <location filename="../oscar/preferencesdialog.cpp" line="469"/>
        <location filename="../oscar/preferencesdialog.cpp" line="600"/>
        <source>Overview</source>
        <translation>Überblick</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2470"/>
        <source>Tooltip Timeout</source>
        <translation>Kurzinfo Zeitüberschreitung</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="29"/>
        <source>Preferences</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1184"/>
        <source>General CPAP and Related Settings</source>
        <translation>Allgemeine CPAP und verwandte Einstellungen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2577"/>
        <source>Default display height of graphs in pixels</source>
        <translation>Standardanzeige Höhe von Diagrammen in Pixel</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2400"/>
        <source>Overlay Flags</source>
        <translation>Überlagerungs-Flag</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2052"/>
        <source>Try changing this from the default setting (Desktop OpenGL) if you experience rendering problems with OSCAR&apos;s graphs.</source>
        <translation>Ändern Sie die Standardeinstellung (Desktop OpenGL), wenn Sie Probleme mit der Darstellung der OSCAR-Diagramme haben.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2692"/>
        <source>Makes certain plots look more &quot;square waved&quot;.</source>
        <translation>Macht bestimmte Abschnitte vom Aussehen her &quot;schwenkbar&quot;.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2312"/>
        <location filename="../oscar/preferencesdialog.ui" line="2351"/>
        <source>Welcome</source>
        <translation>Willkommen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1735"/>
        <source>Percentage drop in oxygen saturation</source>
        <translation>Prozentualer Abfall der Sauerstoffsättigung</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1967"/>
        <source>&amp;General</source>
        <translation>&amp;Allgemein</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1467"/>
        <source>Standard average of indice</source>
        <translation>Standarddurchschnitt von indice</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1175"/>
        <source> If you need to conserve disk space, please remember to carry out manual backups.</source>
        <translation> Wenn Sie Speicherplatz sparen müssen, denken Sie daran, manuelle Sicherungen durchzuführen.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="441"/>
        <source>Compress SD Card Backups (slower first import, but makes backups smaller)</source>
        <translation>Komprimieren der Backups auf der SD-Karte (langsamer beim ersten Import, aber macht Backups kleiner)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="601"/>
        <source>Keep Waveform/Event data in memory</source>
        <translation>Halten Sie die Wellenform / Sitzungsdaten im Speicher</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="680"/>
        <source>Whether a breakdown of this waveform displays in overview.</source>
        <translation>Ob eine Aufschlüsselung dieser Wellenform im Überblick gezeigt werden soll.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1419"/>
        <source>Normal Average</source>
        <translation>Normaler Durchschnitt</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="623"/>
        <source>Positional Waveforms</source>
        <translation>Positions-Wellenform</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="810"/>
        <source>A data reindexing proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>Ein Datenindexierungsvorgang ist erforderlich, um diese Änderungen anzuwenden. Dieser Vorgang kann einige Minuten dauern.

Sind Sie sicher, dass Sie diese Änderungen vornehmen wollen?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="490"/>
        <source>Positional Events</source>
        <translation>Positions Ereignisse</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1348"/>
        <source>Preferred Calculation Methods</source>
        <translation>Bevorzugte Berechnungsmethoden</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1457"/>
        <source>Combined Count divided by Total Hours</source>
        <translation>Kombinierte Anzahl geteilt durch die Gesamtstunden</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2550"/>
        <source>Graph Tooltips</source>
        <translation>Diagramm Tooltips</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1518"/>
        <source>&amp;Oximetry</source>
        <translation>&amp;Oxymetrie</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="706"/>
        <source>CPAP Clock Drift</source>
        <translation>CPAP Wecker benutzen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="685"/>
        <source>Here you can set the &lt;b&gt;lower&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>Hier können Sie einstellen &lt;b&gt;unteren&lt;/b&gt; Schwelle für bestimmte Berechnungen welche die Wellenform verwendet %1</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="578"/>
        <source>Auto-Launch CPAP Importer after opening profile</source>
        <translation>CPAP-Importeur nach dem Öffnen des Profils automatisch starten</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2695"/>
        <source>Square Wave Plots</source>
        <translation>Quadratwelle-Anschläge</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2187"/>
        <source>TextLabel</source>
        <translation>Textlabel</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1255"/>
        <source>Preferred major event index</source>
        <translation>Index für bevorzugte wichtige Ereignisse</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2891"/>
        <source>Application</source>
        <translation>Anwendung</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2410"/>
        <source>Line Thickness</source>
        <translation>Linienstärke</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="423"/>
        <source>Changing SD Backup compression options doesn&apos;t automatically recompress backup data.</source>
        <translation>Das Ändern der SD Backup-Komprimierungsoptionen führt nicht automatisch zu einer Neukomprimierung der Backup-Daten.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="819"/>
        <source>Your masks vent rate at 20 cmH2O pressure</source>
        <translation>Ihre Maskenlüftungsrate bei 20 cmH2O Druck</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="875"/>
        <source>Your masks vent rate at 4 cmH2O pressure</source>
        <translation>Ihre Maskenlüftungsrate bei 4 cmH2O Druck</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2745"/>
        <source>Include Serial Number</source>
        <translation>Seriennummer einbeziehen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="645"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Provide an alert when importing data that is somehow different from anything previously seen by OSCAR developers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Geben Sie beim Importieren von Daten eine Warnung aus, die sich irgendwie von allem unterscheidet, was die OSCAR-Entwickler zuvor gesehen haben.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="648"/>
        <source>Warn when previously unseen data is encountered</source>
        <translation>Warnen Sie, wenn Sie auf bisher ungesehene Daten stoßen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2020"/>
        <source>Always save screenshots in the OSCAR Data folder</source>
        <translation>Screenshots immer im OSCAR-Datenordner speichern</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2074"/>
        <source>Check For Updates</source>
        <translation>Nach Updates suchen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2089"/>
        <source>You are using a test version of OSCAR. Test versions check for updates automatically at least once every seven days.  You may set the interval to less than seven days.</source>
        <translation>Sie verwenden eine Testversion von OSCAR. Testversionen suchen mindestens alle sieben Tage automatisch nach Updates.  Sie können das Intervall auf weniger als sieben Tage einstellen.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2106"/>
        <source>Automatically check for updates</source>
        <translation>Automatisch nach Updates suchen</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2128"/>
        <source>How often OSCAR should check for updates.</source>
        <translation>Wie oft OSCAR nach Aktualisierungen suchen sollte.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2209"/>
        <source>If you are interested in helping test new features and bugfixes early, click here.</source>
        <translation>Wenn Sie daran interessiert sind, neue Funktionen und Fehlerbehebungen frühzeitig zu testen, klicken Sie hier.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2225"/>
        <source>If you would like to help test early versions of OSCAR, please see the Wiki page about testing OSCAR.  We welcome everyone who would like to test OSCAR, help develop OSCAR, and help with translations to existing or new languages. https://www.sleepfiles.com/OSCAR</source>
        <translation>Wenn Sie helfen möchten, frühe Versionen von OSCAR zu testen, lesen Sie bitte die Wiki-Seite über das Testen von OSCAR.  Wir heißen alle willkommen, die OSCAR testen, an der Entwicklung von OSCAR mitwirken und bei Übersetzungen in bestehende oder neue Sprachen helfen möchten. https://www.sleepfiles.com/OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2742"/>
        <source>Whether to include device serial number on device settings changes report</source>
        <translation>Ob die Seriennummer des Geräts in den Änderungsbericht der Geräteeinstellungen aufgenommen werden soll</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2752"/>
        <source>Print reports in black and white, which can be more legible on non-color printers</source>
        <translation>Berichte in Schwarzweiß drucken, die auf Nicht-Farbdruckern besser lesbar sein können</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2755"/>
        <source>Print reports in black and white (monochrome)</source>
        <translation>Berichte in Schwarzweiß (monochrom) drucken</translation>
    </message>
</context>
<context>
    <name>ProfileSelector</name>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>KB</source>
        <translation>KB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="95"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>Bytes</source>
        <translation>Bytes</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="418"/>
        <source>Sorry</source>
        <translation>Entschuldigung</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="515"/>
        <source>Profile: %1</source>
        <translation>Profile: %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="399"/>
        <source>Think carefully, as this will irretrievably delete the profile along with all &lt;b&gt;backup data&lt;/b&gt; stored under&lt;br/&gt;%2.</source>
        <translation>Denken Sie sorgfältig nach, da dadurch das Profil mit allen unwiederbringlich gelöscht wird &lt;b&gt;Backup-Daten&lt;/b&gt; gespeichert unter&lt;br/&gt;%2.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="506"/>
        <source>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="435"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>Profil &apos;%1&apos; wurde erfolgreich gelöscht</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="465"/>
        <source>Summaries:</source>
        <translation>Zusammenfassungen:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="417"/>
        <source>DELETE</source>
        <translation>DELETE</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="252"/>
        <source>Forgot your password?</source>
        <translation>Haben Sie Ihr Passwort vergessen?</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="240"/>
        <source>&amp;New Profile</source>
        <translation>&amp;Neues Profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="431"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>Es gab einen Fehler beim Löschen des Profil-Verzeichnisses. Sie müssen es manuell entfernen.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="482"/>
        <source>Show disk usage information</source>
        <translation>Informationen zur Festplattennutzung anzeigen</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="503"/>
        <source>Phone: %1</source>
        <translation>Telefon: %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="233"/>
        <location filename="../oscar/profileselector.cpp" line="367"/>
        <source>Enter Password for %1</source>
        <translation>Passwort eingeben für %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="94"/>
        <source>Last Imported</source>
        <translation>Letzter Import</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="90"/>
        <source>Profile</source>
        <translation>Profile</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="467"/>
        <source>Backups:</source>
        <translation>Sicherungen:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="500"/>
        <source>Name: %1, %2</source>
        <translation>Name: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="278"/>
        <source>Please select or create a profile...</source>
        <translation>Bitte wählen oder erstellen Sie ein Profil...</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="249"/>
        <location filename="../oscar/profileselector.cpp" line="386"/>
        <source>You entered an incorrect password</source>
        <translation>Sie haben ein falsches Passwort eingegeben</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="479"/>
        <location filename="../oscar/profileselector.cpp" line="519"/>
        <source>Hide disk usage information</source>
        <translation>Informationen zur Festplattennutzung ausblenden</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="512"/>
        <source>No profile information given</source>
        <translation>Keine Profilinformationen angegeben</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="509"/>
        <source>Address:</source>
        <translation>Adresse:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="252"/>
        <source>Ask on the forums how to reset it, it&apos;s actually pretty easy.</source>
        <translation>Fragen Sie in den Foren nach, wie Sie es zurücksetzen können. Es ist eigentlich ziemlich einfach.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="322"/>
        <source>Select a profile first</source>
        <translation>Wählen Sie zuerst ein Profil aus</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="93"/>
        <source>Other Data</source>
        <translation>Andere Daten</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="198"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="466"/>
        <source>Events:</source>
        <translation>Sitzungen:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="215"/>
        <source>&amp;Open Profile</source>
        <translation>&amp;Profil öffnen</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="26"/>
        <source>Filter:</source>
        <translation>Filter:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="328"/>
        <source>Destroy Profile</source>
        <translation>Profil löschen</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="226"/>
        <source>&amp;Edit Profile</source>
        <translation>&amp;Profil bearbeiten</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="389"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to either reset it or delete the profile folder manually.</source>
        <translation>Wenn Sie versuchen, das Profil zu löschen, weil Sie das Kennwort vergessen haben, müssen Sie es entweder zurücksetzen oder den Profilordner manuell löschen.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="399"/>
        <source>Enter the word &lt;b&gt;DELETE&lt;/b&gt; below (exactly as shown) to confirm.</source>
        <translation>Geben Sie das Wort ein &lt;b&gt;DELETE&lt;/b&gt; unten (genau wie gezeigt) zur Bestätigung.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="91"/>
        <source>Ventilator Brand</source>
        <translation>Geräte-Marke</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="258"/>
        <source>Profile: None</source>
        <translation>Profil: Keine</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="92"/>
        <source>Ventilator Model</source>
        <translation>Geräte-Model</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="399"/>
        <source>You are about to destroy profile &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</source>
        <translation>Sie sind dabei, Ihr Profil zu zerstören &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="418"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>Sie müssen DELETE in Großbuchstaben eingeben.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="170"/>
        <source>You must create a profile</source>
        <translation>Sie müssen ein Profil erstellen</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="36"/>
        <source>Reset filter to see all profiles</source>
        <translation>Filter zurücksetzen, um alle Profile zu sehen</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="357"/>
        <source>The selected profile does not appear to contain any data and cannot be removed by OSCAR</source>
        <translation>Das ausgewählte Profil scheint keine Daten zu enthalten und kann von OSCAR nicht entfernt werden</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="57"/>
        <source>Abort</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="721"/>
        <source>Only Settings and Compliance Data Available</source>
        <translation>Nur Einstellungen und Compliance-Daten verfügbar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="723"/>
        <source>Summary Data Only</source>
        <translation>Nur zusammenfassende Daten</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="773"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="771"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="785"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="696"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="697"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="698"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="685"/>
        <source> m</source>
        <translation> m</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1621"/>
        <source>Your machine doesn&apos;t record data to graph in Daily View</source>
        <translation>Ihr Gerät zeichnet in der Tagesansicht keine Daten auf, um sie grafisch darzustellen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="798"/>
        <source>AI</source>
        <translation>AI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="775"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="160"/>
        <source>CA</source>
        <translation>CA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="779"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="189"/>
        <source>EP</source>
        <translation>EP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="776"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="170"/>
        <source>FL</source>
        <translation>FL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="799"/>
        <source>HI</source>
        <translation>HI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="811"/>
        <source>IE</source>
        <translation>IE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="702"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="778"/>
        <source>LE</source>
        <translation>LE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="179"/>
        <source>LF</source>
        <translation>LF</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="828"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>LL</source>
        <translation>LL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="789"/>
        <source>O2</source>
        <translation>O2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="772"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="162"/>
        <source>OA</source>
        <translation>OA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="787"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>NR</source>
        <translation>NR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="807"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="158"/>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="790"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2849"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="217"/>
        <source>PC</source>
        <translation>PC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="878"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="784"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2832"/>
        <source>PP</source>
        <translation>PP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="795"/>
        <source>PS</source>
        <translation>PS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="847"/>
        <source>Device</source>
        <translation>Gerät</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="874"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="192"/>
        <source>On</source>
        <translation>An</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="786"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>RE</source>
        <translation>RE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="93"/>
        <source>S9</source>
        <translation>S9</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="777"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="192"/>
        <source>SA</source>
        <translation>SA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="220"/>
        <source>SD</source>
        <translation>SD</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="778"/>
        <source>UNKNOWN</source>
        <translation>UNBEKANNT</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="780"/>
        <source>APAP (std)</source>
        <translation>APAP (std)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="781"/>
        <source>APAP (dyn)</source>
        <translation>APAP (dyn)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="782"/>
        <source>Auto S</source>
        <translation>Auto S</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="783"/>
        <source>Auto S/T</source>
        <translation>Auto S/T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="784"/>
        <source>AcSV</source>
        <translation>AcSV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="788"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="789"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="790"/>
        <source>SoftPAP Mode</source>
        <translation>SoftPAP-Modus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="793"/>
        <source>Slight</source>
        <translation>Leicht</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="797"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="798"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="799"/>
        <source>PSoft</source>
        <translation>PSoft</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="803"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="804"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="805"/>
        <source>PSoftMin</source>
        <translation>PSoftMin</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="809"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="810"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="811"/>
        <source>AutoStart</source>
        <translation>Auto-Start</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="817"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="818"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="819"/>
        <source>Softstart_Time</source>
        <translation>Softstart_Zeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="823"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="824"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="825"/>
        <source>Softstart_TimeMax</source>
        <translation>Softstart_ZeitMax</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="829"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="830"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="831"/>
        <source>Softstart_Pressure</source>
        <translation>Softstart_Druck</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="835"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="836"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="837"/>
        <source>PMaxOA</source>
        <translation>PMaxOA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="841"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="842"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="843"/>
        <source>EEPAPMin</source>
        <translation>EEPAPMin</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="847"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="848"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="849"/>
        <source>EEPAPMax</source>
        <translation>EEPAPMax</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="853"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="854"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="855"/>
        <source>HumidifierLevel</source>
        <translation>Befeuchterstufe</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="859"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="860"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="861"/>
        <source>TubeType</source>
        <translation>Schlauchtyp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="875"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="878"/>
        <source>ObstructLevel</source>
        <translation>Hindernisebene</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="877"/>
        <source>Obstruction Level</source>
        <translation>Hindernisstufe</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="885"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="887"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="888"/>
        <source>rMVFluctuation</source>
        <translation>rMV-Schwankung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="895"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="897"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="898"/>
        <source>rRMV</source>
        <translation>rRMV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="903"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="905"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="906"/>
        <source>PressureMeasured</source>
        <translation>Druck gemessen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="911"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="913"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="914"/>
        <source>FlowFull</source>
        <translation>voller Durchfluss</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="919"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="921"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="922"/>
        <source>SPRStatus</source>
        <translation>SPR- Status</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="928"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="930"/>
        <source>Artifact</source>
        <translation>Artefakt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="931"/>
        <source>ART</source>
        <translation>ART</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="936"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="938"/>
        <source>CriticalLeak</source>
        <translation>Kritisches Leck</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="939"/>
        <source>CL</source>
        <translation>CL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="944"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="946"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="947"/>
        <source>eMO</source>
        <translation>eMO</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="953"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="955"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="956"/>
        <source>eSO</source>
        <translation>eSO</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="962"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="964"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="965"/>
        <source>eS</source>
        <translation>eS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="971"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="973"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="974"/>
        <source>eFL</source>
        <translation>eFL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="980"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="982"/>
        <source>DeepSleep</source>
        <translation>Tiefschlaf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="983"/>
        <source>DS</source>
        <translation>DS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="990"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="992"/>
        <source>TimedBreath</source>
        <translation>Zeitgesteuerter Atem</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="993"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3089"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="774"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>UA</source>
        <translation>UA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="780"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>VS</source>
        <translation>VS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="688"/>
        <source>ft</source>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="689"/>
        <source>lb</source>
        <translation>lb</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="706"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="699"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="690"/>
        <source>oz</source>
        <translation>oz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="686"/>
        <source> cm</source>
        <translation> cm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="729"/>
        <source>&amp;No</source>
        <translation>&amp;Nein</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="796"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="273"/>
        <source>AHI</source>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="765"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2846"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="124"/>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="734"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>BMI</source>
        <translation>BMI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3082"/>
        <source>BND</source>
        <translation>BND</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="801"/>
        <source>CAI</source>
        <translation>CAI</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Apr</source>
        <translation>Apr</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="806"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="156"/>
        <source>CSR</source>
        <translation>CSR</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Aug</source>
        <translation>Aug</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="224"/>
        <location filename="../oscar/SleepLib/common.cpp" line="886"/>
        <source>Avg</source>
        <translation>Gem</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="856"/>
        <source>DOB</source>
        <translation>Geburtsdatum</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="805"/>
        <source>EPI</source>
        <translation>EPI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="132"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1030"/>
        <source>EPR</source>
        <translation>EPR</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Dec</source>
        <translation>Dez</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="802"/>
        <source>FLI</source>
        <translation>FLI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="873"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>End</source>
        <translation>Ende</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Feb</source>
        <translation>Feb</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Jan</source>
        <translation>Jan</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Jul</source>
        <translation>Jul</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Jun</source>
        <translation>Jun</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="788"/>
        <source>NRI</source>
        <translation>NRI</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Mar</source>
        <translation>März</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="881"/>
        <source>Max</source>
        <translation>Max</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>May</source>
        <translation>Mai</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="882"/>
        <source>Med</source>
        <translation>Med</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="880"/>
        <source>Min</source>
        <translation>Min</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Nov</source>
        <translation>Nov</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Oct</source>
        <translation>Okt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="875"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="792"/>
        <source>Off</source>
        <translation>Aus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="797"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="282"/>
        <source>RDI</source>
        <translation>RDI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="804"/>
        <source>REI</source>
        <translation>REI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="800"/>
        <source>UAI</source>
        <translation>UAI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="214"/>
        <source>Compiler:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="687"/>
        <source>in</source>
        <translation>in</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="691"/>
        <source>kg</source>
        <translation>kg</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="704"/>
        <source>l/min</source>
        <translation>l/min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="758"/>
        <source>EEPAP</source>
        <translation>EEPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="791"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="792"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="198"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="793"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="201"/>
        <source>UF3</source>
        <translation>UF3</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Sep</source>
        <translation>Sep</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="782"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="176"/>
        <source>VS2</source>
        <translation>VS2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="877"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="40"/>
        <source>Zeo</source>
        <translation>Zeo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="703"/>
        <source>bpm</source>
        <translation>bpm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="335"/>
        <source>Brain Wave</source>
        <translation>Gehirn Wellen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="728"/>
        <source>&amp;Yes</source>
        <translation>&amp;Ja</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2962"/>
        <source>15mm</source>
        <translation>15mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2961"/>
        <source>22mm</source>
        <translation>22mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="764"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="118"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="754"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="779"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2841"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="117"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="876"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2945"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3036"/>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="717"/>
        <source>Busy</source>
        <translation>Beschäftigt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="759"/>
        <source>Min EPAP</source>
        <translation>Min EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="757"/>
        <source>EPAP</source>
        <translation>EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="861"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="78"/>
        <source>ICON</source>
        <translation>ICON</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="762"/>
        <source>Min IPAP</source>
        <translation>Min IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="761"/>
        <source>IPAP</source>
        <translation>IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="871"/>
        <source>Last</source>
        <translation>Letzte Verwendung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="825"/>
        <source>Leak</source>
        <translation>Leck</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="201"/>
        <source>Mask</source>
        <translation>Maske</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="220"/>
        <source>Med.</source>
        <translation>Med.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="842"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="774"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2836"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2838"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="113"/>
        <source>Mode</source>
        <translation>Modus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="855"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="867"/>
        <source>None</source>
        <translation>Keiner</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="783"/>
        <source>RERA</source>
        <translation>RERA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="209"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1041"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <source>Ramp</source>
        <translation>Rampe</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="779"/>
        <source>Zero</source>
        <translation>Null</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="814"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>Resp. Event</source>
        <translation>Resp. Ereignis</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="851"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="289"/>
        <source>Inclination</source>
        <translation>Neigung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="485"/>
        <source>Launching Windows Explorer failed</source>
        <translation>Das Starten vom Windows Explorer ist gescheitert</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="552"/>
        <source>&lt;i&gt;Your old device data should be regenerated provided this backup feature has not been disabled in preferences during a previous data import.&lt;/i&gt;</source>
        <translation>&lt;i&gt;Ihre alten Gerätedaten sollten neu generiert werden, sofern diese Sicherungsfunktion nicht bei einem vorherigen Datenimport in den Einstellungen deaktiviert wurde.&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="556"/>
        <source>This means you will need to import this device data again afterwards from your own backups or data card.</source>
        <translation>Das bedeutet, dass Sie diese Gerätedaten anschließend erneut von Ihrer eigenen Sicherung oder Datenkarte importieren müssen.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="564"/>
        <source>Device Database Changes</source>
        <translation>Änderungen der Gerätedatenbank</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="576"/>
        <source>The device data folder needs to be removed manually.</source>
        <translation>Der Gerätedatenordner muss manuell entfernt werden.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="594"/>
        <source>Would you like to switch on automatic backups, so next time a new version of OSCAR needs to do so, it can rebuild from these?</source>
        <translation>Möchten Sie die automatischen Sicherungen einschalten, so dass das nächste Mal, wenn eine neue Version von OSCAR erforderlich ist, diese neu erstellt werden kann?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2957"/>
        <source>Hose Diameter</source>
        <translation>Schlauchdurchmesser</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="732"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="766"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="311"/>
        <source>AVAPS</source>
        <translation>AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="41"/>
        <source>CMS50</source>
        <translation>CMS50</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Therapy Pressure</source>
        <translation>Therapiedruck</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="755"/>
        <source>BiPAP</source>
        <translation>BiPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="844"/>
        <source>Brand</source>
        <translation>Marke</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="126"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.h" line="98"/>
        <source>EPR: </source>
        <translation>EPR: </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="742"/>
        <source>Daily</source>
        <translation>Täglich</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="859"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="714"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2920"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="870"/>
        <source>First</source>
        <translation>Erste Verwendung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="148"/>
        <source>Ramp Pressure</source>
        <translation>Rampen Druck</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="377"/>
        <location filename="../oscar/Graphs/gUsageChart.cpp" line="45"/>
        <location filename="../oscar/SleepLib/common.cpp" line="693"/>
        <source>Hours</source>
        <translation>Stunden</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="41"/>
        <source>MD300</source>
        <translation>MD300</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="826"/>
        <source>Leaks</source>
        <translation>Lecks</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="274"/>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="284"/>
        <source>Max: </source>
        <translation>Max: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="269"/>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="279"/>
        <source>Min: </source>
        <translation>Min: </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="843"/>
        <source>Model</source>
        <translation>Model</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="205"/>
        <source>Nasal</source>
        <translation>Nase</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="319"/>
        <source>Notes</source>
        <translation>Aufzeichnungen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="857"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="868"/>
        <source>Ready</source>
        <translation>Bereit</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gTTIAChart.cpp" line="71"/>
        <source>TTIA:</source>
        <translation>TTIA:</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="222"/>
        <location filename="../oscar/SleepLib/common.cpp" line="887"/>
        <source>W-Avg</source>
        <translation>W-Durchschnitt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="824"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2804"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="236"/>
        <source>Snore</source>
        <translation>Schnarchen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="872"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="834"/>
        <source>Usage</source>
        <translation>Verwendung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="692"/>
        <source>cmH2O</source>
        <translation>cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="142"/>
        <source>Pressure Support</source>
        <translation>Druckunterstützung</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1139"/>
        <source>Bedtime: %1</source>
        <translation>Schlafenszeit: %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="709"/>
        <source>ratio</source>
        <translation>Verhältnis</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="822"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Tidal Volume</source>
        <translation>AZV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2694"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="523"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="872"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="225"/>
        <source>Getting Ready...</source>
        <translation>Fertig werden...</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="443"/>
        <source>Entire Day</source>
        <translation>Ganzer Tag</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2787"/>
        <source>Intellipap pressure relief mode.</source>
        <translation>Intellipap Druckmodus.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="40"/>
        <source>Personal Sleep Coach</source>
        <translation>Persönlichen Schlaftrainer</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="360"/>
        <source>Most recent Oximetry data: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </source>
        <translation>Neueste Oxymetriedaten: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="706"/>
        <source>Scanning Files</source>
        <translation>Scanne Dateien</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="70"/>
        <source>Respironics</source>
        <translation>Respironics</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>Heart rate in beats per minute</source>
        <translation>Die Herzfrequenz in Schlägen pro Minute</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="39"/>
        <source>Somnopose Software</source>
        <translation>Somnopose Software</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="338"/>
        <source>Time spent awake</source>
        <translation>Zeit im Wachliegen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="176"/>
        <source>Temp. Enable</source>
        <translation>Temp. aktivieren</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3087"/>
        <source>Timed Breath</source>
        <translation>Zeitüberschreitung Atem</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="552"/>
        <source>Pop out Graph</source>
        <translation>grafische Darstellung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="295"/>
        <source>Mask On Time</source>
        <translation>Masken-Einschaltzeit</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="672"/>
        <source>It is likely that doing this will cause data corruption, are you sure you want to do this?</source>
        <translation>Es ist wahrscheinlich, dass dabei eine Datenbeschädigung auftreten kann. Sind Sie sicher, dass Sie das tun wollen?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="502"/>
        <source>Loading profile &quot;%1&quot;...</source>
        <translation>Profil laden &quot;%1&quot;...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3080"/>
        <source>Breathing Not Detected</source>
        <translation>Atmung nicht erkannt</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1624"/>
        <source>There is no data to graph</source>
        <translation>Es gibt keine Daten zum Darstellen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>Journal</source>
        <translation>Journal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="547"/>
        <source>Locating STR.edf File(s)...</source>
        <translation>Suche nach STR.edf-Datei (en)...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="818"/>
        <source>Pat. Trig. Breath</source>
        <translation>Pat. Trig. Atem</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1254"/>
        <source>(Summary Only)</source>
        <translation>(Nur Zusammenfassung)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>Ramp Delay Period</source>
        <translation>Rampen-Verzögerungszeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="724"/>
        <source>Sessions Switched Off</source>
        <translation>Ereignisse abgemeldet</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="336"/>
        <source>Awakenings</source>
        <translation>Erwachen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="577"/>
        <source>This folder currently resides at the following location:</source>
        <translation>Dieser Ordner befindet sich derzeit an der folgenden Position:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="337"/>
        <source>Morning Feel</source>
        <translation>Morgen erwartet Sie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2907"/>
        <source>Disconnected</source>
        <translation>Getrennt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="833"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="333"/>
        <source>Sleep Stage</source>
        <translation>Schlafstadium</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="821"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="239"/>
        <source>Minute Vent.</source>
        <translation>Minuten Vent.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <source>Ramp Event</source>
        <translation>Rampenereignis</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="192"/>
        <source>SensAwake feature will reduce pressure when waking is detected.</source>
        <translation>SensAwake reduziert den Druck beim Erkennen des Wachzustandes.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2436"/>
        <source>Show All Events</source>
        <translation>Alle Ereignisse anzeigen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="280"/>
        <source>CMS50E/F</source>
        <translation>CMS50E/F</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="289"/>
        <source>Upright angle in degrees</source>
        <translation>Bis rechten Winkel in Grad</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="628"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="905"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="743"/>
        <location filename="../oscar/mainwindow.cpp" line="2338"/>
        <source>Importing Sessions...</source>
        <translation>Importiere Sitzung...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>Higher Expiratory Pressure</source>
        <translation>Höherer Expirationsdruck</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="295"/>
        <source>NRI=%1 LKI=%2 EPI=%3</source>
        <translation>NRI=%1 LKI=%2 EPI=%3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="70"/>
        <source>M-Series</source>
        <translation>M-Serie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="560"/>
        <source>If you are concerned, click No to exit, and backup your profile manually, before starting OSCAR again.</source>
        <translation>Wenn Sie Bedenken haben, klicken Sie auf Nein, um den Vorgang zu beenden, und sichern Sie Ihr Profil manuell, bevor Sie OSCAR erneut starten.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>A vibratory snore</source>
        <translation>Eine Schnarchvibration</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="525"/>
        <source>As you did not select a data folder, OSCAR will exit.</source>
        <translation>Da Sie keinen Datenordner ausgewählt haben, wird OSCAR beendet.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="136"/>
        <source>Lower Inspiratory Pressure</source>
        <translation>Niedrigster Inspirationsdruck</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="153"/>
        <source>Humidifier Enabled Status</source>
        <translation>Befeuchtungsstatus aktiviert</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="204"/>
        <source>Full Face</source>
        <translation>Mund-Nase-Maske</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2794"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="136"/>
        <source>Full Time</source>
        <translation>Volle Zeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2797"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2799"/>
        <source>SmartFlex Level</source>
        <translation>Smartflex Ebene</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="29"/>
        <source>Journal Data</source>
        <translation>Journal Daten</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1014"/>
        <source>(%1% compliant, defined as &gt; %2 hours)</source>
        <translation>(%1% konform, definiert als &gt; %2 Stunden)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="823"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="242"/>
        <source>Resp. Rate</source>
        <translation>Resp. Rate</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="812"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>Insp. Time</source>
        <translation>Einatmungszeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="813"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="255"/>
        <source>Exp. Time</source>
        <translation>Ausatmungszeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="611"/>
        <source>OSCAR will now exit, then (attempt to) launch your computers file manager so you can manually back your profile up:</source>
        <translation>OSCAR wird nun beendet und (versucht) den Dateimanager Ihres Computers zu starten, damit Sie Ihr Profil manuell sichern können:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="172"/>
        <source>ClimateLine Temperature</source>
        <translation>Schlauchtemperatur</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="187"/>
        <source>Philips Respironics</source>
        <translation>Philips Respironics</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="154"/>
        <source>Your %1 %2 (%3) generated data that OSCAR has never seen before.</source>
        <translation>Ihre %1 %2 (%3) haben Daten generiert, die OSCAR noch nie zuvor gesehen hat.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="155"/>
        <source>The imported data may not be entirely accurate, so the developers would like a .zip copy of this device&apos;s SD card and matching clinician .pdf reports to make sure OSCAR is handling the data correctly.</source>
        <translation>Die importierten Daten sind möglicherweise nicht ganz genau, daher möchten die Entwickler eine .zip-Kopie der SD-Karte dieses Geräts und passende .pdf-Berichte des Arztes, um sicherzustellen, dass OSCAR die Daten korrekt verarbeitet.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="164"/>
        <source>Non Data Capable Device</source>
        <translation>Nicht datenfähiges Gerät</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="165"/>
        <source>Your %1 CPAP Device (Model %2) is unfortunately not a data capable model.</source>
        <translation>Ihr CPAP-Gerät %1 (Modell %2) ist leider kein datenfähiges Modell.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="166"/>
        <source>I&apos;m sorry to report that OSCAR can only track hours of use and very basic settings for this device.</source>
        <translation>Es tut mir leid, Ihnen mitteilen zu müssen, dass OSCAR nur Nutzungsstunden und sehr grundlegende Einstellungen für dieses Gerät verfolgen kann.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="178"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="454"/>
        <source>Device Untested</source>
        <translation>Gerät ungetestet</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="179"/>
        <source>Your %1 CPAP Device (Model %2) has not been tested yet.</source>
        <translation>Ihr CPAP-Gerät %1 (Modell %2) wurde noch nicht getestet.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="180"/>
        <source>It seems similar enough to other devices that it might work, but the developers would like a .zip copy of this device&apos;s SD card and matching clinician .pdf reports to make sure it works with OSCAR.</source>
        <translation>Es scheint anderen Geräten ähnlich genug zu sein, dass es funktionieren könnte, aber die Entwickler möchten eine .zip-Kopie der SD-Karte dieses Geräts und passende .pdf-Berichte des Arztes, um sicherzustellen, dass es mit OSCAR funktioniert.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="188"/>
        <source>Device Unsupported</source>
        <translation>Gerät wird nicht unterstützt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="189"/>
        <source>Sorry, your %1 CPAP Device (%2) is not supported yet.</source>
        <translation>Entschuldigung, Ihr CPAP-Gerät %1 (%2) wird noch nicht unterstützt.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="190"/>
        <source>The developers need a .zip copy of this device&apos;s SD card and matching clinician .pdf reports to make it work with OSCAR.</source>
        <translation>Die Entwickler benötigen eine .zip-Kopie der SD-Karte dieses Geräts und passende klinische .pdf-Berichte, damit es mit OSCAR funktioniert.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="118"/>
        <source>SOMNOsoft2</source>
        <translation>SOMNOsoft2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="214"/>
        <source>A relative assessment of the pulse strength at the monitoring site</source>
        <translation>Eine relative Bewertung der Pulsstärke an der Messstelle</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="224"/>
        <source>Mask On</source>
        <translation>Maske auf</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="303"/>
        <source>Max: %1</source>
        <translation>Max: %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="575"/>
        <source>Sorry, the purge operation failed, which means this version of OSCAR can&apos;t start.</source>
        <translation>Die Bereinigungsoperation ist fehlgeschlagen. Das bedeutet, dass diese Version von OSCAR nicht gestartet werden kann.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="220"/>
        <source>A sudden (user definable) drop in blood oxygen saturation</source>
        <translation>Ein plötzlicher (frei definierbarer) Abfall der Blutsauerstoffsättigung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="341"/>
        <source>Time spent in deep sleep</source>
        <translation>Zeit im Tiefschlaf</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="45"/>
        <source>There are no graphs visible to print</source>
        <translation>Keine Diagramme zum Drucken</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="58"/>
        <source>OSCAR picked only the first one of these, and will use it in future:

</source>
        <translation>OSCAR hat nur das Erste ausgewählt und wird es zukünftig verwenden:

</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="820"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Target Vent.</source>
        <translation>Ziel Vent.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="286"/>
        <source>Sleep position in degrees</source>
        <translation>Schlafposition in Grad</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/MinutesAtPressure.cpp" line="809"/>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1034"/>
        <source>Plots Disabled</source>
        <translation>Diagramme deaktiviert</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="238"/>
        <source>Min: %1</source>
        <translation>Min: %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="694"/>
        <source>Minutes</source>
        <translation>Minuten</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2779"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2854"/>
        <source>Popout %1 Graph</source>
        <translation>Ausschalten %1 Grafik</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2793"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="135"/>
        <source>Ramp Only</source>
        <translation>Nur Rampe</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>Ramp Time</source>
        <translation>Rampenzeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="57"/>
        <source>For some reason, OSCAR couldn&apos;t find a journal object record in your profile, but did find multiple Journal data folders.

</source>
        <translation>Aus irgendeinem Grund konnte OSCAR keinen Journalobjektdatensatz in Ihrem Profil finden. Es wurden jedoch mehrere Journaldatenordner gefunden.

</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2855"/>
        <source>PRS1 pressure relief mode.</source>
        <translation>PRS1 Druckentlastungsmodus.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="158"/>
        <source>An abnormal period of Periodic Breathing</source>
        <translation>Eine abnormale Periode der periodischen Atmung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="201"/>
        <source>ResMed Mask Setting</source>
        <translation>ResMed Maskeneinstellung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="132"/>
        <source>ResMed Exhale Pressure Relief</source>
        <translation>ResMed Ausatmungsdruckentlastung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2861"/>
        <source>A-Flex</source>
        <translation>A-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="140"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1036"/>
        <source>EPR Level</source>
        <translation>EPR Ebene</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="830"/>
        <source>Unintentional Leaks</source>
        <translation>Unbeabsichtigte Lecks</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="60"/>
        <source>Would you like to show bookmarked areas in this report?</source>
        <translation>Möchten Sie die Lesezeichenbereiche in diesem Bericht anzeigen?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="39"/>
        <source>Somnopose</source>
        <translation>Somnopose</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1118"/>
        <source>AHI %1</source>
        <translation>AHI %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2859"/>
        <source>C-Flex</source>
        <translation>C-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="123"/>
        <source>VPAPauto</source>
        <translation>VPAPauto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="318"/>
        <source>Physical Height</source>
        <translation>Physische Größe</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="189"/>
        <source>Pt. Access</source>
        <translation>Pt. Zugriff</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="43"/>
        <source>CMS50F</source>
        <translation>CMS50F</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="309"/>
        <source>ASV (Fixed EPAP)</source>
        <translation>ASV (Fest-EPAP)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="245"/>
        <source>Patient Triggered Breaths</source>
        <translation>Durch Patienten ausgelöste Atemzüge</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="41"/>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="43"/>
        <source>Contec</source>
        <translation>Contec</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="379"/>
        <source>Events</source>
        <translation>Ereignisse</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2934"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="159"/>
        <source>Humid. Level</source>
        <translation>Befeuchtungsstärke</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="183"/>
        <source>AB Filter</source>
        <translation>AB Filter</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="318"/>
        <source>Height</source>
        <translation>Höhe</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="209"/>
        <source>Ramp Enable</source>
        <translation>Rampe aktivieren</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="394"/>
        <source>(% %1 in events)</source>
        <translation>(% %1 der Ereignisse)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="785"/>
        <source>Lower Threshold</source>
        <translation>untere Schwelle</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/MinutesAtPressure.cpp" line="820"/>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1260"/>
        <source>No Data</source>
        <translation>Keine Daten</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Zeo sleep quality measurement</source>
        <translation>Schlafqualitätsmessung</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="568"/>
        <source>Page %1 of %2</source>
        <translation>Seite %1 von %2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="705"/>
        <source>Litres</source>
        <translation>Liter</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="198"/>
        <source>Manual</source>
        <translation>Handbuch</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="885"/>
        <source>Median</source>
        <translation>Median</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1588"/>
        <source>Fixed %1 (%2)</source>
        <translation>Fest %1 (%2)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="383"/>
        <source>Min %1</source>
        <translation>Min %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="486"/>
        <source>Could not find explorer.exe in path to launch Windows Explorer.</source>
        <translation>Explorer.exe nicht im PATH. Windows-Explorer kann nicht gestartet werden.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2908"/>
        <source>Connected</source>
        <translation>Angeschlossen</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1013"/>
        <source>Low Usage Days: %1</source>
        <translation>Tage mit geringer Nutzung: %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>PS Max</source>
        <translation>PS Max</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>PS Min</source>
        <translation>PS Min</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="196"/>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="206"/>
        <source>Selection Length</source>
        <translation>Auswahllänge</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="249"/>
        <source>Database Outdated
Please Rebuild CPAP Data</source>
        <translation>veraltete Datenbank
CPAP Daten wiederherstellen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Flow Limit.</source>
        <translation>Fließgrenze.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="276"/>
        <source>Detected mask leakage including natural Mask leakages</source>
        <translation>Erkannte Masken Lecks einschließlich der natürlichen Maskenlecks</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="739"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="211"/>
        <source>Plethy</source>
        <translation>Plethy (Ein Gerät bestimmen und Registrieren der Variationen in der Größe oder des Volumens eines Schenkels, in Arm oder Bein, und der Änderung der Blutmenge im Glied.)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="817"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1024"/>
        <source>SensAwake</source>
        <translation>Druckverminderungstechnologie während des Wachwerdens</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="767"/>
        <source>ST/ASV</source>
        <translation>ST/ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="279"/>
        <source>Median Leaks</source>
        <translation>Median Lecks</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="137"/>
        <source>%1 Report</source>
        <translation>%1 Bericht</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="92"/>
        <source>ResMed</source>
        <translation>ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="836"/>
        <source>Pr. Relief</source>
        <translation>Druckentlastung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="722"/>
        <source>Graphs Switched Off</source>
        <translation>Diagramme ausgeblendet</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="845"/>
        <source>Serial</source>
        <translation>Serien</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="846"/>
        <source>Series</source>
        <translation>Serie</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="361"/>
        <source>(last night)</source>
        <translation>(letzte Nacht)</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="250"/>
        <source>AHI	%1
</source>
        <translation>AHI	%1
</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="735"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="317"/>
        <source>Weight</source>
        <translation>Gewicht</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>ZEO ZQ</source>
        <translation>Schlafqualitätsmessung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2871"/>
        <source>PRS1 pressure relief setting.</source>
        <translation>PRS1 Druckentlastungseinstellung.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="852"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="286"/>
        <source>Orientation</source>
        <translation>Orientierung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="147"/>
        <source>Smart Start</source>
        <translation>Smart Start</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="748"/>
        <source>Event Flags</source>
        <translation>Ereignis-Flag</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Zeo ZQ</source>
        <translation>Schlafqualitätsmessung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="740"/>
        <source>Migrating Summary File Location</source>
        <translation>Ändern des Speicherorts der Zusammenfassungsdatei</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="736"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Zombie</source>
        <translation>Mir geht es</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2860"/>
        <source>C-Flex+</source>
        <translation>C-Flex+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="838"/>
        <source>Bookmarks</source>
        <translation>Lesezeichen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="775"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="776"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2837"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="301"/>
        <source>PAP Mode</source>
        <translation>PAP Modus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="113"/>
        <source>CPAP Mode</source>
        <translation>CPAP Modus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="342"/>
        <source>Time taken to get to sleep</source>
        <translation>Einschlafzeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="86"/>
        <source>SmartFlex Settings</source>
        <translation>SmartFlex Einstellungen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="160"/>
        <source>An apnea where the airway is open</source>
        <translation>Atemaussetzer obwohl Atemwege offen sind</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="815"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Flow Limitation</source>
        <translation>Flusslimitierung</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2856"/>
        <source>Pin %1 Graph</source>
        <translation>Einheften Graph %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2777"/>
        <source>Unpin %1 Graph</source>
        <translation>Loslösen Graph %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="727"/>
        <source>Queueing Import Tasks...</source>
        <translation>Warteschlangenimportaufgaben...</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="161"/>
        <source>Hours: %1h, %2m, %3s</source>
        <translation>Stunden: %1h, %2m, %3s</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="555"/>
        <source>OSCAR does not yet have any automatic card backups stored for this device.</source>
        <translation>Für OSCAR sind noch keine automatischen Kartensicherungen für dieses Gerät gespeichert.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="235"/>
        <source>%1
Length: %3
Start: %2</source>
        <translation>%1
Länge: %3
Start: %2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="43"/>
        <source>CMS50F3.7</source>
        <translation>CMS50F3.7</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="248"/>
        <source>RDI	%1
</source>
        <translation>RDI	%1
</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="125"/>
        <source>ASVAuto</source>
        <translation>ASVAuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1599"/>
        <source>PS %1 over %2-%3 (%4)</source>
        <translation>PS %1 über %2-%3 (%4)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="832"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="224"/>
        <source>Flow Rate</source>
        <translation>Fließrate</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="255"/>
        <source>Time taken to breathe out</source>
        <translation>Ausatmungszeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="559"/>
        <source>Important:</source>
        <translation>Wichtig:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="682"/>
        <source>ANGLE / OpenGLES</source>
        <translation>ANGLE / OpenGLES</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="211"/>
        <source>An optical Photo-plethysomogram showing heart rhythm</source>
        <translation>Eine optische Darstellung vom Herzrhythmus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="695"/>
        <source>Loading %1 data for %2...</source>
        <translation>Lade %1 Daten für %2...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="203"/>
        <source>Pillows</source>
        <translation>Kissen</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="222"/>
        <source>%1
Length: %3
Start: %2
</source>
        <translation>%1
Länge: %3
Start: %2
</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="338"/>
        <source>Time Awake</source>
        <translation>Aufwachzeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="337"/>
        <source>How you felt in the morning</source>
        <translation>Wie fühlten Sie sich am Morgen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>I:E Ratio</source>
        <translation>I: E-Verhältnis</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Amount of air displaced per breath</source>
        <translation>Pro Atemzug verdrängte Luftmenge</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="245"/>
        <source>Pat. Trig. Breaths</source>
        <translation>Patientenatemverursachte Atemzüge</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="371"/>
        <source>% in %1</source>
        <translation>% in %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="159"/>
        <source>Humidity Level</source>
        <translation>Feuchtigkeitsgrad</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="743"/>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="858"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="248"/>
        <source>Leak Rate</source>
        <translation>Leckrate</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="910"/>
        <source>Loading Summaries.xml.gz</source>
        <translation>Zusammenfassungsdaten xml.gz werden geladen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="176"/>
        <source>ClimateLine Temperature Enable</source>
        <translation>Schlauchtemperatur einschalten</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="710"/>
        <source>Severity (0-1)</source>
        <translation>Schwere (0-1)</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="366"/>
        <source>Reporting from %1 to %2</source>
        <translation>Berichterstattung vom %1 bis %2</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1209"/>
        <source>Are you sure you want to reset all your channel colors and settings to defaults?</source>
        <translation>Sind Sie sicher, dass Sie alle Kanal-Farben und Einstellungen auf Standardwerte zurücksetzen wollen?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1219"/>
        <source>Are you sure you want to reset all your oximetry settings to defaults?</source>
        <translation>Möchten Sie wirklich alle Oximetrieeinstellungen auf die Standardwerte zurücksetzen?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="335"/>
        <source>BrainWave</source>
        <translation>Gehirnwellen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Inspiratory Pressure</source>
        <translation>Einatmungsdruck</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="336"/>
        <source>Number of Awakenings</source>
        <translation>Anzahl Aufwachereignisse</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2831"/>
        <source>A pulse of pressure &apos;pinged&apos; to detect a closed airway.</source>
        <translation>Ein Druckimpuls um geschlossene Atemwege zu detektieren.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2798"/>
        <source>Intellipap pressure relief level.</source>
        <translation>Intellipap Druckentlastungsniveau.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="280"/>
        <source>CMS50D+</source>
        <translation>CMS50D+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="279"/>
        <source>Median Leak Rate</source>
        <translation>Median Leckrate</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="432"/>
        <source> (%3 sec)</source>
        <translation> (%3 sek)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="242"/>
        <source>Rate of breaths per minute</source>
        <translation>Atemzüge pro Minute</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="561"/>
        <source>Are you ready to upgrade, so you can run the new version of OSCAR?</source>
        <translation>Sind Sie bereit für ein Upgrade, damit eine neue Version von OSCAR ausgeführt wird?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="214"/>
        <source>Perfusion Index</source>
        <translation>Perfusionen-Index</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="236"/>
        <source>Graph displaying snore volume</source>
        <translation>Graphische Anzeige Schnarchvolumen</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="224"/>
        <source>Mask Off</source>
        <translation>Maske ab</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="760"/>
        <source>Max EPAP</source>
        <translation>Max EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="763"/>
        <source>Max IPAP</source>
        <translation>Max IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="863"/>
        <source>Bedtime</source>
        <translation>Schlafenszeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2864"/>
        <source>Bi-Flex</source>
        <translation>Bi-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1595"/>
        <source>EPAP %1 IPAP %2 (%3)</source>
        <translation>EPAP %1 IPAP %2 (%3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="740"/>
        <source>Pressure</source>
        <translation>Druck</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2985"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2987"/>
        <source>Auto On</source>
        <translation>Automatisch ein</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="884"/>
        <source>Average</source>
        <translation>Durchschnitt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Target Minute Ventilation</source>
        <translation>Zielminutenvolumen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="239"/>
        <source>Amount of air displaced per minute</source>
        <translation>Atemminutenvolumen</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gTTIAChart.cpp" line="83"/>
        <source>
TTIA: %1</source>
        <translation>
TTIA: %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="245"/>
        <source>Percentage of breaths triggered by patient</source>
        <translation>Prozentualer Anteil der vom Patienten ausgelösten Atemzüge</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1009"/>
        <source>Days: %1</source>
        <translation>Tage: %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="211"/>
        <source>Plethysomogram</source>
        <translation>Plethysomogramm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="198"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="201"/>
        <source>A user definable event detected by OSCAR&apos;s flow waveform processor.</source>
        <translation>Ein vom Benutzer definierbares Ereignis, das vom Flow-Wave-Prozessor von OSCAR erkannt wird.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="681"/>
        <source>Software Engine</source>
        <translation>Software</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="307"/>
        <source>Auto Bi-Level (Fixed PS)</source>
        <translation>Auto Bi-Level (Feste PS)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="718"/>
        <source>Please Note</source>
        <translation>Bitte warten Sie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="148"/>
        <source>Starting Ramp Pressure</source>
        <translation>Anlaufdruck</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Last Updated</source>
        <translation>Letzte Aktualisierung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="189"/>
        <source>Intellipap event where you breathe out your mouth.</source>
        <translation>Intellipap Ereignis, bei dem Sie durch den Mund ausatmen.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="310"/>
        <source>ASV (Variable EPAP)</source>
        <translation>ASV (Variables EPAP)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="140"/>
        <source>Exhale Pressure Relief Level</source>
        <translation>Ausatemdruckentlastungs-Niveau</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="816"/>
        <source>Flow Limit</source>
        <translation>Fließgrenze</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="293"/>
        <source>UAI=%1 </source>
        <translation>UAI=%1 </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gUsageChart.cpp" line="30"/>
        <source>
Length: %1</source>
        <translation>
Dauer: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gUsageChart.cpp" line="96"/>
        <source>%1 low usage, %2 no usage, out of %3 days (%4% compliant.) Length: %5 / %6 / %7</source>
        <translation>%1 geringer Nutzung, %2 keine Verwendung, von %3 Tage (%4% konform.) Länge: %5 / %6 / %7</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="1042"/>
        <source>Loading Summary Data</source>
        <translation>Zusammenfassungsdaten laden</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="716"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="737"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>Pulse Rate</source>
        <translation>Pulsrate</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2863"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2887"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2889"/>
        <source>Rise Time</source>
        <translation>Anstiegszeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="147"/>
        <source>SmartStart</source>
        <translation>SmartStart</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="273"/>
        <source>Graph showing running AHI for the past hour</source>
        <translation>Anzeige des AHI der letzten Stunde</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="282"/>
        <source>Graph showing running RDI for the past hour</source>
        <translation>Anzeige des RDI der letzten Stunde</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="176"/>
        <source>Temperature Enable</source>
        <translation>Temperatur aktivieren</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="695"/>
        <source>Seconds</source>
        <translation>Sekunden</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="309"/>
        <source>%1 (%2 days): </source>
        <translation>%1 (%2 Tage): </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="683"/>
        <source>Desktop OpenGL</source>
        <translation>Desktop OpenGL</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="430"/>
        <source>Snapshot %1</source>
        <translation>Schnappschuss %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="865"/>
        <source>Mask Time</source>
        <translation>Maskenzeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>How you feel (0 = like crap, 10 = unstoppable)</source>
        <translation>Wie fühlen Sie sich (0 = nicht gut, 10 = hervorragend)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>Time in REM Sleep</source>
        <translation>Zeit im Traum/REM-Schlaf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="848"/>
        <source>Channel</source>
        <translation>Kanal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="128"/>
        <source>Auto for Her</source>
        <translation>Auto für Sie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="341"/>
        <source>Time In Deep Sleep</source>
        <translation>Zeit im Tiefschlaf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="341"/>
        <source>Time in Deep Sleep</source>
        <translation>Zeit im Tiefschlaf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>Pressure Max</source>
        <translation>Maximaler Druck</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Pressure Min</source>
        <translation>Minimaler Druck</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2958"/>
        <source>Diameter of primary CPAP hose</source>
        <translation>Durchmesser des primären CPAP Schlauchs</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>Max Leaks</source>
        <translation>Max Lecks</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2870"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2872"/>
        <source>Flex Level</source>
        <translation>Flex-Ebene</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="342"/>
        <source>Time to Sleep</source>
        <translation>Einschlafzeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="153"/>
        <source>Humid. Status</source>
        <translation>Feucht. Status</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1131"/>
        <source>(Sess: %1)</source>
        <translation>(Sitzung: %1)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="195"/>
        <source>Climate Control</source>
        <translation>Klimakontrolle</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="214"/>
        <source>Perf. Index %</source>
        <translation>Perfusionen-Index %</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="60"/>
        <source>If your old data is missing, copy the contents of all the other Journal_XXXXXXX folders to this one manually.</source>
        <translation>Wenn ihre alten Daten fehlen, kopieren Sie den Inhalt aller andere Journal_XXXXXXX  Ordner  in  diesenl.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="730"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abrechen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1604"/>
        <location filename="../oscar/SleepLib/day.cpp" line="1613"/>
        <source>Min EPAP %1 Max IPAP %2 PS %3-%4 (%5)</source>
        <translation>Min EPAP %1 Max IPAP %2 PS %3-%4 (%5)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="187"/>
        <source>System One</source>
        <translation>System One</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="751"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="707"/>
        <source>Breaths/min</source>
        <translation>Atmungen/min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="711"/>
        <source>Degrees</source>
        <translation>Grad</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="731"/>
        <source>&amp;Destroy</source>
        <translation>&amp;Vernichten</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="487"/>
        <source>There is a lockfile already present for this profile &apos;%1&apos;, claimed on &apos;%2&apos;.</source>
        <translation>Es ist bereits eine Sperrdatei für dieses Profil vorhanden &apos;%1&apos;, beansprucht am &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="601"/>
        <source>OSCAR will now start the import wizard so you can reinstall your %1 data.</source>
        <translation>OSCAR startet nun den Importassistenten, damit Sie Ihr    %1     Daten neu installieren kann.</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="289"/>
        <source>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%%</source>
        <translation>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%%</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="279"/>
        <source>Median rate of detected mask leakage</source>
        <translation>Median Rate der bemerkten Masken Lecks</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="319"/>
        <source>Bookmark Notes</source>
        <translation>Lesezeichen-Notizen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Bookmark Start</source>
        <translation>Beginn Lesezeichen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="301"/>
        <source>PAP Device Mode</source>
        <translation>PAP Gerätemodus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="227"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Mask Pressure</source>
        <translation>Maskendruck</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="368"/>
        <source>No oximetry data has been imported yet.</source>
        <translation>Es wurden noch keine Oxymetriedaten importiert.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="50"/>
        <source>Please be careful when playing in OSCAR&apos;s profile folders :-P</source>
        <translation>Seien Sie vorsichtig, wenn Sie mit dem OSCAR-Profilordnern spielen :-P</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="333"/>
        <source>1=Awake 2=REM 3=Light Sleep 4=Deep Sleep</source>
        <translation>1 = Wach 2 = Traum 3 = Leichter Schlaf 4 = Tiefschlaf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>Respiratory Event</source>
        <translation>Atemereignis</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="141"/>
        <source>End Expiratory Pressure</source>
        <translation>Beenden Sie den Ausatmungsdruck</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="156"/>
        <source>An abnormal period of Cheyne Stokes Respiration</source>
        <translation>Eine abnormale Periode von Cheyne-Stokes-Atmung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="156"/>
        <source>Cheyne Stokes Respiration (CSR)</source>
        <translation>Cheyne-Stokes-Atmung (CSR)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="158"/>
        <source>Periodic Breathing (PB)</source>
        <translation>Periodische Atmung (PB)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="160"/>
        <source>Clear Airway (CA)</source>
        <translation>Freie Atemwege (CA)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="162"/>
        <source>Obstructive Apnea (OA)</source>
        <translation>Obstruktive Apnoe (OA)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>Hypopnea (H)</source>
        <translation>Hypopnoe (H)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>Unclassified Apnea (UA)</source>
        <translation>Nicht klassifizierte Apnoe (UA)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>Apnea (A)</source>
        <translation>Apnoe (A)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>An apnea reportred by your CPAP device.</source>
        <translation>Eine von Ihrem CPAP-Gerät gemeldete Apnoe.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="170"/>
        <source>Flow Limitation (FL)</source>
        <translation>Durchflussbegrenzung (FL)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>RERA (RE)</source>
        <translation>RERA (RE)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>Respiratory Effort Related Arousal: A restriction in breathing that causes either awakening or sleep disturbance.</source>
        <translation>Atemanstrengungsbedingte Erregung: Eine Atembehinderung, die entweder zu Aufwach- oder Schlafstörungen führt.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>Vibratory Snore (VS)</source>
        <translation>Vibrationsschnarchen (VS)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="179"/>
        <source>Leak Flag (LF)</source>
        <translation>Leck-Kennzeichnung (LF)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="179"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>A large mask leak affecting device performance.</source>
        <translation>Ein großes Maskenleck beeinträchtigt die Geräteleistung.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>Large Leak (LL)</source>
        <translation>Großes Leck (LL)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>A type of respiratory event that won&apos;t respond to a pressure increase.</source>
        <translation>Ein Atemereignis, das nicht auf Druckanstieg reagiert.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="183"/>
        <source>Antibacterial Filter</source>
        <translation>Antibakterienfilter</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="37"/>
        <source>Windows User</source>
        <translation>Windows-Benutzer</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="708"/>
        <source>Cataloguing EDF Files...</source>
        <translation>EDF-Dateien werden katalogisiert...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="713"/>
        <source>Question</source>
        <translation>Frage</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="340"/>
        <source>Time spent in light sleep</source>
        <translation>Zeit in Leichtschlaf</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1141"/>
        <source>Waketime: %1</source>
        <translation>Aufwachzeit: %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>Time In REM Sleep</source>
        <translation>Zeit in Traum/REM-Schlaf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="137"/>
        <source>Higher Inspiratory Pressure</source>
        <translation>Obererr Inspirationsdruck</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="118"/>
        <source>Weinmann</source>
        <translation>Weinmann</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="298"/>
        <source>Summary Only</source>
        <translation>Nur Zusammenfassung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>Bookmark End</source>
        <translation>Ende Lesezeichen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="756"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2844"/>
        <source>Bi-Level</source>
        <translation>Bi Ebene</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="78"/>
        <source>Intellipap</source>
        <translation>Intellipap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="866"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="129"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="206"/>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="126"/>
        <source>Unknown</source>
        <translation>Unbekannt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="756"/>
        <source>Finishing Up...</source>
        <translation>Beenden...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="700"/>
        <source>Events/hr</source>
        <translation>Ereignisse/Stunde</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2904"/>
        <source>PRS1 humidifier connected?</source>
        <translation>PRS1 Luftbefeuchter angeschlossen?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="298"/>
        <source>CPAP Session contains summary data only</source>
        <translation>CPAP Sitzung enthält nur Übersichtsdaten</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2748"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="368"/>
        <location filename="../oscar/mainwindow.cpp" line="754"/>
        <location filename="../oscar/mainwindow.cpp" line="2360"/>
        <source>Finishing up...</source>
        <translation>Beenden...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="78"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.h" line="88"/>
        <source>Fisher &amp; Paykel</source>
        <translation>Fisher &amp; Paykel</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="377"/>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="380"/>
        <source>Duration</source>
        <translation>Dauer</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="535"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="897"/>
        <source>Scanning Files...</source>
        <translation>Scanne Dateien...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2854"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2856"/>
        <source>Flex Mode</source>
        <translation>Flex-Modus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="835"/>
        <source>Sessions</source>
        <translation>Sitzungen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2994"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2996"/>
        <source>Auto Off</source>
        <translation>Automatisch aus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="849"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="744"/>
        <source>Overview</source>
        <translation>Überblick</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="537"/>
        <source>The folder you chose is not empty, nor does it already contain valid OSCAR data.</source>
        <translation>Der von Ihnen gewählte Ordner ist weder leer noch enthält er gültige OSCAR-Daten.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="172"/>
        <source>Temperature</source>
        <translation>Temperatur</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="431"/>
        <source>Entire Day&apos;s Flow Waveform</source>
        <translation>Fluss-Wellenform des ganzen Tages</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="524"/>
        <location filename="../oscar/main.cpp" line="576"/>
        <location filename="../oscar/main.cpp" line="591"/>
        <source>Exiting</source>
        <translation>Verlassen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="78"/>
        <source>DeVilbiss</source>
        <translation>DeVilbiss</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="340"/>
        <source>Time in Light Sleep</source>
        <translation>Zeit in Leichtschlaf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="340"/>
        <source>Time In Light Sleep</source>
        <translation>Zeit in Leichtschlaf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="306"/>
        <source>Fixed Bi-Level</source>
        <translation>Bi-Level fix</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="264"/>
        <source>Machine Information</source>
        <translation>Geräte-Informationen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>Pressure Support Maximum</source>
        <translation>Druckunterstützungs-Maximum</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Graph showing severity of flow limitations</source>
        <translation>Graphische Darstellung der Schwere der Flussbegrenzung</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="195"/>
        <source>: %1 hours, %2 minutes, %3 seconds
</source>
        <translation>: %1 Stunden, %2 Minuten, %3 Sekunden
</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>Auto Bi-Level (Variable PS)</source>
        <translation>Bi-Level (Variable PS)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3003"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3005"/>
        <source>Mask Alert</source>
        <translation>Maskenalarm</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="304"/>
        <source>OSCAR Reminder</source>
        <translation>OSCAR-Erinnerung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="47"/>
        <source>OSCAR found an old Journal folder, but it looks like it&apos;s been renamed:</source>
        <translation>OSCAR hat einen alten Journalordner gefunden, der jedoch umbenannt wurde:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>A partially obstructed airway</source>
        <translation>Teilweise behinderte Atemwege</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>Pressure Support Minimum</source>
        <translation>Druckunterstützungs-Minimum</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="827"/>
        <source>Large Leak</source>
        <translation>Großes Leck</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="295"/>
        <source>Time started according to str.edf</source>
        <translation>Startzeit laut str.edf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="864"/>
        <source>Wake-up</source>
        <translation>Aufwachzeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="715"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="865"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="866"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="867"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Min Pressure</source>
        <translation>Mindestdruck</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="276"/>
        <source>Total Leak Rate</source>
        <translation>Gesamtleckrate</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>Max Pressure</source>
        <translation>Größter Druck</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="831"/>
        <source>MaskPressure</source>
        <translation>Maskendruck</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1117"/>
        <source>Duration %1:%2:%3</source>
        <translation>Dauer %1:%2:%3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="782"/>
        <source>Upper Threshold</source>
        <translation>obere Schwelle</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="49"/>
        <source>OSCAR will not touch this folder, and will create a new one instead.</source>
        <translation>OSCAR benutzt diesen Ordner nicht und erstellt stattdessen einen neuen.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="829"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="276"/>
        <source>Total Leaks</source>
        <translation>Anzahl der Lecks</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="239"/>
        <source>Minute Ventilation</source>
        <translation>Minutenvolumen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="248"/>
        <source>Rate of detected mask leakage</source>
        <translation>Anzahl erkannter Maskenlecks</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="224"/>
        <source>Breathing flow rate waveform</source>
        <translation>Atemflussrate-Wellenform</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="139"/>
        <source>Lower Expiratory Pressure</source>
        <translation>Niedriger Ausatemdruck</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="176"/>
        <source>A vibratory snore as detected by a System One device</source>
        <translation>Ein vibrierendes Schnarchen, wie es von einem System One-Gerät erkannt wird</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>Non Responding Event (NR)</source>
        <translation>Keine-Antwort-Ereignis (NR)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="189"/>
        <source>Expiratory Puff (EP)</source>
        <translation>Ausatmungsstoß (EP)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="192"/>
        <source>SensAwake (SA)</source>
        <translation>SensAwake (SA)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>User Flag #1 (UF1)</source>
        <translation>Benutzerkennzeichnung #1 (UF1)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="198"/>
        <source>User Flag #2 (UF2)</source>
        <translation>Benutzerkennzeichnung #2 (UF2)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="201"/>
        <source>User Flag #3 (UF3)</source>
        <translation>Benutzerkennzeichnung #3 (UF3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="217"/>
        <source>Pulse Change (PC)</source>
        <translation>Pulsänderung (PC)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="220"/>
        <source>SpO2 Drop (SD)</source>
        <translation>SpO2-Abfall (SD)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="273"/>
        <source>Apnea Hypopnea Index (AHI)</source>
        <translation>Apnoe-Hypopnoe-Index (AHI)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="282"/>
        <source>Respiratory Disturbance Index (RDI)</source>
        <translation>Atemstörungsindex (RDI)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>Time spent in REM Sleep</source>
        <translation>Zeit in REM-Schlaf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1591"/>
        <source>Min %1 Max %2 (%3)</source>
        <translation>Min %1 Max %2 (%3)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="489"/>
        <source>If you are using cloud storage, make sure OSCAR is closed and syncing has completed first on the other computer before proceeding.</source>
        <translation>Wenn Sie Cloud-Speicher verwenden, stellen Sie sicher, dass OSCAR geschlossen ist und die Synchronisierung zuerst auf dem anderen Computer abgeschlossen ist, bevor Sie fortfahren.</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="283"/>
        <source>AI=%1 HI=%2 CAI=%3 </source>
        <translation>AI=%1 HI=%2 CAI=%3 </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>Time taken to breathe in</source>
        <translation>Einatmungszeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>Maximum Therapy Pressure</source>
        <translation>Maximaler Therapiedruck</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="538"/>
        <source>Are you sure you want to use this folder?</source>
        <translation>Sind Sie sicher, dass Sie diesen Ordner nutzen möchten?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="613"/>
        <source>Use your file manager to make a copy of your profile directory, then afterwards, restart OSCAR and complete the upgrade process.</source>
        <translation>Verwenden Sie Ihren Dateimanager, um eine Kopie Ihres Profilverzeichnisses zu erstellen. Starten Sie anschließend OSCAR erneut, und schließen Sie den Aktualisierungsvorgang ab.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="311"/>
        <source>%1 (%2 day): </source>
        <translation>%1 (%2 Tag): </translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="433"/>
        <source>Current Selection</source>
        <translation>Aktuelle Auswahl</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="208"/>
        <source>Blood-oxygen saturation percentage</source>
        <translation>Blutsauerstoffsättigung in Prozent</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="551"/>
        <source>&lt;b&gt;OSCAR maintains a backup of your devices data card that it uses for this purpose.&lt;/b&gt;</source>
        <translation>&lt;b&gt;OSCAR unterhält eine Sicherung Ihrer Gerätedatenkarte, die es für diesen Zweck verwendet.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>Inspiratory Time</source>
        <translation>Einatemzeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="242"/>
        <source>Respiratory Rate</source>
        <translation>Atemfrequenz</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2435"/>
        <source>Hide All Events</source>
        <translation>Alle Ereignisse verbergen</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="104"/>
        <source>Printing %1 Report</source>
        <translation>Bericht %1 Drucken</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="255"/>
        <source>Expiratory Time</source>
        <translation>Ausatemzeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>Maximum Leak</source>
        <translation>Maximales Leck</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>Ratio between Inspiratory and Expiratory time</source>
        <translation>Verhältnis Ein-/Ausatemzeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="305"/>
        <source>APAP (Variable)</source>
        <translation>APAP (Variabel)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Minimum Therapy Pressure</source>
        <translation>Kleinster Theraphiedruck</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="217"/>
        <source>A sudden (user definable) change in heart rate</source>
        <translation>Eine plötzliche (frei definierbare) Veränderung der Herzfrequenz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>Body Mass Index</source>
        <translation>BMI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="745"/>
        <source>Oximetry</source>
        <translation>Oxymetrie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="747"/>
        <source>Oximeter</source>
        <translation>Oxymeter</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="720"/>
        <source>No Data Available</source>
        <translation>Keine Daten verfügbar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>The maximum rate of mask leakage</source>
        <translation>Der Höchstsatz der Maskenlecks</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2903"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="153"/>
        <source>Humidifier Status</source>
        <translation>Luftbefeuchter-Status</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3088"/>
        <source>Machine Initiated Breath</source>
        <translation>Gerät-initiierter Zugang</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2786"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2788"/>
        <source>SmartFlex Mode</source>
        <translation>Smart-Flex-Modus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>Journal Notes</source>
        <translation>Journal-Notizen</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="430"/>
        <source> (%2 min, %3 sec)</source>
        <translation> (%2 min, %3 sek)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="488"/>
        <source>You can only work with one instance of an individual OSCAR profile at a time.</source>
        <translation>Sie kann nur mit einer einzigen Instanz eines OSCAR-Profils arbeiten.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>Expiratory Pressure</source>
        <translation>Ausatmungsdruck</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3012"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3014"/>
        <source>Show AHI</source>
        <translation>Zeige AHI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="819"/>
        <source>Tgt. Min. Vent</source>
        <translation>Ziel-Minutenventilation</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="585"/>
        <source>Rebuilding from %1 Backup</source>
        <translation>Wiederherstellung vom%1 Backup</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1298"/>
        <source>Are you sure you want to reset all your waveform channel colors and settings to defaults?</source>
        <translation>Sind Sie sicher, dass Sie alle Ihre Wellenform-Kanal-Farben und Einstellungen auf die Standardwerte zurücksetzen wollen?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2830"/>
        <source>Pressure Pulse</source>
        <translation>Druckimpuls</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="41"/>
        <source>ChoiceMMed</source>
        <translation>ChoiceMMed</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="102"/>
        <source>Sessions: %1 / %2 / %3 Length: %4 / %5 / %6 Longest: %7 / %8 / %9</source>
        <translation>Sitzungen: %1 / %2 / %3 Länge: %4 / %5 / %6 Längste: %7 / %8 / %9</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="769"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2905"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2932"/>
        <source>Humidifier</source>
        <translation>Luftbefeuchter</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="155"/>
        <source>Relief: %1</source>
        <translation>Relief: %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="860"/>
        <source>Patient ID</source>
        <translation>Patienten-ID</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="137"/>
        <source>Patient???</source>
        <translation>Patient???</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="162"/>
        <source>An apnea caused by airway obstruction</source>
        <translation>Eine Apnoe durch Obstruktion der Atemwege verursacht</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="176"/>
        <source>Vibratory Snore (VS2) </source>
        <translation>Vibrations-Schnarchen (VS2) </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="15"/>
        <source>Please Wait...</source>
        <translation>Bitte warten...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="202"/>
        <source>Using </source>
        <translation>Verwendung von </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="202"/>
        <source>, found SleepyHead -
</source>
        <translation>, gefunden SleepyHead -
</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="203"/>
        <source>You must run the OSCAR Migration Tool</source>
        <translation>Sie müssen das OSCAR-Migrationswerkzeug ausführen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>An apnea that couldn&apos;t be determined as Central or Obstructive.</source>
        <translation>Eine Apnoe, die nicht als zentral oder obstruktiv bestimmt werden konnte.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="170"/>
        <source>A restriction in breathing from normal, causing a flattening of the flow waveform.</source>
        <translation>Eine Einschränkung der Atmung aus dem Normalzustand, die zu einer Verflachung der Strömungswellenform führt.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="194"/>
        <source>or CANCEL to skip migration.</source>
        <translation>oder CANCEL, um die Migration zu überspringen.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="209"/>
        <source>You cannot use this folder:</source>
        <translation>Sie können diesen Ordner nicht verwenden:</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="521"/>
        <source>Choose or create a new folder for OSCAR data</source>
        <translation>Auswählen oder Erstellen eines neuen Ordners für OSCAR-Daten</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="224"/>
        <source>Migrating </source>
        <translation>Migration </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="224"/>
        <source> files</source>
        <translation> Dateien</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="225"/>
        <source>from </source>
        <translation>von </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="225"/>
        <source>to </source>
        <translation>zu </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="511"/>
        <source>OSCAR will set up a folder for your data.</source>
        <translation>OSCAR richtet einen Ordner für Ihre Daten ein.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="514"/>
        <source>We suggest you use this folder: </source>
        <translation>Wir empfehlen Ihnen, diesen Ordner zu verwenden: </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="515"/>
        <source>Click Ok to accept this, or No if you want to use a different folder.</source>
        <translation>Klicken Sie auf Ok, um dies zu akzeptieren, oder auf Nein, wenn Sie einen anderen Ordner verwenden möchten.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="526"/>
        <source>Next time you run OSCAR, you will be asked again.</source>
        <translation>Wenn Sie OSCAR das nächste Mal ausführen, werden Sie erneut gefragt.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="221"/>
        <source>App key:</source>
        <translation>App-Taste:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="209"/>
        <source>Operating system:</source>
        <translation>Betriebssystem:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="210"/>
        <source>Graphics Engine:</source>
        <translation>Grafik-Modul:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="211"/>
        <source>Graphics Engine type:</source>
        <translation>Typ der Grafik-Engine:</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="558"/>
        <source>Data directory:</source>
        <translation>Datenverzeichnis:</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="168"/>
        <source>Updating Statistics cache</source>
        <translation>Aktualisieren des Statistik-Cache</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="715"/>
        <source>Usage Statistics</source>
        <translation>Statistik der Anwendung</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1771"/>
        <source>d MMM yyyy [ %1 - %2 ]</source>
        <translation>d MMM yyyy [ %1 - %2 ]</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1609"/>
        <source>EPAP %1 PS %2-%3 (%4)</source>
        <translation>EPAP %1 PS %2-%3 (%4)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>Pressure Set</source>
        <translation>Eingestellter Druck</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>Pressure Setting</source>
        <translation>Druckeinstellung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>IPAP Set</source>
        <translation>IPAP-Set</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>IPAP Setting</source>
        <translation>IPAP-Einstellung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>EPAP Set</source>
        <translation>EPAP Set</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>EPAP Setting</source>
        <translation>EPAP Einstellungen</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="752"/>
        <source>Loading summaries</source>
        <translation>Laden von Zusammenfassungen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="207"/>
        <source>Built with Qt %1 on %2</source>
        <translation>Gebaut mit Qt %1 on %2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="853"/>
        <source>Motion</source>
        <translation>Antrag</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1571"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/dreem_loader.h" line="37"/>
        <source>Dreem</source>
        <translation>Dreem</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="153"/>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="92"/>
        <source>Untested Data</source>
        <translation>Ungeprüfte Daten</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2862"/>
        <source>P-Flex</source>
        <translation>P-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2912"/>
        <source>Humidification Mode</source>
        <translation>Befeuchtungsmodus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2913"/>
        <source>PRS1 Humidification Mode</source>
        <translation>PRS1 Befeuchtungsmodus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2914"/>
        <source>Humid. Mode</source>
        <translation>Feucht. Modus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2916"/>
        <source>Fixed (Classic)</source>
        <translation>Fixiert (klassisch)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2917"/>
        <source>Adaptive (System One)</source>
        <translation>Anpassungsfähig an (System One)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2918"/>
        <source>Heated Tube</source>
        <translation>Beheizte Schläuche</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2924"/>
        <source>Tube Temperature</source>
        <translation>Schlauchtemperatur</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2925"/>
        <source>PRS1 Heated Tube Temperature</source>
        <translation>PRS1 Temperatur des beheizten Schlauches</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2926"/>
        <source>Tube Temp.</source>
        <translation>Schlauch-Temp.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2933"/>
        <source>PRS1 Humidifier Setting</source>
        <translation>PRS1-Luftbefeuchter-Einstellung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2963"/>
        <source>12mm</source>
        <translation>12mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="93"/>
        <source>Your Viatom device generated data that OSCAR has never seen before.</source>
        <translation>Ihr Viatom-Gerät generierte Daten, die OSCAR noch nie zuvor gesehen hat.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="94"/>
        <source>The imported data may not be entirely accurate, so the developers would like a copy of your Viatom files to make sure OSCAR is handling the data correctly.</source>
        <translation>Die importierten Daten sind möglicherweise nicht ganz korrekt, weshalb die Entwickler eine Kopie Ihrer Viatom-Dateien wünschen, um sicherzustellen, dass OSCAR die Daten korrekt verarbeitet.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.h" line="40"/>
        <source>Viatom</source>
        <translation>Viatom</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.h" line="40"/>
        <source>Viatom Software</source>
        <translation>Viatom-Software</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="538"/>
        <source>OSCAR %1 needs to upgrade its database for %2 %3 %4</source>
        <translation>OSCAR %1 muss seine Datenbank für %2 %3 %4 sichern</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="292"/>
        <source>Movement</source>
        <translation>Bewegung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="292"/>
        <source>Movement detector</source>
        <translation>Bewegungsmelder</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="659"/>
        <source>Version &quot;%1&quot; is invalid, cannot continue!</source>
        <translation>Version &quot;%1 ist ungültig, kann nicht fortgesetzt werden!</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="668"/>
        <source>The version of OSCAR you are running (%1) is OLDER than the one used to create this data (%2).</source>
        <translation>Die Version von OSCAR, die Sie betreiben (%1) ist ÄLTER als derjenige, der zur Erstellung dieser Daten verwendet wurde (%2).</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="304"/>
        <source>Don&apos;t forget to place your datacard back in your CPAP device</source>
        <translation>Vergessen Sie nicht, Ihre Datenkarte wieder in Ihr CPAP-Gerät einzulegen</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2666"/>
        <source>Please select a location for your zip other than the data card itself!</source>
        <translation>Bitte wählen Sie einen anderen Ort für Ihren Zip als die Datenkarte selbst!</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2713"/>
        <location filename="../oscar/mainwindow.cpp" line="2763"/>
        <location filename="../oscar/mainwindow.cpp" line="2822"/>
        <source>Unable to create zip!</source>
        <translation>Zip kann nicht erstellt werden!</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1225"/>
        <source>Parsing STR.edf records...</source>
        <translation>Analysieren von STR.edf-Einträgen...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Mask Pressure (High frequency)</source>
        <translation>Maskendruck (Hochfrequenz)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>A ResMed data item: Trigger Cycle Event</source>
        <translation>Ein ResMed-Datenelement: Zyklus-Ereignis auslösen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="526"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="888"/>
        <source>Backing Up Files...</source>
        <translation>Sichern von Dateien...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="354"/>
        <source>Debugging channel #1</source>
        <translation>Debugging-Kanal #1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="354"/>
        <source>Test #1</source>
        <translation>Test #1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="355"/>
        <source>Debugging channel #2</source>
        <translation>Debugging-Kanal #2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="355"/>
        <source>Test #2</source>
        <translation>Test #2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1624"/>
        <source>EPAP %1 IPAP %2-%3 (%4)</source>
        <translation>EPAP %1 IPAP %2-%3 (%4)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2840"/>
        <source>CPAP-Check</source>
        <translation>CPAP-Check</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2842"/>
        <source>AutoCPAP</source>
        <translation>AutoCPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2843"/>
        <source>Auto-Trial</source>
        <translation>Auto-Versuch</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2845"/>
        <source>AutoBiLevel</source>
        <translation>AutoBiLevel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2847"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2848"/>
        <source>S/T</source>
        <translation>S/T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2850"/>
        <source>S/T - AVAPS</source>
        <translation>S/T - AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2851"/>
        <source>PC - AVAPS</source>
        <translation>PC - AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2878"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2880"/>
        <source>Flex Lock</source>
        <translation>Flex-Verschluss</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2879"/>
        <source>Whether Flex settings are available to you.</source>
        <translation>Ob Ihnen Flex-Einstellungen zur Verfügung stehen.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2888"/>
        <source>Amount of time it takes to transition from EPAP to IPAP, the higher the number the slower the transition</source>
        <translation>Zeitaufwand für den Übergang von EPAP zu IPAP, je höher die Zahl, desto langsamer der Übergang</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2894"/>
        <source>Rise Time Lock</source>
        <translation>Zeitschloss für den Aufstieg</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2895"/>
        <source>Whether Rise Time settings are available to you.</source>
        <translation>Ob Ihnen die Einstellungen der Anstiegszeit zur Verfügung stehen.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2896"/>
        <source>Rise Lock</source>
        <translation>Aufstiegshilfe</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2949"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2950"/>
        <source>Mask Resistance Setting</source>
        <translation>Einstellung des Maskenwiderstands</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2951"/>
        <source>Mask Resist.</source>
        <translation>Maske Widerstand.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2959"/>
        <source>Hose Diam.</source>
        <translation>Schlauch Diam.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2967"/>
        <source>Tubing Type Lock</source>
        <translation>Schlauchtyp-Sperre</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2968"/>
        <source>Whether tubing type settings are available to you.</source>
        <translation>Ob Ihnen die Einstellungen für den Schlauchtyp zur Verfügung stehen.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2969"/>
        <source>Tube Lock</source>
        <translation>Rohrschloss</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2976"/>
        <source>Mask Resistance Lock</source>
        <translation>Masken-Widerstandsschloss</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2977"/>
        <source>Whether mask resistance settings are available to you.</source>
        <translation>Ob Ihnen Maskenwiderstandseinstellungen zur Verfügung stehen.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2978"/>
        <source>Mask Res. Lock</source>
        <translation>Maske Res. Sperre</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3021"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3023"/>
        <source>Ramp Type</source>
        <translation>Rampentyp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3022"/>
        <source>Type of ramp curve to use.</source>
        <translation>Art der zu verwendenden Rampenkurve.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3025"/>
        <source>Linear</source>
        <translation>Linear</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3026"/>
        <source>SmartRamp</source>
        <translation>Intelligente Rampe</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3027"/>
        <source>Ramp+</source>
        <translation>Rampe+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3031"/>
        <source>Backup Breath Mode</source>
        <translation>Sicherungs-Atemmodus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3032"/>
        <source>The kind of backup breath rate in use: none (off), automatic, or fixed</source>
        <translation>Die Art der verwendeten Backup-Atemfrequenz: keine (ausgeschaltet), automatisch oder fest</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3033"/>
        <source>Breath Rate</source>
        <translation>Atemfrequenz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3037"/>
        <source>Fixed</source>
        <translation>Festgelegt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3041"/>
        <source>Fixed Backup Breath BPM</source>
        <translation>Festgelegte Sicherung des BPM-Atems</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3042"/>
        <source>Minimum breaths per minute (BPM) below which a timed breath will be initiated</source>
        <translation>Minimale Atemzüge pro Minute (BPM), unterhalb derer ein zeitgesteuerter Atemzug eingeleitet wird</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3043"/>
        <source>Breath BPM</source>
        <translation>Atmung BPM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3048"/>
        <source>Timed Inspiration</source>
        <translation>Zeitliche Inspiration</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3049"/>
        <source>The time that a timed breath will provide IPAP before transitioning to EPAP</source>
        <translation>Die Zeit, die ein zeitgesteuerter Atemzug IPAP vor dem Übergang zu EPAP liefert</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3050"/>
        <source>Timed Insp.</source>
        <translation>Zeitgesteuerte Insp.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3055"/>
        <source>Auto-Trial Duration</source>
        <translation>Dauer der automatischen Prüfung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3057"/>
        <source>Auto-Trial Dur.</source>
        <translation>Auto-Versuch Dur.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3062"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3064"/>
        <source>EZ-Start</source>
        <translation>EZ-Start</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3063"/>
        <source>Whether or not EZ-Start is enabled</source>
        <translation>Ob EZ-Start aktiviert ist oder nicht</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3071"/>
        <source>Variable Breathing</source>
        <translation>Variable Atmung</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3072"/>
        <source>UNCONFIRMED: Possibly variable breathing, which are periods of high deviation from the peak inspiratory flow trend</source>
        <translation>UNBESTÄTIGT: Möglicherweise variable Atmung, d.h. Perioden mit hoher Abweichung vom Spitzenwert des inspiratorischen Flusses</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="559"/>
        <source>Once you upgrade, you &lt;font size=+1&gt;cannot&lt;/font&gt; use this profile with the previous version anymore.</source>
        <translation>Sobald Sie ein Upgrade durchführen, können Sie dieses Profil nicht mehr mit der vorherigen Version verwenden.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2919"/>
        <source>Passover</source>
        <translation>Befeuchter, bei denen die Luft nur die Wasseroberfläche überströmt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2986"/>
        <source>A few breaths automatically starts device</source>
        <translation>Ein paar Atemzüge startet das Gerät automatisch</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2995"/>
        <source>Device automatically switches off</source>
        <translation>Gerät schaltet automatisch ab</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3004"/>
        <source>Whether or not device allows Mask checking.</source>
        <translation>Ob das Gerät die Maskenprüfung zulässt oder nicht.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3013"/>
        <source>Whether or not device shows AHI via built-in display.</source>
        <translation>Ob das Gerät AHI über das eingebaute Display anzeigt oder nicht.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3056"/>
        <source>The number of days in the Auto-CPAP trial period, after which the device will revert to CPAP</source>
        <translation>Die Anzahl der Tage im Auto-CPAP-Testzeitraum, nach denen das Gerät wieder auf CPAP umschaltet</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3081"/>
        <source>A period during a session where the device could not detect flow.</source>
        <translation>Ein Zeitraum während einer Sitzung, in dem das Gerät keinen Durchfluss erkennen konnte.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3095"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3097"/>
        <source>Peak Flow</source>
        <translation>Spitzenfluss</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3096"/>
        <source>Peak flow during a 2-minute interval</source>
        <translation>Spitzenfluss während eines 2-Minuten-Intervalls</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2139"/>
        <source>Recompressing Session Files</source>
        <translation>Sitzungsdateien neu komprimieren</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="343"/>
        <source>OSCAR crashed due to an incompatibility with your graphics hardware.</source>
        <translation>OSCAR stürzte aufgrund einer Inkompatibilität mit Ihrer Grafikhardware ab.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="344"/>
        <source>To resolve this, OSCAR has reverted to a slower but more compatible method of drawing.</source>
        <translation>Um dieses Problem zu lösen, ist OSCAR zu einer langsameren, aber kompatibleren Zeichenmethode zurückgekehrt.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="121"/>
        <source>Couldn&apos;t parse Channels.xml, OSCAR cannot continue and is exiting.</source>
        <translation>Konnte Channels.xml nicht parsen, OSCAR kann nicht weitermachen und wird beendet.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="362"/>
        <source>(1 day ago)</source>
        <translation>(vor 1 Tag)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="363"/>
        <source>(%2 days ago)</source>
        <translation>(%2 Tage zuvor)</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="152"/>
        <source>New versions file improperly formed</source>
        <translation>Neue Versionen Datei unsachgemäß gebildet</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="175"/>
        <source>A more recent version of OSCAR is available</source>
        <translation>Eine neuere Version von OSCAR ist verfügbar</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="170"/>
        <source>release</source>
        <translation>Veröffentlichung</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="170"/>
        <source>test version</source>
        <translation>Testversion</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="171"/>
        <source>You are running the latest %1 of OSCAR</source>
        <translation>Sie verwenden die neueste %1 von OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="172"/>
        <location filename="../oscar/checkupdates.cpp" line="176"/>
        <source>You are running OSCAR %1</source>
        <translation>Sie verwenden OSCAR %1</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="178"/>
        <source>OSCAR %1 is available &lt;a href=&apos;%2&apos;&gt;here&lt;/a&gt;.</source>
        <translation>OSCAR %1 ist verfügbar &lt;a href=&apos;%2&apos;&gt;hier&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="181"/>
        <source>Information about more recent test version %1 is available at &lt;a href=&apos;%2&apos;&gt;%2&lt;/a&gt;</source>
        <translation>Informationen über die neuere Testversion %1 sind unter &lt;a href=&apos;%2&apos;&gt;%2&lt;/a&gt; verfügbar</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="209"/>
        <source>Check for OSCAR Updates</source>
        <translation>Nach OSCAR-Updates suchen</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="577"/>
        <source>Unable to create the OSCAR data folder at</source>
        <translation>Der OSCAR-Datenordner konnte nicht erstellt werden unter</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="634"/>
        <source>The popout window is full. You should capture the existing
popout window, delete it, then pop out this graph again.</source>
        <translation>Das Popup-Fenster ist voll. Sie sollten die vorhandenen
Popup-Fenster, löschen Sie es, und öffnen Sie dann dieses Diagramm erneut.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="189"/>
        <source>Essentials</source>
        <translation>Grundlagen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="191"/>
        <source>Plus</source>
        <translation>Plus</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="587"/>
        <source>Unable to write to OSCAR data directory</source>
        <translation>OSCAR-Datenverzeichnis kann nicht beschrieben werden</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="588"/>
        <source>Error code</source>
        <translation>Fehlercode</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="589"/>
        <source>OSCAR cannot continue and is exiting.</source>
        <translation>OSCAR kann nicht weitergeführt werden und wird geschlossen.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="600"/>
        <source>Unable to write to debug log. You can still use the debug pane (Help/Troubleshooting/Show Debug Pane) but the debug log will not be written to disk.</source>
        <translation>Kann nicht in das Debug-Protokoll schreiben. Sie können immer noch das Debug-Fenster (Hilfe/Fehlerbehebung/Debug-Fenster anzeigen) verwenden, aber das Debug-Protokoll wird nicht auf die Festplatte geschrieben.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="354"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="355"/>
        <source>For internal use only</source>
        <translation>Nur für den internen Gebrauch</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="193"/>
        <source>Choose the SleepyHead or OSCAR data folder to migrate</source>
        <translation>Wählen Sie den zu migrierenden SleepyHead- oder OSCAR-Datenordner</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="208"/>
        <source>The folder you chose does not contain valid SleepyHead or OSCAR data.</source>
        <translation>Der von Ihnen gewählte Ordner enthält keine gültigen SleepyHead- oder OSCAR-Daten.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="512"/>
        <source>If you have been using SleepyHead or an older version of OSCAR,</source>
        <translation>Wenn Sie SleepyHead oder eine ältere Version von OSCAR verwendet haben,</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="513"/>
        <source>OSCAR can copy your old data to this folder later.</source>
        <translation>OSCAR kann Ihre alten Daten später in diesen Ordner kopieren.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="566"/>
        <source>Migrate SleepyHead or OSCAR Data?</source>
        <translation>SleepyHead- oder OSCAR-Daten migrieren?</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="567"/>
        <source>On the next screen OSCAR will ask you to select a folder with SleepyHead or OSCAR data</source>
        <translation>Auf dem nächsten Bildschirm werden Sie von OSCAR aufgefordert, einen Ordner mit SleepyHead- oder OSCAR-Daten auszuwählen</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="568"/>
        <source>Click [OK] to go to the next screen or [No] if you do not wish to use any SleepyHead or OSCAR data.</source>
        <translation>Klicken Sie auf [OK], um zum nächsten Bildschirm zu gelangen, oder auf [Nein], wenn Sie keine SleepyHead- oder OSCAR-Daten verwenden möchten.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="968"/>
        <source>Chromebook file system detected, but no removable device found
</source>
        <translation>Chromebook-Dateisystem erkannt, aber kein Wechseldatenträger gefunden
</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="969"/>
        <source>You must share your SD card with Linux using the ChromeOS Files program</source>
        <translation>Sie müssen Ihre SD-Karte unter Linux mit dem Programm ChromeOS Files freigeben</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2866"/>
        <source>Flex</source>
        <translation>Flex</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="276"/>
        <source>Unable to check for updates. Please try again later.</source>
        <translation>Suche nach Updates nicht möglich. Bitte versuchen Sie es später noch einmal.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1537"/>
        <source>varies</source>
        <translation>variiert</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1630"/>
        <source>EPAP %1-%2 IPAP %3-%4 (%5)</source>
        <translation>EPAP %1-%2 IPAP %3-%4 (%5)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2717"/>
        <source>Backing up files...</source>
        <translation>Sichern von Dateien...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2724"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="249"/>
        <source>Reading data files...</source>
        <translation>Lesen von Datendateien...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2805"/>
        <source>Snoring event.</source>
        <translation>Schnarch-Ereignis.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2806"/>
        <source>SN</source>
        <translation>SN</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="963"/>
        <source>model %1</source>
        <translation>Modell %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="966"/>
        <source>unknown model</source>
        <translation>unbekanntes Modell</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2940"/>
        <source>Target Time</source>
        <translation>Zielzeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2941"/>
        <source>PRS1 Humidifier Target Time</source>
        <translation>PRS1 Luftbefeuchter Zielzeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2942"/>
        <source>Hum. Tgt Time</source>
        <translation>Brummen. Tgt Zeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="126"/>
        <source>iVAPS</source>
        <translation>iVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="219"/>
        <source>Soft</source>
        <translation>Weich</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="794"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="218"/>
        <source>Standard</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="119"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="122"/>
        <source>BiPAP-T</source>
        <translation>BiPAP-T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="120"/>
        <source>BiPAP-S</source>
        <translation>BiPAP-S</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="121"/>
        <source>BiPAP-S/T</source>
        <translation>BiPAP-S/T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="127"/>
        <source>PAC</source>
        <translation>PAC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="147"/>
        <source>Device auto starts by breathing</source>
        <translation>Das Gerät startet automatisch durch Atmen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="222"/>
        <source>SmartStop</source>
        <translation>Intelligenter Stop</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="455"/>
        <source>Your ResMed CPAP device (Model %1) has not been tested yet.</source>
        <translation>Ihr ResMed CPAP-Gerät (Modell %1) wurde noch nicht getestet.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="456"/>
        <source>It seems similar enough to other devices that it might work, but the developers would like a .zip copy of this device&apos;s SD card to make sure it works with OSCAR.</source>
        <translation>Es scheint anderen Geräten ähnlich genug zu sein, dass es funktionieren könnte, aber die Entwickler möchten eine .zip-Kopie der SD-Karte dieses Geräts, um sicherzustellen, dass es mit OSCAR funktioniert.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="222"/>
        <source>Smart Stop</source>
        <translation>Intelligenter Stop</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="222"/>
        <source>Device auto stops by breathing</source>
        <translation>Das Gerät stoppt automatisch durch Atmen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="231"/>
        <source>Simple</source>
        <translation>Einfach</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="230"/>
        <source>Advanced</source>
        <translation>Fortgeschrittene</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1048"/>
        <source>Humidity</source>
        <translation>Luftfeuchtigkeit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.h" line="88"/>
        <source>SleepStyle</source>
        <translation>Schlafstil</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="298"/>
        <source>AI=%1 </source>
        <translation>AI=%1 </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="216"/>
        <source>Response</source>
        <translation>Antwort</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="228"/>
        <source>Patient View</source>
        <translation>Patient Ansicht</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1022"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1023"/>
        <source>SensAwake level</source>
        <translation>Sinneswahrnehmungslevel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1030"/>
        <source>Expiratory Relief</source>
        <translation>Druckentlastung beim Ausatmen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1036"/>
        <source>Expiratory Relief Level</source>
        <translation>Ausatemdruckentlastungs-Niveau</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="81"/>
        <source>This page in other languages:</source>
        <translation>Diese Seite in anderen Sprachen:</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2643"/>
        <location filename="../oscar/overview.cpp" line="471"/>
        <source>%1 Graphs</source>
        <translation>%1 Grafiken</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2647"/>
        <location filename="../oscar/overview.cpp" line="475"/>
        <source>%1 of %2 Graphs</source>
        <translation>%1 von %2 Grafiken</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2674"/>
        <source>%1 Event Types</source>
        <translation>%1 Ereignistypen</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2678"/>
        <source>%1 of %2 Event Types</source>
        <translation>%1 von %2 Ereignistypen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.h" line="223"/>
        <source>Löwenstein</source>
        <translation>Löwenstein</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.h" line="223"/>
        <source>Prisma Smart</source>
        <translation>Prisma Smart</translation>
    </message>
</context>
<context>
    <name>SaveGraphLayoutSettings</name>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="179"/>
        <source>Manage Save Layout Settings</source>
        <translation>Layouteinstellungen speichern verwalten</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="186"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="187"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="186"/>
        <source>Add Feature inhibited. The maximum number of Items has been exceeded.</source>
        <translation>Funktion hinzufügen gesperrt. Die maximale Anzahl von Artikeln wurde überschritten.</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="187"/>
        <source>creates new copy of current settings.</source>
        <translation>Erstellt eine neue Kopie der aktuellen Einstellungen.</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="188"/>
        <source>Restore</source>
        <translation>Wiederherstellen</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="188"/>
        <source>Restores saved settings from selection.</source>
        <translation>Stellt gespeicherte Einstellungen aus der Auswahl wieder her.</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="189"/>
        <source>Rename</source>
        <translation>Umbenennen</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="189"/>
        <source>Renames the selection. Must edit existing name then press enter.</source>
        <translation>Benennt die Auswahl um. Vorhandenen Namen bearbeiten und dann Enter drücken.</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="190"/>
        <source>Update</source>
        <translation>Aktualisieren</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="190"/>
        <source>Updates the selection with current settings.</source>
        <translation>Aktualisiert die Auswahl mit den aktuellen Einstellungen.</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="191"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="191"/>
        <source>Deletes the selection.</source>
        <translation>Löscht die Auswahl.</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="192"/>
        <source>Expanded Help menu.</source>
        <translation>Erweitertes Hilfemenü.</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="193"/>
        <source>Exits the Layout menu.</source>
        <translation>Beendet das Layout-Menü.</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="228"/>
        <source>&lt;h4&gt;Help Menu - Manage Layout Settings&lt;/h4&gt;</source>
        <translation>&lt;h4&gt;Hilfemenü – Layouteinstellungen verwalten&lt;/h4&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="237"/>
        <source>Exits the help menu.</source>
        <translation>Beendet das Hilfemenü.</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="238"/>
        <source>Exits the dialog menu.</source>
        <translation>Beendet das Dialogmenü.</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="256"/>
        <source>     &lt;p style=&quot;color:black;&quot;&gt;        This feature manages the saving and restoring of Layout Settings.      &lt;br&gt;      Layout Settings control the layout of a graph or chart.      &lt;br&gt;      Different Layouts Settings can be saved and later restored.      &lt;br&gt;     &lt;/p&gt;     &lt;table width=&quot;100%&quot;&gt;         &lt;tr&gt;&lt;td&gt;&lt;b&gt;Button&lt;/b&gt;&lt;/td&gt;             &lt;td&gt;&lt;b&gt;Description&lt;/b&gt;&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td valign=&quot;top&quot;&gt;Add&lt;/td&gt; 			&lt;td&gt;Creates a copy of the current Layout Settings. &lt;br&gt; 				The default description is the current date. &lt;br&gt; 				The description may be changed. &lt;br&gt; 				The Add button will be greyed out when maximum number is reached.&lt;/td&gt;&lt;/tr&gt;                 &lt;br&gt;         &lt;tr&gt;&lt;td&gt;&lt;i&gt;&lt;u&gt;Other Buttons&lt;/u&gt; &lt;/i&gt;&lt;/td&gt;              &lt;td&gt;Greyed out when there are no selections&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;Restore&lt;/td&gt; 			&lt;td&gt;Loads the Layout Settings from the selection. Automatically exits. &lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;Rename &lt;/td&gt;         			&lt;td&gt;Modify the description of the selection. Same as a double click.&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td valign=&quot;top&quot;&gt;Update&lt;/td&gt;&lt;td&gt; Saves the current Layout Settings to the selection.&lt;br&gt; 		        Prompts for confirmation.&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td valign=&quot;top&quot;&gt;Delete&lt;/td&gt; 			&lt;td&gt;Deletes the selecton. &lt;br&gt; 			    Prompts for confirmation.&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;&lt;i&gt;&lt;u&gt;Control&lt;/u&gt; &lt;/i&gt;&lt;/td&gt;              &lt;td&gt;&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;Exit &lt;/td&gt; 			&lt;td&gt;(Red circle with a white &quot;X&quot;.) Returns to OSCAR menu.&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;Return&lt;/td&gt; 			&lt;td&gt;Next to Exit icon. Only in Help Menu. Returns to Layout menu.&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;Escape Key&lt;/td&gt; 			&lt;td&gt;Exit the Help or Layout menu.&lt;/td&gt;&lt;/tr&gt;       &lt;/table&gt;        &lt;p&gt;&lt;b&gt;Layout Settings&lt;/b&gt;&lt;/p&gt;       &lt;table width=&quot;100%&quot;&gt;          &lt;tr&gt; 			&lt;td&gt;* Name&lt;/td&gt; 			&lt;td&gt;* Pinning&lt;/td&gt; 			&lt;td&gt;* Plots Enabled &lt;/td&gt; 			&lt;td&gt;* Height&lt;/td&gt; 		&lt;/tr&gt;         &lt;tr&gt; 			&lt;td&gt;* Order&lt;/td&gt; 			&lt;td&gt;* Event Flags&lt;/td&gt; 			&lt;td&gt;* Dotted Lines&lt;/td&gt; 			&lt;td&gt;* Height Options&lt;/td&gt; 		&lt;/tr&gt;       &lt;/table&gt;        &lt;p&gt;&lt;b&gt;General Information&lt;/b&gt;&lt;/p&gt; 	  &lt;ul style=margin-left=&quot;20&quot;; &gt;  		&lt;li&gt; Maximum description size = 80 characters.	&lt;/li&gt;  		&lt;li&gt; Maximum Saved Layout Settings = 30.	&lt;/li&gt;  		&lt;li&gt; Saved Layout Settings can be accessed by all profiles.  		&lt;li&gt; Layout Settings only control the layout of a graph or chart. &lt;br&gt;               They do not contain any other data. &lt;br&gt;              They do not control if a graph is displayed or not. &lt;/li&gt; 		&lt;li&gt; Layout Settings for daily and overview are managed independantly. &lt;/li&gt;	  &lt;/ul&gt;   </source>
        <translation>     &lt;p style=&quot;color:black;&quot;&gt;        Diese Funktion verwaltet das Speichern und Wiederherstellen von Layouteinstellungen.      &lt;br&gt;      Layouteinstellungen steuern das Layout einer Grafik oder eines Diagramms.      &lt;br&gt;      Verschiedene Layout-Einstellungen können gespeichert und später wiederhergestellt werden.      &lt;br&gt;     &lt;/p&gt;     &lt;table width=&quot;100%&quot;&gt;         &lt;tr&gt;&lt;td&gt;&lt;b&gt;Schaltfläche&lt;/b&gt;&lt;/td&gt;             &lt;td&gt;&lt;b&gt;Beschreibung&lt;/b&gt;&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td valign=&quot;top&quot;&gt;Hinzufügen&lt;/td&gt; 			&lt;td&gt;Erstellt eine Kopie der aktuellen Layout-Einstellungen. &lt;br&gt; 				Die Standardbeschreibung ist das aktuelle Datum. &lt;br&gt; 				Die Beschreibung kann geändert werden. &lt;br&gt; 				Die Schaltfläche „Hinzufügen“ wird ausgegraut, wenn die maximale Anzahl erreicht ist.&lt;/td&gt;&lt;/tr&gt;                 &lt;br&gt;         &lt;tr&gt;&lt;td&gt;&lt;i&gt;&lt;u&gt;Andere Schaltflächen&lt;/u&gt; &lt;/i&gt;&lt;/td&gt;              &lt;td&gt;Ausgegraut, wenn keine Auswahl vorhanden ist&lt;/td&gt;&lt;/tr&gt; &lt;tr&gt;&lt;td&gt;Wiederherstellen&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;Restore&lt;/td&gt; 			&lt;td&gt;Lädt die Layout-Einstellungen aus der Auswahl. Beendet automatisch. &lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;Umbenennen &lt;/td&gt;         			&lt;td&gt;Beschreibung der Auswahl ändern. Gleich wie ein Doppelklick.&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td valign=&quot;top&quot;&gt;Aktualisieren&lt;/td&gt;&lt;td&gt; Speichert die aktuellen Layout-Einstellungen in der Auswahl.&lt;br&gt; Fordert zur Bestätigung auf.&lt;br&gt; 		        Prompts for confirmation.&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td valign=&quot;top&quot;&gt;Löschen&lt;/td&gt; 			&lt;td&gt;Löscht die Auswahl. &lt;br&gt; 			    Aufforderung zur Bestätigung.&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;&lt;i&gt;&lt;u&gt;Steuerung&lt;/u&gt; &lt;/i&gt;&lt;/td&gt;              &lt;td&gt;&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;Beenden &lt;/td&gt; 			&lt;td&gt;(Roter Kreis mit weißem „X“.) Kehrt zum OSCAR-Menü zurück.&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;Zurück&lt;/td&gt; 			&lt;td&gt;Neben dem Beenden-Symbol. Nur im Hilfemenü. Kehrt zum Layout-Menü zurück.&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;Escape-Taste&lt;/td&gt; 			&lt;td&gt;Verlässt das Hilfe- oder Layout-Menü.&lt;/td&gt;&lt;/tr&gt;       &lt;/table&gt;        &lt;p&gt;&lt;b&gt;Layout-Einstellungen&lt;/b&gt;&lt;/p&gt;       &lt;table width=&quot;100%&quot;&gt;          &lt;tr&gt; 			&lt;td&gt;* Name&lt;/td&gt; 			&lt;td&gt;*Anheften&lt;/td&gt; 			&lt;td&gt;* Diagramme aktiviert &lt;/td&gt; 			&lt;td&gt;* Höhe&lt;/td&gt; 		&lt;/tr&gt;         &lt;tr&gt; 			&lt;td&gt;*Reihenfolger&lt;/td&gt; 			&lt;td&gt;*Ereignismarkierungen&lt;/td&gt; 			&lt;td&gt;* Gepunktete Linien&lt;/td&gt; 			&lt;td&gt;* Höhenoptionen&lt;/td&gt; 		&lt;/tr&gt;       &lt;/table&gt;        &lt;p&gt;&lt;b&gt;Allgemeine Informationen&lt;/b&gt;&lt;/p&gt; 	  &lt;ul style=margin-left=&quot;20&quot;; &gt;  		&lt;li&gt; Maximale Beschreibungsgröße = 80 Zeichen.	&lt;/li&gt;  		&lt;li&gt; Maximal gespeicherte Layouteinstellungen = 30.	&lt;/li&gt;  		&lt;li&gt;Alle Profile können auf gespeicherte Layouteinstellungen zugreifen.  		&lt;li&gt; Layouteinstellungen steuern nur das Layout einer Grafik oder eines Diagramms. &lt;br&gt;               Sie enthalten keine weiteren Daten. &lt;br&gt;              Sie kontrollieren nicht, ob ein Diagramm angezeigt wird oder nicht. &lt;/li&gt; 		&lt;li&gt; Layouteinstellungen für Tages- und Übersicht werden unabhängig voneinander verwaltet. &lt;/li&gt;	  &lt;/ul&gt;   </translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="458"/>
        <source>Maximum number of Items exceeded.</source>
        <translation>Maximale Anzahl von Artikeln überschritten.</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="464"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="473"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="482"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="509"/>
        <source>No Item Selected</source>
        <translation>Kein Element ausgewählt</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="465"/>
        <source>Ok to Update?</source>
        <translation>Fertig zum Aktualisieren?</translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="510"/>
        <source>Ok To Delete?</source>
        <translation>OK zum Löschen?</translation>
    </message>
</context>
<context>
    <name>SessionBar</name>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="247"/>
        <source>%1h %2m</source>
        <translation>%1h %2m</translation>
    </message>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="290"/>
        <source>No Sessions Present</source>
        <translation>Gegenwärtig keine Sitzung</translation>
    </message>
</context>
<context>
    <name>SleepStyleLoader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="213"/>
        <source>Import Error</source>
        <translation>Import Fehler</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="214"/>
        <source>This device Record cannot be imported in this profile.</source>
        <translation>Dieser Gerätedatensatz kann nicht in dieses Profil importiert werden.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="214"/>
        <source>The Day records overlap with already existing content.</source>
        <translation>Die Aufzeichnungen dieses Tages überschneiden sich mit bereits vorhandenen Inhalt.</translation>
    </message>
</context>
<context>
    <name>Statistics</name>
    <message>
        <location filename="../oscar/statistics.cpp" line="986"/>
        <source>Days</source>
        <translation>Tage</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1491"/>
        <source>Worst Flow Limtation</source>
        <translation>Schlechteste Flusslimitierung</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1524"/>
        <source>Worst Large Leaks</source>
        <translation>Die schlechtesten großen Lecks</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="586"/>
        <source>Oximeter Statistics</source>
        <translation>Oxymetrie-Statistik</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1532"/>
        <source>Date: %1 Leak: %2%</source>
        <translation>Datum: %1 Leck: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="539"/>
        <location filename="../oscar/statistics.cpp" line="1400"/>
        <source>CPAP Usage</source>
        <translation>CPAP-Nutzung</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="590"/>
        <source>Blood Oxygen Saturation</source>
        <translation>Blutsauerstoffsättigung</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1637"/>
        <location filename="../oscar/statistics.cpp" line="1649"/>
        <source>Date: %1 - %2</source>
        <translation>Datum: %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1604"/>
        <source>No PB on record</source>
        <translation>Kein PB aufgezeichnet</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="613"/>
        <source>% of time in %1</source>
        <translation>% der Zeit in %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1168"/>
        <source>Last 30 Days</source>
        <translation>Die letzten 30 Tage</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1612"/>
        <source>Want more information?</source>
        <translation>Möchten Sie weitere Informationen?</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1401"/>
        <source>Days Used: %1</source>
        <translation>Tage verwendet: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="612"/>
        <source>%1 Index</source>
        <translation>%1 Index</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1646"/>
        <source>Worst RX Setting</source>
        <translation>Schlechteste RX Einstellungen</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1634"/>
        <source>Best RX Setting</source>
        <translation>Beste RX Einstellungen</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1236"/>
        <source>%1 day of %2 Data on %3</source>
        <translation>%1 Tage %2 Daten über %3</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1569"/>
        <source>Date: %1 CSR: %2%</source>
        <translation>Datum: %1 CSR: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="614"/>
        <source>% of time above %1 threshold</source>
        <translation>% der Zeit über %1 Schwelle</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="543"/>
        <source>Therapy Efficacy</source>
        <translation>Therapie Effizienz</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="615"/>
        <source>% of time below %1 threshold</source>
        <translation>% der Zeit unter %1 Schwelle</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="611"/>
        <source>Max %1</source>
        <translation>Max %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="606"/>
        <source>%1 Median</source>
        <translation>%1 Medianwert</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="610"/>
        <source>Min %1</source>
        <translation>Min %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="919"/>
        <source>Device Information</source>
        <translation>Geräteinformation</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="978"/>
        <source>Changes to Device Settings</source>
        <translation>Änderungen an den Geräteeinstellungen</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1166"/>
        <source>Most Recent</source>
        <translation>Der letzte Tag</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1614"/>
        <source>Please enable Pre-Load Summaries checkbox in preferences to make sure this data is available.</source>
        <translation>Bitte aktivieren Sie das Vor-Laden der Zusammenfassungen Checkbox in den Einstellungen, um sicherzustellen, dass diese Daten verfügbar sind.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="992"/>
        <source>Pressure Settings</source>
        <translation>Druckeinstellungen</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="642"/>
        <source>Phone: %1</source>
        <translation>Telefon: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1591"/>
        <source>Worst PB</source>
        <translation>Schlechtesten PB</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="561"/>
        <source>Pressure Statistics</source>
        <translation>Druck-Statistik</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="637"/>
        <source>Name: %1, %2</source>
        <translation>Name: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1169"/>
        <source>Last 6 Months</source>
        <translation>Die letzten 6 Monate</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="645"/>
        <source>Email: %1</source>
        <translation>E-mail: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="607"/>
        <location filename="../oscar/statistics.cpp" line="608"/>
        <source>Average %1</source>
        <translation>Durchschnittliche(r) %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1233"/>
        <source>No %1 data available.</source>
        <translation>Keine %1 Daten verfügbar.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="926"/>
        <source>Last Use</source>
        <translation>Zuletzt verwendet</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="990"/>
        <source>Pressure Relief</source>
        <translation>Druckentlastung</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="639"/>
        <source>DOB: %1</source>
        <translation>Geb.-Datum: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="595"/>
        <source>Pulse Rate</source>
        <translation>Pulsrate</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="925"/>
        <source>First Use</source>
        <translation>Erste Verwendung</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1561"/>
        <source>Worst CSR</source>
        <translation>Schlechtester CSR</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1444"/>
        <source>Worst AHI</source>
        <translation>Schlechtester AHI</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1167"/>
        <source>Last Week</source>
        <translation>Letzte Woche</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1170"/>
        <source>Last Year</source>
        <translation>Letztes Jahr</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1481"/>
        <source>Best Flow Limitation</source>
        <translation>Beste Flusslimitierung</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="648"/>
        <source>Address:</source>
        <translation>Adresse:</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1219"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1503"/>
        <source>No Flow Limitation on record</source>
        <translation>Keine eingeschränkte Durchflussbegrenzung</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1242"/>
        <source>%1 days of %2 Data, between %3 and %4</source>
        <translation>%1 Tage %2 Daten, zwischen %3 und %4</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1538"/>
        <source>No Large Leaks on record</source>
        <translation>Keine großen Lecks aufgenommen</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1599"/>
        <source>Date: %1 PB: %2%</source>
        <translation>Daten: %1 PB: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1434"/>
        <source>Best AHI</source>
        <translation>Bester AHI</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1174"/>
        <source>Last Session</source>
        <translation>Die letzte Sitzung</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1438"/>
        <location filename="../oscar/statistics.cpp" line="1450"/>
        <source>Date: %1 AHI: %2</source>
        <translation>Datum: %1 AHI: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="536"/>
        <source>CPAP Statistics</source>
        <translation>CPAP-Statistik</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1403"/>
        <source>Compliance: %1%</source>
        <translation>Therapietreue: %1%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1613"/>
        <source>OSCAR needs all summary data loaded to calculate best/worst data for individual days.</source>
        <translation>OSCAR benötigt alle geladenen Übersichtsdaten, um die besten/schlechtesten Daten für einzelne Tage zu berechnen.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1485"/>
        <location filename="../oscar/statistics.cpp" line="1498"/>
        <source>Date: %1 FL: %2</source>
        <translation>Datum: %1 FL: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1427"/>
        <source>Days AHI of 5 or greater: %1</source>
        <translation>Tage mit einem AHI von 5 oder mehr als: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1402"/>
        <source>Low Use Days: %1</source>
        <translation>Tage mit geringer Nutzung: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="556"/>
        <source>Leak Statistics</source>
        <translation>Leck-Statistik</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1574"/>
        <source>No CSR on record</source>
        <translation>Kein CSR aufgenommen</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="540"/>
        <source>Average Hours per Night</source>
        <translation>Durchschnittliche Stunden pro Nacht</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="737"/>
        <source>OSCAR is free open-source CPAP report software</source>
        <translation>OSCAR ist eine kostenlose Open-Source CPAP-Berichtssoftware</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1066"/>
        <source>Oscar has no data to report :(</source>
        <translation>Oscar hat keine Daten zu melden :(</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="541"/>
        <source>Compliance (%1 hrs/day)</source>
        <translation>Einhaltung (%1 Std./Tag)</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1064"/>
        <source>No data found?!?</source>
        <translation>Keine Daten gefunden?!??</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1640"/>
        <location filename="../oscar/statistics.cpp" line="1652"/>
        <source>AHI: %1</source>
        <translation>AHI: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1641"/>
        <location filename="../oscar/statistics.cpp" line="1653"/>
        <source>Total Hours: %1</source>
        <translation>Total Stunden: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="734"/>
        <source>This report was prepared on %1 by OSCAR %2</source>
        <translation>Dieser Bericht wurde  am %1 von OSCAR %2 erstellt</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../oscar/welcome.cpp" line="211"/>
        <source>over</source>
        <translation>über</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="210"/>
        <source>under</source>
        <translation>unter</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="162"/>
        <source>Note that some preferences are forced when a ResMed device is detected</source>
        <translation>Beachten Sie, dass einige Einstellungen erzwungen werden, wenn ein ResMed-Gerät erkannt wird</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="182"/>
        <source>today</source>
        <translation>Heute</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="196"/>
        <source>Your device was on for %1.</source>
        <translation>Ihr Gerät war %1 lang eingeschaltet.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="258"/>
        <source>Your CPAP device used a constant %1 %2 of air</source>
        <translation>Ihr CPAP-Gerät hat konstant %1 %2 Luft verbraucht</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="271"/>
        <source>Your device used a constant %1-%2 %3 of air.</source>
        <translation>Ihr Gerät hat konstant %1-%2 %3 Luft verbraucht.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="278"/>
        <source>Your device was under %1-%2 %3 for %4% of the time.</source>
        <translation>Ihr Gerät war %4&#xa0;% der Zeit unter %1-%2 %3.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="318"/>
        <source>Your average leaks were %1 %2, which is %3 your %4 day average of %5.</source>
        <translation>Der durchschnittliche Leckwert war %1 %2, was  %3 Ihrem %4 Tagesdurchschnitt von %5 liegt.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="324"/>
        <source>No CPAP data has been imported yet.</source>
        <translation>Es wurden noch keine CPAP-Daten importiert.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="261"/>
        <source>Daily View</source>
        <translation>Tagesansicht</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="223"/>
        <source>Oximetry Wizard</source>
        <translation>Oxymetrie-Assistent</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="180"/>
        <source>last night</source>
        <translation>letzte Nacht</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="142"/>
        <source>What would you like to do?</source>
        <translation>Was möchten Sie tun?</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="185"/>
        <source>was %1 (on %2)</source>
        <translation>war %1 (am %2)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="161"/>
        <source>as there are some options that affect import.</source>
        <translation>da es einige Optionen gibt, die den Import beeinflussen.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="227"/>
        <source>You had an AHI of %1, which is %2 your %3 day average of %4.</source>
        <translation>Sie hatten einen AHI von %1. Das liegt %2 Ihrem %3 Tagesdurchschnitt von %4.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="197"/>
        <source>&lt;font color = red&gt;You only had the mask on for %1.&lt;/font&gt;</source>
        <translation>&lt;font color = red&gt;Sie benutzten die Maske nur %1.&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="163"/>
        <source>First import can take a few minutes.</source>
        <translation>Der erste Import kann ein paar Minuten dauern.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="213"/>
        <source>equal to</source>
        <translation>gleich</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="160"/>
        <source>It would be a good idea to check File-&gt;Preferences first,</source>
        <translation>Es empfiehlt sich, zuerst die Datei-&gt; Preferences zu überprüfen,</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="183"/>
        <source>%2 days ago</source>
        <translation>vor %2 Tagen</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="337"/>
        <source>Statistics</source>
        <translation>Statistiken</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="185"/>
        <source>CPAP Importer</source>
        <translation>CPAP-Importeur</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="299"/>
        <source>Overview</source>
        <translation>Überblick</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="580"/>
        <source>&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;ResMed S9 SDCards need to be locked &lt;/span&gt;&lt;span style=&quot; font-weight:600; color:#ff0000;&quot;&gt;before inserting into your computer.&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br&gt;Some operating systems write index files to the card without asking, which can render your card unreadable by your cpap device.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;span style=&quot; font-weight:600;&quot;&gt;Warnung: &lt;/span&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;ResMed S9 SDCards müssen gesperrt werden &lt;/span&gt;&lt;span style=&quot; font-weight :600; color:#ff0000;&quot;&gt;bevor Sie sie in Ihren Computer einsetzen.&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br&gt;Einige Betriebssysteme schreiben Indexdateien auf die Karte ohne zu fragen, wodurch Ihre Karte für Ihr cpap-Gerät unlesbar werden kann.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="212"/>
        <source>reasonably close to</source>
        <translation>ziemlich nahe an</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="193"/>
        <source>%1 hours, %2 minutes and %3 seconds</source>
        <translation>%1 Stunden, %2 Minuten und %3 Sekunden</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="176"/>
        <source>The last time you used your %1...</source>
        <translation>Die letzte Verwendung von %1...</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="127"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>Willkommen beim Open Source CPAP Analysis Reporter</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="263"/>
        <source>Your pressure was under %1 %2 for %3% of the time.</source>
        <translation>Ihr Druck lag unter %1 %2 für %3% dieser Zeit.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="286"/>
        <source>Your EPAP pressure fixed at %1 %2.</source>
        <translation>Ihr EPAP Druck fixiert auf %1 %2.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="289"/>
        <location filename="../oscar/welcome.cpp" line="298"/>
        <source>Your IPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>Ihr IPAP- Druck war unter %1 %2 für %3% dieser Zeit.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="297"/>
        <source>Your EPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>Ihre EPAP- Druck war unter %1 %2 für %3% dieser Zeit.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="181"/>
        <source>1 day ago</source>
        <translation>vor 1 Tag</translation>
    </message>
</context>
<context>
    <name>gGraph</name>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="652"/>
        <source>Double click Y-axis: Return to AUTO-FIT Scaling</source>
        <translation>Doppelklicken Sie auf die Y-Achse: Zurück zur AUTO-FIT-Skalierung</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="654"/>
        <source>Double click Y-axis: Return to DEFAULT Scaling</source>
        <translation>Doppelklick auf Y-Achse: Rückkehr zur STANDARD-Skalierung</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="656"/>
        <source>Double click Y-axis: Return to OVERRIDE Scaling</source>
        <translation>Doppelklick Y-Achse: Zurück zu Skalierung übersteuern</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="659"/>
        <source>Double click Y-axis: For Dynamic Scaling</source>
        <translation>Doppelklicken Sie auf die Y-Achse: Für dynamische Skalierung</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="663"/>
        <source>Double click Y-axis: Select DEFAULT Scaling</source>
        <translation>Doppelklicken Sie auf die Y-Achse: Wählen Sie DEFAULT-Skalierung</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="665"/>
        <source>Double click Y-axis: Select AUTO-FIT Scaling</source>
        <translation>Doppelklicken Sie auf die Y-Achse: Wählen Sie AUTO-FIT-Skalierung</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="924"/>
        <source>%1 days</source>
        <translation>%1 Tage</translation>
    </message>
</context>
<context>
    <name>gGraphView</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2270"/>
        <source>Clone %1 Graph</source>
        <translation>Klone Grafik %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="576"/>
        <source>Oximeter Overlays</source>
        <translation>Oxymeter-Überlagerung</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="568"/>
        <source>Plots</source>
        <translation>Plots</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="564"/>
        <source>Resets all graphs to a uniform height and default order.</source>
        <translation>Setzen Sie alle auf einheitliche Höhe und Standardreihenfolge.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2266"/>
        <source>Remove Clone</source>
        <translation>Klon entfernen</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="579"/>
        <source>Dotted Lines</source>
        <translation>gestrichelte Linien</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="573"/>
        <source>CPAP Overlays</source>
        <translation>CPAP-Überlagerung</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="567"/>
        <source>Y-Axis</source>
        <translation>Y-Achse</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="563"/>
        <source>Reset Graph Layout</source>
        <translation>Zurücksetzen von Grafiklayout</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="557"/>
        <source>100% zoom level</source>
        <translation>100% Zoom-Stufe</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1967"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2020"/>
        <source>Double click title to pin / unpin
Click and drag to reorder graphs</source>
        <translation>Doppelklicken Sie auf den Titel, umm anheften/entfernen
Klicken und ziehen Sie, um Diagramme neu zu ordnen</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="559"/>
        <source>Restore X-axis zoom to 100% to view entire selected period.</source>
        <translation>Stellen Sie den X-Achsen-Zoom auf 100% zurück, um den gesamten ausgewählten Zeitraum zu betrachten.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="561"/>
        <source>Restore X-axis zoom to 100% to view entire day&apos;s data.</source>
        <translation>Stellen Sie den X-Achsen-Zoom auf 100% wieder her, um die Daten des gesamten Tages anzuzeigen.</translation>
    </message>
</context>
</TS>
